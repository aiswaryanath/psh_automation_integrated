update business_date_master set eod_sod_flg=0;
commit;

Delete from GTB_CUSTOMER WHERE customer_id IN ('98789', '12345');
Delete from GTB_CUSTOMER_PREF where company_id IN ('98789', '12345');
Delete from GTB_CUST_PROD_PREF where company_id IN ('98789', '12345');
Delete from GTB_CUST_FILE_PREF where company_id IN ('98789', '12345');
Delete from GTB_CUST_ACCOUNT where company_id IN ('98789', '12345');
Delete from GTB_CUST_ACCOUNT_PREF Where company_id IN ('98789', '12345');
Delete from GTB_ACC_OWNERSHIP where company_id IN ('98789', '12345');
Delete from GTB_ORIGINATOR Where company_id IN ('98789', '12345');
Delete from GTB_CLIENT_MIGRATION Where company_id IN ('98789', '12345');
commit;
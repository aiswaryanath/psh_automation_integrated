update business_date_master set eod_sod_flg=0;  
Delete from stub_wiredebit where debit_acc in ('99000021','2000029');
commit; 
Insert into stub_wiredebit (PAN_NO,DEBIT_ACC,TRANSIT_NO,APPROVAL_CODE,RESPONSE_CODE,RESPONSE_TYPE,TXN_CLGSYSREF) values (2000000000000004,'2000029','00021','1234','000','S',null);
Insert into stub_wiredebit (PAN_NO,DEBIT_ACC,TRANSIT_NO,APPROVAL_CODE,RESPONSE_CODE,RESPONSE_TYPE,TXN_CLGSYSREF) values (9900008099000001,'99000021','60011','1234','000','S',null);
Update GTB_ORIGINATOR set EXPIRY_DATE = (SELECT add_months(to_date(To_Char(sysdate,'dd-mm-yy')), 24 ) FROM DUAL);
Update stub_fxrd set valdate = (SELECT add_months(to_date(To_Char(sysdate,'dd-mm-yy')), 12 ) FROM DUAL);
commit; 
update gtb_system_param set system_param_value = 'Yes' where system_param_name = 'IWS_SYSTEM_AVAILABILITY_FLAG';
update gtb_system_param set system_param_value = '999999999999999' where system_param_name = 'TXN_QUEUE_AGE_SECS';
update gtb_system_param set system_param_value = '5000' where system_param_name = 'QUEUE_DEPTH';
commit;
update stub_elcm set code='LM-00113',msgstat='WARNING',descp='Line 1 - Amount exceeds line limit. Limit = 1000.  Utilization = 2000.  Overdraft = 3000',respone_type='W' where cust_id IN ('12000048');
update stub_elcm set code='LM-00113',msgstat='WARNING',descp='Line 1 - Amount exceeds line limit. Limit = 1000.  Utilization = 2000.  Overdraft = 3000',respone_type='W' where cust_id IN ('10006722');
update stub_elcm set code='LM-00113',msgstat='WARNING',descp='Line 1 - Amount exceeds line limit. Limit = 1000.  Utilization = 2000.  Overdraft = 3000',respone_type='W' where cust_id IN ('10000056');
update stub_elcm set msgstat='SUCCESS',code='0001',descp='SUCCESSFUL',respone_type='S' where cust_id='12000056'; 
update stub_elcm set msgstat='SUCCESS',code='0001',descp='SUCCESSFUL',respone_type='S' where cust_id='20200821';
update stub_elcm set msgstat='SUCCESS',code='0001',descp='SUCCESSFUL',respone_type='S' where cust_id='10006687'; 
update stub_psh_envoy set balancesavailableamt=1000000000 where accounttransitnumber='2012008'; 
update stub_psh_envoy set balancesavailableamt=1000000000 where accounttransitnumber='1700010';
update stub_psh_envoy set balancesavailableamt=1000000000 where accounttransitnumber='1700011';
--SANITY INTERIM
update stub_psh_envoy set balancesavailableamt=0,RESP_TYPE='S',statuscode='DI00'  where  accounttransitnumber in('0364118') AND accounttransitid='00002';

update stub_wire_pmt  set statuscode='000', resp_type='S',APPROVALCODE='APPROV' where pan='4591237894251369';
update stub_wiredebit set response_code='911',response_type='T' where debit_acc='0310611';
commit;
delete from STUB_CONFIG_RESPNSE_FILES where ORIGINATOR_ID IN ('9708234614','0100777006');
COMMIT;
UPDATE GTB_SYSTEM_PARAM SET SYSTEM_PARAM_VALUE ='Yes' WHERE SYSTEM_PARAM_NAME='IWS_SYSTEM_AVAILABILITY_FLAG';
update GTB_SYSTEM_PARAM set SYSTEM_PARAM_VALUE = '99900000' WHERE SYSTEM_PARAM_NAME LIKE '%IWS_SLA_BREACH%';
update GTB_SYSTEM_PARAM set SYSTEM_PARAM_VALUE = '100' WHERE SYSTEM_PARAM_NAME LIKE '%IWS_FEED_CHECK%';
update STUB_FXRD set VALDATE = '31-DEC-25';
update stub_fx_bookrate set code='0',description='Transaction Successful',respone_type='S';
update stub_fxgetrate set code='0',description='Transaction Successful',response_type='S';
UPDATE GTB_CCY_PAIR SET EXPIRY_DATE='31-12-25';
UPDATE GTB_ORIGINATOR SET IS_ACTIVE=0;
update BENE_ACC_BANK_DTLS set mod_chk='N';
commit;



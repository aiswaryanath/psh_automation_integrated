----------------------Stub Set-up before execution------------------
-----------------------------------------------UMM Stub Queries----------------------------------------------------------------------------
delete from STUB_WIREDEBIT where DEBIT_ACC in ('1208001');
Commit;
Insert into STUB_WIREDEBIT(PAN_NO,DEBIT_ACC,TRANSIT_NO,APPROVAL_CODE,RESPONSE_CODE,RESPONSE_TYPE) values ('1208000000000001','1208001','12081','1234','000','S');
Commit;
-----------------------------------------------UMM FEE Stub Queries----------------------------------------------------------------------------
delete from STUB_WIRE_FEE_DEBIT where CORR_FEE_ACC in ('1208001');
Commit;
Insert into STUB_WIRE_FEE_DEBIT(PAN_NO,CORR_FEE_ACC,CORR_TRANSIT_NO,APPROVAL_CODE,RESPONSE_CODE,RESPONSE_TYPE) values ('1208000000000001','1208001','12081','1234','000','S');

Commit;
-----------------*------------------------*----------------------------*------------------------*-----------------------------
-------------------------------Query run before CC2xBVxTC10xF1xTXN1 case exicution---------------------------------

update CHANNEL_PREFERENCE SET ALLOWED_PAYMENT_TYPES = 'Wires' WHERE CHANNEL_SOURCE = 'CMO';
Commit;

update business_date_master set eod_sod_flg=0;
UPDATE STUB_PSH_ENVOY SET STATUSCODE='DI00',CDIAGNOSTICTEXT='TRANSACTION SUCCESSFUL',RESP_TYPE='S',BALANCESAVAILABLEAMT='0' WHERE ACCOUNTTRANSITNUMBER IN ('1700011','');
update stub_psh_envoy set statuscode='DI00',cdiagnostictext='Transaction successful',resp_type='S' where accounttransitnumber='1700015' and accounttransitid='00017';
UPDATE STUB_ELCM SET MSGSTAT='WARNING',CODE='LM-00113',DESCP='AMOUNT EXCEED LINE LIMIT. LIMIT = 1000.  UTILIZATION = 2000.  OVERDRAFT = 3000',RESPONE_TYPE='W' WHERE CUST_ID IN ('17000003','');
UPDATE STUB_EMTDEBIT SET RESPONSE_CODE = '000',RESPONSE_TYPE = 'S' WHERE DEBIT_ACC = '1700015';
UPDATE STUB_EMTDEBIT SET RESPONSE_CODE = '000',RESPONSE_TYPE = 'S' WHERE DEBIT_ACC = '1700014';
UPDATE STUB_EMTDEBIT SET RESPONSE_CODE = '000',RESPONSE_TYPE = 'S' WHERE DEBIT_ACC = '1700013';
UPDATE STUB_EMTDEBIT SET RESPONSE_CODE = '000',RESPONSE_TYPE = 'S' WHERE DEBIT_ACC = '1700011';
UPDATE STUB_EMTDEBIT SET RESPONSE_CODE = '000',RESPONSE_TYPE = 'S' WHERE DEBIT_ACC = '1700012';


delete from gtb_originator where company_id in (17000003);
delete from gtb_customer where company_id in (17000003);
delete from gtb_cust_account where company_id in (17000003);
delete from gtb_cust_prod_pref where company_id in (17000003);
delete from gtb_cust_file_pref where company_id in (17000003);
delete from gtb_customer_pref where company_id in (17000003);
delete from gtb_acc_ownership where company_id in (17000003);
delete from gtb_cust_account_pref where company_id in (17000003);
commit;

Insert into GTB_CUSTOMER (APPLICATION_ID,ENTITY_ID,CUSTOMER_ID,CUSTOMER_LEGAL_NAME,CUSTOMER_SHORT_NAME,DOB_DOI,CUSTOMER_TYPE,GENDER,LEGAL_STATUS,KYC_STATUS,ACTIVATION_DATE,COUNTRY_CODE,IS_ACTIVE,CUSTOMER_CATEGORY_CODE,SALUTATION,FIRST_NAME,MIDDLE_NAME,LAST_NAME,GRP_CODE,PRIMARY_ENTITY,INDUSTRY_TYPE,CREDIT_RATING,BANK_CODE,BEI,EXPECTED_TURNOVER,KYC_DATE,KYC_REVIEW_DATE,KYC_OFFICER,DEACTIVATION_DATE,REASON,BANK_IBAN,BANK_BIC,BANK_TELEX,BANK_ABA,MAKER_ID,MAKER_DATE,AUTHORISER_ID,AUTHORISED_DATE,COMPANY_ID,PAY_AUTH_ID,FX_ID,CLIENT_TYPE,LANGUAGE,ENTITY_LEVEL,INS_PARTY_LINK,LEGAL_NAME,LEGACY_CLIENT_IND) values (1,3,'17000003','Bulk eTransfer Client03',null,null,'C',null,null,null,null,null,'0',null,null,null,null,null,null,null,null,null,'CIBCCATT',null,null,null,null,null,null,null,null,'BETZCA13',null,null,'BCCMKR5',to_date('26-06-19','DD-MM-RR'),'BCCCKR1 ',to_date('26-06-19','DD-MM-RR'),17000003,1700000000000003,'FXID1703','E','en_US','Primary','17000006',null,'N');



Insert into GTB_CUST_ACCOUNT (APPLICATION_ID,ENTITY_ID,ACCOUNT_NO,CURRENCY_CODE,ACCOUNT_TYPE,PRODUCT,OPEN_DATE,COUNTRY_CODE,IS_ACTIVE,ACTIVATION_DATE,BRANCH_CODE,ACCOUNT_DESC,CLOSE_DATE,LANGUAGE,IBAN,RM,ACCOUNT_GROUP_ID,IS_INTERNAL_ACC,IS_STAFF_ACCOUNT,IS_DORMANT,IS_CREDIT_BLOCK,IS_DEBIT_BLOCK,IS_FROZEN,DEACTIVATION_DATE,REASON,MAKER_ID,MAKER_DATE,AUTHORISER_ID,AUTHORISED_DATE,ACCOUNT_STATUS,COMPANY_NAME,COMPANY_ID,ENTITY_CODE,TRANSIT_NO,ECIF_ID) values (1,3,'1700011','CAD','CFM','WIRES',null,'CA','0',null,'00017','Bulk eTransfer Client03',null,null,null,null,null,null,null,null,null,null,null,null,null,'BBDUSER01',to_date('21-06-19','DD-MM-RR'),'BBDUSER01',to_date('21-06-19','DD-MM-RR'),null,null,'17000003',null,'00017',null);
Insert into GTB_CUST_ACCOUNT (APPLICATION_ID,ENTITY_ID,ACCOUNT_NO,CURRENCY_CODE,ACCOUNT_TYPE,PRODUCT,OPEN_DATE,COUNTRY_CODE,IS_ACTIVE,ACTIVATION_DATE,BRANCH_CODE,ACCOUNT_DESC,CLOSE_DATE,LANGUAGE,IBAN,RM,ACCOUNT_GROUP_ID,IS_INTERNAL_ACC,IS_STAFF_ACCOUNT,IS_DORMANT,IS_CREDIT_BLOCK,IS_DEBIT_BLOCK,IS_FROZEN,DEACTIVATION_DATE,REASON,MAKER_ID,MAKER_DATE,AUTHORISER_ID,AUTHORISED_DATE,ACCOUNT_STATUS,COMPANY_NAME,COMPANY_ID,ENTITY_CODE,TRANSIT_NO,ECIF_ID) values (1,3,'1700012','CAD','CFM','WIRES',null,'CA','0',null,'00017','Bulk eTransfer Client03',null,null,null,null,null,null,null,null,null,null,null,null,null,'BBDUSER01',to_date('21-06-19','DD-MM-RR'),'BBDUSER01',to_date('21-06-19','DD-MM-RR'),null,null,'17000003',null,'00017',null);
Insert into GTB_CUST_ACCOUNT (APPLICATION_ID,ENTITY_ID,ACCOUNT_NO,CURRENCY_CODE,ACCOUNT_TYPE,PRODUCT,OPEN_DATE,COUNTRY_CODE,IS_ACTIVE,ACTIVATION_DATE,BRANCH_CODE,ACCOUNT_DESC,CLOSE_DATE,LANGUAGE,IBAN,RM,ACCOUNT_GROUP_ID,IS_INTERNAL_ACC,IS_STAFF_ACCOUNT,IS_DORMANT,IS_CREDIT_BLOCK,IS_DEBIT_BLOCK,IS_FROZEN,DEACTIVATION_DATE,REASON,MAKER_ID,MAKER_DATE,AUTHORISER_ID,AUTHORISED_DATE,ACCOUNT_STATUS,COMPANY_NAME,COMPANY_ID,ENTITY_CODE,TRANSIT_NO,ECIF_ID) values (1,3,'1700013','CAD','CFM','WIRES',null,'CA','0',null,'00017','Bulk eTransfer Client03',null,null,null,null,null,null,null,null,null,null,null,null,null,'BBDUSER01',to_date('21-06-19','DD-MM-RR'),'BBDUSER01',to_date('21-06-19','DD-MM-RR'),null,null,'17000003',null,'00017',null);
Insert into GTB_CUST_ACCOUNT (APPLICATION_ID,ENTITY_ID,ACCOUNT_NO,CURRENCY_CODE,ACCOUNT_TYPE,PRODUCT,OPEN_DATE,COUNTRY_CODE,IS_ACTIVE,ACTIVATION_DATE,BRANCH_CODE,ACCOUNT_DESC,CLOSE_DATE,LANGUAGE,IBAN,RM,ACCOUNT_GROUP_ID,IS_INTERNAL_ACC,IS_STAFF_ACCOUNT,IS_DORMANT,IS_CREDIT_BLOCK,IS_DEBIT_BLOCK,IS_FROZEN,DEACTIVATION_DATE,REASON,MAKER_ID,MAKER_DATE,AUTHORISER_ID,AUTHORISED_DATE,ACCOUNT_STATUS,COMPANY_NAME,COMPANY_ID,ENTITY_CODE,TRANSIT_NO,ECIF_ID) values (1,3,'1700014','CAD','CFM','WIRES',null,'CA','0',null,'00017','Bulk eTransfer Client03',null,null,null,null,null,null,null,null,null,null,null,null,null,'BBDUSER01',to_date('21-06-19','DD-MM-RR'),'BBDUSER01',to_date('21-06-19','DD-MM-RR'),null,null,'17000003',null,'00017',null);
Insert into GTB_CUST_ACCOUNT (APPLICATION_ID,ENTITY_ID,ACCOUNT_NO,CURRENCY_CODE,ACCOUNT_TYPE,PRODUCT,OPEN_DATE,COUNTRY_CODE,IS_ACTIVE,ACTIVATION_DATE,BRANCH_CODE,ACCOUNT_DESC,CLOSE_DATE,LANGUAGE,IBAN,RM,ACCOUNT_GROUP_ID,IS_INTERNAL_ACC,IS_STAFF_ACCOUNT,IS_DORMANT,IS_CREDIT_BLOCK,IS_DEBIT_BLOCK,IS_FROZEN,DEACTIVATION_DATE,REASON,MAKER_ID,MAKER_DATE,AUTHORISER_ID,AUTHORISED_DATE,ACCOUNT_STATUS,COMPANY_NAME,COMPANY_ID,ENTITY_CODE,TRANSIT_NO,ECIF_ID) values (1,3,'1700015','CAD','CFM','WIRES',null,'CA','0',null,'00017','Bulk eTransfer Client03',null,null,null,null,null,null,null,null,null,null,null,null,null,'BBDUSER01',to_date('21-06-19','DD-MM-RR'),'BBDUSER01',to_date('21-06-19','DD-MM-RR'),null,null,'17000003',null,'00017',null);


Insert into GTB_CUST_PROD_PREF (COMPANY_ID,FUTURE_DATE_ALLO,NO_FUTURE_DAYS,TXN_LIMIT_REQD,TXN_LIMIT_CURRENCY,TXN_LIMIT_FROM,TXN_LIMIT_TO,EXT_CUTOFF_REQD,EXT_CUTOFF_TIME,PREF_CURRENCY,PREF_COUNTRY,MAKER_ID,MAKER_DATE,AUTHORISER_ID,AUTHORISED_DATE,SEQNO,PROD_ID,PROD_DESC,REMIT_ALLOWED,IS_PREF_SERVICE,IS_BATCH_DEBIT) values (17000003,null,null,null,null,null,null,null,null,null,null,'BCCMKR5',to_date('25-11-19','DD-MM-RR'),'BCCCKR1',to_date('25-11-19','DD-MM-RR'),1,'EMT','Bulk eTransfer',null,null,'Single');

commit;



Insert into GTB_CUST_FILE_PREF (COMPANY_ID,FORMAT_ID,OD_REQUIRED,LIMIT_CHECK,RECORD_COUNT,FILE_SIZE,RECIPTION_ACK_REQD,EOP_ACK_REQD,EOD_ACK_REQD,EOW_ACK_REQD,EOM_ACK_REQD,INCULDE_FILE_PSR,ACK_LANGUAGE,CHANNEL_ID,MAKER_ID,MAKER_DATE,AUTHORISER_ID,AUTHORISED_DATE,SEQNO,FORMAT_DESC,MACHINE_RESP_TYPE,RESP_FILE_FORMAT,MACHINE_RESP_CHANNEL) values (17000003,'2000015',null,null,null,null,null,null,null,null,null,null,null,'FTS','BCCMKR5',to_date('25-11-19','DD-MM-RR'),'BCCCKR1',to_date('25-11-19','DD-MM-RR'),1,'ISO Pain 001.001.03 (Credits Only)','File Ack/Nack
Payments Status Report
Daily Outstanding Transfers Report
Daily Finalized Transfers Report
Intraday Outstanding Transfers Report
Intraday Finalized Transfers Report','2000024
1050124791','FTS');
Insert into GTB_CUST_FILE_PREF (COMPANY_ID,FORMAT_ID,OD_REQUIRED,LIMIT_CHECK,RECORD_COUNT,FILE_SIZE,RECIPTION_ACK_REQD,EOP_ACK_REQD,EOD_ACK_REQD,EOW_ACK_REQD,EOM_ACK_REQD,INCULDE_FILE_PSR,ACK_LANGUAGE,CHANNEL_ID,MAKER_ID,MAKER_DATE,AUTHORISER_ID,AUTHORISED_DATE,SEQNO,FORMAT_DESC,MACHINE_RESP_TYPE,RESP_FILE_FORMAT,MACHINE_RESP_CHANNEL) values (17000003,'1050124787',null,null,null,null,null,null,null,null,null,null,null,'FTS','BCCMKR1',to_date('25-11-19','DD-MM-RR'),'BCCCKR1',to_date('25-11-19','DD-MM-RR'),3,'ISO CAMT055.001.07','Cancellation ACK/NACK','40902255','FTS');

Insert into GTB_CUSTOMER_PREF (IS_ACTIVE,ACTIVATION_DATE,DEACTIVATION_DATE,MAKER_ID,MAKER_DATE,AUTHORISER_ID,AUTHORISED_DATE,CHANNEL,ALERT_LANGUAGE,CUSTOMER_LEGAL_NAME,COMPANY_ID,PROD_ID,BRANCH_CODE,CREATE_DT,HUMAN_RESP_TYPE,HR_RESP_CHNL,HR_RESP_FMT,IS_INITIATE_PRTY) values ('0',to_date('21-06-19','DD-MM-RR'),to_date('31-12-99','DD-MM-RR'),'BCCMKR5',to_date('25-11-19','DD-MM-RR'),'BCCCKR1 ',to_date('25-11-19','DD-MM-RR'),null,null,'Bulk eTransfer Client03',17000003,null,null,null,'File Ack/Nack
Payments Status Report
EOD Status Report
Return Item Report','FTS','CSV','No');




Insert into GTB_ACC_OWNERSHIP (ACCOUNT_NO,SEQNO,BRANCH_CODE,HOLDER_NO,IS_PREF,MAKER_ID,MAKER_DATE,AUTHORISER_ID,AUTHORISED_DATE,COMPANY_ID,COMPANY_NAME,IS_ACTIVE,ECIF_REFNO) values ('1700011',0,'00017',null,null,'BBDUSER01',to_date('21-06-19','DD-MM-RR'),'BBDUSER01',to_date('21-06-19','DD-MM-RR'),17000003,'Bulk eTransfer Client03','0','BETZIF111');
Insert into GTB_ACC_OWNERSHIP (ACCOUNT_NO,SEQNO,BRANCH_CODE,HOLDER_NO,IS_PREF,MAKER_ID,MAKER_DATE,AUTHORISER_ID,AUTHORISED_DATE,COMPANY_ID,COMPANY_NAME,IS_ACTIVE,ECIF_REFNO) values ('1700012',1,'00017',null,null,'BBDUSER01',to_date('21-06-19','DD-MM-RR'),'BBDUSER01',to_date('21-06-19','DD-MM-RR'),17000003,'Bulk eTransfer Client03','0','BETZIF112');
Insert into GTB_ACC_OWNERSHIP (ACCOUNT_NO,SEQNO,BRANCH_CODE,HOLDER_NO,IS_PREF,MAKER_ID,MAKER_DATE,AUTHORISER_ID,AUTHORISED_DATE,COMPANY_ID,COMPANY_NAME,IS_ACTIVE,ECIF_REFNO) values ('1700013',2,'00017',null,null,'BBDUSER01',to_date('21-06-19','DD-MM-RR'),'BBDUSER01',to_date('21-06-19','DD-MM-RR'),17000003,'Bulk eTransfer Client03','0','BETZIF113');
Insert into GTB_ACC_OWNERSHIP (ACCOUNT_NO,SEQNO,BRANCH_CODE,HOLDER_NO,IS_PREF,MAKER_ID,MAKER_DATE,AUTHORISER_ID,AUTHORISED_DATE,COMPANY_ID,COMPANY_NAME,IS_ACTIVE,ECIF_REFNO) values ('1700014',3,'00017',null,null,'BBDUSER01',to_date('21-06-19','DD-MM-RR'),'BBDUSER01',to_date('21-06-19','DD-MM-RR'),17000003,'Bulk eTransfer Client03','0','BETZIF114');
Insert into GTB_ACC_OWNERSHIP (ACCOUNT_NO,SEQNO,BRANCH_CODE,HOLDER_NO,IS_PREF,MAKER_ID,MAKER_DATE,AUTHORISER_ID,AUTHORISED_DATE,COMPANY_ID,COMPANY_NAME,IS_ACTIVE,ECIF_REFNO) values ('1700015',4,'00017',null,null,'BBDUSER01',to_date('21-06-19','DD-MM-RR'),'BBDUSER01',to_date('21-06-19','DD-MM-RR'),17000003,'Bulk eTransfer Client03','0','BETZIF115');



Insert into GTB_CUST_ACCOUNT_PREF (COMPANY_ID,ACCOUNT_NO,PAYMENT_PROD_TYPE,FUND_UTIL_TYPE,MAKER_ID,MAKER_DATE,AUTHORISER_ID,AUTHORISED_DATE,SEQNO,CORR_FEE_ACC_NO,BRANCH_NO,COMPANY_NAME,CORP_FEE_BRANCH_NO,SETTL_ACC_CURR,CORR_ACC_CURR,ORIGINATOR_ID,PRODUCT_CLASSIFICATION,ORIGINATOR_CURR,ACCOUNT_USAGE,DEFAULT_ACCOUNT,CHQ_STOCK_TYPE,ABA_ROUTING_NO,EXTERNAL_ACC_TYPE,INSTITUTION_NUMBER,EXTERNAL_ACCOUNT) values (17000003,'1700014','EMT','L','BCCMKR5',to_date('25-11-19','DD-MM-RR'),'BCCCKR1',to_date('25-11-19','DD-MM-RR'),5,null,'00017','Bulk eTransfer Client03',null,'CAD',null,null,null,null,null,null,null,null,null,null,'No');
Insert into GTB_CUST_ACCOUNT_PREF (COMPANY_ID,ACCOUNT_NO,PAYMENT_PROD_TYPE,FUND_UTIL_TYPE,MAKER_ID,MAKER_DATE,AUTHORISER_ID,AUTHORISED_DATE,SEQNO,CORR_FEE_ACC_NO,BRANCH_NO,COMPANY_NAME,CORP_FEE_BRANCH_NO,SETTL_ACC_CURR,CORR_ACC_CURR,ORIGINATOR_ID,PRODUCT_CLASSIFICATION,ORIGINATOR_CURR,ACCOUNT_USAGE,DEFAULT_ACCOUNT,CHQ_STOCK_TYPE,ABA_ROUTING_NO,EXTERNAL_ACC_TYPE,INSTITUTION_NUMBER,EXTERNAL_ACCOUNT) values (17000003,'1700013','EMT','L','BCCMKR5',to_date('25-11-19','DD-MM-RR'),'BCCCKR1',to_date('25-11-19','DD-MM-RR'),4,null,'00017','Bulk eTransfer Client03',null,'CAD',null,null,null,null,null,null,null,null,null,null,'No');
Insert into GTB_CUST_ACCOUNT_PREF (COMPANY_ID,ACCOUNT_NO,PAYMENT_PROD_TYPE,FUND_UTIL_TYPE,MAKER_ID,MAKER_DATE,AUTHORISER_ID,AUTHORISED_DATE,SEQNO,CORR_FEE_ACC_NO,BRANCH_NO,COMPANY_NAME,CORP_FEE_BRANCH_NO,SETTL_ACC_CURR,CORR_ACC_CURR,ORIGINATOR_ID,PRODUCT_CLASSIFICATION,ORIGINATOR_CURR,ACCOUNT_USAGE,DEFAULT_ACCOUNT,CHQ_STOCK_TYPE,ABA_ROUTING_NO,EXTERNAL_ACC_TYPE,INSTITUTION_NUMBER,EXTERNAL_ACCOUNT) values (17000003,'1700012','EMT','L','BCCMKR5',to_date('25-11-19','DD-MM-RR'),'BCCCKR1',to_date('25-11-19','DD-MM-RR'),3,null,'00017','Bulk eTransfer Client03',null,'CAD',null,null,null,null,null,null,null,null,null,null,'No');
Insert into GTB_CUST_ACCOUNT_PREF (COMPANY_ID,ACCOUNT_NO,PAYMENT_PROD_TYPE,FUND_UTIL_TYPE,MAKER_ID,MAKER_DATE,AUTHORISER_ID,AUTHORISED_DATE,SEQNO,CORR_FEE_ACC_NO,BRANCH_NO,COMPANY_NAME,CORP_FEE_BRANCH_NO,SETTL_ACC_CURR,CORR_ACC_CURR,ORIGINATOR_ID,PRODUCT_CLASSIFICATION,ORIGINATOR_CURR,ACCOUNT_USAGE,DEFAULT_ACCOUNT,CHQ_STOCK_TYPE,ABA_ROUTING_NO,EXTERNAL_ACC_TYPE,INSTITUTION_NUMBER,EXTERNAL_ACCOUNT) values (17000003,'1700015','EMT','A','BCCMKR5',to_date('25-11-19','DD-MM-RR'),'BCCCKR1',to_date('25-11-19','DD-MM-RR'),2,null,'00017','Bulk eTransfer Client03',null,'CAD',null,null,null,null,null,null,null,null,null,null,'No');
Insert into GTB_CUST_ACCOUNT_PREF (COMPANY_ID,ACCOUNT_NO,PAYMENT_PROD_TYPE,FUND_UTIL_TYPE,MAKER_ID,MAKER_DATE,AUTHORISER_ID,AUTHORISED_DATE,SEQNO,CORR_FEE_ACC_NO,BRANCH_NO,COMPANY_NAME,CORP_FEE_BRANCH_NO,SETTL_ACC_CURR,CORR_ACC_CURR,ORIGINATOR_ID,PRODUCT_CLASSIFICATION,ORIGINATOR_CURR,ACCOUNT_USAGE,DEFAULT_ACCOUNT,CHQ_STOCK_TYPE,ABA_ROUTING_NO,EXTERNAL_ACC_TYPE,INSTITUTION_NUMBER,EXTERNAL_ACCOUNT) values (17000003,'1700011','EMT','A','BCCMKR5',to_date('25-11-19','DD-MM-RR'),'BCCCKR1',to_date('25-11-19','DD-MM-RR'),1,null,'00017','Bulk eTransfer Client03',null,'CAD',null,null,null,null,null,null,null,null,null,null,'No');


COMMIT;

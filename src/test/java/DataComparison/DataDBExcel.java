package DataComparison;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;
import java.util.Map.Entry;

import Utilities.DBConnector;

public class DataDBExcel {
	PSHPojo p;
	HashMap<String,PSHPojo> data=new HashMap<String,PSHPojo>();
	public HashMap<String,PSHPojo> getDbcolumns(String fw) throws Exception
	{
		Class.forName("oracle.jdbc.driver.OracleDriver");
		DBConnector db=new DBConnector();
		Connection c=db.getConnection("");
		String query="SELECT f.FILE_STATUS, t.TXNS_OTI09, t.TXN_STATUS, o.QUEUEid, C.QUEUELABEL, o.TASKid, B.TASKLABEL, e.EXCEPTION_ID, e.EXCEPTION_DESC, e.EXTERNAL_EXCEPTION_ID, e.EXTERNAL_EXCEPTION_DESC, t.CUST_REF_NUM, t.INIT_PRTY_BEI, t.CLIENT_ID, t.CLIENT_NAME, t.CUST_ID, t.DR_NAME, t.DR_STREET, t.DR_BLDG_NO, t.DR_CITY, t.DR_STATE, t.DR_PSTL_CODE, t.DR_AGT_NAME, t.DR_AGT_CITY, t.DR_AGT_STATE, t.DR_AGT_CTRY, t.DR_AGT_PSTL_CD, t.DR_CTRY, t.DR_ADDR1, t.DR_ADDR2, t.DR_ADDR3, t.DR_ACCT_NO, t.SETTL_METHOD, t.DR_AGT_CLRING_SYS_CD, t.ACC_CCY, t.TXN_CUST_SM, t.DEBIT_AMOUNT, t.INSTR_AMOUNT, t.LIMIT_CHK_EQUI_AMT, t.DEBIT_CURRENCY, t.INSTR_CURRENCY, t.LIMIT_CHK_EQUI_CCY, t.BAL_CHK_EQUI_AMT, t.LIMIT_BAL_CHK_FLAG, t.INSTR_CODE1, t.TXN_FX_ID, t.FX_RATE_TYPE, t.FX_CONTRACT_ID, t.FX_RATE, t.NOTIONAL_EXCH_RATE, t.CR_NAME, t.CR_CTRY, t.CR_ADDR1, t.CR_ADDR2, t.CR_ADDR3, t.CR_AGT_NAME, t.CR_AGT_BIC, t.CR_AGT_CLRING_SYS, t.CR_AGT_CLRING_SYS_CD, t.CR_ACC_NO, t.CR_IBAN, t.CR_AGT_CTRY, t.CR_AGT_ADDR1, t.CR_AGT_ADDR2, t.CR_AGT_ADDR3, t.CHARGE_TYPE, t.CORR_FEE_AMT, t.FEE_CCY, t.CORR_FEE_ACC_NO, t.CORR_TRANSIT, t.REMIT_INFO_1, t.REMIT_INFO_2, t.REMIT_INFO_3, t.REMIT_INFO_4, t.REMT_STATUS_DESC, t.REMT_TYPE, t.SAR_LOC_MTD, t.REMT_CR_LCFX, t.REMT_CR_LCEM, t.REMT_CR_NM_TXN, t.REMT_CR_STRT_TXN, t.REMT_CR_BLDG_TXN, t.REMT_CR_TW_TXN, t.REMT_CR_SD_TXN, t.REMT_CR_CTRY_TXN, t.REMT_CR_PSTL_TXN, t.INTER_AGT_NAME, t.INTER_AGT_BIC, t.INTER_AGT_CLRING_SYS, t.INTER_AGT_CLRING_SYS_CD, t.RBPMT_STATUS_CD, t.RBPMT_STATUS_DESC, t.RBFEE_STATUS_CD, t.RBFEE_STATUS_DESC, t.RBPMT_STATUS_RSN_CD, t.RBPMT_STATUS_RSN_DESC, t.RBFEE_STATUS_RSN_CD, t.RBFEE_STATUS_RSN_DESC, t.UMMREV_STAT_CD, t.UMMREV_STAT_DESC, t.FEEREV_STAT_CD, t.FEEREV_STAT_DESC, t.UMMREV_RSN_CD, t.UMMREV_RSN_DESC, t.FEEREV_RSN_CD, t.FEEREV_RSN_DESC, t.TXN_CLGSYSREF, t.IWS_STATUS_CD, t.IWS_STATUS_DESC, t.CLR_SYSM_CODE, t.TXN_SWIFT_UETR, t.TXN_GOW_NOTF_FLG, t.TXN_GOW_ACCT_STAT, t.NOC_FLAG, t.LCL_REC_TYPEID, t.INIT_PRTY_ID, t.ACNO_RETURN, t.TXNS_OTI08, t.TFRI_PUR_FT, t.TIBI_CA_ID, t.NOC_CHG_CODE, t.TPOI_IP_NM2, t.NOC_CORR_INFO, t.B_SRVCLVL_CD, t.TFBI_CR_ACTYFT, t.CR_CITY, t.CR_STATE, t.CR_PSTL_CD, t.ADDITONAL_INFO, t.TRN_STATUS_CODE, t.UMM_STAT_CD, t.UMM_STAT_CD_DESC, t.UMM_PSH_STAT_CD, t.UMM_PSH_STAT_CD_DESC, t.CHQ_DLVY_MTD, t.CR_STREET, t.CR_BLDG_NO, t.CHQ_FRM_NM, t.CHQ_FRM_STRT, t.CHQ_FRM_BLDG, t.CHQ_FRM_TW, t.CHQ_FRM_SD, t.CHQ_FRM_CTRY, t.CHQ_FRM_PSTL, t.CHQ_TO_NM, t.CHQ_TO_STRT, t.CHQ_TO_BLDG, t.CHQ_TO_TW, t.CHQ_TO_SD, t.CHQ_TO_CTRY, t.CHQ_TO_PSTL, t.CR_LANG_PREF, t.CR_EMAIL, t.CR_MOB_NB, t.RESP_CODE, t.RESP_DESC, t.RESP_REF_NUM, t.TXN_RTP_INTERAC_REF_NUM, t.TXN_RTP_REF_NUM, t.TXN_CONTACT_ID, t.TXN_STSUPD_CD, t.CUST_CNCL_ID, t.FI_CANC_REF_NO, t.PARAM_CNCL_REQ_DATE, t.PARAM_CNCL_REQ_STAT, t.PARAM_CNCL_RSN_CODE, t.PARAM_CNCL_RSN_DESC, t.IS_BATCH_DEBIT, t.T_ORG_DFI_QLFR, t.TICI_I2_CSID, t.INSTR_CODE_PROP, t.TXNS_BGN01\r\n" + 
				"FROM FILE_PROC_OPN f, OW_WORKITEM_INSTANCE o, OW_TASK B, OW_QUEUE C, TXN_PROC_OPN t left outer join GTB_EXCEP_GRID_DETAIL e on t.workitemid = e.workitemid\r\n" + 
				"WHERE F.WORKITEMID = t.File_Workitem_Id\r\n" + 
				"and o.TASKID = B.TASKID\r\n" + 
				"and o.QUEUEID = C.QUEUEID\r\n" + 
				"and o.WORKITEMID = t.Workitemid\r\n" + 
				"and t.FILE_WORKITEM_ID IN ("+fw+")\n" + 
				"Order By t.CUST_REF_NUM";
		Statement s=c.createStatement();
		ResultSet rs=s.executeQuery(query);
		HashMap<String,PSHPojo> hm=new HashMap<String,PSHPojo>();
		System.out.println(rs.getFetchSize());
		while(rs.next())
		{
			p=new PSHPojo();
			//System.out.println(rs.getString("CUST_REF_NUM"));
			//System.out.println(rs.getString("TXNS_OTI09"));
			//System.out.println(rs.getString("TXN_STATUS"));
			String sp=(rs.getString("DR_AGT_CITY")!=null)?rs.getString("DR_AGT_CITY"):"";
			//System.out.println(sp);

			p.setFILE_STATUS((rs.getString("FILE_STATUS")!=null)?rs.getString("FILE_STATUS"):"");
			p.setTXN_TYPE((rs.getString("TXNS_OTI09")!=null)?rs.getString("TXNS_OTI09"):"");
			p.setTXN_STATUS((rs.getString("TXN_STATUS")!=null)?rs.getString("TXN_STATUS"):"");
			//p.setEXCEPTION_ID(rs.getString("EXCEPTION_ID"));
			p.setQUEUEID((rs.getString("QUEUEID")!=null)?rs.getString("QUEUEID"):"");
			p.setQUEUELABEL((rs.getString("QUEUELABEL")!=null)?rs.getString("QUEUELABEL"):"");
			p.setTASKID((rs.getString("TASKID")!=null)?rs.getString("TASKID"):"");
			p.setTASKLABEL((rs.getString("TASKLABEL")!=null)?rs.getString("TASKLABEL"):"");
			p.setEXCEPTION_ID((rs.getString("EXCEPTION_ID")!=null)?rs.getString("EXCEPTION_ID"):"");
			p.setEXCEPTION_DESC((rs.getString("EXCEPTION_DESC")!=null)?rs.getString("EXCEPTION_DESC"):"");
			p.setEXTERNAL_EXCEPTION_ID((rs.getString("EXTERNAL_EXCEPTION_ID")!=null)?rs.getString("EXTERNAL_EXCEPTION_ID"):"");
			p.setEXTERNAL_EXCEPTION_DESC((rs.getString("EXTERNAL_EXCEPTION_DESC")!=null)?rs.getString("EXTERNAL_EXCEPTION_DESC"):"");
			p.setCUST_REF_NUM((rs.getString("CUST_REF_NUM")!=null)?rs.getString("CUST_REF_NUM"):"");
			p.setINIT_PRTY_BEI((rs.getString("INIT_PRTY_BEI")!=null)?rs.getString("INIT_PRTY_BEI"):"");
			p.setCLIENT_ID((rs.getString("CLIENT_ID")!=null)?rs.getString("CLIENT_ID"):"");
			p.setCLIENT_NAME((rs.getString("CLIENT_NAME")!=null)?rs.getString("CLIENT_NAME"):"");
			p.setCUST_ID((rs.getString("CUST_ID")!=null)?rs.getString("CUST_ID"):"");
			p.setDR_NAME((rs.getString("DR_NAME")!=null)?rs.getString("DR_NAME"):"");
			p.setDR_STREET((rs.getString("DR_STREET")!=null)?rs.getString("DR_STREET"):"");
			p.setDR_BLDG_NO((rs.getString("DR_BLDG_NO")!=null)?rs.getString("DR_BLDG_NO"):"");
			p.setDR_CITY((rs.getString("DR_CITY")!=null)?rs.getString("DR_CITY"):"");
			p.setDR_STATE((rs.getString("DR_STATE")!=null)?rs.getString("DR_STATE"):"");
			p.setDR_STREET((rs.getString("DR_STREET")!=null)?rs.getString("DR_STREET"):"");
			p.setDR_BLDG_NO((rs.getString("DR_BLDG_NO")!=null)?rs.getString("DR_BLDG_NO"):"");
			p.setDR_CITY((rs.getString("DR_CITY")!=null)?rs.getString("DR_CITY"):"");
			p.setDR_STATE((rs.getString("DR_STATE")!=null)?rs.getString("DR_STATE"):"");
			p.setDR_PSTL_CODE((rs.getString("DR_PSTL_CODE")!=null)?rs.getString("DR_PSTL_CODE"):"");
			p.setDR_AGT_NAME((rs.getString("DR_AGT_NAME")!=null)?rs.getString("DR_AGT_NAME"):"");
			p.setDR_AGT_CITY((rs.getString("DR_AGT_CITY")!=null)?rs.getString("DR_AGT_CITY"):"");
			p.setDR_AGT_STATE((rs.getString("DR_AGT_STATE")!=null)?rs.getString("DR_AGT_STATE"):"");
			p.setDR_AGT_CTRY((rs.getString("DR_AGT_CTRY")!=null)?rs.getString("DR_AGT_CTRY"):"");
			p.setDR_AGT_PSTL_CD((rs.getString("DR_AGT_PSTL_CD")!=null)?rs.getString("DR_AGT_PSTL_CD"):"");
			p.setDR_CTRY((rs.getString("DR_CTRY")!=null)?rs.getString("DR_CTRY"):"");
			p.setDR_ADDR1((rs.getString("DR_ADDR1")!=null)?rs.getString("DR_ADDR1"):"");
			p.setDR_ADDR2((rs.getString("DR_ADDR2")!=null)?rs.getString("DR_ADDR2"):"");
			p.setDR_ADDR3((rs.getString("DR_ADDR3")!=null)?rs.getString("DR_ADDR3"):"");
			p.setDR_ACCT_NO((rs.getString("DR_ACCT_NO")!=null)?rs.getString("DR_ACCT_NO"):"");
			p.setSETTL_METHOD((rs.getString("SETTL_METHOD")!=null)?rs.getString("SETTL_METHOD"):"");
			p.setDR_AGT_CLRING_SYS_CD((rs.getString("DR_AGT_CLRING_SYS_CD")!=null)?rs.getString("DR_AGT_CLRING_SYS_CD"):"");
			p.setACC_CCY((rs.getString("ACC_CCY")!=null)?rs.getString("ACC_CCY"):"");
			p.setTXN_CUST_SM((rs.getString("TXN_CUST_SM")!=null)?rs.getString("TXN_CUST_SM"):"");
			p.setDEBIT_AMOUNT((rs.getString("DEBIT_AMOUNT")!=null)?rs.getString("DEBIT_AMOUNT"):"");
			p.setINSTR_AMOUNT((rs.getString("INSTR_AMOUNT")!=null)?rs.getString("INSTR_AMOUNT"):"");
			p.setLIMIT_CHK_EQUI_AMT((rs.getString("LIMIT_CHK_EQUI_AMT")!=null)?rs.getString("LIMIT_CHK_EQUI_AMT"):"");
			p.setDEBIT_CURRENCY((rs.getString("DEBIT_CURRENCY")!=null)?rs.getString("DEBIT_CURRENCY"):"");
			p.setINSTR_CURRENCY((rs.getString("INSTR_CURRENCY")!=null)?rs.getString("INSTR_CURRENCY"):"");
			p.setLIMIT_CHK_EQUI_CCY((rs.getString("LIMIT_CHK_EQUI_CCY")!=null)?rs.getString("LIMIT_CHK_EQUI_CCY"):"");
			p.setBAL_CHK_EQUI_AMT((rs.getString("BAL_CHK_EQUI_AMT")!=null)?rs.getString("BAL_CHK_EQUI_AMT"):"");
			p.setLIMIT_BAL_CHK_FLAG((rs.getString("LIMIT_BAL_CHK_FLAG")!=null)?rs.getString("LIMIT_BAL_CHK_FLAG"):"");
			p.setINSTR_CODE1((rs.getString("INSTR_CODE1")!=null)?rs.getString("INSTR_CODE1"):"");
			p.setTXN_FX_ID((rs.getString("TXN_FX_ID")!=null)?rs.getString("TXN_FX_ID"):"");
			p.setFX_RATE_TYPE((rs.getString("FX_RATE_TYPE")!=null)?rs.getString("FX_RATE_TYPE"):"");
			p.setFX_CONTRACT_ID((rs.getString("FX_CONTRACT_ID")!=null)?rs.getString("FX_CONTRACT_ID"):"");
			p.setFX_RATE((rs.getString("FX_RATE")!=null)?rs.getString("FX_RATE"):"");
			p.setNOTIONAL_EXCH_RATE((rs.getString("NOTIONAL_EXCH_RATE")!=null)?rs.getString("NOTIONAL_EXCH_RATE"):"");
			p.setCR_NAME((rs.getString("CR_NAME")!=null)?rs.getString("CR_NAME"):"");
			p.setCR_CTRY((rs.getString("CR_CTRY")!=null)?rs.getString("CR_CTRY"):"");
			p.setCR_ADDR1((rs.getString("CR_ADDR1")!=null)?rs.getString("CR_ADDR1"):"");
			p.setCR_ADDR2((rs.getString("CR_ADDR2")!=null)?rs.getString("CR_ADDR2"):"");
			p.setCR_ADDR3((rs.getString("CR_ADDR3")!=null)?rs.getString("CR_ADDR3"):"");
			p.setCR_AGT_NAME((rs.getString("CR_AGT_NAME")!=null)?rs.getString("CR_AGT_NAME"):"");
			p.setCR_AGT_BIC((rs.getString("CR_AGT_BIC")!=null)?rs.getString("CR_AGT_BIC"):"");
			p.setCR_AGT_CLRING_SYS((rs.getString("CR_AGT_CLRING_SYS")!=null)?rs.getString("CR_AGT_CLRING_SYS"):"");
			p.setCR_AGT_CLRING_SYS_CD((rs.getString("CR_AGT_CLRING_SYS_CD")!=null)?rs.getString("CR_AGT_CLRING_SYS_CD"):"");
			p.setCR_ACC_NO((rs.getString("CR_ACC_NO")!=null)?rs.getString("CR_ACC_NO"):"");
			p.setCR_IBAN((rs.getString("CR_IBAN")!=null)?rs.getString("CR_IBAN"):"");
			p.setCR_AGT_CTRY((rs.getString("CR_AGT_CTRY")!=null)?rs.getString("CR_AGT_CTRY"):"");
			p.setCR_AGT_ADDR1((rs.getString("CR_AGT_ADDR1")!=null)?rs.getString("CR_AGT_ADDR1"):"");
			p.setCR_AGT_ADDR2((rs.getString("CR_AGT_ADDR2")!=null)?rs.getString("CR_AGT_ADDR2"):"");
			p.setCR_AGT_ADDR3((rs.getString("CR_AGT_ADDR3")!=null)?rs.getString("CR_AGT_ADDR3"):"");
			p.setCHARGE_TYPE((rs.getString("CHARGE_TYPE")!=null)?rs.getString("CHARGE_TYPE"):"");
			p.setCORR_FEE_AMT((rs.getString("CORR_FEE_AMT")!=null)?rs.getString("CORR_FEE_AMT"):"");
			p.setFEE_CCY((rs.getString("FEE_CCY")!=null)?rs.getString("FEE_CCY"):"");
			p.setCORR_FEE_ACC_NO((rs.getString("CORR_FEE_ACC_NO")!=null)?rs.getString("CORR_FEE_ACC_NO"):"");
			p.setCORR_TRANSIT((rs.getString("CORR_TRANSIT")!=null)?rs.getString("CORR_TRANSIT"):"");
			p.setREMIT_INFO_1((rs.getString("REMIT_INFO_1")!=null)?rs.getString("REMIT_INFO_1"):"");
			p.setREMIT_INFO_2((rs.getString("REMIT_INFO_2")!=null)?rs.getString("REMIT_INFO_2"):"");
			p.setREMIT_INFO_3((rs.getString("REMIT_INFO_3")!=null)?rs.getString("REMIT_INFO_3"):"");
			p.setREMIT_INFO_4((rs.getString("REMIT_INFO_4")!=null)?rs.getString("REMIT_INFO_4"):"");
			p.setREMT_STATUS_DESC((rs.getString("REMT_STATUS_DESC")!=null)?rs.getString("REMT_STATUS_DESC"):"");
			p.setREMT_TYPE((rs.getString("REMT_TYPE")!=null)?rs.getString("REMT_TYPE"):"");
			p.setSAR_LOC_MTD((rs.getString("SAR_LOC_MTD")!=null)?rs.getString("SAR_LOC_MTD"):"");
			p.setREMT_CR_LCFX((rs.getString("REMT_CR_LCFX")!=null)?rs.getString("REMT_CR_LCFX"):"");
			p.setREMT_CR_LCEM((rs.getString("REMT_CR_LCEM")!=null)?rs.getString("REMT_CR_LCEM"):"");
			p.setREMT_CR_NM_TXN((rs.getString("REMT_CR_NM_TXN")!=null)?rs.getString("REMT_CR_NM_TXN"):"");
			p.setREMT_CR_STRT_TXN((rs.getString("REMT_CR_STRT_TXN")!=null)?rs.getString("REMT_CR_STRT_TXN"):"");
			p.setREMT_CR_BLDG_TXN((rs.getString("REMT_CR_BLDG_TXN")!=null)?rs.getString("REMT_CR_BLDG_TXN"):"");
			p.setREMT_CR_TW_TXN((rs.getString("REMT_CR_TW_TXN")!=null)?rs.getString("REMT_CR_TW_TXN"):"");
			p.setREMT_CR_SD_TXN((rs.getString("REMT_CR_SD_TXN")!=null)?rs.getString("REMT_CR_SD_TXN"):"");
			p.setREMT_CR_CTRY_TXN((rs.getString("REMT_CR_CTRY_TXN")!=null)?rs.getString("REMT_CR_CTRY_TXN"):"");
			p.setREMT_CR_PSTL_TXN((rs.getString("REMT_CR_PSTL_TXN")!=null)?rs.getString("REMT_CR_PSTL_TXN"):"");
			p.setINTER_AGT_NAME((rs.getString("INTER_AGT_NAME")!=null)?rs.getString("INTER_AGT_NAME"):"");
			p.setINTER_AGT_BIC((rs.getString("INTER_AGT_BIC")!=null)?rs.getString("INTER_AGT_BIC"):"");
			p.setINTER_AGT_CLRING_SYS((rs.getString("INTER_AGT_CLRING_SYS")!=null)?rs.getString("INTER_AGT_CLRING_SYS"):"");
			p.setINTER_AGT_CLRING_SYS_CD((rs.getString("INTER_AGT_CLRING_SYS_CD")!=null)?rs.getString("INTER_AGT_CLRING_SYS_CD"):"");
			p.setRBPMT_STATUS_CD((rs.getString("RBPMT_STATUS_CD")!=null)?rs.getString("RBPMT_STATUS_CD"):"");
			p.setRBPMT_STATUS_DESC((rs.getString("RBPMT_STATUS_DESC")!=null)?rs.getString("RBPMT_STATUS_DESC"):"");
			p.setRBFEE_STATUS_CD((rs.getString("RBFEE_STATUS_CD")!=null)?rs.getString("RBFEE_STATUS_CD"):"");
			p.setRBFEE_STATUS_DESC((rs.getString("RBFEE_STATUS_DESC")!=null)?rs.getString("RBFEE_STATUS_DESC"):"");
			p.setRBPMT_STATUS_RSN_CD((rs.getString("RBPMT_STATUS_RSN_CD")!=null)?rs.getString("RBPMT_STATUS_RSN_CD"):"");
			p.setRBPMT_STATUS_RSN_DESC((rs.getString("RBPMT_STATUS_RSN_DESC")!=null)?rs.getString("RBPMT_STATUS_RSN_DESC"):"");
			p.setRBFEE_STATUS_RSN_CD((rs.getString("RBFEE_STATUS_RSN_CD")!=null)?rs.getString("RBFEE_STATUS_RSN_CD"):"");
			p.setRBFEE_STATUS_RSN_DESC((rs.getString("RBFEE_STATUS_RSN_DESC")!=null)?rs.getString("RBFEE_STATUS_RSN_DESC"):"");
			p.setUMMREV_STAT_CD((rs.getString("UMMREV_STAT_CD")!=null)?rs.getString("UMMREV_STAT_CD"):"");
			p.setUMMREV_STAT_DESC((rs.getString("UMMREV_STAT_DESC")!=null)?rs.getString("UMMREV_STAT_DESC"):"");
			p.setFEEREV_STAT_CD((rs.getString("FEEREV_STAT_CD")!=null)?rs.getString("FEEREV_STAT_CD"):"");
			p.setFEEREV_STAT_DESC((rs.getString("FEEREV_STAT_DESC")!=null)?rs.getString("FEEREV_STAT_DESC"):"");
			p.setUMMREV_RSN_CD((rs.getString("UMMREV_RSN_CD")!=null)?rs.getString("UMMREV_RSN_CD"):"");
			p.setUMMREV_RSN_DESC((rs.getString("UMMREV_RSN_DESC")!=null)?rs.getString("UMMREV_RSN_DESC"):"");
			p.setFEEREV_RSN_CD((rs.getString("FEEREV_RSN_CD")!=null)?rs.getString("FEEREV_RSN_CD"):"");
			p.setFEEREV_RSN_DESC((rs.getString("FEEREV_RSN_DESC")!=null)?rs.getString("FEEREV_RSN_DESC"):"");
			p.setTXN_CLGSYSREF((rs.getString("TXN_CLGSYSREF")!=null)?rs.getString("TXN_CLGSYSREF"):"");
			p.setIWS_STATUS_CD((rs.getString("IWS_STATUS_CD")!=null)?rs.getString("IWS_STATUS_CD"):"");
			p.setIWS_STATUS_DESC((rs.getString("IWS_STATUS_DESC")!=null)?rs.getString("IWS_STATUS_DESC"):"");
			p.setCLR_SYSM_CODE((rs.getString("CLR_SYSM_CODE")!=null)?rs.getString("CLR_SYSM_CODE"):"");
			p.setTXN_SWIFT_UETR((rs.getString("TXN_SWIFT_UETR")!=null)?rs.getString("TXN_SWIFT_UETR"):"");
			p.setTXN_GOW_NOTF_FLG((rs.getString("TXN_GOW_NOTF_FLG")!=null)?rs.getString("TXN_GOW_NOTF_FLG"):"");
			p.setTXN_GOW_ACCT_STAT((rs.getString("TXN_GOW_ACCT_STAT")!=null)?rs.getString("TXN_GOW_ACCT_STAT"):"");
			p.setNOC_FLAG((rs.getString("NOC_FLAG")!=null)?rs.getString("NOC_FLAG"):"");
			p.setLCL_REC_TYPEID((rs.getString("LCL_REC_TYPEID")!=null)?rs.getString("LCL_REC_TYPEID"):"");
			p.setINIT_PRTY_ID((rs.getString("INIT_PRTY_ID")!=null)?rs.getString("INIT_PRTY_ID"):"");
			p.setACNO_RETURN((rs.getString("ACNO_RETURN")!=null)?rs.getString("ACNO_RETURN"):"");
			p.setTXNS_OTI08((rs.getString("TXNS_OTI08")!=null)?rs.getString("TXNS_OTI08"):"");
			p.setTFRI_PUR_FT((rs.getString("TFRI_PUR_FT")!=null)?rs.getString("TFRI_PUR_FT"):"");
			p.setTIBI_CA_ID((rs.getString("TIBI_CA_ID")!=null)?rs.getString("TIBI_CA_ID"):"");
			p.setNOC_CHG_CODE((rs.getString("NOC_CHG_CODE")!=null)?rs.getString("NOC_CHG_CODE"):"");
			p.setTPOI_IP_NM2((rs.getString("TPOI_IP_NM2")!=null)?rs.getString("TPOI_IP_NM2"):"");
			p.setNOC_CORR_INFO((rs.getString("NOC_CORR_INFO")!=null)?rs.getString("NOC_CORR_INFO"):"");
			p.setB_SRVCLVL_CD((rs.getString("B_SRVCLVL_CD")!=null)?rs.getString("B_SRVCLVL_CD"):"");
			p.setTFBI_CR_ACTYFT((rs.getString("TFBI_CR_ACTYFT")!=null)?rs.getString("TFBI_CR_ACTYFT"):"");
			p.setCR_CITY((rs.getString("CR_CITY")!=null)?rs.getString("CR_CITY"):"");
			p.setCR_STATE((rs.getString("CR_STATE")!=null)?rs.getString("CR_STATE"):"");
			p.setCR_PSTL_CD((rs.getString("CR_PSTL_CD")!=null)?rs.getString("CR_PSTL_CD"):"");
			p.setADDITONAL_INFO((rs.getString("ADDITONAL_INFO")!=null)?rs.getString("ADDITONAL_INFO"):"");
			p.setTRN_STATUS_CODE((rs.getString("TRN_STATUS_CODE")!=null)?rs.getString("TRN_STATUS_CODE"):"");
			p.setUMM_STAT_CD((rs.getString("UMM_STAT_CD")!=null)?rs.getString("UMM_STAT_CD"):"");
			p.setUMM_STAT_CD_DESC((rs.getString("UMM_STAT_CD_DESC")!=null)?rs.getString("UMM_STAT_CD_DESC"):"");
			p.setUMM_PSH_STAT_CD((rs.getString("UMM_PSH_STAT_CD")!=null)?rs.getString("UMM_PSH_STAT_CD"):"");
			p.setUMM_PSH_STAT_CD_DESC((rs.getString("UMM_PSH_STAT_CD_DESC")!=null)?rs.getString("UMM_PSH_STAT_CD_DESC"):"");
			p.setCHQ_DLVY_MTD((rs.getString("CHQ_DLVY_MTD")!=null)?rs.getString("CHQ_DLVY_MTD"):"");
			p.setCR_STREET((rs.getString("CR_STREET")!=null)?rs.getString("CR_STREET"):"");
			p.setCR_BLDG_NO1((rs.getString("CR_BLDG_NO")!=null)?rs.getString("CR_BLDG_NO"):"");
			p.setCHQ_FRM_NM((rs.getString("CHQ_FRM_NM")!=null)?rs.getString("CHQ_FRM_NM"):"");
			p.setCHQ_FRM_STRT((rs.getString("CHQ_FRM_STRT")!=null)?rs.getString("CHQ_FRM_STRT"):"");
			p.setCHQ_FRM_BLDG((rs.getString("CHQ_FRM_BLDG")!=null)?rs.getString("CHQ_FRM_BLDG"):"");
			p.setCHQ_FRM_TW((rs.getString("CHQ_FRM_TW")!=null)?rs.getString("CHQ_FRM_TW"):"");
			p.setCHQ_FRM_SD((rs.getString("CHQ_FRM_SD")!=null)?rs.getString("CHQ_FRM_SD"):"");
			p.setCHQ_FRM_CTRY((rs.getString("CHQ_FRM_CTRY")!=null)?rs.getString("CHQ_FRM_CTRY"):"");
			p.setCHQ_FRM_PSTL((rs.getString("CHQ_FRM_PSTL")!=null)?rs.getString("CHQ_FRM_PSTL"):"");
			p.setCHQ_TO_NM((rs.getString("CHQ_TO_NM")!=null)?rs.getString("CHQ_TO_NM"):"");
			p.setCHQ_TO_STRT((rs.getString("CHQ_TO_STRT")!=null)?rs.getString("CHQ_TO_STRT"):"");
			p.setCHQ_TO_BLDG((rs.getString("CHQ_TO_BLDG")!=null)?rs.getString("CHQ_TO_BLDG"):"");
			p.setCHQ_TO_TW((rs.getString("CHQ_TO_TW")!=null)?rs.getString("CHQ_TO_TW"):"");
			p.setCHQ_TO_SD((rs.getString("CHQ_TO_SD")!=null)?rs.getString("CHQ_TO_SD"):"");
			p.setCHQ_TO_CTRY((rs.getString("CHQ_TO_CTRY")!=null)?rs.getString("CHQ_TO_CTRY"):"");
			p.setCHQ_TO_PSTL((rs.getString("CHQ_TO_PSTL")!=null)?rs.getString("CHQ_TO_PSTL"):"");
			p.setCR_LANG_PREF((rs.getString("CR_LANG_PREF")!=null)?rs.getString("CR_LANG_PREF"):"");
			p.setCR_EMAIL((rs.getString("CR_EMAIL")!=null)?rs.getString("CR_EMAIL"):"");
			p.setCR_MOB_NB((rs.getString("CR_MOB_NB")!=null)?rs.getString("CR_MOB_NB"):"");
			p.setRESP_CODE((rs.getString("RESP_CODE")!=null)?rs.getString("RESP_CODE"):"");
			p.setRESP_DESC((rs.getString("RESP_DESC")!=null)?rs.getString("RESP_DESC"):"");
			p.setRESP_REF_NUM((rs.getString("RESP_REF_NUM")!=null)?rs.getString("RESP_REF_NUM"):"");
			p.setTXN_RTP_INTERAC_REF_NUM((rs.getString("TXN_RTP_INTERAC_REF_NUM")!=null)?rs.getString("TXN_RTP_INTERAC_REF_NUM"):"");
			p.setTXN_RTP_REF_NUM((rs.getString("TXN_RTP_REF_NUM")!=null)?rs.getString("TXN_RTP_REF_NUM"):"");
			p.setTXN_CONTACT_ID((rs.getString("TXN_CONTACT_ID")!=null)?rs.getString("TXN_CONTACT_ID"):"");
			p.setTXN_STSUPD_CD((rs.getString("TXN_STSUPD_CD")!=null)?rs.getString("TXN_STSUPD_CD"):"");
			p.setCUST_CNCL_ID((rs.getString("CUST_CNCL_ID")!=null)?rs.getString("CUST_CNCL_ID"):"");
			p.setFI_CANC_REF_NO((rs.getString("FI_CANC_REF_NO")!=null)?rs.getString("FI_CANC_REF_NO"):"");
			p.setPARAM_CNCL_REQ_DATE((rs.getString("PARAM_CNCL_REQ_DATE")!=null)?rs.getString("PARAM_CNCL_REQ_DATE"):"");
			p.setPARAM_CNCL_REQ_STAT((rs.getString("PARAM_CNCL_REQ_STAT")!=null)?rs.getString("PARAM_CNCL_REQ_STAT"):"");
			p.setPARAM_CNCL_RSN_CODE((rs.getString("PARAM_CNCL_RSN_CODE")!=null)?rs.getString("PARAM_CNCL_RSN_CODE"):"");
			p.setPARAM_CNCL_RSN_DESC((rs.getString("PARAM_CNCL_RSN_DESC")!=null)?rs.getString("PARAM_CNCL_RSN_DESC"):"");
			p.setIS_BATCH_DEBIT((rs.getString("IS_BATCH_DEBIT")!=null)?rs.getString("IS_BATCH_DEBIT"):"");
			p.setT_ORG_DFI_QLFR((rs.getString("T_ORG_DFI_QLFR")!=null)?rs.getString("T_ORG_DFI_QLFR"):"");
			p.setTICI_I2_CSID((rs.getString("TICI_I2_CSID")!=null)?rs.getString("TICI_I2_CSID"):"");
			p.setINSTR_CODE_PROP((rs.getString("INSTR_CODE_PROP")!=null)?rs.getString("INSTR_CODE_PROP"):"");
			p.setTXNS_BGN01((rs.getString("TXNS_BGN01")!=null)?rs.getString("TXNS_BGN01"):"");
			data.put(rs.getString("CUST_REF_NUM"),p);
			//break;
			//hm.put
		}
	  //  System.out.println("---------Run database----------------");
		//for(Entry<String, PSHPojo> a:data.entrySet())
		{
			//System.out.println(a.getKey()+":"+a.getValue().toString());
		}
		
		rs.close();
		c.close();
		
		return data;
	
		
	}
	public static void main(String[] args) throws Exception {
		DataDBExcel d=new DataDBExcel();
		HashMap<String,PSHPojo> hm=d.getDbcolumns("11341068");
		for(Entry<String, PSHPojo> a:hm.entrySet())
		{
			System.out.println(a.getValue().toString());
		}
	//System.out.println(t.toString());
		String s1=null;
				String s2;
			s2=(s1!=null)?s1:"-";
		//	System.out.println(s2);
	
	}
}

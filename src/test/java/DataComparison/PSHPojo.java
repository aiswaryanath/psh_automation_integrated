package DataComparison;

public class PSHPojo {
String FILE_STATUS;
@Override
public String toString() {
	StringBuilder builder = new StringBuilder();
	builder.append("PSHPojo [FILE_STATUS=");
	builder.append(FILE_STATUS);
	builder.append(", TXN_TYPE=");
	builder.append(TXN_TYPE);
	builder.append(", TXN_STATUS=");
	builder.append(TXN_STATUS);
	builder.append(", QUEUEID=");
	builder.append(QUEUEID);
	builder.append(", QUEUELABEL=");
	builder.append(QUEUELABEL);
	builder.append(", TASKID=");
	builder.append(TASKID);
	builder.append(", TASKLABEL=");
	builder.append(TASKLABEL);
	builder.append(", EXCEPTION_ID=");
	builder.append(EXCEPTION_ID);
	builder.append(", EXCEPTION_DESC=");
	builder.append(EXCEPTION_DESC);
	builder.append(", EXTERNAL_EXCEPTION_ID=");
	builder.append(EXTERNAL_EXCEPTION_ID);
	builder.append(", EXTERNAL_EXCEPTION_DESC=");
	builder.append(EXTERNAL_EXCEPTION_DESC);
	builder.append(", CUST_REF_NUM=");
	builder.append(CUST_REF_NUM);
	builder.append(", INIT_PRTY_BEI=");
	builder.append(INIT_PRTY_BEI);
	builder.append(", CLIENT_ID=");
	builder.append(CLIENT_ID);
	builder.append(", CLIENT_NAME=");
	builder.append(CLIENT_NAME);
	builder.append(", CUST_ID=");
	builder.append(CUST_ID);
	builder.append(", DR_NAME=");
	builder.append(DR_NAME);
	builder.append(", DR_STREET=");
	builder.append(DR_STREET);
	builder.append(", DR_BLDG_NO=");
	builder.append(DR_BLDG_NO);
	builder.append(", DR_CITY=");
	builder.append(DR_CITY);
	builder.append(", DR_STATE=");
	builder.append(DR_STATE);
	builder.append(", DR_PSTL_CODE=");
	builder.append(DR_PSTL_CODE);
	builder.append(", DR_AGT_NAME=");
	builder.append(DR_AGT_NAME);
	builder.append(", DR_AGT_CITY=");
	builder.append(DR_AGT_CITY);
	builder.append(", DR_AGT_STATE=");
	builder.append(DR_AGT_STATE);
	builder.append(", DR_AGT_CTRY=");
	builder.append(DR_AGT_CTRY);
	builder.append(", DR_AGT_PSTL_CD=");
	builder.append(DR_AGT_PSTL_CD);
	builder.append(", DR_CTRY=");
	builder.append(DR_CTRY);
	builder.append(", DR_ADDR1=");
	builder.append(DR_ADDR1);
	builder.append(", DR_ADDR2=");
	builder.append(DR_ADDR2);
	builder.append(", DR_ADDR3=");
	builder.append(DR_ADDR3);
	builder.append(", DR_ACCT_NO=");
	builder.append(DR_ACCT_NO);
	builder.append(", SETTL_METHOD=");
	builder.append(SETTL_METHOD);
	builder.append(", DR_AGT_CLRING_SYS_CD=");
	builder.append(DR_AGT_CLRING_SYS_CD);
	builder.append(", ACC_CCY=");
	builder.append(ACC_CCY);
	builder.append(", TXN_CUST_SM=");
	builder.append(TXN_CUST_SM);
	builder.append(", DEBIT_AMOUNT=");
	builder.append(DEBIT_AMOUNT);
	builder.append(", INSTR_AMOUNT=");
	builder.append(INSTR_AMOUNT);
	builder.append(", LIMIT_CHK_EQUI_AMT=");
	builder.append(LIMIT_CHK_EQUI_AMT);
	builder.append(", DEBIT_CURRENCY=");
	builder.append(DEBIT_CURRENCY);
	builder.append(", INSTR_CURRENCY=");
	builder.append(INSTR_CURRENCY);
	builder.append(", LIMIT_CHK_EQUI_CCY=");
	builder.append(LIMIT_CHK_EQUI_CCY);
	builder.append(", BAL_CHK_EQUI_AMT=");
	builder.append(BAL_CHK_EQUI_AMT);
	builder.append(", LIMIT_BAL_CHK_FLAG=");
	builder.append(LIMIT_BAL_CHK_FLAG);
	builder.append(", INSTR_CODE1=");
	builder.append(INSTR_CODE1);
	builder.append(", TXN_FX_ID=");
	builder.append(TXN_FX_ID);
	builder.append(", FX_RATE_TYPE=");
	builder.append(FX_RATE_TYPE);
	builder.append(", FX_CONTRACT_ID=");
	builder.append(FX_CONTRACT_ID);
	builder.append(", FX_RATE=");
	builder.append(FX_RATE);
	builder.append(", NOTIONAL_EXCH_RATE=");
	builder.append(NOTIONAL_EXCH_RATE);
	builder.append(", CR_NAME=");
	builder.append(CR_NAME);
	builder.append(", CR_CTRY=");
	builder.append(CR_CTRY);
	builder.append(", CR_ADDR1=");
	builder.append(CR_ADDR1);
	builder.append(", CR_ADDR2=");
	builder.append(CR_ADDR2);
	builder.append(", CR_ADDR3=");
	builder.append(CR_ADDR3);
	builder.append(", CR_AGT_NAME=");
	builder.append(CR_AGT_NAME);
	builder.append(", CR_AGT_BIC=");
	builder.append(CR_AGT_BIC);
	builder.append(", CR_AGT_CLRING_SYS=");
	builder.append(CR_AGT_CLRING_SYS);
	builder.append(", CR_AGT_CLRING_SYS_CD=");
	builder.append(CR_AGT_CLRING_SYS_CD);
	builder.append(", CR_ACC_NO=");
	builder.append(CR_ACC_NO);
	builder.append(", CR_IBAN=");
	builder.append(CR_IBAN);
	builder.append(", CR_AGT_CTRY=");
	builder.append(CR_AGT_CTRY);
	builder.append(", CR_AGT_ADDR1=");
	builder.append(CR_AGT_ADDR1);
	builder.append(", CR_AGT_ADDR2=");
	builder.append(CR_AGT_ADDR2);
	builder.append(", CR_AGT_ADDR3=");
	builder.append(CR_AGT_ADDR3);
	builder.append(", CHARGE_TYPE=");
	builder.append(CHARGE_TYPE);
	builder.append(", CORR_FEE_AMT=");
	builder.append(CORR_FEE_AMT);
	builder.append(", FEE_CCY=");
	builder.append(FEE_CCY);
	builder.append(", CORR_FEE_ACC_NO=");
	builder.append(CORR_FEE_ACC_NO);
	builder.append(", CORR_TRANSIT=");
	builder.append(CORR_TRANSIT);
	builder.append(", REMIT_INFO_1=");
	builder.append(REMIT_INFO_1);
	builder.append(", REMIT_INFO_2=");
	builder.append(REMIT_INFO_2);
	builder.append(", REMIT_INFO_3=");
	builder.append(REMIT_INFO_3);
	builder.append(", REMIT_INFO_4=");
	builder.append(REMIT_INFO_4);
	builder.append(", REMT_STATUS_DESC=");
	builder.append(REMT_STATUS_DESC);
	builder.append(", REMT_TYPE=");
	builder.append(REMT_TYPE);
	builder.append(", SAR_LOC_MTD=");
	builder.append(SAR_LOC_MTD);
	builder.append(", REMT_CR_LCFX=");
	builder.append(REMT_CR_LCFX);
	builder.append(", REMT_CR_LCEM=");
	builder.append(REMT_CR_LCEM);
	builder.append(", REMT_CR_NM_TXN=");
	builder.append(REMT_CR_NM_TXN);
	builder.append(", REMT_CR_STRT_TXN=");
	builder.append(REMT_CR_STRT_TXN);
	builder.append(", REMT_CR_BLDG_TXN=");
	builder.append(REMT_CR_BLDG_TXN);
	builder.append(", REMT_CR_TW_TXN=");
	builder.append(REMT_CR_TW_TXN);
	builder.append(", REMT_CR_SD_TXN=");
	builder.append(REMT_CR_SD_TXN);
	builder.append(", REMT_CR_CTRY_TXN=");
	builder.append(REMT_CR_CTRY_TXN);
	builder.append(", REMT_CR_PSTL_TXN=");
	builder.append(REMT_CR_PSTL_TXN);
	builder.append(", INTER_AGT_NAME=");
	builder.append(INTER_AGT_NAME);
	builder.append(", INTER_AGT_BIC=");
	builder.append(INTER_AGT_BIC);
	builder.append(", INTER_AGT_CLRING_SYS=");
	builder.append(INTER_AGT_CLRING_SYS);
	builder.append(", INTER_AGT_CLRING_SYS_CD=");
	builder.append(INTER_AGT_CLRING_SYS_CD);
	builder.append(", RBPMT_STATUS_CD=");
	builder.append(RBPMT_STATUS_CD);
	builder.append(", RBPMT_STATUS_DESC=");
	builder.append(RBPMT_STATUS_DESC);
	builder.append(", RBFEE_STATUS_CD=");
	builder.append(RBFEE_STATUS_CD);
	builder.append(", RBFEE_STATUS_DESC=");
	builder.append(RBFEE_STATUS_DESC);
	builder.append(", RBPMT_STATUS_RSN_CD=");
	builder.append(RBPMT_STATUS_RSN_CD);
	builder.append(", RBPMT_STATUS_RSN_DESC=");
	builder.append(RBPMT_STATUS_RSN_DESC);
	builder.append(", RBFEE_STATUS_RSN_CD=");
	builder.append(RBFEE_STATUS_RSN_CD);
	builder.append(", RBFEE_STATUS_RSN_DESC=");
	builder.append(RBFEE_STATUS_RSN_DESC);
	builder.append(", UMMREV_STAT_CD=");
	builder.append(UMMREV_STAT_CD);
	builder.append(", UMMREV_STAT_DESC=");
	builder.append(UMMREV_STAT_DESC);
	builder.append(", FEEREV_STAT_CD=");
	builder.append(FEEREV_STAT_CD);
	builder.append(", FEEREV_STAT_DESC=");
	builder.append(FEEREV_STAT_DESC);
	builder.append(", UMMREV_RSN_CD=");
	builder.append(UMMREV_RSN_CD);
	builder.append(", UMMREV_RSN_DESC=");
	builder.append(UMMREV_RSN_DESC);
	builder.append(", FEEREV_RSN_CD=");
	builder.append(FEEREV_RSN_CD);
	builder.append(", FEEREV_RSN_DESC=");
	builder.append(FEEREV_RSN_DESC);
	builder.append(", TXN_CLGSYSREF=");
	builder.append(TXN_CLGSYSREF);
	builder.append(", IWS_STATUS_CD=");
	builder.append(IWS_STATUS_CD);
	builder.append(", IWS_STATUS_DESC=");
	builder.append(IWS_STATUS_DESC);
	builder.append(", CLR_SYSM_CODE=");
	builder.append(CLR_SYSM_CODE);
	builder.append(", TXN_SWIFT_UETR=");
	builder.append(TXN_SWIFT_UETR);
	builder.append(", TXN_GOW_NOTF_FLG=");
	builder.append(TXN_GOW_NOTF_FLG);
	builder.append(", TXN_GOW_ACCT_STAT=");
	builder.append(TXN_GOW_ACCT_STAT);
	builder.append(", NOC_FLAG=");
	builder.append(NOC_FLAG);
	builder.append(", LCL_REC_TYPEID=");
	builder.append(LCL_REC_TYPEID);
	builder.append(", INIT_PRTY_ID=");
	builder.append(INIT_PRTY_ID);
	builder.append(", ACNO_RETURN=");
	builder.append(ACNO_RETURN);
	builder.append(", TXNS_OTI08=");
	builder.append(TXNS_OTI08);
	builder.append(", TFRI_PUR_FT=");
	builder.append(TFRI_PUR_FT);
	builder.append(", TIBI_CA_ID=");
	builder.append(TIBI_CA_ID);
	builder.append(", NOC_CHG_CODE=");
	builder.append(NOC_CHG_CODE);
	builder.append(", TPOI_IP_NM2=");
	builder.append(TPOI_IP_NM2);
	builder.append(", NOC_CORR_INFO=");
	builder.append(NOC_CORR_INFO);
	builder.append(", B_SRVCLVL_CD=");
	builder.append(B_SRVCLVL_CD);
	builder.append(", TFBI_CR_ACTYFT=");
	builder.append(TFBI_CR_ACTYFT);
	builder.append(", CR_CITY=");
	builder.append(CR_CITY);
	builder.append(", CR_STATE=");
	builder.append(CR_STATE);
	builder.append(", CR_PSTL_CD=");
	builder.append(CR_PSTL_CD);
	builder.append(", ADDITONAL_INFO=");
	builder.append(ADDITONAL_INFO);
	builder.append(", TRN_STATUS_CODE=");
	builder.append(TRN_STATUS_CODE);
	builder.append(", UMM_STAT_CD=");
	builder.append(UMM_STAT_CD);
	builder.append(", UMM_STAT_CD_DESC=");
	builder.append(UMM_STAT_CD_DESC);
	builder.append(", UMM_PSH_STAT_CD=");
	builder.append(UMM_PSH_STAT_CD);
	builder.append(", UMM_PSH_STAT_CD_DESC=");
	builder.append(UMM_PSH_STAT_CD_DESC);
	builder.append(", CHQ_DLVY_MTD=");
	builder.append(CHQ_DLVY_MTD);
	builder.append(", CR_STREET=");
	builder.append(CR_STREET);
	builder.append(", CR_BLDG_NO1=");
	builder.append(CR_BLDG_NO1);
	builder.append(", CHQ_FRM_NM=");
	builder.append(CHQ_FRM_NM);
	builder.append(", CHQ_FRM_STRT=");
	builder.append(CHQ_FRM_STRT);
	builder.append(", CHQ_FRM_BLDG=");
	builder.append(CHQ_FRM_BLDG);
	builder.append(", CHQ_FRM_TW=");
	builder.append(CHQ_FRM_TW);
	builder.append(", CHQ_FRM_SD=");
	builder.append(CHQ_FRM_SD);
	builder.append(", CHQ_FRM_CTRY=");
	builder.append(CHQ_FRM_CTRY);
	builder.append(", CHQ_FRM_PSTL=");
	builder.append(CHQ_FRM_PSTL);
	builder.append(", CHQ_TO_NM=");
	builder.append(CHQ_TO_NM);
	builder.append(", CHQ_TO_STRT=");
	builder.append(CHQ_TO_STRT);
	builder.append(", CHQ_TO_BLDG=");
	builder.append(CHQ_TO_BLDG);
	builder.append(", CHQ_TO_TW=");
	builder.append(CHQ_TO_TW);
	builder.append(", CHQ_TO_SD=");
	builder.append(CHQ_TO_SD);
	builder.append(", CHQ_TO_CTRY=");
	builder.append(CHQ_TO_CTRY);
	builder.append(", CHQ_TO_PSTL=");
	builder.append(CHQ_TO_PSTL);
	builder.append(", CR_LANG_PREF=");
	builder.append(CR_LANG_PREF);
	builder.append(", CR_EMAIL=");
	builder.append(CR_EMAIL);
	builder.append(", CR_MOB_NB=");
	builder.append(CR_MOB_NB);
	builder.append(", RESP_CODE=");
	builder.append(RESP_CODE);
	builder.append(", RESP_DESC=");
	builder.append(RESP_DESC);
	builder.append(", RESP_REF_NUM=");
	builder.append(RESP_REF_NUM);
	builder.append(", TXN_RTP_INTERAC_REF_NUM=");
	builder.append(TXN_RTP_INTERAC_REF_NUM);
	builder.append(", TXN_RTP_REF_NUM=");
	builder.append(TXN_RTP_REF_NUM);
	builder.append(", TXN_CONTACT_ID=");
	builder.append(TXN_CONTACT_ID);
	builder.append(", TXN_STSUPD_CD=");
	builder.append(TXN_STSUPD_CD);
	builder.append(", CUST_CNCL_ID=");
	builder.append(CUST_CNCL_ID);
	builder.append(", FI_CANC_REF_NO=");
	builder.append(FI_CANC_REF_NO);
	builder.append(", PARAM_CNCL_REQ_DATE=");
	builder.append(PARAM_CNCL_REQ_DATE);
	builder.append(", PARAM_CNCL_REQ_STAT=");
	builder.append(PARAM_CNCL_REQ_STAT);
	builder.append(", PARAM_CNCL_RSN_CODE=");
	builder.append(PARAM_CNCL_RSN_CODE);
	builder.append(", PARAM_CNCL_RSN_DESC=");
	builder.append(PARAM_CNCL_RSN_DESC);
	builder.append(", IS_BATCH_DEBIT=");
	builder.append(IS_BATCH_DEBIT);
	builder.append(", T_ORG_DFI_QLFR=");
	builder.append(T_ORG_DFI_QLFR);
	builder.append(", TICI_I2_CSID=");
	builder.append(TICI_I2_CSID);
	builder.append(", INSTR_CODE_PROP=");
	builder.append(INSTR_CODE_PROP);
	builder.append(", TXNS_BGN01=");
	builder.append(TXNS_BGN01);
	builder.append("]");
	return builder.toString();
}
String TXN_TYPE;
String TXN_STATUS;
String QUEUEID;
String QUEUELABEL;
String TASKID;
String TASKLABEL;
String EXCEPTION_ID;
String EXCEPTION_DESC;
String EXTERNAL_EXCEPTION_ID;
String EXTERNAL_EXCEPTION_DESC;
String CUST_REF_NUM;
String INIT_PRTY_BEI;
String CLIENT_ID;
String CLIENT_NAME;
String CUST_ID;
String DR_NAME;
String DR_STREET;
String DR_BLDG_NO;
String DR_CITY;
String DR_STATE	;
String DR_PSTL_CODE;
String DR_AGT_NAME;
String DR_AGT_CITY;
String DR_AGT_STATE;
String DR_AGT_CTRY;
String DR_AGT_PSTL_CD;
String DR_CTRY;
String DR_ADDR1;
String DR_ADDR2;
String DR_ADDR3;
String DR_ACCT_NO;
String SETTL_METHOD;
String DR_AGT_CLRING_SYS_CD;
String ACC_CCY;
String TXN_CUST_SM;
String DEBIT_AMOUNT;
String INSTR_AMOUNT;
String LIMIT_CHK_EQUI_AMT;
String DEBIT_CURRENCY;
String INSTR_CURRENCY;
String LIMIT_CHK_EQUI_CCY;
String BAL_CHK_EQUI_AMT;
String LIMIT_BAL_CHK_FLAG;
String INSTR_CODE1;
String TXN_FX_ID;
String FX_RATE_TYPE;
String FX_CONTRACT_ID;
String FX_RATE;
String NOTIONAL_EXCH_RATE;
String CR_NAME;
String CR_CTRY;
String CR_ADDR1;
String CR_ADDR2;
String CR_ADDR3;
String CR_AGT_NAME;
String CR_AGT_BIC;
String CR_AGT_CLRING_SYS;
String CR_AGT_CLRING_SYS_CD;
String CR_ACC_NO;
String CR_IBAN;
String CR_AGT_CTRY;
String CR_AGT_ADDR1;
String CR_AGT_ADDR2;

String CR_AGT_ADDR3;
String CHARGE_TYPE;
String CORR_FEE_AMT;
String FEE_CCY;
String CORR_FEE_ACC_NO;
String CORR_TRANSIT;
String REMIT_INFO_1;
String REMIT_INFO_2;
String REMIT_INFO_3;
String REMIT_INFO_4;
String REMT_STATUS_DESC;
String REMT_TYPE;
String SAR_LOC_MTD;
String REMT_CR_LCFX;
String REMT_CR_LCEM;
String REMT_CR_NM_TXN;
String REMT_CR_STRT_TXN;
String REMT_CR_BLDG_TXN;
String REMT_CR_TW_TXN;
String REMT_CR_SD_TXN;
String REMT_CR_CTRY_TXN;
String REMT_CR_PSTL_TXN;
String INTER_AGT_NAME;
String INTER_AGT_BIC;
String INTER_AGT_CLRING_SYS;
String INTER_AGT_CLRING_SYS_CD;
String RBPMT_STATUS_CD;
String RBPMT_STATUS_DESC;
String RBFEE_STATUS_CD;
String RBFEE_STATUS_DESC;
String RBPMT_STATUS_RSN_CD;
String RBPMT_STATUS_RSN_DESC;
String RBFEE_STATUS_RSN_CD;
String RBFEE_STATUS_RSN_DESC;
String UMMREV_STAT_CD;
String UMMREV_STAT_DESC;
String FEEREV_STAT_CD;
String FEEREV_STAT_DESC;
String UMMREV_RSN_CD;
String UMMREV_RSN_DESC;
String FEEREV_RSN_CD;
String FEEREV_RSN_DESC;
String TXN_CLGSYSREF;
String IWS_STATUS_CD;
String IWS_STATUS_DESC;
String CLR_SYSM_CODE;
String TXN_SWIFT_UETR;
String TXN_GOW_NOTF_FLG;
String TXN_GOW_ACCT_STAT;
String NOC_FLAG;
String LCL_REC_TYPEID;
String INIT_PRTY_ID;
String ACNO_RETURN;
String TXNS_OTI08;
String TFRI_PUR_FT;
String TIBI_CA_ID;
String NOC_CHG_CODE;
String TPOI_IP_NM2;
String NOC_CORR_INFO;
String B_SRVCLVL_CD;
String TFBI_CR_ACTYFT;
String CR_CITY;
String CR_STATE;
String CR_PSTL_CD;
String ADDITONAL_INFO;
String TRN_STATUS_CODE;
String UMM_STAT_CD;
String UMM_STAT_CD_DESC;
String UMM_PSH_STAT_CD;
String UMM_PSH_STAT_CD_DESC;
String CHQ_DLVY_MTD;
String CR_STREET;
String CR_BLDG_NO1;
String CHQ_FRM_NM;
String CHQ_FRM_STRT;
String CHQ_FRM_BLDG;
String CHQ_FRM_TW;
String CHQ_FRM_SD;
String CHQ_FRM_CTRY;
String CHQ_FRM_PSTL;
String CHQ_TO_NM;
String CHQ_TO_STRT;
String CHQ_TO_BLDG;
String CHQ_TO_TW;
String CHQ_TO_SD;
String CHQ_TO_CTRY;
String CHQ_TO_PSTL;
String CR_LANG_PREF;
String CR_EMAIL;
String CR_MOB_NB;
String RESP_CODE;
String RESP_DESC;
String RESP_REF_NUM;
String TXN_RTP_INTERAC_REF_NUM;
String TXN_RTP_REF_NUM;
String TXN_CONTACT_ID;
String TXN_STSUPD_CD;
String CUST_CNCL_ID;
String FI_CANC_REF_NO;
String PARAM_CNCL_REQ_DATE;
String PARAM_CNCL_REQ_STAT;
String PARAM_CNCL_RSN_CODE;
String PARAM_CNCL_RSN_DESC;
String IS_BATCH_DEBIT;
String T_ORG_DFI_QLFR;
String TICI_I2_CSID;
String INSTR_CODE_PROP;
String TXNS_BGN01;
public String getFILE_STATUS() {
	return FILE_STATUS;
}
public void setFILE_STATUS(String fILE_STATUS) {
	FILE_STATUS = fILE_STATUS;
}
public String getTXN_TYPE() {
	return TXN_TYPE;
}
public void setTXN_TYPE(String tXN_TYPE) {
	TXN_TYPE = tXN_TYPE;
}
public String getTXN_STATUS() {
	return TXN_STATUS;
}
public void setTXN_STATUS(String tXN_STATUS) {
	TXN_STATUS = tXN_STATUS;
}
public String getQUEUEID() {
	return QUEUEID;
}
public void setQUEUEID(String qUEUEID) {
	QUEUEID = qUEUEID;
}
public String getQUEUELABEL() {
	return QUEUELABEL;
}
public void setQUEUELABEL(String qUEUELABEL) {
	QUEUELABEL = qUEUELABEL;
}
public String getTASKID() {
	return TASKID;
}
public void setTASKID(String tASKID) {
	TASKID = tASKID;
}
public String getTASKLABEL() {
	return TASKLABEL;
}
public void setTASKLABEL(String tASKLABEL) {
	TASKLABEL = tASKLABEL;
}
public String getEXCEPTION_ID() {
	return EXCEPTION_ID;
}
public void setEXCEPTION_ID(String eXCEPTION_ID) {
	EXCEPTION_ID = eXCEPTION_ID;
}
public String getEXCEPTION_DESC() {
	return EXCEPTION_DESC;
}
public void setEXCEPTION_DESC(String eXCEPTION_DESC) {
	EXCEPTION_DESC = eXCEPTION_DESC;
}
public String getEXTERNAL_EXCEPTION_ID() {
	return EXTERNAL_EXCEPTION_ID;
}
public void setEXTERNAL_EXCEPTION_ID(String eXTERNAL_EXCEPTION_ID) {
	EXTERNAL_EXCEPTION_ID = eXTERNAL_EXCEPTION_ID;
}
public String getEXTERNAL_EXCEPTION_DESC() {
	return EXTERNAL_EXCEPTION_DESC;
}
public void setEXTERNAL_EXCEPTION_DESC(String eXTERNAL_EXCEPTION_DESC) {
	EXTERNAL_EXCEPTION_DESC = eXTERNAL_EXCEPTION_DESC;
}
public String getCUST_REF_NUM() {
	return CUST_REF_NUM;
}
public void setCUST_REF_NUM(String cUST_REF_NUM) {
	CUST_REF_NUM = cUST_REF_NUM;
}
public String getINIT_PRTY_BEI() {
	return INIT_PRTY_BEI;
}
public void setINIT_PRTY_BEI(String iNIT_PRTY_BEI) {
	INIT_PRTY_BEI = iNIT_PRTY_BEI;
}
public String getCLIENT_ID() {
	return CLIENT_ID;
}
public void setCLIENT_ID(String cLIENT_ID) {
	CLIENT_ID = cLIENT_ID;
}
public String getCLIENT_NAME() {
	return CLIENT_NAME;
}
public void setCLIENT_NAME(String cLIENT_NAME) {
	CLIENT_NAME = cLIENT_NAME;
}
public String getCUST_ID() {
	return CUST_ID;
}
public void setCUST_ID(String cUST_ID) {
	CUST_ID = cUST_ID;
}
public String getDR_NAME() {
	return DR_NAME;
}
public void setDR_NAME(String dR_NAME) {
	DR_NAME = dR_NAME;
}
public String getDR_STREET() {
	return DR_STREET;
}
public void setDR_STREET(String dR_STREET) {
	DR_STREET = dR_STREET;
}
public String getDR_BLDG_NO() {
	return DR_BLDG_NO;
}
public void setDR_BLDG_NO(String dR_BLDG_NO) {
	DR_BLDG_NO = dR_BLDG_NO;
}
public String getDR_CITY() {
	return DR_CITY;
}
public void setDR_CITY(String dR_CITY) {
	DR_CITY = dR_CITY;
}
public String getDR_STATE() {
	return DR_STATE;
}
public void setDR_STATE(String dR_STATE) {
	DR_STATE = dR_STATE;
}
public String getDR_PSTL_CODE() {
	return DR_PSTL_CODE;
}
public void setDR_PSTL_CODE(String dR_PSTL_CODE) {
	DR_PSTL_CODE = dR_PSTL_CODE;
}
public String getDR_AGT_NAME() {
	return DR_AGT_NAME;
}
public void setDR_AGT_NAME(String dR_AGT_NAME) {
	DR_AGT_NAME = dR_AGT_NAME;
}
public String getDR_AGT_CITY() {
	return DR_AGT_CITY;
}
public void setDR_AGT_CITY(String DR_AGT_CITY) {
	DR_AGT_CITY = DR_AGT_CITY;
}
public String getDR_AGT_STATE() {
	return DR_AGT_STATE;
}
public void setDR_AGT_STATE(String dR_AGT_STATE) {
	DR_AGT_STATE = dR_AGT_STATE;
}
public String getDR_AGT_CTRY() {
	return DR_AGT_CTRY;
}
public void setDR_AGT_CTRY(String dR_AGT_CTRY) {
	DR_AGT_CTRY = dR_AGT_CTRY;
}
public String getDR_AGT_PSTL_CD() {
	return DR_AGT_PSTL_CD;
}
public void setDR_AGT_PSTL_CD(String dR_AGT_PSTL_CD) {
	DR_AGT_PSTL_CD = dR_AGT_PSTL_CD;
}
public String getDR_CTRY() {
	return DR_CTRY;
}
public void setDR_CTRY(String dR_CTRY) {
	DR_CTRY = dR_CTRY;
}
public String getDR_ADDR1() {
	return DR_ADDR1;
}
public void setDR_ADDR1(String dR_ADDR1) {
	DR_ADDR1 = dR_ADDR1;
}
public String getDR_ADDR2() {
	return DR_ADDR2;
}
public void setDR_ADDR2(String dR_ADDR2) {
	DR_ADDR2 = dR_ADDR2;
}
public String getDR_ADDR3() {
	return DR_ADDR3;
}
public void setDR_ADDR3(String dR_ADDR3) {
	DR_ADDR3 = dR_ADDR3;
}
public String getDR_ACCT_NO() {
	return DR_ACCT_NO;
}
public void setDR_ACCT_NO(String dR_ACCT_NO) {
	DR_ACCT_NO = dR_ACCT_NO;
}
public String getSETTL_METHOD() {
	return SETTL_METHOD;
}
public void setSETTL_METHOD(String sETTL_METHOD) {
	SETTL_METHOD = sETTL_METHOD;
}
public String getDR_AGT_CLRING_SYS_CD() {
	return DR_AGT_CLRING_SYS_CD;
}
public void setDR_AGT_CLRING_SYS_CD(String dR_AGT_CLRING_SYS_CD) {
	DR_AGT_CLRING_SYS_CD = dR_AGT_CLRING_SYS_CD;
}
public String getACC_CCY() {
	return ACC_CCY;
}
public void setACC_CCY(String aCC_CCY) {
	ACC_CCY = aCC_CCY;
}
public String getTXN_CUST_SM() {
	return TXN_CUST_SM;
}
public void setTXN_CUST_SM(String tXN_CUST_SM) {
	TXN_CUST_SM = tXN_CUST_SM;
}
public String getDEBIT_AMOUNT() {
	return DEBIT_AMOUNT;
}
public void setDEBIT_AMOUNT(String dEBIT_AMOUNT) {
	DEBIT_AMOUNT = dEBIT_AMOUNT;
}
public String getINSTR_AMOUNT() {
	return INSTR_AMOUNT;
}
public void setINSTR_AMOUNT(String iNSTR_AMOUNT) {
	INSTR_AMOUNT = iNSTR_AMOUNT;
}
public String getLIMIT_CHK_EQUI_AMT() {
	return LIMIT_CHK_EQUI_AMT;
}
public void setLIMIT_CHK_EQUI_AMT(String lIMIT_CHK_EQUI_AMT) {
	LIMIT_CHK_EQUI_AMT = lIMIT_CHK_EQUI_AMT;
}
public String getDEBIT_CURRENCY() {
	return DEBIT_CURRENCY;
}
public void setDEBIT_CURRENCY(String dEBIT_CURRENCY) {
	DEBIT_CURRENCY = dEBIT_CURRENCY;
}
public String getINSTR_CURRENCY() {
	return INSTR_CURRENCY;
}
public void setINSTR_CURRENCY(String iNSTR_CURRENCY) {
	INSTR_CURRENCY = iNSTR_CURRENCY;
}
public String getLIMIT_CHK_EQUI_CCY() {
	return LIMIT_CHK_EQUI_CCY;
}
public void setLIMIT_CHK_EQUI_CCY(String lIMIT_CHK_EQUI_CCY) {
	LIMIT_CHK_EQUI_CCY = lIMIT_CHK_EQUI_CCY;
}
public String getBAL_CHK_EQUI_AMT() {
	return BAL_CHK_EQUI_AMT;
}
public void setBAL_CHK_EQUI_AMT(String bAL_CHK_EQUI_AMT) {
	BAL_CHK_EQUI_AMT = bAL_CHK_EQUI_AMT;
}
public String getLIMIT_BAL_CHK_FLAG() {
	return LIMIT_BAL_CHK_FLAG;
}
public void setLIMIT_BAL_CHK_FLAG(String lIMIT_BAL_CHK_FLAG) {
	LIMIT_BAL_CHK_FLAG = lIMIT_BAL_CHK_FLAG;
}
public String getINSTR_CODE1() {
	return INSTR_CODE1;
}
public void setINSTR_CODE1(String iNSTR_CODE1) {
	INSTR_CODE1 = iNSTR_CODE1;
}
public String getTXN_FX_ID() {
	return TXN_FX_ID;
}
public void setTXN_FX_ID(String tXN_FX_ID) {
	TXN_FX_ID = tXN_FX_ID;
}
public String getFX_RATE_TYPE() {
	return FX_RATE_TYPE;
}
public void setFX_RATE_TYPE(String fX_RATE_TYPE) {
	FX_RATE_TYPE = fX_RATE_TYPE;
}
public String getFX_CONTRACT_ID() {
	return FX_CONTRACT_ID;
}
public void setFX_CONTRACT_ID(String fX_CONTRACT_ID) {
	FX_CONTRACT_ID = fX_CONTRACT_ID;
}
public String getFX_RATE() {
	return FX_RATE;
}
public void setFX_RATE(String fX_RATE) {
	FX_RATE = fX_RATE;
}
public String getNOTIONAL_EXCH_RATE() {
	return NOTIONAL_EXCH_RATE;
}
public void setNOTIONAL_EXCH_RATE(String nOTIONAL_EXCH_RATE) {
	NOTIONAL_EXCH_RATE = nOTIONAL_EXCH_RATE;
}
public String getCR_NAME() {
	return CR_NAME;
}
public void setCR_NAME(String cR_NAME) {
	CR_NAME = cR_NAME;
}
public String getCR_CTRY() {
	return CR_CTRY;
}
public void setCR_CTRY(String cR_CTRY) {
	CR_CTRY = cR_CTRY;
}
public String getCR_ADDR1() {
	return CR_ADDR1;
}
public void setCR_ADDR1(String cR_ADDR1) {
	CR_ADDR1 = cR_ADDR1;
}
public String getCR_ADDR2() {
	return CR_ADDR2;
}
public void setCR_ADDR2(String cR_ADDR2) {
	CR_ADDR2 = cR_ADDR2;
}
public String getCR_ADDR3() {
	return CR_ADDR3;
}
public void setCR_ADDR3(String cR_ADDR3) {
	CR_ADDR3 = cR_ADDR3;
}
public String getCR_AGT_NAME() {
	return CR_AGT_NAME;
}
public void setCR_AGT_NAME(String cR_AGT_NAME) {
	CR_AGT_NAME = cR_AGT_NAME;
}
public String getCR_AGT_BIC() {
	return CR_AGT_BIC;
}
public void setCR_AGT_BIC(String cR_AGT_BIC) {
	CR_AGT_BIC = cR_AGT_BIC;
}
public String getCR_AGT_CLRING_SYS() {
	return CR_AGT_CLRING_SYS;
}
public void setCR_AGT_CLRING_SYS(String cR_AGT_CLRING_SYS) {
	CR_AGT_CLRING_SYS = cR_AGT_CLRING_SYS;
}
public String getCR_AGT_CLRING_SYS_CD() {
	return CR_AGT_CLRING_SYS_CD;
}
public void setCR_AGT_CLRING_SYS_CD(String cR_AGT_CLRING_SYS_CD) {
	CR_AGT_CLRING_SYS_CD = cR_AGT_CLRING_SYS_CD;
}
public String getCR_ACC_NO() {
	return CR_ACC_NO;
}
public void setCR_ACC_NO(String cR_ACC_NO) {
	CR_ACC_NO = cR_ACC_NO;
}
public String getCR_IBAN() {
	return CR_IBAN;
}
public void setCR_IBAN(String cR_IBAN) {
	CR_IBAN = cR_IBAN;
}
public String getCR_AGT_CTRY() {
	return CR_AGT_CTRY;
}
public void setCR_AGT_CTRY(String cR_AGT_CTRY) {
	CR_AGT_CTRY = cR_AGT_CTRY;
}
public String getCR_AGT_ADDR1() {
	return CR_AGT_ADDR1;
}
public void setCR_AGT_ADDR1(String cR_AGT_ADDR1) {
	CR_AGT_ADDR1 = cR_AGT_ADDR1;
}
public String getCR_AGT_ADDR2() {
	return CR_AGT_ADDR2;
}
public void setCR_AGT_ADDR2(String cR_AGT_ADDR2) {
	CR_AGT_ADDR2 = cR_AGT_ADDR2;
}
public String getCR_AGT_ADDR3() {
	return CR_AGT_ADDR3;
}
public void setCR_AGT_ADDR3(String cR_AGT_ADDR3) {
	CR_AGT_ADDR3 = cR_AGT_ADDR3;
}
public String getCHARGE_TYPE() {
	return CHARGE_TYPE;
}
public void setCHARGE_TYPE(String cHARGE_TYPE) {
	CHARGE_TYPE = cHARGE_TYPE;
}
public String getCORR_FEE_AMT() {
	return CORR_FEE_AMT;
}
public void setCORR_FEE_AMT(String cORR_FEE_AMT) {
	CORR_FEE_AMT = cORR_FEE_AMT;
}
public String getFEE_CCY() {
	return FEE_CCY;
}
public void setFEE_CCY(String fEE_CCY) {
	FEE_CCY = fEE_CCY;
}
public String getCORR_FEE_ACC_NO() {
	return CORR_FEE_ACC_NO;
}
public void setCORR_FEE_ACC_NO(String cORR_FEE_ACC_NO) {
	CORR_FEE_ACC_NO = cORR_FEE_ACC_NO;
}
public String getCORR_TRANSIT() {
	return CORR_TRANSIT;
}
public void setCORR_TRANSIT(String cORR_TRANSIT) {
	CORR_TRANSIT = cORR_TRANSIT;
}
public String getREMIT_INFO_1() {
	return REMIT_INFO_1;
}
public void setREMIT_INFO_1(String rEMIT_INFO_1) {
	REMIT_INFO_1 = rEMIT_INFO_1;
}
public String getREMIT_INFO_2() {
	return REMIT_INFO_2;
}
public void setREMIT_INFO_2(String rEMIT_INFO_2) {
	REMIT_INFO_2 = rEMIT_INFO_2;
}
public String getREMIT_INFO_3() {
	return REMIT_INFO_3;
}
public void setREMIT_INFO_3(String rEMIT_INFO_3) {
	REMIT_INFO_3 = rEMIT_INFO_3;
}
public String getREMIT_INFO_4() {
	return REMIT_INFO_4;
}
public void setREMIT_INFO_4(String rEMIT_INFO_4) {
	REMIT_INFO_4 = rEMIT_INFO_4;
}
public String getREMT_STATUS_DESC() {
	return REMT_STATUS_DESC;
}
public void setREMT_STATUS_DESC(String rEMT_STATUS_DESC) {
	REMT_STATUS_DESC = rEMT_STATUS_DESC;
}
public String getREMT_TYPE() {
	return REMT_TYPE;
}
public void setREMT_TYPE(String rEMT_TYPE) {
	REMT_TYPE = rEMT_TYPE;
}
public String getSAR_LOC_MTD() {
	return SAR_LOC_MTD;
}
public void setSAR_LOC_MTD(String sAR_LOC_MTD) {
	SAR_LOC_MTD = sAR_LOC_MTD;
}
public String getREMT_CR_LCFX() {
	return REMT_CR_LCFX;
}
public void setREMT_CR_LCFX(String rEMT_CR_LCFX) {
	REMT_CR_LCFX = rEMT_CR_LCFX;
}
public String getREMT_CR_LCEM() {
	return REMT_CR_LCEM;
}
public void setREMT_CR_LCEM(String rEMT_CR_LCEM) {
	REMT_CR_LCEM = rEMT_CR_LCEM;
}
public String getREMT_CR_NM_TXN() {
	return REMT_CR_NM_TXN;
}
public void setREMT_CR_NM_TXN(String rEMT_CR_NM_TXN) {
	REMT_CR_NM_TXN = rEMT_CR_NM_TXN;
}
public String getREMT_CR_STRT_TXN() {
	return REMT_CR_STRT_TXN;
}
public void setREMT_CR_STRT_TXN(String rEMT_CR_STRT_TXN) {
	REMT_CR_STRT_TXN = rEMT_CR_STRT_TXN;
}
public String getREMT_CR_BLDG_TXN() {
	return REMT_CR_BLDG_TXN;
}
public void setREMT_CR_BLDG_TXN(String rEMT_CR_BLDG_TXN) {
	REMT_CR_BLDG_TXN = rEMT_CR_BLDG_TXN;
}
public String getREMT_CR_TW_TXN() {
	return REMT_CR_TW_TXN;
}
public void setREMT_CR_TW_TXN(String rEMT_CR_TW_TXN) {
	REMT_CR_TW_TXN = rEMT_CR_TW_TXN;
}
public String getREMT_CR_SD_TXN() {
	return REMT_CR_SD_TXN;
}
public void setREMT_CR_SD_TXN(String rEMT_CR_SD_TXN) {
	REMT_CR_SD_TXN = rEMT_CR_SD_TXN;
}
public String getREMT_CR_CTRY_TXN() {
	return REMT_CR_CTRY_TXN;
}
public void setREMT_CR_CTRY_TXN(String rEMT_CR_CTRY_TXN) {
	REMT_CR_CTRY_TXN = rEMT_CR_CTRY_TXN;
}
public String getREMT_CR_PSTL_TXN() {
	return REMT_CR_PSTL_TXN;
}
public void setREMT_CR_PSTL_TXN(String rEMT_CR_PSTL_TXN) {
	REMT_CR_PSTL_TXN = rEMT_CR_PSTL_TXN;
}
public String getINTER_AGT_NAME() {
	return INTER_AGT_NAME;
}
public void setINTER_AGT_NAME(String iNTER_AGT_NAME) {
	INTER_AGT_NAME = iNTER_AGT_NAME;
}
public String getINTER_AGT_BIC() {
	return INTER_AGT_BIC;
}
public void setINTER_AGT_BIC(String iNTER_AGT_BIC) {
	INTER_AGT_BIC = iNTER_AGT_BIC;
}
public String getINTER_AGT_CLRING_SYS() {
	return INTER_AGT_CLRING_SYS;
}
public void setINTER_AGT_CLRING_SYS(String iNTER_AGT_CLRING_SYS) {
	INTER_AGT_CLRING_SYS = iNTER_AGT_CLRING_SYS;
}
public String getINTER_AGT_CLRING_SYS_CD() {
	return INTER_AGT_CLRING_SYS_CD;
}
public void setINTER_AGT_CLRING_SYS_CD(String iNTER_AGT_CLRING_SYS_CD) {
	INTER_AGT_CLRING_SYS_CD = iNTER_AGT_CLRING_SYS_CD;
}
public String getRBPMT_STATUS_CD() {
	return RBPMT_STATUS_CD;
}
public void setRBPMT_STATUS_CD(String rBPMT_STATUS_CD) {
	RBPMT_STATUS_CD = rBPMT_STATUS_CD;
}
public String getRBPMT_STATUS_DESC() {
	return RBPMT_STATUS_DESC;
}
public void setRBPMT_STATUS_DESC(String rBPMT_STATUS_DESC) {
	RBPMT_STATUS_DESC = rBPMT_STATUS_DESC;
}
public String getRBFEE_STATUS_CD() {
	return RBFEE_STATUS_CD;
}
public void setRBFEE_STATUS_CD(String rBFEE_STATUS_CD) {
	RBFEE_STATUS_CD = rBFEE_STATUS_CD;
}
public String getRBFEE_STATUS_DESC() {
	return RBFEE_STATUS_DESC;
}
public void setRBFEE_STATUS_DESC(String rBFEE_STATUS_DESC) {
	RBFEE_STATUS_DESC = rBFEE_STATUS_DESC;
}
public String getRBPMT_STATUS_RSN_CD() {
	return RBPMT_STATUS_RSN_CD;
}
public void setRBPMT_STATUS_RSN_CD(String rBPMT_STATUS_RSN_CD) {
	RBPMT_STATUS_RSN_CD = rBPMT_STATUS_RSN_CD;
}
public String getRBPMT_STATUS_RSN_DESC() {
	return RBPMT_STATUS_RSN_DESC;
}
public void setRBPMT_STATUS_RSN_DESC(String rBPMT_STATUS_RSN_DESC) {
	RBPMT_STATUS_RSN_DESC = rBPMT_STATUS_RSN_DESC;
}
public String getRBFEE_STATUS_RSN_CD() {
	return RBFEE_STATUS_RSN_CD;
}
public void setRBFEE_STATUS_RSN_CD(String rBFEE_STATUS_RSN_CD) {
	RBFEE_STATUS_RSN_CD = rBFEE_STATUS_RSN_CD;
}
public String getRBFEE_STATUS_RSN_DESC() {
	return RBFEE_STATUS_RSN_DESC;
}
public void setRBFEE_STATUS_RSN_DESC(String rBFEE_STATUS_RSN_DESC) {
	RBFEE_STATUS_RSN_DESC = rBFEE_STATUS_RSN_DESC;
}
public String getUMMREV_STAT_CD() {
	return UMMREV_STAT_CD;
}
public void setUMMREV_STAT_CD(String uMMREV_STAT_CD) {
	UMMREV_STAT_CD = uMMREV_STAT_CD;
}
public String getUMMREV_STAT_DESC() {
	return UMMREV_STAT_DESC;
}
public void setUMMREV_STAT_DESC(String uMMREV_STAT_DESC) {
	UMMREV_STAT_DESC = uMMREV_STAT_DESC;
}
public String getFEEREV_STAT_CD() {
	return FEEREV_STAT_CD;
}
public void setFEEREV_STAT_CD(String fEEREV_STAT_CD) {
	FEEREV_STAT_CD = fEEREV_STAT_CD;
}
public String getFEEREV_STAT_DESC() {
	return FEEREV_STAT_DESC;
}
public void setFEEREV_STAT_DESC(String fEEREV_STAT_DESC) {
	FEEREV_STAT_DESC = fEEREV_STAT_DESC;
}
public String getUMMREV_RSN_CD() {
	return UMMREV_RSN_CD;
}
public void setUMMREV_RSN_CD(String uMMREV_RSN_CD) {
	UMMREV_RSN_CD = uMMREV_RSN_CD;
}
public String getUMMREV_RSN_DESC() {
	return UMMREV_RSN_DESC;
}
public void setUMMREV_RSN_DESC(String uMMREV_RSN_DESC) {
	UMMREV_RSN_DESC = uMMREV_RSN_DESC;
}
public String getFEEREV_RSN_CD() {
	return FEEREV_RSN_CD;
}
public void setFEEREV_RSN_CD(String fEEREV_RSN_CD) {
	FEEREV_RSN_CD = fEEREV_RSN_CD;
}
public String getFEEREV_RSN_DESC() {
	return FEEREV_RSN_DESC;
}
public void setFEEREV_RSN_DESC(String fEEREV_RSN_DESC) {
	FEEREV_RSN_DESC = fEEREV_RSN_DESC;
}
public String getTXN_CLGSYSREF() {
	return TXN_CLGSYSREF;
}
public void setTXN_CLGSYSREF(String tXN_CLGSYSREF) {
	TXN_CLGSYSREF = tXN_CLGSYSREF;
}
public String getIWS_STATUS_CD() {
	return IWS_STATUS_CD;
}
public void setIWS_STATUS_CD(String iWS_STATUS_CD) {
	IWS_STATUS_CD = iWS_STATUS_CD;
}
public String getIWS_STATUS_DESC() {
	return IWS_STATUS_DESC;
}
public void setIWS_STATUS_DESC(String iWS_STATUS_DESC) {
	IWS_STATUS_DESC = iWS_STATUS_DESC;
}
public String getCLR_SYSM_CODE() {
	return CLR_SYSM_CODE;
}
public void setCLR_SYSM_CODE(String cLR_SYSM_CODE) {
	CLR_SYSM_CODE = cLR_SYSM_CODE;
}
public String getTXN_SWIFT_UETR() {
	return TXN_SWIFT_UETR;
}
public void setTXN_SWIFT_UETR(String tXN_SWIFT_UETR) {
	TXN_SWIFT_UETR = tXN_SWIFT_UETR;
}
public String getTXN_GOW_NOTF_FLG() {
	return TXN_GOW_NOTF_FLG;
}
public void setTXN_GOW_NOTF_FLG(String tXN_GOW_NOTF_FLG) {
	TXN_GOW_NOTF_FLG = tXN_GOW_NOTF_FLG;
}
public String getTXN_GOW_ACCT_STAT() {
	return TXN_GOW_ACCT_STAT;
}
public void setTXN_GOW_ACCT_STAT(String tXN_GOW_ACCT_STAT) {
	TXN_GOW_ACCT_STAT = tXN_GOW_ACCT_STAT;
}
public String getNOC_FLAG() {
	return NOC_FLAG;
}
public void setNOC_FLAG(String nOC_FLAG) {
	NOC_FLAG = nOC_FLAG;
}
public String getLCL_REC_TYPEID() {
	return LCL_REC_TYPEID;
}
public void setLCL_REC_TYPEID(String lCL_REC_TYPEID) {
	LCL_REC_TYPEID = lCL_REC_TYPEID;
}
public String getINIT_PRTY_ID() {
	return INIT_PRTY_ID;
}
public void setINIT_PRTY_ID(String iNIT_PRTY_ID) {
	INIT_PRTY_ID = iNIT_PRTY_ID;
}
public String getACNO_RETURN() {
	return ACNO_RETURN;
}
public void setACNO_RETURN(String aCNO_RETURN) {
	ACNO_RETURN = aCNO_RETURN;
}
public String getTXNS_OTI08() {
	return TXNS_OTI08;
}
public void setTXNS_OTI08(String tXNS_OTI08) {
	TXNS_OTI08 = tXNS_OTI08;
}
public String getTFRI_PUR_FT() {
	return TFRI_PUR_FT;
}
public void setTFRI_PUR_FT(String tFRI_PUR_FT) {
	TFRI_PUR_FT = tFRI_PUR_FT;
}
public String getTIBI_CA_ID() {
	return TIBI_CA_ID;
}
public void setTIBI_CA_ID(String tIBI_CA_ID) {
	TIBI_CA_ID = tIBI_CA_ID;
}
public String getNOC_CHG_CODE() {
	return NOC_CHG_CODE;
}
public void setNOC_CHG_CODE(String nOC_CHG_CODE) {
	NOC_CHG_CODE = nOC_CHG_CODE;
}
public String getTPOI_IP_NM2() {
	return TPOI_IP_NM2;
}
public void setTPOI_IP_NM2(String tPOI_IP_NM2) {
	TPOI_IP_NM2 = tPOI_IP_NM2;
}
public String getNOC_CORR_INFO() {
	return NOC_CORR_INFO;
}
public void setNOC_CORR_INFO(String nOC_CORR_INFO) {
	NOC_CORR_INFO = nOC_CORR_INFO;
}
public String getB_SRVCLVL_CD() {
	return B_SRVCLVL_CD;
}
public void setB_SRVCLVL_CD(String b_SRVCLVL_CD) {
	B_SRVCLVL_CD = b_SRVCLVL_CD;
}
public String getTFBI_CR_ACTYFT() {
	return TFBI_CR_ACTYFT;
}
public void setTFBI_CR_ACTYFT(String tFBI_CR_ACTYFT) {
	TFBI_CR_ACTYFT = tFBI_CR_ACTYFT;
}
public String getCR_CITY() {
	return CR_CITY;
}
public void setCR_CITY(String cR_CITY) {
	CR_CITY = cR_CITY;
}
public String getCR_STATE() {
	return CR_STATE;
}
public void setCR_STATE(String cR_STATE) {
	CR_STATE = cR_STATE;
}
public String getCR_PSTL_CD() {
	return CR_PSTL_CD;
}
public void setCR_PSTL_CD(String cR_PSTL_CD) {
	CR_PSTL_CD = cR_PSTL_CD;
}
public String getADDITONAL_INFO() {
	return ADDITONAL_INFO;
}
public void setADDITONAL_INFO(String aDDITONAL_INFO) {
	ADDITONAL_INFO = aDDITONAL_INFO;
}
public String getTRN_STATUS_CODE() {
	return TRN_STATUS_CODE;
}
public void setTRN_STATUS_CODE(String tRN_STATUS_CODE) {
	TRN_STATUS_CODE = tRN_STATUS_CODE;
}
public String getUMM_STAT_CD() {
	return UMM_STAT_CD;
}
public void setUMM_STAT_CD(String uMM_STAT_CD) {
	UMM_STAT_CD = uMM_STAT_CD;
}
public String getUMM_STAT_CD_DESC() {
	return UMM_STAT_CD_DESC;
}
public void setUMM_STAT_CD_DESC(String uMM_STAT_CD_DESC) {
	UMM_STAT_CD_DESC = uMM_STAT_CD_DESC;
}
public String getUMM_PSH_STAT_CD() {
	return UMM_PSH_STAT_CD;
}
public void setUMM_PSH_STAT_CD(String uMM_PSH_STAT_CD) {
	UMM_PSH_STAT_CD = uMM_PSH_STAT_CD;
}
public String getUMM_PSH_STAT_CD_DESC() {
	return UMM_PSH_STAT_CD_DESC;
}
public void setUMM_PSH_STAT_CD_DESC(String uMM_PSH_STAT_CD_DESC) {
	UMM_PSH_STAT_CD_DESC = uMM_PSH_STAT_CD_DESC;
}
public String getCHQ_DLVY_MTD() {
	return CHQ_DLVY_MTD;
}
public void setCHQ_DLVY_MTD(String cHQ_DLVY_MTD) {
	CHQ_DLVY_MTD = cHQ_DLVY_MTD;
}
public String getCR_STREET() {
	return CR_STREET;
}
public void setCR_STREET(String cR_STREET) {
	CR_STREET = cR_STREET;
}
public String getCR_BLDG_NO1() {
	return CR_BLDG_NO1;
}
public void setCR_BLDG_NO1(String cR_BLDG_NO1) {
	CR_BLDG_NO1 = cR_BLDG_NO1;
}
public String getCHQ_FRM_NM() {
	return CHQ_FRM_NM;
}
public void setCHQ_FRM_NM(String cHQ_FRM_NM) {
	CHQ_FRM_NM = cHQ_FRM_NM;
}
public String getCHQ_FRM_STRT() {
	return CHQ_FRM_STRT;
}
public void setCHQ_FRM_STRT(String cHQ_FRM_STRT) {
	CHQ_FRM_STRT = cHQ_FRM_STRT;
}
public String getCHQ_FRM_BLDG() {
	return CHQ_FRM_BLDG;
}
public void setCHQ_FRM_BLDG(String cHQ_FRM_BLDG) {
	CHQ_FRM_BLDG = cHQ_FRM_BLDG;
}
public String getCHQ_FRM_TW() {
	return CHQ_FRM_TW;
}
public void setCHQ_FRM_TW(String cHQ_FRM_TW) {
	CHQ_FRM_TW = cHQ_FRM_TW;
}
public String getCHQ_FRM_SD() {
	return CHQ_FRM_SD;
}
public void setCHQ_FRM_SD(String cHQ_FRM_SD) {
	CHQ_FRM_SD = cHQ_FRM_SD;
}
public String getCHQ_FRM_CTRY() {
	return CHQ_FRM_CTRY;
}
public void setCHQ_FRM_CTRY(String cHQ_FRM_CTRY) {
	CHQ_FRM_CTRY = cHQ_FRM_CTRY;
}
public String getCHQ_FRM_PSTL() {
	return CHQ_FRM_PSTL;
}
public void setCHQ_FRM_PSTL(String cHQ_FRM_PSTL) {
	CHQ_FRM_PSTL = cHQ_FRM_PSTL;
}
public String getCHQ_TO_NM() {
	return CHQ_TO_NM;
}
public void setCHQ_TO_NM(String cHQ_TO_NM) {
	CHQ_TO_NM = cHQ_TO_NM;
}
public String getCHQ_TO_STRT() {
	return CHQ_TO_STRT;
}
public void setCHQ_TO_STRT(String cHQ_TO_STRT) {
	CHQ_TO_STRT = cHQ_TO_STRT;
}
public String getCHQ_TO_BLDG() {
	return CHQ_TO_BLDG;
}
public void setCHQ_TO_BLDG(String cHQ_TO_BLDG) {
	CHQ_TO_BLDG = cHQ_TO_BLDG;
}
public String getCHQ_TO_TW() {
	return CHQ_TO_TW;
}
public void setCHQ_TO_TW(String cHQ_TO_TW) {
	CHQ_TO_TW = cHQ_TO_TW;
}
public String getCHQ_TO_SD() {
	return CHQ_TO_SD;
}
public void setCHQ_TO_SD(String cHQ_TO_SD) {
	CHQ_TO_SD = cHQ_TO_SD;
}
public String getCHQ_TO_CTRY() {
	return CHQ_TO_CTRY;
}
public void setCHQ_TO_CTRY(String cHQ_TO_CTRY) {
	CHQ_TO_CTRY = cHQ_TO_CTRY;
}
public String getCHQ_TO_PSTL() {
	return CHQ_TO_PSTL;
}
public void setCHQ_TO_PSTL(String cHQ_TO_PSTL) {
	CHQ_TO_PSTL = cHQ_TO_PSTL;
}
public String getCR_LANG_PREF() {
	return CR_LANG_PREF;
}
public void setCR_LANG_PREF(String cR_LANG_PREF) {
	CR_LANG_PREF = cR_LANG_PREF;
}
public String getCR_EMAIL() {
	return CR_EMAIL;
}
public void setCR_EMAIL(String cR_EMAIL) {
	CR_EMAIL = cR_EMAIL;
}
public String getCR_MOB_NB() {
	return CR_MOB_NB;
}
public void setCR_MOB_NB(String cR_MOB_NB) {
	CR_MOB_NB = cR_MOB_NB;
}
public String getRESP_CODE() {
	return RESP_CODE;
}
public void setRESP_CODE(String rESP_CODE) {
	RESP_CODE = rESP_CODE;
}
public String getRESP_DESC() {
	return RESP_DESC;
}
public void setRESP_DESC(String rESP_DESC) {
	RESP_DESC = rESP_DESC;
}
public String getRESP_REF_NUM() {
	return RESP_REF_NUM;
}
public void setRESP_REF_NUM(String rESP_REF_NUM) {
	RESP_REF_NUM = rESP_REF_NUM;
}
public String getTXN_RTP_INTERAC_REF_NUM() {
	return TXN_RTP_INTERAC_REF_NUM;
}
public void setTXN_RTP_INTERAC_REF_NUM(String tXN_RTP_INTERAC_REF_NUM) {
	TXN_RTP_INTERAC_REF_NUM = tXN_RTP_INTERAC_REF_NUM;
}
public String getTXN_RTP_REF_NUM() {
	return TXN_RTP_REF_NUM;
}
public void setTXN_RTP_REF_NUM(String tXN_RTP_REF_NUM) {
	TXN_RTP_REF_NUM = tXN_RTP_REF_NUM;
}
public String getTXN_CONTACT_ID() {
	return TXN_CONTACT_ID;
}
public void setTXN_CONTACT_ID(String tXN_CONTACT_ID) {
	TXN_CONTACT_ID = tXN_CONTACT_ID;
}
public String getTXN_STSUPD_CD() {
	return TXN_STSUPD_CD;
}
public void setTXN_STSUPD_CD(String tXN_STSUPD_CD) {
	TXN_STSUPD_CD = tXN_STSUPD_CD;
}
public String getCUST_CNCL_ID() {
	return CUST_CNCL_ID;
}
public void setCUST_CNCL_ID(String cUST_CNCL_ID) {
	CUST_CNCL_ID = cUST_CNCL_ID;
}
public String getFI_CANC_REF_NO() {
	return FI_CANC_REF_NO;
}
public void setFI_CANC_REF_NO(String fI_CANC_REF_NO) {
	FI_CANC_REF_NO = fI_CANC_REF_NO;
}
public String getPARAM_CNCL_REQ_DATE() {
	return PARAM_CNCL_REQ_DATE;
}
public void setPARAM_CNCL_REQ_DATE(String pARAM_CNCL_REQ_DATE) {
	PARAM_CNCL_REQ_DATE = pARAM_CNCL_REQ_DATE;
}
public String getPARAM_CNCL_REQ_STAT() {
	return PARAM_CNCL_REQ_STAT;
}
public void setPARAM_CNCL_REQ_STAT(String pARAM_CNCL_REQ_STAT) {
	PARAM_CNCL_REQ_STAT = pARAM_CNCL_REQ_STAT;
}
public String getPARAM_CNCL_RSN_CODE() {
	return PARAM_CNCL_RSN_CODE;
}
public void setPARAM_CNCL_RSN_CODE(String pARAM_CNCL_RSN_CODE) {
	PARAM_CNCL_RSN_CODE = pARAM_CNCL_RSN_CODE;
}
public String getPARAM_CNCL_RSN_DESC() {
	return PARAM_CNCL_RSN_DESC;
}
public void setPARAM_CNCL_RSN_DESC(String pARAM_CNCL_RSN_DESC) {
	PARAM_CNCL_RSN_DESC = pARAM_CNCL_RSN_DESC;
}
public String getIS_BATCH_DEBIT() {
	return IS_BATCH_DEBIT;
}
public void setIS_BATCH_DEBIT(String iS_BATCH_DEBIT) {
	IS_BATCH_DEBIT = iS_BATCH_DEBIT;
}
public String getT_ORG_DFI_QLFR() {
	return T_ORG_DFI_QLFR;
}
public void setT_ORG_DFI_QLFR(String t_ORG_DFI_QLFR) {
	T_ORG_DFI_QLFR = t_ORG_DFI_QLFR;
}
public String getTICI_I2_CSID() {
	return TICI_I2_CSID;
}
public void setTICI_I2_CSID(String tICI_I2_CSID) {
	TICI_I2_CSID = tICI_I2_CSID;
}
public String getINSTR_CODE_PROP() {
	return INSTR_CODE_PROP;
}
public void setINSTR_CODE_PROP(String iNSTR_CODE_PROP) {
	INSTR_CODE_PROP = iNSTR_CODE_PROP;
}
public String getTXNS_BGN01() {
	return TXNS_BGN01;
}
public void setTXNS_BGN01(String tXNS_BGN01) {
	TXNS_BGN01 = tXNS_BGN01;
}
@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((ACC_CCY == null) ? 0 : ACC_CCY.hashCode());
	result = prime * result + ((ACNO_RETURN == null) ? 0 : ACNO_RETURN.hashCode());
	result = prime * result + ((ADDITONAL_INFO == null) ? 0 : ADDITONAL_INFO.hashCode());
	result = prime * result + ((BAL_CHK_EQUI_AMT == null) ? 0 : BAL_CHK_EQUI_AMT.hashCode());
	result = prime * result + ((B_SRVCLVL_CD == null) ? 0 : B_SRVCLVL_CD.hashCode());
	result = prime * result + ((CHARGE_TYPE == null) ? 0 : CHARGE_TYPE.hashCode());
	result = prime * result + ((CHQ_DLVY_MTD == null) ? 0 : CHQ_DLVY_MTD.hashCode());
	result = prime * result + ((CHQ_FRM_BLDG == null) ? 0 : CHQ_FRM_BLDG.hashCode());
	result = prime * result + ((CHQ_FRM_CTRY == null) ? 0 : CHQ_FRM_CTRY.hashCode());
	result = prime * result + ((CHQ_FRM_NM == null) ? 0 : CHQ_FRM_NM.hashCode());
	result = prime * result + ((CHQ_FRM_PSTL == null) ? 0 : CHQ_FRM_PSTL.hashCode());
	result = prime * result + ((CHQ_FRM_SD == null) ? 0 : CHQ_FRM_SD.hashCode());
	result = prime * result + ((CHQ_FRM_STRT == null) ? 0 : CHQ_FRM_STRT.hashCode());
	result = prime * result + ((CHQ_FRM_TW == null) ? 0 : CHQ_FRM_TW.hashCode());
	result = prime * result + ((CHQ_TO_BLDG == null) ? 0 : CHQ_TO_BLDG.hashCode());
	result = prime * result + ((CHQ_TO_CTRY == null) ? 0 : CHQ_TO_CTRY.hashCode());
	result = prime * result + ((CHQ_TO_NM == null) ? 0 : CHQ_TO_NM.hashCode());
	result = prime * result + ((CHQ_TO_PSTL == null) ? 0 : CHQ_TO_PSTL.hashCode());
	result = prime * result + ((CHQ_TO_SD == null) ? 0 : CHQ_TO_SD.hashCode());
	result = prime * result + ((CHQ_TO_STRT == null) ? 0 : CHQ_TO_STRT.hashCode());
	result = prime * result + ((CHQ_TO_TW == null) ? 0 : CHQ_TO_TW.hashCode());
	result = prime * result + ((CLIENT_ID == null) ? 0 : CLIENT_ID.hashCode());
	result = prime * result + ((CLIENT_NAME == null) ? 0 : CLIENT_NAME.hashCode());
	result = prime * result + ((CLR_SYSM_CODE == null) ? 0 : CLR_SYSM_CODE.hashCode());
	result = prime * result + ((CORR_FEE_ACC_NO == null) ? 0 : CORR_FEE_ACC_NO.hashCode());
	result = prime * result + ((CORR_FEE_AMT == null) ? 0 : CORR_FEE_AMT.hashCode());
	result = prime * result + ((CORR_TRANSIT == null) ? 0 : CORR_TRANSIT.hashCode());
	result = prime * result + ((CR_ACC_NO == null) ? 0 : CR_ACC_NO.hashCode());
	result = prime * result + ((CR_ADDR1 == null) ? 0 : CR_ADDR1.hashCode());
	result = prime * result + ((CR_ADDR2 == null) ? 0 : CR_ADDR2.hashCode());
	result = prime * result + ((CR_ADDR3 == null) ? 0 : CR_ADDR3.hashCode());
	result = prime * result + ((CR_AGT_ADDR1 == null) ? 0 : CR_AGT_ADDR1.hashCode());
	result = prime * result + ((CR_AGT_ADDR2 == null) ? 0 : CR_AGT_ADDR2.hashCode());
	result = prime * result + ((CR_AGT_ADDR3 == null) ? 0 : CR_AGT_ADDR3.hashCode());
	result = prime * result + ((CR_AGT_BIC == null) ? 0 : CR_AGT_BIC.hashCode());
	result = prime * result + ((CR_AGT_CLRING_SYS == null) ? 0 : CR_AGT_CLRING_SYS.hashCode());
	result = prime * result + ((CR_AGT_CLRING_SYS_CD == null) ? 0 : CR_AGT_CLRING_SYS_CD.hashCode());
	result = prime * result + ((CR_AGT_CTRY == null) ? 0 : CR_AGT_CTRY.hashCode());
	result = prime * result + ((CR_AGT_NAME == null) ? 0 : CR_AGT_NAME.hashCode());
	result = prime * result + ((CR_BLDG_NO1 == null) ? 0 : CR_BLDG_NO1.hashCode());
	result = prime * result + ((CR_CITY == null) ? 0 : CR_CITY.hashCode());
	result = prime * result + ((CR_CTRY == null) ? 0 : CR_CTRY.hashCode());
	result = prime * result + ((CR_EMAIL == null) ? 0 : CR_EMAIL.hashCode());
	result = prime * result + ((CR_IBAN == null) ? 0 : CR_IBAN.hashCode());
	result = prime * result + ((CR_LANG_PREF == null) ? 0 : CR_LANG_PREF.hashCode());
	result = prime * result + ((CR_MOB_NB == null) ? 0 : CR_MOB_NB.hashCode());
	result = prime * result + ((CR_NAME == null) ? 0 : CR_NAME.hashCode());
	result = prime * result + ((CR_PSTL_CD == null) ? 0 : CR_PSTL_CD.hashCode());
	result = prime * result + ((CR_STATE == null) ? 0 : CR_STATE.hashCode());
	result = prime * result + ((CR_STREET == null) ? 0 : CR_STREET.hashCode());
	result = prime * result + ((CUST_CNCL_ID == null) ? 0 : CUST_CNCL_ID.hashCode());
	result = prime * result + ((CUST_ID == null) ? 0 : CUST_ID.hashCode());
	result = prime * result + ((CUST_REF_NUM == null) ? 0 : CUST_REF_NUM.hashCode());
	result = prime * result + ((DEBIT_AMOUNT == null) ? 0 : DEBIT_AMOUNT.hashCode());
	result = prime * result + ((DEBIT_CURRENCY == null) ? 0 : DEBIT_CURRENCY.hashCode());
	result = prime * result + ((DR_ACCT_NO == null) ? 0 : DR_ACCT_NO.hashCode());
	result = prime * result + ((DR_ADDR1 == null) ? 0 : DR_ADDR1.hashCode());
	result = prime * result + ((DR_ADDR2 == null) ? 0 : DR_ADDR2.hashCode());
	result = prime * result + ((DR_ADDR3 == null) ? 0 : DR_ADDR3.hashCode());
	result = prime * result + ((DR_AGT_CITY == null) ? 0 : DR_AGT_CITY.hashCode());
	result = prime * result + ((DR_AGT_CLRING_SYS_CD == null) ? 0 : DR_AGT_CLRING_SYS_CD.hashCode());
	result = prime * result + ((DR_AGT_CTRY == null) ? 0 : DR_AGT_CTRY.hashCode());
	result = prime * result + ((DR_AGT_NAME == null) ? 0 : DR_AGT_NAME.hashCode());
	result = prime * result + ((DR_AGT_PSTL_CD == null) ? 0 : DR_AGT_PSTL_CD.hashCode());
	result = prime * result + ((DR_AGT_STATE == null) ? 0 : DR_AGT_STATE.hashCode());
	result = prime * result + ((DR_BLDG_NO == null) ? 0 : DR_BLDG_NO.hashCode());
	result = prime * result + ((DR_CITY == null) ? 0 : DR_CITY.hashCode());
	result = prime * result + ((DR_CTRY == null) ? 0 : DR_CTRY.hashCode());
	result = prime * result + ((DR_NAME == null) ? 0 : DR_NAME.hashCode());
	result = prime * result + ((DR_PSTL_CODE == null) ? 0 : DR_PSTL_CODE.hashCode());
	result = prime * result + ((DR_STATE == null) ? 0 : DR_STATE.hashCode());
	result = prime * result + ((DR_STREET == null) ? 0 : DR_STREET.hashCode());
	result = prime * result + ((EXCEPTION_DESC == null) ? 0 : EXCEPTION_DESC.hashCode());
	result = prime * result + ((EXCEPTION_ID == null) ? 0 : EXCEPTION_ID.hashCode());
	result = prime * result + ((EXTERNAL_EXCEPTION_DESC == null) ? 0 : EXTERNAL_EXCEPTION_DESC.hashCode());
	result = prime * result + ((EXTERNAL_EXCEPTION_ID == null) ? 0 : EXTERNAL_EXCEPTION_ID.hashCode());
	result = prime * result + ((FEEREV_RSN_CD == null) ? 0 : FEEREV_RSN_CD.hashCode());
	result = prime * result + ((FEEREV_RSN_DESC == null) ? 0 : FEEREV_RSN_DESC.hashCode());
	result = prime * result + ((FEEREV_STAT_CD == null) ? 0 : FEEREV_STAT_CD.hashCode());
	result = prime * result + ((FEEREV_STAT_DESC == null) ? 0 : FEEREV_STAT_DESC.hashCode());
	result = prime * result + ((FEE_CCY == null) ? 0 : FEE_CCY.hashCode());
	result = prime * result + ((FILE_STATUS == null) ? 0 : FILE_STATUS.hashCode());
	result = prime * result + ((FI_CANC_REF_NO == null) ? 0 : FI_CANC_REF_NO.hashCode());
	result = prime * result + ((FX_CONTRACT_ID == null) ? 0 : FX_CONTRACT_ID.hashCode());
	result = prime * result + ((FX_RATE == null) ? 0 : FX_RATE.hashCode());
	result = prime * result + ((FX_RATE_TYPE == null) ? 0 : FX_RATE_TYPE.hashCode());
	result = prime * result + ((INIT_PRTY_BEI == null) ? 0 : INIT_PRTY_BEI.hashCode());
	result = prime * result + ((INIT_PRTY_ID == null) ? 0 : INIT_PRTY_ID.hashCode());
	result = prime * result + ((INSTR_AMOUNT == null) ? 0 : INSTR_AMOUNT.hashCode());
	result = prime * result + ((INSTR_CODE1 == null) ? 0 : INSTR_CODE1.hashCode());
	result = prime * result + ((INSTR_CODE_PROP == null) ? 0 : INSTR_CODE_PROP.hashCode());
	result = prime * result + ((INSTR_CURRENCY == null) ? 0 : INSTR_CURRENCY.hashCode());
	result = prime * result + ((INTER_AGT_BIC == null) ? 0 : INTER_AGT_BIC.hashCode());
	result = prime * result + ((INTER_AGT_CLRING_SYS == null) ? 0 : INTER_AGT_CLRING_SYS.hashCode());
	result = prime * result + ((INTER_AGT_CLRING_SYS_CD == null) ? 0 : INTER_AGT_CLRING_SYS_CD.hashCode());
	result = prime * result + ((INTER_AGT_NAME == null) ? 0 : INTER_AGT_NAME.hashCode());
	result = prime * result + ((IS_BATCH_DEBIT == null) ? 0 : IS_BATCH_DEBIT.hashCode());
	result = prime * result + ((IWS_STATUS_CD == null) ? 0 : IWS_STATUS_CD.hashCode());
	result = prime * result + ((IWS_STATUS_DESC == null) ? 0 : IWS_STATUS_DESC.hashCode());
	result = prime * result + ((LCL_REC_TYPEID == null) ? 0 : LCL_REC_TYPEID.hashCode());
	result = prime * result + ((LIMIT_BAL_CHK_FLAG == null) ? 0 : LIMIT_BAL_CHK_FLAG.hashCode());
	result = prime * result + ((LIMIT_CHK_EQUI_AMT == null) ? 0 : LIMIT_CHK_EQUI_AMT.hashCode());
	result = prime * result + ((LIMIT_CHK_EQUI_CCY == null) ? 0 : LIMIT_CHK_EQUI_CCY.hashCode());
	result = prime * result + ((NOC_CHG_CODE == null) ? 0 : NOC_CHG_CODE.hashCode());
	result = prime * result + ((NOC_CORR_INFO == null) ? 0 : NOC_CORR_INFO.hashCode());
	result = prime * result + ((NOC_FLAG == null) ? 0 : NOC_FLAG.hashCode());
	result = prime * result + ((NOTIONAL_EXCH_RATE == null) ? 0 : NOTIONAL_EXCH_RATE.hashCode());
	result = prime * result + ((PARAM_CNCL_REQ_DATE == null) ? 0 : PARAM_CNCL_REQ_DATE.hashCode());
	result = prime * result + ((PARAM_CNCL_REQ_STAT == null) ? 0 : PARAM_CNCL_REQ_STAT.hashCode());
	result = prime * result + ((PARAM_CNCL_RSN_CODE == null) ? 0 : PARAM_CNCL_RSN_CODE.hashCode());
	result = prime * result + ((PARAM_CNCL_RSN_DESC == null) ? 0 : PARAM_CNCL_RSN_DESC.hashCode());
	result = prime * result + ((QUEUEID == null) ? 0 : QUEUEID.hashCode());
	result = prime * result + ((QUEUELABEL == null) ? 0 : QUEUELABEL.hashCode());
	result = prime * result + ((RBFEE_STATUS_CD == null) ? 0 : RBFEE_STATUS_CD.hashCode());
	result = prime * result + ((RBFEE_STATUS_DESC == null) ? 0 : RBFEE_STATUS_DESC.hashCode());
	result = prime * result + ((RBFEE_STATUS_RSN_CD == null) ? 0 : RBFEE_STATUS_RSN_CD.hashCode());
	result = prime * result + ((RBFEE_STATUS_RSN_DESC == null) ? 0 : RBFEE_STATUS_RSN_DESC.hashCode());
	result = prime * result + ((RBPMT_STATUS_CD == null) ? 0 : RBPMT_STATUS_CD.hashCode());
	result = prime * result + ((RBPMT_STATUS_DESC == null) ? 0 : RBPMT_STATUS_DESC.hashCode());
	result = prime * result + ((RBPMT_STATUS_RSN_CD == null) ? 0 : RBPMT_STATUS_RSN_CD.hashCode());
	result = prime * result + ((RBPMT_STATUS_RSN_DESC == null) ? 0 : RBPMT_STATUS_RSN_DESC.hashCode());
	result = prime * result + ((REMIT_INFO_1 == null) ? 0 : REMIT_INFO_1.hashCode());
	result = prime * result + ((REMIT_INFO_2 == null) ? 0 : REMIT_INFO_2.hashCode());
	result = prime * result + ((REMIT_INFO_3 == null) ? 0 : REMIT_INFO_3.hashCode());
	result = prime * result + ((REMIT_INFO_4 == null) ? 0 : REMIT_INFO_4.hashCode());
	result = prime * result + ((REMT_CR_BLDG_TXN == null) ? 0 : REMT_CR_BLDG_TXN.hashCode());
	result = prime * result + ((REMT_CR_CTRY_TXN == null) ? 0 : REMT_CR_CTRY_TXN.hashCode());
	result = prime * result + ((REMT_CR_LCEM == null) ? 0 : REMT_CR_LCEM.hashCode());
	result = prime * result + ((REMT_CR_LCFX == null) ? 0 : REMT_CR_LCFX.hashCode());
	result = prime * result + ((REMT_CR_NM_TXN == null) ? 0 : REMT_CR_NM_TXN.hashCode());
	result = prime * result + ((REMT_CR_PSTL_TXN == null) ? 0 : REMT_CR_PSTL_TXN.hashCode());
	result = prime * result + ((REMT_CR_SD_TXN == null) ? 0 : REMT_CR_SD_TXN.hashCode());
	result = prime * result + ((REMT_CR_STRT_TXN == null) ? 0 : REMT_CR_STRT_TXN.hashCode());
	result = prime * result + ((REMT_CR_TW_TXN == null) ? 0 : REMT_CR_TW_TXN.hashCode());
	result = prime * result + ((REMT_STATUS_DESC == null) ? 0 : REMT_STATUS_DESC.hashCode());
	result = prime * result + ((REMT_TYPE == null) ? 0 : REMT_TYPE.hashCode());
	result = prime * result + ((RESP_CODE == null) ? 0 : RESP_CODE.hashCode());
	result = prime * result + ((RESP_DESC == null) ? 0 : RESP_DESC.hashCode());
	result = prime * result + ((RESP_REF_NUM == null) ? 0 : RESP_REF_NUM.hashCode());
	result = prime * result + ((SAR_LOC_MTD == null) ? 0 : SAR_LOC_MTD.hashCode());
	result = prime * result + ((SETTL_METHOD == null) ? 0 : SETTL_METHOD.hashCode());
	result = prime * result + ((TASKID == null) ? 0 : TASKID.hashCode());
	result = prime * result + ((TASKLABEL == null) ? 0 : TASKLABEL.hashCode());
	result = prime * result + ((TFBI_CR_ACTYFT == null) ? 0 : TFBI_CR_ACTYFT.hashCode());
	result = prime * result + ((TFRI_PUR_FT == null) ? 0 : TFRI_PUR_FT.hashCode());
	result = prime * result + ((TIBI_CA_ID == null) ? 0 : TIBI_CA_ID.hashCode());
	result = prime * result + ((TICI_I2_CSID == null) ? 0 : TICI_I2_CSID.hashCode());
	result = prime * result + ((TPOI_IP_NM2 == null) ? 0 : TPOI_IP_NM2.hashCode());
	result = prime * result + ((TRN_STATUS_CODE == null) ? 0 : TRN_STATUS_CODE.hashCode());
	result = prime * result + ((TXNS_BGN01 == null) ? 0 : TXNS_BGN01.hashCode());
	result = prime * result + ((TXNS_OTI08 == null) ? 0 : TXNS_OTI08.hashCode());
	result = prime * result + ((TXN_CLGSYSREF == null) ? 0 : TXN_CLGSYSREF.hashCode());
	result = prime * result + ((TXN_CONTACT_ID == null) ? 0 : TXN_CONTACT_ID.hashCode());
	result = prime * result + ((TXN_CUST_SM == null) ? 0 : TXN_CUST_SM.hashCode());
	result = prime * result + ((TXN_FX_ID == null) ? 0 : TXN_FX_ID.hashCode());
	result = prime * result + ((TXN_GOW_ACCT_STAT == null) ? 0 : TXN_GOW_ACCT_STAT.hashCode());
	result = prime * result + ((TXN_GOW_NOTF_FLG == null) ? 0 : TXN_GOW_NOTF_FLG.hashCode());
	result = prime * result + ((TXN_RTP_INTERAC_REF_NUM == null) ? 0 : TXN_RTP_INTERAC_REF_NUM.hashCode());
	result = prime * result + ((TXN_RTP_REF_NUM == null) ? 0 : TXN_RTP_REF_NUM.hashCode());
	result = prime * result + ((TXN_STATUS == null) ? 0 : TXN_STATUS.hashCode());
	result = prime * result + ((TXN_STSUPD_CD == null) ? 0 : TXN_STSUPD_CD.hashCode());
	result = prime * result + ((TXN_SWIFT_UETR == null) ? 0 : TXN_SWIFT_UETR.hashCode());
	result = prime * result + ((TXN_TYPE == null) ? 0 : TXN_TYPE.hashCode());
	result = prime * result + ((T_ORG_DFI_QLFR == null) ? 0 : T_ORG_DFI_QLFR.hashCode());
	result = prime * result + ((UMMREV_RSN_CD == null) ? 0 : UMMREV_RSN_CD.hashCode());
	result = prime * result + ((UMMREV_RSN_DESC == null) ? 0 : UMMREV_RSN_DESC.hashCode());
	result = prime * result + ((UMMREV_STAT_CD == null) ? 0 : UMMREV_STAT_CD.hashCode());
	result = prime * result + ((UMMREV_STAT_DESC == null) ? 0 : UMMREV_STAT_DESC.hashCode());
	result = prime * result + ((UMM_PSH_STAT_CD == null) ? 0 : UMM_PSH_STAT_CD.hashCode());
	result = prime * result + ((UMM_PSH_STAT_CD_DESC == null) ? 0 : UMM_PSH_STAT_CD_DESC.hashCode());
	result = prime * result + ((UMM_STAT_CD == null) ? 0 : UMM_STAT_CD.hashCode());
	result = prime * result + ((UMM_STAT_CD_DESC == null) ? 0 : UMM_STAT_CD_DESC.hashCode());
	return result;
}
@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	PSHPojo other = (PSHPojo) obj;
	if (ACC_CCY == null) {
		if (other.ACC_CCY != null)
			return false;
	} else if (!ACC_CCY.equals(other.ACC_CCY))
		return false;
	if (ACNO_RETURN == null) {
		if (other.ACNO_RETURN != null)
			return false;
	} else if (!ACNO_RETURN.equals(other.ACNO_RETURN))
		return false;
	if (ADDITONAL_INFO == null) {
		if (other.ADDITONAL_INFO != null)
			return false;
	} else if (!ADDITONAL_INFO.equals(other.ADDITONAL_INFO))
		return false;
	if (BAL_CHK_EQUI_AMT == null) {
		if (other.BAL_CHK_EQUI_AMT != null)
			return false;
	} else if (!BAL_CHK_EQUI_AMT.equals(other.BAL_CHK_EQUI_AMT))
		return false;
	if (B_SRVCLVL_CD == null) {
		if (other.B_SRVCLVL_CD != null)
			return false;
	} else if (!B_SRVCLVL_CD.equals(other.B_SRVCLVL_CD))
		return false;
	if (CHARGE_TYPE == null) {
		if (other.CHARGE_TYPE != null)
			return false;
	} else if (!CHARGE_TYPE.equals(other.CHARGE_TYPE))
		return false;
	if (CHQ_DLVY_MTD == null) {
		if (other.CHQ_DLVY_MTD != null)
			return false;
	} else if (!CHQ_DLVY_MTD.equals(other.CHQ_DLVY_MTD))
		return false;
	if (CHQ_FRM_BLDG == null) {
		if (other.CHQ_FRM_BLDG != null)
			return false;
	} else if (!CHQ_FRM_BLDG.equals(other.CHQ_FRM_BLDG))
		return false;
	if (CHQ_FRM_CTRY == null) {
		if (other.CHQ_FRM_CTRY != null)
			return false;
	} else if (!CHQ_FRM_CTRY.equals(other.CHQ_FRM_CTRY))
		return false;
	if (CHQ_FRM_NM == null) {
		if (other.CHQ_FRM_NM != null)
			return false;
	} else if (!CHQ_FRM_NM.equals(other.CHQ_FRM_NM))
		return false;
	if (CHQ_FRM_PSTL == null) {
		if (other.CHQ_FRM_PSTL != null)
			return false;
	} else if (!CHQ_FRM_PSTL.equals(other.CHQ_FRM_PSTL))
		return false;
	if (CHQ_FRM_SD == null) {
		if (other.CHQ_FRM_SD != null)
			return false;
	} else if (!CHQ_FRM_SD.equals(other.CHQ_FRM_SD))
		return false;
	if (CHQ_FRM_STRT == null) {
		if (other.CHQ_FRM_STRT != null)
			return false;
	} else if (!CHQ_FRM_STRT.equals(other.CHQ_FRM_STRT))
		return false;
	if (CHQ_FRM_TW == null) {
		if (other.CHQ_FRM_TW != null)
			return false;
	} else if (!CHQ_FRM_TW.equals(other.CHQ_FRM_TW))
		return false;
	if (CHQ_TO_BLDG == null) {
		if (other.CHQ_TO_BLDG != null)
			return false;
	} else if (!CHQ_TO_BLDG.equals(other.CHQ_TO_BLDG))
		return false;
	if (CHQ_TO_CTRY == null) {
		if (other.CHQ_TO_CTRY != null)
			return false;
	} else if (!CHQ_TO_CTRY.equals(other.CHQ_TO_CTRY))
		return false;
	if (CHQ_TO_NM == null) {
		if (other.CHQ_TO_NM != null)
			return false;
	} else if (!CHQ_TO_NM.equals(other.CHQ_TO_NM))
		return false;
	if (CHQ_TO_PSTL == null) {
		if (other.CHQ_TO_PSTL != null)
			return false;
	} else if (!CHQ_TO_PSTL.equals(other.CHQ_TO_PSTL))
		return false;
	if (CHQ_TO_SD == null) {
		if (other.CHQ_TO_SD != null)
			return false;
	} else if (!CHQ_TO_SD.equals(other.CHQ_TO_SD))
		return false;
	if (CHQ_TO_STRT == null) {
		if (other.CHQ_TO_STRT != null)
			return false;
	} else if (!CHQ_TO_STRT.equals(other.CHQ_TO_STRT))
		return false;
	if (CHQ_TO_TW == null) {
		if (other.CHQ_TO_TW != null)
			return false;
	} else if (!CHQ_TO_TW.equals(other.CHQ_TO_TW))
		return false;
	if (CLIENT_ID == null) {
		if (other.CLIENT_ID != null)
			return false;
	} else if (!CLIENT_ID.equals(other.CLIENT_ID))
		return false;
	if (CLIENT_NAME == null) {
		if (other.CLIENT_NAME != null)
			return false;
	} else if (!CLIENT_NAME.equals(other.CLIENT_NAME))
		return false;
	if (CLR_SYSM_CODE == null) {
		if (other.CLR_SYSM_CODE != null)
			return false;
	} else if (!CLR_SYSM_CODE.equals(other.CLR_SYSM_CODE))
		return false;
	if (CORR_FEE_ACC_NO == null) {
		if (other.CORR_FEE_ACC_NO != null)
			return false;
	} else if (!CORR_FEE_ACC_NO.equals(other.CORR_FEE_ACC_NO))
		return false;
	if (CORR_FEE_AMT == null) {
		if (other.CORR_FEE_AMT != null)
			return false;
	} else if (!CORR_FEE_AMT.equals(other.CORR_FEE_AMT))
		return false;
	if (CORR_TRANSIT == null) {
		if (other.CORR_TRANSIT != null)
			return false;
	} else if (!CORR_TRANSIT.equals(other.CORR_TRANSIT))
		return false;
	if (CR_ACC_NO == null) {
		if (other.CR_ACC_NO != null)
			return false;
	} else if (!CR_ACC_NO.equals(other.CR_ACC_NO))
		return false;
	if (CR_ADDR1 == null) {
		if (other.CR_ADDR1 != null)
			return false;
	} else if (!CR_ADDR1.equals(other.CR_ADDR1))
		return false;
	if (CR_ADDR2 == null) {
		if (other.CR_ADDR2 != null)
			return false;
	} else if (!CR_ADDR2.equals(other.CR_ADDR2))
		return false;
	if (CR_ADDR3 == null) {
		if (other.CR_ADDR3 != null)
			return false;
	} else if (!CR_ADDR3.equals(other.CR_ADDR3))
		return false;
	if (CR_AGT_ADDR1 == null) {
		if (other.CR_AGT_ADDR1 != null)
			return false;
	} else if (!CR_AGT_ADDR1.equals(other.CR_AGT_ADDR1))
		return false;
	if (CR_AGT_ADDR2 == null) {
		if (other.CR_AGT_ADDR2 != null)
			return false;
	} else if (!CR_AGT_ADDR2.equals(other.CR_AGT_ADDR2))
		return false;
	if (CR_AGT_ADDR3 == null) {
		if (other.CR_AGT_ADDR3 != null)
			return false;
	} else if (!CR_AGT_ADDR3.equals(other.CR_AGT_ADDR3))
		return false;
	if (CR_AGT_BIC == null) {
		if (other.CR_AGT_BIC != null)
			return false;
	} else if (!CR_AGT_BIC.equals(other.CR_AGT_BIC))
		return false;
	if (CR_AGT_CLRING_SYS == null) {
		if (other.CR_AGT_CLRING_SYS != null)
			return false;
	} else if (!CR_AGT_CLRING_SYS.equals(other.CR_AGT_CLRING_SYS))
		return false;
	if (CR_AGT_CLRING_SYS_CD == null) {
		if (other.CR_AGT_CLRING_SYS_CD != null)
			return false;
	} else if (!CR_AGT_CLRING_SYS_CD.equals(other.CR_AGT_CLRING_SYS_CD))
		return false;
	if (CR_AGT_CTRY == null) {
		if (other.CR_AGT_CTRY != null)
			return false;
	} else if (!CR_AGT_CTRY.equals(other.CR_AGT_CTRY))
		return false;
	if (CR_AGT_NAME == null) {
		if (other.CR_AGT_NAME != null)
			return false;
	} else if (!CR_AGT_NAME.equals(other.CR_AGT_NAME))
		return false;
	if (CR_BLDG_NO1 == null) {
		if (other.CR_BLDG_NO1 != null)
			return false;
	} else if (!CR_BLDG_NO1.equals(other.CR_BLDG_NO1))
		return false;
	if (CR_CITY == null) {
		if (other.CR_CITY != null)
			return false;
	} else if (!CR_CITY.equals(other.CR_CITY))
		return false;
	if (CR_CTRY == null) {
		if (other.CR_CTRY != null)
			return false;
	} else if (!CR_CTRY.equals(other.CR_CTRY))
		return false;
	if (CR_EMAIL == null) {
		if (other.CR_EMAIL != null)
			return false;
	} else if (!CR_EMAIL.equals(other.CR_EMAIL))
		return false;
	if (CR_IBAN == null) {
		if (other.CR_IBAN != null)
			return false;
	} else if (!CR_IBAN.equals(other.CR_IBAN))
		return false;
	if (CR_LANG_PREF == null) {
		if (other.CR_LANG_PREF != null)
			return false;
	} else if (!CR_LANG_PREF.equals(other.CR_LANG_PREF))
		return false;
	if (CR_MOB_NB == null) {
		if (other.CR_MOB_NB != null)
			return false;
	} else if (!CR_MOB_NB.equals(other.CR_MOB_NB))
		return false;
	if (CR_NAME == null) {
		if (other.CR_NAME != null)
			return false;
	} else if (!CR_NAME.equals(other.CR_NAME))
		return false;
	if (CR_PSTL_CD == null) {
		if (other.CR_PSTL_CD != null)
			return false;
	} else if (!CR_PSTL_CD.equals(other.CR_PSTL_CD))
		return false;
	if (CR_STATE == null) {
		if (other.CR_STATE != null)
			return false;
	} else if (!CR_STATE.equals(other.CR_STATE))
		return false;
	if (CR_STREET == null) {
		if (other.CR_STREET != null)
			return false;
	} else if (!CR_STREET.equals(other.CR_STREET))
		return false;
	if (CUST_CNCL_ID == null) {
		if (other.CUST_CNCL_ID != null)
			return false;
	} else if (!CUST_CNCL_ID.equals(other.CUST_CNCL_ID))
		return false;
	if (CUST_ID == null) {
		if (other.CUST_ID != null)
			return false;
	} else if (!CUST_ID.equals(other.CUST_ID))
		return false;
	if (CUST_REF_NUM == null) {
		if (other.CUST_REF_NUM != null)
			return false;
	} else if (!CUST_REF_NUM.equals(other.CUST_REF_NUM))
		return false;
	if (DEBIT_AMOUNT == null) {
		if (other.DEBIT_AMOUNT != null)
			return false;
	} else if (!DEBIT_AMOUNT.equals(other.DEBIT_AMOUNT))
		return false;
	if (DEBIT_CURRENCY == null) {
		if (other.DEBIT_CURRENCY != null)
			return false;
	} else if (!DEBIT_CURRENCY.equals(other.DEBIT_CURRENCY))
		return false;
	if (DR_ACCT_NO == null) {
		if (other.DR_ACCT_NO != null)
			return false;
	} else if (!DR_ACCT_NO.equals(other.DR_ACCT_NO))
		return false;
	if (DR_ADDR1 == null) {
		if (other.DR_ADDR1 != null)
			return false;
	} else if (!DR_ADDR1.equals(other.DR_ADDR1))
		return false;
	if (DR_ADDR2 == null) {
		if (other.DR_ADDR2 != null)
			return false;
	} else if (!DR_ADDR2.equals(other.DR_ADDR2))
		return false;
	if (DR_ADDR3 == null) {
		if (other.DR_ADDR3 != null)
			return false;
	} else if (!DR_ADDR3.equals(other.DR_ADDR3))
		return false;
	if (DR_AGT_CITY == null) {
		if (other.DR_AGT_CITY != null)
			return false;
	} else if (!DR_AGT_CITY.equals(other.DR_AGT_CITY))
		return false;
	if (DR_AGT_CLRING_SYS_CD == null) {
		if (other.DR_AGT_CLRING_SYS_CD != null)
			return false;
	} else if (!DR_AGT_CLRING_SYS_CD.equals(other.DR_AGT_CLRING_SYS_CD))
		return false;
	if (DR_AGT_CTRY == null) {
		if (other.DR_AGT_CTRY != null)
			return false;
	} else if (!DR_AGT_CTRY.equals(other.DR_AGT_CTRY))
		return false;
	if (DR_AGT_NAME == null) {
		if (other.DR_AGT_NAME != null)
			return false;
	} else if (!DR_AGT_NAME.equals(other.DR_AGT_NAME))
		return false;
	if (DR_AGT_PSTL_CD == null) {
		if (other.DR_AGT_PSTL_CD != null)
			return false;
	} else if (!DR_AGT_PSTL_CD.equals(other.DR_AGT_PSTL_CD))
		return false;
	if (DR_AGT_STATE == null) {
		if (other.DR_AGT_STATE != null)
			return false;
	} else if (!DR_AGT_STATE.equals(other.DR_AGT_STATE))
		return false;
	if (DR_BLDG_NO == null) {
		if (other.DR_BLDG_NO != null)
			return false;
	} else if (!DR_BLDG_NO.equals(other.DR_BLDG_NO))
		return false;
	if (DR_CITY == null) {
		if (other.DR_CITY != null)
			return false;
	} else if (!DR_CITY.equals(other.DR_CITY))
		return false;
	if (DR_CTRY == null) {
		if (other.DR_CTRY != null)
			return false;
	} else if (!DR_CTRY.equals(other.DR_CTRY))
		return false;
	if (DR_NAME == null) {
		if (other.DR_NAME != null)
			return false;
	} else if (!DR_NAME.equals(other.DR_NAME))
		return false;
	if (DR_PSTL_CODE == null) {
		if (other.DR_PSTL_CODE != null)
			return false;
	} else if (!DR_PSTL_CODE.equals(other.DR_PSTL_CODE))
		return false;
	if (DR_STATE == null) {
		if (other.DR_STATE != null)
			return false;
	} else if (!DR_STATE.equals(other.DR_STATE))
		return false;
	if (DR_STREET == null) {
		if (other.DR_STREET != null)
			return false;
	} else if (!DR_STREET.equals(other.DR_STREET))
		return false;
	if (EXCEPTION_DESC == null) {
		if (other.EXCEPTION_DESC != null)
			return false;
	} else if (!EXCEPTION_DESC.equals(other.EXCEPTION_DESC))
		return false;
	if (EXCEPTION_ID == null) {
		if (other.EXCEPTION_ID != null)
			return false;
	} else if (!EXCEPTION_ID.equals(other.EXCEPTION_ID))
		return false;
	if (EXTERNAL_EXCEPTION_DESC == null) {
		if (other.EXTERNAL_EXCEPTION_DESC != null)
			return false;
	} else if (!EXTERNAL_EXCEPTION_DESC.equals(other.EXTERNAL_EXCEPTION_DESC))
		return false;
	if (EXTERNAL_EXCEPTION_ID == null) {
		if (other.EXTERNAL_EXCEPTION_ID != null)
			return false;
	} else if (!EXTERNAL_EXCEPTION_ID.equals(other.EXTERNAL_EXCEPTION_ID))
		return false;
	if (FEEREV_RSN_CD == null) {
		if (other.FEEREV_RSN_CD != null)
			return false;
	} else if (!FEEREV_RSN_CD.equals(other.FEEREV_RSN_CD))
		return false;
	if (FEEREV_RSN_DESC == null) {
		if (other.FEEREV_RSN_DESC != null)
			return false;
	} else if (!FEEREV_RSN_DESC.equals(other.FEEREV_RSN_DESC))
		return false;
	if (FEEREV_STAT_CD == null) {
		if (other.FEEREV_STAT_CD != null)
			return false;
	} else if (!FEEREV_STAT_CD.equals(other.FEEREV_STAT_CD))
		return false;
	if (FEEREV_STAT_DESC == null) {
		if (other.FEEREV_STAT_DESC != null)
			return false;
	} else if (!FEEREV_STAT_DESC.equals(other.FEEREV_STAT_DESC))
		return false;
	if (FEE_CCY == null) {
		if (other.FEE_CCY != null)
			return false;
	} else if (!FEE_CCY.equals(other.FEE_CCY))
		return false;
	if (FILE_STATUS == null) {
		if (other.FILE_STATUS != null)
			return false;
	} else if (!FILE_STATUS.equals(other.FILE_STATUS))
		return false;
	if (FI_CANC_REF_NO == null) {
		if (other.FI_CANC_REF_NO != null)
			return false;
	} else if (!FI_CANC_REF_NO.equals(other.FI_CANC_REF_NO))
		return false;
	if (FX_CONTRACT_ID == null) {
		if (other.FX_CONTRACT_ID != null)
			return false;
	} else if (!FX_CONTRACT_ID.equals(other.FX_CONTRACT_ID))
		return false;
	if (FX_RATE == null) {
		if (other.FX_RATE != null)
			return false;
	} else if (!FX_RATE.equals(other.FX_RATE))
		return false;
	if (FX_RATE_TYPE == null) {
		if (other.FX_RATE_TYPE != null)
			return false;
	} else if (!FX_RATE_TYPE.equals(other.FX_RATE_TYPE))
		return false;
	if (INIT_PRTY_BEI == null) {
		if (other.INIT_PRTY_BEI != null)
			return false;
	} else if (!INIT_PRTY_BEI.equals(other.INIT_PRTY_BEI))
		return false;
	if (INIT_PRTY_ID == null) {
		if (other.INIT_PRTY_ID != null)
			return false;
	} else if (!INIT_PRTY_ID.equals(other.INIT_PRTY_ID))
		return false;
	if (INSTR_AMOUNT == null) {
		if (other.INSTR_AMOUNT != null)
			return false;
	} else if (!INSTR_AMOUNT.equals(other.INSTR_AMOUNT))
		return false;
	if (INSTR_CODE1 == null) {
		if (other.INSTR_CODE1 != null)
			return false;
	} else if (!INSTR_CODE1.equals(other.INSTR_CODE1))
		return false;
	if (INSTR_CODE_PROP == null) {
		if (other.INSTR_CODE_PROP != null)
			return false;
	} else if (!INSTR_CODE_PROP.equals(other.INSTR_CODE_PROP))
		return false;
	if (INSTR_CURRENCY == null) {
		if (other.INSTR_CURRENCY != null)
			return false;
	} else if (!INSTR_CURRENCY.equals(other.INSTR_CURRENCY))
		return false;
	if (INTER_AGT_BIC == null) {
		if (other.INTER_AGT_BIC != null)
			return false;
	} else if (!INTER_AGT_BIC.equals(other.INTER_AGT_BIC))
		return false;
	if (INTER_AGT_CLRING_SYS == null) {
		if (other.INTER_AGT_CLRING_SYS != null)
			return false;
	} else if (!INTER_AGT_CLRING_SYS.equals(other.INTER_AGT_CLRING_SYS))
		return false;
	if (INTER_AGT_CLRING_SYS_CD == null) {
		if (other.INTER_AGT_CLRING_SYS_CD != null)
			return false;
	} else if (!INTER_AGT_CLRING_SYS_CD.equals(other.INTER_AGT_CLRING_SYS_CD))
		return false;
	if (INTER_AGT_NAME == null) {
		if (other.INTER_AGT_NAME != null)
			return false;
	} else if (!INTER_AGT_NAME.equals(other.INTER_AGT_NAME))
		return false;
	if (IS_BATCH_DEBIT == null) {
		if (other.IS_BATCH_DEBIT != null)
			return false;
	} else if (!IS_BATCH_DEBIT.equals(other.IS_BATCH_DEBIT))
		return false;
	if (IWS_STATUS_CD == null) {
		if (other.IWS_STATUS_CD != null)
			return false;
	} else if (!IWS_STATUS_CD.equals(other.IWS_STATUS_CD))
		return false;
	if (IWS_STATUS_DESC == null) {
		if (other.IWS_STATUS_DESC != null)
			return false;
	} else if (!IWS_STATUS_DESC.equals(other.IWS_STATUS_DESC))
		return false;
	if (LCL_REC_TYPEID == null) {
		if (other.LCL_REC_TYPEID != null)
			return false;
	} else if (!LCL_REC_TYPEID.equals(other.LCL_REC_TYPEID))
		return false;
	if (LIMIT_BAL_CHK_FLAG == null) {
		if (other.LIMIT_BAL_CHK_FLAG != null)
			return false;
	} else if (!LIMIT_BAL_CHK_FLAG.equals(other.LIMIT_BAL_CHK_FLAG))
		return false;
	if (LIMIT_CHK_EQUI_AMT == null) {
		if (other.LIMIT_CHK_EQUI_AMT != null)
			return false;
	} else if (!LIMIT_CHK_EQUI_AMT.equals(other.LIMIT_CHK_EQUI_AMT))
		return false;
	if (LIMIT_CHK_EQUI_CCY == null) {
		if (other.LIMIT_CHK_EQUI_CCY != null)
			return false;
	} else if (!LIMIT_CHK_EQUI_CCY.equals(other.LIMIT_CHK_EQUI_CCY))
		return false;
	if (NOC_CHG_CODE == null) {
		if (other.NOC_CHG_CODE != null)
			return false;
	} else if (!NOC_CHG_CODE.equals(other.NOC_CHG_CODE))
		return false;
	if (NOC_CORR_INFO == null) {
		if (other.NOC_CORR_INFO != null)
			return false;
	} else if (!NOC_CORR_INFO.equals(other.NOC_CORR_INFO))
		return false;
	if (NOC_FLAG == null) {
		if (other.NOC_FLAG != null)
			return false;
	} else if (!NOC_FLAG.equals(other.NOC_FLAG))
		return false;
	if (NOTIONAL_EXCH_RATE == null) {
		if (other.NOTIONAL_EXCH_RATE != null)
			return false;
	} else if (!NOTIONAL_EXCH_RATE.equals(other.NOTIONAL_EXCH_RATE))
		return false;
	if (PARAM_CNCL_REQ_DATE == null) {
		if (other.PARAM_CNCL_REQ_DATE != null)
			return false;
	} else if (!PARAM_CNCL_REQ_DATE.equals(other.PARAM_CNCL_REQ_DATE))
		return false;
	if (PARAM_CNCL_REQ_STAT == null) {
		if (other.PARAM_CNCL_REQ_STAT != null)
			return false;
	} else if (!PARAM_CNCL_REQ_STAT.equals(other.PARAM_CNCL_REQ_STAT))
		return false;
	if (PARAM_CNCL_RSN_CODE == null) {
		if (other.PARAM_CNCL_RSN_CODE != null)
			return false;
	} else if (!PARAM_CNCL_RSN_CODE.equals(other.PARAM_CNCL_RSN_CODE))
		return false;
	if (PARAM_CNCL_RSN_DESC == null) {
		if (other.PARAM_CNCL_RSN_DESC != null)
			return false;
	} else if (!PARAM_CNCL_RSN_DESC.equals(other.PARAM_CNCL_RSN_DESC))
		return false;
	if (QUEUEID == null) {
		if (other.QUEUEID != null)
			return false;
	} else if (!QUEUEID.equals(other.QUEUEID))
		return false;
	if (QUEUELABEL == null) {
		if (other.QUEUELABEL != null)
			return false;
	} else if (!QUEUELABEL.equals(other.QUEUELABEL))
		return false;
	if (RBFEE_STATUS_CD == null) {
		if (other.RBFEE_STATUS_CD != null)
			return false;
	} else if (!RBFEE_STATUS_CD.equals(other.RBFEE_STATUS_CD))
		return false;
	if (RBFEE_STATUS_DESC == null) {
		if (other.RBFEE_STATUS_DESC != null)
			return false;
	} else if (!RBFEE_STATUS_DESC.equals(other.RBFEE_STATUS_DESC))
		return false;
	if (RBFEE_STATUS_RSN_CD == null) {
		if (other.RBFEE_STATUS_RSN_CD != null)
			return false;
	} else if (!RBFEE_STATUS_RSN_CD.equals(other.RBFEE_STATUS_RSN_CD))
		return false;
	if (RBFEE_STATUS_RSN_DESC == null) {
		if (other.RBFEE_STATUS_RSN_DESC != null)
			return false;
	} else if (!RBFEE_STATUS_RSN_DESC.equals(other.RBFEE_STATUS_RSN_DESC))
		return false;
	if (RBPMT_STATUS_CD == null) {
		if (other.RBPMT_STATUS_CD != null)
			return false;
	} else if (!RBPMT_STATUS_CD.equals(other.RBPMT_STATUS_CD))
		return false;
	if (RBPMT_STATUS_DESC == null) {
		if (other.RBPMT_STATUS_DESC != null)
			return false;
	} else if (!RBPMT_STATUS_DESC.equals(other.RBPMT_STATUS_DESC))
		return false;
	if (RBPMT_STATUS_RSN_CD == null) {
		if (other.RBPMT_STATUS_RSN_CD != null)
			return false;
	} else if (!RBPMT_STATUS_RSN_CD.equals(other.RBPMT_STATUS_RSN_CD))
		return false;
	if (RBPMT_STATUS_RSN_DESC == null) {
		if (other.RBPMT_STATUS_RSN_DESC != null)
			return false;
	} else if (!RBPMT_STATUS_RSN_DESC.equals(other.RBPMT_STATUS_RSN_DESC))
		return false;
	if (REMIT_INFO_1 == null) {
		if (other.REMIT_INFO_1 != null)
			return false;
	} else if (!REMIT_INFO_1.equals(other.REMIT_INFO_1))
		return false;
	if (REMIT_INFO_2 == null) {
		if (other.REMIT_INFO_2 != null)
			return false;
	} else if (!REMIT_INFO_2.equals(other.REMIT_INFO_2))
		return false;
	if (REMIT_INFO_3 == null) {
		if (other.REMIT_INFO_3 != null)
			return false;
	} else if (!REMIT_INFO_3.equals(other.REMIT_INFO_3))
		return false;
	if (REMIT_INFO_4 == null) {
		if (other.REMIT_INFO_4 != null)
			return false;
	} else if (!REMIT_INFO_4.equals(other.REMIT_INFO_4))
		return false;
	if (REMT_CR_BLDG_TXN == null) {
		if (other.REMT_CR_BLDG_TXN != null)
			return false;
	} else if (!REMT_CR_BLDG_TXN.equals(other.REMT_CR_BLDG_TXN))
		return false;
	if (REMT_CR_CTRY_TXN == null) {
		if (other.REMT_CR_CTRY_TXN != null)
			return false;
	} else if (!REMT_CR_CTRY_TXN.equals(other.REMT_CR_CTRY_TXN))
		return false;
	if (REMT_CR_LCEM == null) {
		if (other.REMT_CR_LCEM != null)
			return false;
	} else if (!REMT_CR_LCEM.equals(other.REMT_CR_LCEM))
		return false;
	if (REMT_CR_LCFX == null) {
		if (other.REMT_CR_LCFX != null)
			return false;
	} else if (!REMT_CR_LCFX.equals(other.REMT_CR_LCFX))
		return false;
	if (REMT_CR_NM_TXN == null) {
		if (other.REMT_CR_NM_TXN != null)
			return false;
	} else if (!REMT_CR_NM_TXN.equals(other.REMT_CR_NM_TXN))
		return false;
	if (REMT_CR_PSTL_TXN == null) {
		if (other.REMT_CR_PSTL_TXN != null)
			return false;
	} else if (!REMT_CR_PSTL_TXN.equals(other.REMT_CR_PSTL_TXN))
		return false;
	if (REMT_CR_SD_TXN == null) {
		if (other.REMT_CR_SD_TXN != null)
			return false;
	} else if (!REMT_CR_SD_TXN.equals(other.REMT_CR_SD_TXN))
		return false;
	if (REMT_CR_STRT_TXN == null) {
		if (other.REMT_CR_STRT_TXN != null)
			return false;
	} else if (!REMT_CR_STRT_TXN.equals(other.REMT_CR_STRT_TXN))
		return false;
	if (REMT_CR_TW_TXN == null) {
		if (other.REMT_CR_TW_TXN != null)
			return false;
	} else if (!REMT_CR_TW_TXN.equals(other.REMT_CR_TW_TXN))
		return false;
	if (REMT_STATUS_DESC == null) {
		if (other.REMT_STATUS_DESC != null)
			return false;
	} else if (!REMT_STATUS_DESC.equals(other.REMT_STATUS_DESC))
		return false;
	if (REMT_TYPE == null) {
		if (other.REMT_TYPE != null)
			return false;
	} else if (!REMT_TYPE.equals(other.REMT_TYPE))
		return false;
	if (RESP_CODE == null) {
		if (other.RESP_CODE != null)
			return false;
	} else if (!RESP_CODE.equals(other.RESP_CODE))
		return false;
	if (RESP_DESC == null) {
		if (other.RESP_DESC != null)
			return false;
	} else if (!RESP_DESC.equals(other.RESP_DESC))
		return false;
	if (RESP_REF_NUM == null) {
		if (other.RESP_REF_NUM != null)
			return false;
	} else if (!RESP_REF_NUM.equals(other.RESP_REF_NUM))
		return false;
	if (SAR_LOC_MTD == null) {
		if (other.SAR_LOC_MTD != null)
			return false;
	} else if (!SAR_LOC_MTD.equals(other.SAR_LOC_MTD))
		return false;
	if (SETTL_METHOD == null) {
		if (other.SETTL_METHOD != null)
			return false;
	} else if (!SETTL_METHOD.equals(other.SETTL_METHOD))
		return false;
	if (TASKID == null) {
		if (other.TASKID != null)
			return false;
	} else if (!TASKID.equals(other.TASKID))
		return false;
	if (TASKLABEL == null) {
		if (other.TASKLABEL != null)
			return false;
	} else if (!TASKLABEL.equals(other.TASKLABEL))
		return false;
	if (TFBI_CR_ACTYFT == null) {
		if (other.TFBI_CR_ACTYFT != null)
			return false;
	} else if (!TFBI_CR_ACTYFT.equals(other.TFBI_CR_ACTYFT))
		return false;
	if (TFRI_PUR_FT == null) {
		if (other.TFRI_PUR_FT != null)
			return false;
	} else if (!TFRI_PUR_FT.equals(other.TFRI_PUR_FT))
		return false;
	if (TIBI_CA_ID == null) {
		if (other.TIBI_CA_ID != null)
			return false;
	} else if (!TIBI_CA_ID.equals(other.TIBI_CA_ID))
		return false;
	if (TICI_I2_CSID == null) {
		if (other.TICI_I2_CSID != null)
			return false;
	} else if (!TICI_I2_CSID.equals(other.TICI_I2_CSID))
		return false;
	if (TPOI_IP_NM2 == null) {
		if (other.TPOI_IP_NM2 != null)
			return false;
	} else if (!TPOI_IP_NM2.equals(other.TPOI_IP_NM2))
		return false;
	if (TRN_STATUS_CODE == null) {
		if (other.TRN_STATUS_CODE != null)
			return false;
	} else if (!TRN_STATUS_CODE.equals(other.TRN_STATUS_CODE))
		return false;
	if (TXNS_BGN01 == null) {
		if (other.TXNS_BGN01 != null)
			return false;
	} else if (!TXNS_BGN01.equals(other.TXNS_BGN01))
		return false;
	if (TXNS_OTI08 == null) {
		if (other.TXNS_OTI08 != null)
			return false;
	} else if (!TXNS_OTI08.equals(other.TXNS_OTI08))
		return false;
	if (TXN_CLGSYSREF == null) {
		if (other.TXN_CLGSYSREF != null)
			return false;
	} else if (!TXN_CLGSYSREF.equals(other.TXN_CLGSYSREF))
		return false;
	if (TXN_CONTACT_ID == null) {
		if (other.TXN_CONTACT_ID != null)
			return false;
	} else if (!TXN_CONTACT_ID.equals(other.TXN_CONTACT_ID))
		return false;
	if (TXN_CUST_SM == null) {
		if (other.TXN_CUST_SM != null)
			return false;
	} else if (!TXN_CUST_SM.equals(other.TXN_CUST_SM))
		return false;
	if (TXN_FX_ID == null) {
		if (other.TXN_FX_ID != null)
			return false;
	} else if (!TXN_FX_ID.equals(other.TXN_FX_ID))
		return false;
	if (TXN_GOW_ACCT_STAT == null) {
		if (other.TXN_GOW_ACCT_STAT != null)
			return false;
	} else if (!TXN_GOW_ACCT_STAT.equals(other.TXN_GOW_ACCT_STAT))
		return false;
	if (TXN_GOW_NOTF_FLG == null) {
		if (other.TXN_GOW_NOTF_FLG != null)
			return false;
	} else if (!TXN_GOW_NOTF_FLG.equals(other.TXN_GOW_NOTF_FLG))
		return false;
	if (TXN_RTP_INTERAC_REF_NUM == null) {
		if (other.TXN_RTP_INTERAC_REF_NUM != null)
			return false;
	} else if (!TXN_RTP_INTERAC_REF_NUM.equals(other.TXN_RTP_INTERAC_REF_NUM))
		return false;
	if (TXN_RTP_REF_NUM == null) {
		if (other.TXN_RTP_REF_NUM != null)
			return false;
	} else if (!TXN_RTP_REF_NUM.equals(other.TXN_RTP_REF_NUM))
		return false;
	if (TXN_STATUS == null) {
		if (other.TXN_STATUS != null)
			return false;
	} else if (!TXN_STATUS.equals(other.TXN_STATUS))
		return false;
	if (TXN_STSUPD_CD == null) {
		if (other.TXN_STSUPD_CD != null)
			return false;
	} else if (!TXN_STSUPD_CD.equals(other.TXN_STSUPD_CD))
		return false;
	if (TXN_SWIFT_UETR == null) {
		if (other.TXN_SWIFT_UETR != null)
			return false;
	} else if (!TXN_SWIFT_UETR.equals(other.TXN_SWIFT_UETR))
		return false;
	if (TXN_TYPE == null) {
		if (other.TXN_TYPE != null)
			return false;
	} else if (!TXN_TYPE.equals(other.TXN_TYPE))
		return false;
	if (T_ORG_DFI_QLFR == null) {
		if (other.T_ORG_DFI_QLFR != null)
			return false;
	} else if (!T_ORG_DFI_QLFR.equals(other.T_ORG_DFI_QLFR))
		return false;
	if (UMMREV_RSN_CD == null) {
		if (other.UMMREV_RSN_CD != null)
			return false;
	} else if (!UMMREV_RSN_CD.equals(other.UMMREV_RSN_CD))
		return false;
	if (UMMREV_RSN_DESC == null) {
		if (other.UMMREV_RSN_DESC != null)
			return false;
	} else if (!UMMREV_RSN_DESC.equals(other.UMMREV_RSN_DESC))
		return false;
	if (UMMREV_STAT_CD == null) {
		if (other.UMMREV_STAT_CD != null)
			return false;
	} else if (!UMMREV_STAT_CD.equals(other.UMMREV_STAT_CD))
		return false;
	if (UMMREV_STAT_DESC == null) {
		if (other.UMMREV_STAT_DESC != null)
			return false;
	} else if (!UMMREV_STAT_DESC.equals(other.UMMREV_STAT_DESC))
		return false;
	if (UMM_PSH_STAT_CD == null) {
		if (other.UMM_PSH_STAT_CD != null)
			return false;
	} else if (!UMM_PSH_STAT_CD.equals(other.UMM_PSH_STAT_CD))
		return false;
	if (UMM_PSH_STAT_CD_DESC == null) {
		if (other.UMM_PSH_STAT_CD_DESC != null)
			return false;
	} else if (!UMM_PSH_STAT_CD_DESC.equals(other.UMM_PSH_STAT_CD_DESC))
		return false;
	if (UMM_STAT_CD == null) {
		if (other.UMM_STAT_CD != null)
			return false;
	} else if (!UMM_STAT_CD.equals(other.UMM_STAT_CD))
		return false;
	if (UMM_STAT_CD_DESC == null) {
		if (other.UMM_STAT_CD_DESC != null)
			return false;
	} else if (!UMM_STAT_CD_DESC.equals(other.UMM_STAT_CD_DESC))
		return false;
	return true;
}


}

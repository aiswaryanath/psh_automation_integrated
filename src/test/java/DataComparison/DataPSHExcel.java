package DataComparison;

import java.util.HashMap;
import java.util.Map.Entry;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

import Utilities.FileUtilities;

public class DataPSHExcel {
    boolean flag=false;
    StringBuffer details=new StringBuffer("Match");

	HashMap<String,PSHPojo> data=new HashMap<String,PSHPojo>();
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		DataPSHExcel d=new DataPSHExcel();
		d=d.getExcelcolumnData("SANITY_TC010");
		System.out.println("Final:"+d.getDetails());
	System.out.println("Flag:"+d.getFlag());
	}

	
	public DataPSHExcel getExcelcolumnData(String testid) throws Exception
	{
		String suite="SUITE-SANITY";
		String filename=System.getProperty("user.dir")+"\\src\\test\\resources\\TestData\\"+suite+"\\TestCase_Steps.xls";
		
		Fillo fill=new Fillo();
		Connection conn=fill.getConnection(filename);
		Recordset rs=conn.executeQuery("Select * from Enrichment where TESTCASEID ="+"'"+testid+"'");
		
			 while (rs.next()) {
					PSHPojo P=new PSHPojo();
				 P.setFILE_STATUS(rs.getField("FILE_STATUS"));
				 P.setTXN_TYPE(rs.getField("TXNS_OTI09"));
				 P.setTXN_STATUS(rs.getField("TXN_STATUS"));
				 P.setQUEUEID(rs.getField("QUEUEID"));
				 P.setQUEUELABEL(rs.getField("QUEUELABEL"));
				 P.setTASKID(rs.getField("TASKID"));
				 P.setTASKLABEL(rs.getField("TASKLABEL"));
				 P.setEXCEPTION_ID(rs.getField("EXCEPTION_ID"));
				 P.setEXCEPTION_DESC(rs.getField("EXCEPTION_DESC"));
				 P.setEXTERNAL_EXCEPTION_ID(rs.getField("EXTERNAL_EXCEPTION_ID"));
				 P.setEXTERNAL_EXCEPTION_DESC(rs.getField("EXTERNAL_EXCEPTION_DESC"));
				 P.setCUST_REF_NUM(rs.getField("CUST_REF_NUM"));
				 P.setINIT_PRTY_BEI(rs.getField("INIT_PRTY_BEI"));
				 P.setCLIENT_ID(rs.getField("CLIENT_ID"));
				 P.setCLIENT_NAME(rs.getField("CLIENT_NAME"));
				 P.setCUST_ID(rs.getField("CUST_ID"));
				 P.setDR_NAME(rs.getField("DR_NAME"));
				 P.setDR_STREET(rs.getField("DR_STREET"));
				 P.setDR_BLDG_NO(rs.getField("DR_BLDG_NO"));
				 P.setDR_CITY(rs.getField("DR_CITY"));
				 P.setDR_STATE(rs.getField("DR_STATE"));
				 P.setDR_PSTL_CODE(rs.getField("DR_PSTL_CODE"));
				 P.setDR_AGT_NAME(rs.getField("DR_AGT_NAME"));
				 P.setDR_AGT_CITY(rs.getField("DR_AGT_CITY"));
				 P.setDR_AGT_STATE(rs.getField("DR_AGT_STATE"));
				 P.setDR_AGT_CTRY(rs.getField("DR_AGT_CTRY"));
				 P.setDR_AGT_PSTL_CD(rs.getField("DR_AGT_PSTL_CD"));
				 P.setDR_CTRY(rs.getField("DR_CTRY"));
				 P.setDR_ADDR1(rs.getField("DR_ADDR1"));
				 P.setDR_ADDR2(rs.getField("DR_ADDR2"));
				 P.setDR_ADDR3(rs.getField("DR_ADDR3"));
				 P.setDR_ACCT_NO(rs.getField("DR_ACCT_NO"));
				 P.setSETTL_METHOD(rs.getField("SETTL_METHOD"));
				 P.setDR_AGT_CLRING_SYS_CD(rs.getField("DR_AGT_CLRING_SYS_CD"));
				 P.setACC_CCY(rs.getField("ACC_CCY"));
				 P.setTXN_CUST_SM(rs.getField("TXN_CUST_SM"));
				 P.setDEBIT_AMOUNT(rs.getField("DEBIT_AMOUNT"));
				 P.setINSTR_AMOUNT(rs.getField("INSTR_AMOUNT"));
				 P.setLIMIT_CHK_EQUI_AMT(rs.getField("LIMIT_CHK_EQUI_AMT"));
				 P.setDEBIT_CURRENCY(rs.getField("DEBIT_CURRENCY"));
				 P.setINSTR_CURRENCY(rs.getField("INSTR_CURRENCY"));
				 P.setLIMIT_CHK_EQUI_CCY(rs.getField("LIMIT_CHK_EQUI_CCY"));
				 P.setBAL_CHK_EQUI_AMT(rs.getField("BAL_CHK_EQUI_AMT"));
				 P.setLIMIT_BAL_CHK_FLAG(rs.getField("LIMIT_BAL_CHK_FLAG"));
				 P.setINSTR_CODE1(rs.getField("INSTR_CODE1"));
				 P.setTXN_FX_ID(rs.getField("TXN_FX_ID"));
				 P.setFX_RATE_TYPE(rs.getField("FX_RATE_TYPE"));
				 P.setFX_CONTRACT_ID(rs.getField("FX_CONTRACT_ID"));
				 P.setFX_RATE(rs.getField("FX_RATE"));
				 P.setNOTIONAL_EXCH_RATE(rs.getField("NOTIONAL_EXCH_RATE"));
				 P.setCR_NAME(rs.getField("CR_NAME"));
				 P.setCR_CTRY(rs.getField("CR_CTRY"));
				 P.setCR_ADDR1(rs.getField("CR_ADDR1"));
				 P.setCR_ADDR2(rs.getField("CR_ADDR2"));
				 P.setCR_ADDR3(rs.getField("CR_ADDR3"));
				 P.setCR_AGT_NAME(rs.getField("CR_AGT_NAME"));
				 P.setCR_AGT_BIC(rs.getField("CR_AGT_BIC"));
				 P.setCR_AGT_CLRING_SYS(rs.getField("CR_AGT_CLRING_SYS"));
				 P.setCR_AGT_CLRING_SYS_CD(rs.getField("CR_AGT_CLRING_SYS_CD"));
				 P.setCR_ACC_NO(rs.getField("CR_ACC_NO"));
				 P.setCR_IBAN(rs.getField("CR_IBAN"));
				 P.setCR_AGT_CTRY(rs.getField("CR_AGT_CTRY"));
				 P.setCR_AGT_ADDR1(rs.getField("CR_AGT_ADDR1"));
				 P.setCR_AGT_ADDR2(rs.getField("CR_AGT_ADDR2"));
				 P.setCR_AGT_ADDR3(rs.getField("CR_AGT_ADDR3"));
				 P.setCHARGE_TYPE(rs.getField("CHARGE_TYPE"));
				 P.setCORR_FEE_AMT(rs.getField("CORR_FEE_AMT"));
				 P.setFEE_CCY(rs.getField("FEE_CCY"));
				 P.setCORR_FEE_ACC_NO(rs.getField("CORR_FEE_ACC_NO"));
				 P.setCORR_TRANSIT(rs.getField("CORR_TRANSIT"));
				 P.setREMIT_INFO_1(rs.getField("REMIT_INFO_1"));
				 P.setREMIT_INFO_2(rs.getField("REMIT_INFO_2"));
				 P.setREMIT_INFO_3(rs.getField("REMIT_INFO_3"));
				 P.setREMIT_INFO_4(rs.getField("REMIT_INFO_4"));
				 P.setREMT_STATUS_DESC(rs.getField("REMT_STATUS_DESC"));
				 P.setREMT_TYPE(rs.getField("REMT_TYPE"));
				 P.setSAR_LOC_MTD(rs.getField("SAR_LOC_MTD"));
				 P.setREMT_CR_LCFX(rs.getField("REMT_CR_LCFX"));
				 P.setREMT_CR_LCEM(rs.getField("REMT_CR_LCEM"));
				 P.setREMT_CR_NM_TXN(rs.getField("REMT_CR_NM_TXN"));
				 P.setREMT_CR_STRT_TXN(rs.getField("REMT_CR_STRT_TXN"));
				 P.setREMT_CR_BLDG_TXN(rs.getField("REMT_CR_BLDG_TXN"));
				 P.setREMT_CR_TW_TXN(rs.getField("REMT_CR_TW_TXN"));
				 P.setREMT_CR_SD_TXN(rs.getField("REMT_CR_SD_TXN"));
				 P.setREMT_CR_CTRY_TXN(rs.getField("REMT_CR_CTRY_TXN"));
				 P.setREMT_CR_PSTL_TXN(rs.getField("REMT_CR_PSTL_TXN"));
				 P.setINTER_AGT_NAME(rs.getField("INTER_AGT_NAME"));
				 P.setINTER_AGT_BIC(rs.getField("INTER_AGT_BIC"));
				 P.setINTER_AGT_CLRING_SYS(rs.getField("INTER_AGT_CLRING_SYS"));
				 P.setINTER_AGT_CLRING_SYS_CD(rs.getField("INTER_AGT_CLRING_SYS_CD"));
				 P.setRBPMT_STATUS_CD(rs.getField("RBPMT_STATUS_CD"));
				 P.setRBPMT_STATUS_DESC(rs.getField("RBPMT_STATUS_DESC"));
				 P.setRBFEE_STATUS_CD(rs.getField("RBFEE_STATUS_CD"));
				 P.setRBFEE_STATUS_DESC(rs.getField("RBFEE_STATUS_DESC"));
				 P.setRBPMT_STATUS_RSN_CD(rs.getField("RBPMT_STATUS_RSN_CD"));
				 P.setRBPMT_STATUS_RSN_DESC(rs.getField("RBPMT_STATUS_RSN_DESC"));
				 P.setRBFEE_STATUS_RSN_CD(rs.getField("RBFEE_STATUS_RSN_CD"));
				 P.setRBFEE_STATUS_RSN_DESC(rs.getField("RBFEE_STATUS_RSN_DESC"));
				 P.setUMMREV_STAT_CD(rs.getField("UMMREV_STAT_CD"));
				 P.setUMMREV_STAT_DESC(rs.getField("UMMREV_STAT_DESC"));
				 P.setFEEREV_STAT_CD(rs.getField("FEEREV_STAT_CD"));
				 P.setFEEREV_STAT_DESC(rs.getField("FEEREV_STAT_DESC"));
				 P.setUMMREV_RSN_CD(rs.getField("UMMREV_RSN_CD"));
				 P.setUMMREV_RSN_DESC(rs.getField("UMMREV_RSN_DESC"));
				 P.setFEEREV_RSN_CD(rs.getField("FEEREV_RSN_CD"));
				 P.setFEEREV_RSN_DESC(rs.getField("FEEREV_RSN_DESC"));
				 P.setTXN_CLGSYSREF(rs.getField("TXN_CLGSYSREF"));
				 P.setIWS_STATUS_CD(rs.getField("IWS_STATUS_CD"));
				 P.setIWS_STATUS_DESC(rs.getField("IWS_STATUS_DESC"));
				 P.setCLR_SYSM_CODE(rs.getField("CLR_SYSM_CODE"));
				 P.setTXN_SWIFT_UETR(rs.getField("TXN_SWIFT_UETR"));
				 P.setTXN_GOW_NOTF_FLG(rs.getField("TXN_GOW_NOTF_FLG"));
				 P.setTXN_GOW_ACCT_STAT(rs.getField("TXN_GOW_ACCT_STAT"));
				 P.setNOC_FLAG(rs.getField("NOC_FLAG"));
				 P.setLCL_REC_TYPEID(rs.getField("LCL_REC_TYPEID"));
				 P.setINIT_PRTY_ID(rs.getField("INIT_PRTY_ID"));
				 P.setACNO_RETURN(rs.getField("ACNO_RETURN"));
				 P.setTXNS_OTI08(rs.getField("TXNS_OTI08"));
				 P.setTFRI_PUR_FT(rs.getField("TFRI_PUR_FT"));
				 P.setTIBI_CA_ID(rs.getField("TIBI_CA_ID"));
				 P.setNOC_CHG_CODE(rs.getField("NOC_CHG_CODE"));
				 P.setTPOI_IP_NM2(rs.getField("TPOI_IP_NM2"));
				 P.setNOC_CORR_INFO(rs.getField("NOC_CORR_INFO"));
				 P.setB_SRVCLVL_CD(rs.getField("B_SRVCLVL_CD"));
				 P.setTFBI_CR_ACTYFT(rs.getField("TFBI_CR_ACTYFT"));
				 P.setCR_CITY(rs.getField("CR_CITY"));
				 P.setCR_STATE(rs.getField("CR_STATE"));
				 P.setCR_PSTL_CD(rs.getField("CR_PSTL_CD"));
				 P.setADDITONAL_INFO(rs.getField("ADDITONAL_INFO"));
				 P.setTRN_STATUS_CODE(rs.getField("TRN_STATUS_CODE"));
				 P.setUMM_STAT_CD(rs.getField("UMM_STAT_CD"));
				 P.setUMM_STAT_CD_DESC(rs.getField("UMM_STAT_CD_DESC"));
				 P.setUMM_PSH_STAT_CD(rs.getField("UMM_PSH_STAT_CD"));
				 P.setUMM_PSH_STAT_CD_DESC(rs.getField("UMM_PSH_STAT_CD_DESC"));
				 P.setCHQ_DLVY_MTD(rs.getField("CHQ_DLVY_MTD"));
				 P.setCR_STREET(rs.getField("CR_STREET"));
				 P.setCR_BLDG_NO1(rs.getField("CR_BLDG_NO"));
				 P.setCHQ_FRM_NM(rs.getField("CHQ_FRM_NM"));
				 P.setCHQ_FRM_STRT(rs.getField("CHQ_FRM_STRT"));
				 P.setCHQ_FRM_BLDG(rs.getField("CHQ_FRM_BLDG"));
				 P.setCHQ_FRM_TW(rs.getField("CHQ_FRM_TW"));
				 P.setCHQ_FRM_SD(rs.getField("CHQ_FRM_SD"));
				 P.setCHQ_FRM_CTRY(rs.getField("CHQ_FRM_CTRY"));
				 P.setCHQ_FRM_PSTL(rs.getField("CHQ_FRM_PSTL"));
				 P.setCHQ_TO_NM(rs.getField("CHQ_TO_NM"));
				 P.setCHQ_TO_STRT(rs.getField("CHQ_TO_STRT"));
				 P.setCHQ_TO_BLDG(rs.getField("CHQ_TO_BLDG"));
				 P.setCHQ_TO_TW(rs.getField("CHQ_TO_TW"));
				 P.setCHQ_TO_SD(rs.getField("CHQ_TO_SD"));
				 P.setCHQ_TO_CTRY(rs.getField("CHQ_TO_CTRY"));
				 P.setCHQ_TO_PSTL(rs.getField("CHQ_TO_PSTL"));
				 P.setCR_LANG_PREF(rs.getField("CR_LANG_PREF"));
				 P.setCR_EMAIL(rs.getField("CR_EMAIL"));
				 P.setCR_MOB_NB(rs.getField("CR_MOB_NB"));
				 P.setRESP_CODE(rs.getField("RESP_CODE"));
				 P.setRESP_DESC(rs.getField("RESP_DESC"));
				 P.setRESP_REF_NUM(rs.getField("RESP_REF_NUM"));
				 P.setTXN_RTP_INTERAC_REF_NUM(rs.getField("TXN_RTP_INTERAC_REF_NUM"));
				 P.setTXN_RTP_REF_NUM(rs.getField("TXN_RTP_REF_NUM"));
				 P.setTXN_CONTACT_ID(rs.getField("TXN_CONTACT_ID"));
				 P.setTXN_STSUPD_CD(rs.getField("TXN_STSUPD_CD"));
				 P.setCUST_CNCL_ID(rs.getField("CUST_CNCL_ID"));
				 P.setFI_CANC_REF_NO(rs.getField("FI_CANC_REF_NO"));
				 P.setPARAM_CNCL_REQ_DATE(rs.getField("PARAM_CNCL_REQ_DATE"));
				 P.setPARAM_CNCL_REQ_STAT(rs.getField("PARAM_CNCL_REQ_STAT"));
				 P.setPARAM_CNCL_RSN_CODE(rs.getField("PARAM_CNCL_RSN_CODE"));
				 P.setPARAM_CNCL_RSN_DESC(rs.getField("PARAM_CNCL_RSN_DESC"));
				 P.setIS_BATCH_DEBIT(rs.getField("IS_BATCH_DEBIT"));
				 P.setT_ORG_DFI_QLFR(rs.getField("T_ORG_DFI_QLFR"));
				 P.setTICI_I2_CSID(rs.getField("TICI_I2_CSID"));
				 P.setINSTR_CODE_PROP(rs.getField("INSTR_CODE_PROP"));
				 P.setTXNS_BGN01(rs.getField("TXNS_BGN01"));
				 data.put(rs.getField("CUST_REF_NUM"), P);
				// break;
			 }
			 // System.out.println(hm);
		      HashMap<String,String> hm1=new HashMap<String,String>();
		      DataDBExcel e=new DataDBExcel();
		      HashMap<String,PSHPojo> fromdbl=new HashMap<String,PSHPojo>();
		      String workitemid = FileUtilities.GetFilevalue("VAR_PSHFILEWORKITEMID");
		      System.out.println("Workitemid:"+workitemid);
		      fromdbl=e.getDbcolumns(workitemid);
		      
		      //11341068
		      DataPSHExcel fv=new  DataPSHExcel();
		   /*
		      System.out.println("---------From excel----------------");
		  	for(Entry<String, PSHPojo> a:data.entrySet())
			{
				//System.out.println(a.getKey()+":"+a.getValue().toString());
			}
		    System.out.println("---------From database----------------");
			for(Entry<String, PSHPojo> a:fromdbl.entrySet())
			{
				//System.out.println(a.getKey()+":"+a.getValue().toString());
			}
		  	*/
		 fv= fv.comparerecords(data,fromdbl,fv);
		
		return fv;
		
	}
	
	public DataPSHExcel comparerecords(HashMap<String,PSHPojo> data,HashMap<String,PSHPojo> fromexcel,DataPSHExcel fv)
	{
		   StringBuffer sb=new StringBuffer();
		for(Entry<String, PSHPojo> a:fromexcel.entrySet())
		{
		    Object key = a.getKey();
		    PSHPojo exp=a.getValue();
		    //System.out.println("custrefnum ex:"+key);
		  //  System.out.println("Actual from ex:"+exp.toString());
		    PSHPojo actual=data.get(key);
		    String custfromdb=data.entrySet().stream().filter(x->x.getKey().equals(key)).findFirst().get().getKey();
		   // System.out.println("custrefnum db:"+custfromdb);
		  //  System.out.println("Actual from db:"+actual.toString());
		    if(exp.equals(actual))
    	    	
	    	  {
	    		 System.out.println(key+" :match");
	    	  }
	    	  else
	    		  
	    	  {
	    		  flag=true;
	    		  HashMap<String,String> exp2=  sethashmap(exp);
	    		  HashMap<String,String> db2=  sethashmap(actual);
	    		 StringBuffer se= compareeachdetail(exp2,db2);
	    		  System.out.println("Not match \n"+se.toString());
	    		  if(flag)
	    				  {
	    			  details.append(se);
	    				  }
	    		 // sb.append(key+" - "+"Expected:"+a.getValue()+" Actual:"+data.get(key)+"\n");
	    		  
	    	  }		
		}
		fv.setFlag(flag);
		fv.setDetails(details);
		return fv;
	}
	
	public StringBuffer compareeachdetail(HashMap<String,String> exp2,HashMap<String,String> db2)
	{
		StringBuffer sb=new StringBuffer();
		 for (Entry<String,String> entry : exp2.entrySet()) {
	    	    Object key = entry.getKey();
	    	    if(entry.getValue().equals(db2.get(key)))
	    	    	
	    	    	  {
	    	    		// System.out.println(key+" :match");
	    	    	  }
	    	    	  else
	    	    		  
	    	    	  {
	    	    		  sb.append(key+" - "+"Expected:"+exp2.get(key)+" Actual:"+db2.get(key)+"\n");
	    	    		  
	    	    	  }		    	    		
	    	}
		
		return sb;
		
	}
		public HashMap<String, String> sethashmap(PSHPojo P)
		{
			 HashMap<String,String> hm=new HashMap<String,String>();
		      hm.put("FILE_STATUS", P.getFILE_STATUS());
		      hm.put("TXN_TYPE",P.getTXN_TYPE());
		      hm.put("TXN_STATUS", P.getTXN_STATUS());
		      hm.put("QUEUEID", P.getQUEUEID());
		      hm.put("QUEUELABEL", P.getQUEUELABEL());
		      hm.put("TASKID", P.getTASKID());
		      hm.put("TASKLABEL", P.getTASKLABEL());
		      hm.put("EXCEPTION_ID", P.getEXCEPTION_ID());
		      hm.put("EXCEPTION_DESC", P.getEXCEPTION_DESC());
		      hm.put("EXTERNAL_EXCEPTION_ID", P.getEXTERNAL_EXCEPTION_ID());
		      hm.put("EXTERNAL_EXCEPTION_DESC", P.getEXTERNAL_EXCEPTION_DESC());
		      hm.put("CUST_REF_NUM", P.getCUST_REF_NUM());
		      hm.put("INIT_PRTY_BEI", P.getINIT_PRTY_BEI());
		      hm.put("CLIENT_ID", P.getCLIENT_ID());
		      hm.put("CLIENT_NAME", P.getCLIENT_NAME());
		      hm.put("CUST_ID", P.getCUST_ID());
		      hm.put("DR_NAME", P.getDR_NAME());
		      hm.put("DR_STREET", P.getDR_STREET());
		      hm.put("DR_BLDG_NO", P.getDR_BLDG_NO());
		      hm.put("DR_CITY", P.getDR_CITY());
		      hm.put("DR_STATE", P.getDR_STATE());
		      hm.put("DR_PSTL_CODE", P.getDR_PSTL_CODE());
		      hm.put("DR_AGT_NAME", P.getDR_AGT_NAME());
		      hm.put("DR_AGT_CITY", P.getDR_AGT_CITY());
		      hm.put("DR_AGT_STATE", P.getDR_AGT_STATE());
		      hm.put("DR_AGT_CTRY", P.getDR_AGT_CTRY());
		      hm.put("DR_AGT_PSTL_CD", P.getDR_AGT_PSTL_CD());
		      hm.put("DR_CTRY", P.getDR_CTRY());
		      hm.put("DR_ADDR1", P.getDR_ADDR1());
		      hm.put("DR_ADDR2", P.getDR_ADDR2());
		      hm.put("DR_ADDR3", P.getDR_ADDR3());
		      hm.put("DR_ACCT_NO", P.getDR_ACCT_NO());
		      hm.put("SETTL_METHOD", P.getSETTL_METHOD());
		      hm.put("DR_AGT_CLRING_SYS_CD", P.getDR_AGT_CLRING_SYS_CD());
		      hm.put("ACC_CCY", P.getACC_CCY());
		      hm.put("TXN_CUST_SM", P.getTXN_CUST_SM());
		      hm.put("DEBIT_AMOUNT", P.getDEBIT_AMOUNT());
		      hm.put("INSTR_AMOUNT", P.getINSTR_AMOUNT());
		      hm.put("LIMIT_CHK_EQUI_AMT", P.getLIMIT_CHK_EQUI_AMT());
		      hm.put("DEBIT_CURRENCY", P.getDEBIT_CURRENCY());
		      hm.put("INSTR_CURRENCY", P.getINSTR_CURRENCY());
		      hm.put("LIMIT_CHK_EQUI_CCY", P.getLIMIT_CHK_EQUI_CCY());
		      hm.put("BAL_CHK_EQUI_AMT", P.getBAL_CHK_EQUI_AMT());
		      hm.put("LIMIT_BAL_CHK_FLAG", P.getLIMIT_BAL_CHK_FLAG());
		      hm.put("INSTR_CODE1", P.getINSTR_CODE1());
		      hm.put("TXN_FX_ID", P.getTXN_FX_ID());
		      hm.put("FX_RATE_TYPE", P.getFX_RATE_TYPE());
		      hm.put("FX_CONTRACT_ID", P.getFX_CONTRACT_ID());
		      hm.put("FX_RATE", P.getFX_RATE());
		      hm.put("NOTIONAL_EXCH_RATE", P.getNOTIONAL_EXCH_RATE());
		      hm.put("CR_NAME", P.getCR_NAME());
		      hm.put("CR_CTRY", P.getCR_CTRY());
		      hm.put("CR_ADDR1", P.getCR_ADDR1());
		      hm.put("CR_ADDR2", P.getCR_ADDR2());
		      hm.put("CR_ADDR3", P.getCR_ADDR3());
		      hm.put("CR_AGT_NAME", P.getCR_AGT_NAME());
		      hm.put("CR_AGT_BIC", P.getCR_AGT_BIC());
		      hm.put("CR_AGT_CLRING_SYS", P.getCR_AGT_CLRING_SYS());
		      hm.put("CR_AGT_CLRING_SYS_CD", P.getCR_AGT_CLRING_SYS_CD());
		      hm.put("CR_ACC_NO", P.getCR_ACC_NO());
		      hm.put("CR_IBAN", P.getCR_IBAN());
		      hm.put("CR_AGT_CTRY", P.getCR_AGT_CTRY());
		      hm.put("CR_AGT_ADDR1", P.getCR_AGT_ADDR1());
		      hm.put("CR_AGT_ADDR2", P.getCR_AGT_ADDR2());
		      hm.put("CR_AGT_ADDR3", P.getCR_AGT_ADDR3());
		      hm.put("CHARGE_TYPE", P.getCHARGE_TYPE());
		      hm.put("CORR_FEE_AMT", P.getCORR_FEE_AMT());
		      hm.put("FEE_CCY", P.getFEE_CCY());
		      hm.put("CORR_FEE_ACC_NO", P.getCORR_FEE_ACC_NO());
		      hm.put("CORR_TRANSIT", P.getCORR_TRANSIT());
		      hm.put("REMIT_INFO_1", P.getREMIT_INFO_1());
		      hm.put("REMIT_INFO_2", P.getREMIT_INFO_2());
		      hm.put("REMIT_INFO_3", P.getREMIT_INFO_3());
		      hm.put("REMIT_INFO_4", P.getREMIT_INFO_4());
		      hm.put("REMT_STATUS_DESC", P.getREMT_STATUS_DESC());
		      hm.put("REMT_TYPE", P.getREMT_TYPE());
		      hm.put("SAR_LOC_MTD", P.getSAR_LOC_MTD());
		      hm.put("REMT_CR_LCFX", P.getREMT_CR_LCFX());
		      hm.put("REMT_CR_LCEM", P.getREMT_CR_LCEM());
		      hm.put("REMT_CR_NM_TXN", P.getREMT_CR_NM_TXN());
		      hm.put("REMT_CR_STRT_TXN", P.getREMT_CR_STRT_TXN());
		      hm.put("REMT_CR_BLDG_TXN", P.getREMT_CR_BLDG_TXN());
		      hm.put("REMT_CR_TW_TXN", P.getREMT_CR_TW_TXN());
		      hm.put("REMT_CR_SD_TXN", P.getREMT_CR_SD_TXN());
		      hm.put("REMT_CR_CTRY_TXN", P.getREMT_CR_CTRY_TXN());
		      hm.put("REMT_CR_PSTL_TXN", P.getREMT_CR_PSTL_TXN());
		      hm.put("INTER_AGT_NAME", P.getINTER_AGT_NAME());
		      hm.put("INTER_AGT_BIC", P.getINTER_AGT_BIC());
		      hm.put("INTER_AGT_CLRING_SYS", P.getINTER_AGT_CLRING_SYS());
		      hm.put("INTER_AGT_CLRING_SYS_CD", P.getINTER_AGT_CLRING_SYS_CD());
		      hm.put("RBPMT_STATUS_CD", P.getRBPMT_STATUS_CD());
		      hm.put("RBPMT_STATUS_DESC", P.getRBPMT_STATUS_DESC());
		      hm.put("RBFEE_STATUS_CD", P.getRBFEE_STATUS_CD());
		      hm.put("RBFEE_STATUS_DESC", P.getRBFEE_STATUS_DESC());
		      hm.put("RBPMT_STATUS_RSN_CD", P.getRBPMT_STATUS_RSN_CD());
		      hm.put("RBPMT_STATUS_RSN_DESC", P.getRBPMT_STATUS_RSN_DESC());
		      hm.put("RBFEE_STATUS_RSN_CD", P.getRBFEE_STATUS_RSN_CD());
		      hm.put("RBFEE_STATUS_RSN_DESC", P.getRBFEE_STATUS_RSN_DESC());
		      hm.put("UMMREV_STAT_CD", P.getUMMREV_STAT_CD());
		      hm.put("UMMREV_STAT_DESC", P.getUMMREV_STAT_DESC());
		      hm.put("FEEREV_STAT_CD", P.getFEEREV_STAT_CD());
		      hm.put("FEEREV_STAT_DESC", P.getFEEREV_STAT_DESC());
		      hm.put("UMMREV_RSN_CD", P.getUMMREV_RSN_CD());
		      hm.put("UMMREV_RSN_DESC", P.getUMMREV_RSN_DESC());
		      hm.put("FEEREV_RSN_CD", P.getFEEREV_RSN_CD());
		      hm.put("FEEREV_RSN_DESC", P.getFEEREV_RSN_DESC());
		      hm.put("TXN_CLGSYSREF", P.getTXN_CLGSYSREF());
		      hm.put("IWS_STATUS_CD", P.getIWS_STATUS_CD());
		      hm.put("IWS_STATUS_DESC", P.getIWS_STATUS_DESC());
		      hm.put("CLR_SYSM_CODE", P.getCLR_SYSM_CODE());
		      hm.put("TXN_SWIFT_UETR", P.getTXN_SWIFT_UETR());
		      hm.put("TXN_GOW_NOTF_FLG", P.getTXN_GOW_NOTF_FLG());
		      hm.put("TXN_GOW_ACCT_STAT", P.getTXN_GOW_ACCT_STAT());
		      hm.put("NOC_FLAG", P.getNOC_FLAG());
		      hm.put("LCL_REC_TYPEID", P.getLCL_REC_TYPEID());
		      hm.put("INIT_PRTY_ID", P.getINIT_PRTY_ID());
		      hm.put("ACNO_RETURN", P.getACNO_RETURN());
		      hm.put("TXNS_OTI08", P.getTXNS_OTI08());
		      hm.put("TFRI_PUR_FT", P.getTFRI_PUR_FT());
		      hm.put("TIBI_CA_ID", P.getTIBI_CA_ID());
		      hm.put("NOC_CHG_CODE", P.getNOC_CHG_CODE());
		      hm.put("TPOI_IP_NM2", P.getTPOI_IP_NM2());
		      hm.put("NOC_CORR_INFO", P.getNOC_CORR_INFO());
		      hm.put("B_SRVCLVL_CD", P.getB_SRVCLVL_CD());
		      hm.put("TFBI_CR_ACTYFT", P.getTFBI_CR_ACTYFT());
		      hm.put("CR_CITY", P.getCR_CITY());
		      hm.put("CR_STATE", P.getCR_STATE());
		      hm.put("CR_PSTL_CD", P.getCR_PSTL_CD());
		      hm.put("ADDITONAL_INFO", P.getADDITONAL_INFO());
		      hm.put("TRN_STATUS_CODE", P.getTRN_STATUS_CODE());
		      hm.put("UMM_STAT_CD", P.getUMM_STAT_CD());
		      hm.put("UMM_STAT_CD_DESC", P.getUMM_STAT_CD_DESC());
		      hm.put("UMM_PSH_STAT_CD", P.getUMM_PSH_STAT_CD());
		      hm.put("UMM_PSH_STAT_CD_DESC", P.getUMM_PSH_STAT_CD_DESC());
		      hm.put("CHQ_DLVY_MTD", P.getCHQ_DLVY_MTD());
		      hm.put("CR_STREET", P.getCR_STREET());
		      hm.put("CR_BLDG_NO", P.getCR_BLDG_NO1());
		      hm.put("CHQ_FRM_NM", P.getCHQ_FRM_NM());
		      hm.put("CHQ_FRM_STRT", P.getCHQ_FRM_STRT());
		      hm.put("CHQ_FRM_BLDG", P.getCHQ_FRM_BLDG());
		      hm.put("CHQ_FRM_TW", P.getCHQ_FRM_TW());
		      hm.put("CHQ_FRM_SD", P.getCHQ_FRM_SD());
		      hm.put("CHQ_FRM_CTRY", P.getCHQ_FRM_CTRY());
		      hm.put("CHQ_FRM_PSTL", P.getCHQ_FRM_PSTL());
		      hm.put("CHQ_TO_NM", P.getCHQ_TO_NM());
		      hm.put("CHQ_TO_STRT", P.getCHQ_TO_STRT());
		      hm.put("CHQ_TO_BLDG", P.getCHQ_TO_BLDG());
		      hm.put("CHQ_TO_TW", P.getCHQ_TO_TW());
		      hm.put("CHQ_TO_SD", P.getCHQ_TO_SD());
		      hm.put("CHQ_TO_CTRY", P.getCHQ_TO_CTRY());
		      hm.put("CHQ_TO_PSTL", P.getCHQ_TO_PSTL());
		      hm.put("CR_LANG_PREF", P.getCR_LANG_PREF());
		      hm.put("CR_EMAIL", P.getCR_EMAIL());
		      hm.put("CR_MOB_NB", P.getCR_MOB_NB());
		      hm.put("RESP_CODE", P.getRESP_CODE());
		      hm.put("RESP_DESC", P.getRESP_DESC());
		      hm.put("RESP_REF_NUM", P.getRESP_REF_NUM());
		      hm.put("TXN_RTP_INTERAC_REF_NUM", P.getTXN_RTP_INTERAC_REF_NUM());
		      hm.put("TXN_RTP_REF_NUM", P.getTXN_RTP_REF_NUM());
		      hm.put("TXN_CONTACT_ID", P.getTXN_CONTACT_ID());
		      hm.put("TXN_STSUPD_CD", P.getTXN_STSUPD_CD());
		      hm.put("CUST_CNCL_ID", P.getCUST_CNCL_ID());
		      hm.put("FI_CANC_REF_NO", P.getFI_CANC_REF_NO());
		      hm.put("PARAM_CNCL_REQ_DATE", P.getPARAM_CNCL_REQ_DATE());
		      hm.put("PARAM_CNCL_REQ_STAT", P.getPARAM_CNCL_REQ_STAT());
		      hm.put("PARAM_CNCL_RSN_CODE", P.getPARAM_CNCL_RSN_CODE());
		      hm.put("PARAM_CNCL_RSN_DESC", P.getPARAM_CNCL_RSN_DESC());
		      hm.put("IS_BATCH_DEBIT", P.getIS_BATCH_DEBIT());
		      hm.put("T_ORG_DFI_QLFR", P.getT_ORG_DFI_QLFR());
		      hm.put("TICI_I2_CSID", P.getTICI_I2_CSID());
		      hm.put("INSTR_CODE_PROP", P.getINSTR_CODE_PROP());
		      hm.put("TXNS_BGN01", P.getTXNS_BGN01());	
		      return hm;
		}
		public boolean getFlag() {
			return flag;
		}


		public void setFlag(boolean flag) {
			this.flag = flag;
		}


		public StringBuffer getDetails() {
			return details;
		}


		public void setDetails(StringBuffer details) {
			this.details = details;
		}
}

package Constants;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

public class TestParameters {

	private static String SUITE_USER;
	public static String getSUITE_USER() {
		return SUITE_USER;
	}
	public static void setSUITE_USER(String sUITE_USER) {
		SUITE_USER = sUITE_USER;
	}
	public static void main(String[] args) throws FilloException {
		// TODO Auto-generated method stub
		setConfigValue("SUITE-MA-EFT");

	}
	
	public static void setConfigValue(String suite) 
	{
		Connection conn=null;
		Recordset rs=null;
		try
		{
Fillo fill=new Fillo();
		
	conn=fill.getConnection(System.getProperty("user.dir")+"\\src"+"\\test"+"\\resources"+"\\TestData\\"+suite+"\\TestCase_Steps.xls");
	 rs=conn.executeQuery("Select * from Config");
		while (rs.next()) {
				String config=rs.getField("Config");
				setSUITE_USER(config);
			System.out.println("Suite User Value:"+getSUITE_USER());
		
	}
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
			e.printStackTrace();
		}
		finally
		{
			rs.close();
			conn.close();	
		}

}
}

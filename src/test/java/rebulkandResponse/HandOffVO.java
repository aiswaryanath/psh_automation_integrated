/****************************************************************************/
/* Copyright �  2015 Intellect Design Area Ltd. All rights reserved      	*/
/*                                                                      	*/
/****************************************************************************/
/*  Application  : Intellect Payment Engine 								*/
/*                            												*/
/*  Module Name  : Automation Application               	     			*/
/*  File Name    : HandOffVO.java               		  						*/
/*                                            	                          	*/
/*  Description  :															*/
/*                 	                                                		*/
/*  Author       : Nitin Chourey       										*/
/****************************************************************************/
/* Version Control Block                                              		*/
/* ~~~~~~~ ~~~~~~~ ~~~~~                                              		*/
/*                                                                    		*/
/*    Date    	Version    Author          	Ref.            Description 	*/
/*    ====    	=======    ======          ====             ===========
 / * 18-Jul-2017   1.0    nikita.shirodkar  				Initial Version
 / *
 /****************************************************************************/
package rebulkandResponse;

import org.apache.log4j.Logger;

/**
 * @author nikita.shirodkar this function is used to save data from database and
 *         use using getter setter methods
 */
public class HandOffVO
{
	private static Logger	logger	          = Logger.getLogger("CIBC");
	private String	      strHRFORebulkId	  = null;
	private String	      strHFOFileName	  = null;
	private String	      strHFOOriginatorID	= null;
	private String	      strHFOParentFileID	= null;
	private String	      strHFOPayIdID	      = null;
	private String	      strStatus	          = null;
	private String	      strBatchRes1	      = null;
	private String	      strHFOFileId	      = null;
	private String	      strTxnResp	      = null;
	private String	      strRebulkOpn1	      = null;
	private String	      strTcId	          = null;
	private String	      strEFTChk	          = null;
	private String	      strEODRep	          = null;
	private String	      strFilename	      = null;

	private String	      StrOrg1	          = null;
	private String	      StrOrg2	          = null;
	private String	      StrOrg3	          = null;
	private String	      StrOrg4	          = null;
	private String	      StrOrg5	          = null;
	private static String	nStruniqueChnlRef	= null;
	private String	      fileErrCode;
	private String	      batchErrCode;
	private String	      txnErrCode;
	private String	      multiErrChk;

	public String getFileErrCode()
	{
		return fileErrCode;
	}

	public void setFileErrCode(String fileErrCode)
	{
		this.fileErrCode = fileErrCode;
	}

	public String getBatchErrCode()
	{
		return batchErrCode;
	}

	public void setBatchErrCode(String batchErrCode)
	{
		this.batchErrCode = batchErrCode;
	}

	public String getTxnErrCode()
	{
		return txnErrCode;
	}

	public void setTxnErrCode(String txnErrCode)
	{
		this.txnErrCode = txnErrCode;
	}

	public String getMultiErrChk()
	{
		return multiErrChk;
	}

	public void setMultiErrChk(String multiErrChk)
	{
		this.multiErrChk = multiErrChk;
	}

	private final String	nstrFilefrm	= null;

	public void setHRFORebulkId(String nstrHRFORebulkId)
	{
		this.strHRFORebulkId = nstrHRFORebulkId;
	}

	public String getHRFORebulkId()
	{
		return this.strHRFORebulkId;
	}

	public void setHFOFileName(String nstrHFOFileName)
	{
		this.strHFOFileName = nstrHFOFileName;
	}

	public String getHFOFileName()
	{
		return this.strHFOFileName;
	}

	public void setHFOOriginatorID(String nstrHFOOriginatorID)
	{
		this.strHFOOriginatorID = nstrHFOOriginatorID;
	}

	public String getHFOOriginatorID()
	{
		return this.strHFOOriginatorID;
	}

	public void setHFOParentFileID(String nstrHFOParentFileID)
	{
		this.strHFOParentFileID = nstrHFOParentFileID;
	}

	public String getHFOParentFileID()
	{
		return this.strHFOParentFileID;
	}

	public void setHFOPayIdID(String nstrHFOPayIdID)
	{
		this.strHFOPayIdID = nstrHFOPayIdID;
	}

	public String getHFOPayIdID()
	{
		return this.strHFOPayIdID;
	}

	public void printVo()
	{
		System.out.println("HRFORebulkId = " + strHRFORebulkId + " HFOFileName :" + strHFOFileName + "HFOOriginatorID "
		        + strHFOOriginatorID + " HFOParentFileID :" + strHFOParentFileID + "HFOPayIdID :" + strTcId);

	}

	public void setStrStatus(String nStrStatus)
	{
		this.strStatus = nStrStatus;
	}

	public String getStrStatus()
	{
		return this.strStatus;
	}

	public void setStrBatchRes(String nStrBatchRes1)
	{
		this.strBatchRes1 = nStrBatchRes1;
	}

	public String getStrBatchRes()
	{
		return this.strBatchRes1;
	}

	public void setStrTxnResp(String nStrTxnResp)
	{
		// removed as the code is change in PSH
		this.strTxnResp = nStrTxnResp.replace(" ", ",");
	}

	public String getStrTxnResp()
	{
		return this.strTxnResp;
	}

	public void setStrRebulkOpn(String nStrRebulkOpn1)
	{
		this.strRebulkOpn1 = nStrRebulkOpn1;
	}

	public String getStrRebulkOpn()
	{
		return this.strRebulkOpn1;
	}

	public void setStrTestcaseId(String nstrTcId)
	{
		this.strTcId = nstrTcId;
	}

	public String getStrTestcaseId()
	{
		return this.strTcId;
	}

	// public void setStrFileForm(String nstrFilefrm)
	// {
	// this.nstrFilefrm = nstrFilefrm;
	// }
	//
	// public String getStrFileForm()
	// {
	// return this.nstrFilefrm;
	// }

	public void setStrEFTChk(String nstrEFTChk)
	{
		this.strEFTChk = nstrEFTChk;
	}

	public String getStrEFTChk()
	{
		return this.strEFTChk;
	}

	public void setIntEODRep(String nStrEODRep)
	{
		this.strEODRep = nStrEODRep;
	}

	public String getStrEODRep()
	{
		return this.strEODRep;
	}

	/**
	 * @param nStrHFOFileID
	 */
	public void setHFOFileWorkitemId(String nStrHFOFileID)
	{
		this.strHFOFileId = nStrHFOFileID;
	}

	public String getStrHFOFileWorkitemID()
	{
		return this.strHFOFileId;
	}

	/**
	 * @param nString
	 */
	public void setStrFileName(String nStrFilename)
	{
		this.strFilename = nStrFilename;

	}

	public String getStrFileName()
	{
		return this.strFilename;

	}

	public void setStrOriginator1(String nStrOrig1)
	{
		this.StrOrg1 = nStrOrig1;

	}

	public String getStrOriginator1()
	{
		return this.StrOrg1;

	}

	public String getUnique_Channel_REF()
	{
		System.out.println("nStruniqueChnlRef---->" + nStruniqueChnlRef);
		return nStruniqueChnlRef;

	}

	public void setUnique_Channel_REF(String nStruniqueChnlRef)
	{
		this.nStruniqueChnlRef = nStruniqueChnlRef;

	}

	public void setStrOriginator2(String nStrOrig2)
	{
		this.StrOrg2 = nStrOrig2;

	}

	public String getStrOriginator2()
	{
		return this.StrOrg2;

	}

	public void setStrOriginator3(String nStrOrig3)
	{
		this.StrOrg3 = nStrOrig3;

	}

	public String getStrOriginator3()
	{
		return this.StrOrg3;

	}

	public void setStrOriginator4(String nStrOrig4)
	{
		this.StrOrg4 = nStrOrig4;

	}

	public String getStrOriginator4()
	{
		return this.StrOrg4;

	}

	public void setStrOriginator5(String nStrOrig5)
	{
		this.StrOrg5 = nStrOrig5;

	}

	public String getStrOriginator5()
	{
		return this.StrOrg5;

	}
}

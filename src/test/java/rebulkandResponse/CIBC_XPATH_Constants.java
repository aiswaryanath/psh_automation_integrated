/****************************************************************************/
/* Copyright �  2015 Intellect Design Area Ltd. All rights reserved      	*/
/*                                                                      	*/
/****************************************************************************/
/*  Application  : Intellect Payment Engine 								*/
/*                            												*/
/*  Module Name  : Automation Application               	     			*/
/*  File Name    : CIBC_XPATH_Constants.java                 					*/
/*                                                                      	*/
/*  Description  :															*/
/*                 	                                                		*/
/*  Author       : Nitin Chourey       										*/
/****************************************************************************/
/* Version Control Block                                              		*/
/* ~~~~~~~ ~~~~~~~ ~~~~~                                              		*/
/*                                                                    		*/
/*    Date    	Version    Author          	Ref.            Description 	*/
/*    ====    	=======    ======          ====             ===========
 / * 18-Jul-2017   1.0      Nitin C  						Initial Version
 / *
 /****************************************************************************/
package rebulkandResponse;

public interface CIBC_XPATH_Constants
{
	public static final String	CONFIGPATH					= System.getProperty("user.dir")
	        + "\\Config\\config.properties";
	public static final String	ORPATH						= System.getProperty("user.dir")
	        + "\\Object_Repository\\OR.properties";
	public static final String	USER_ACTION_SUMMARYPTH		= System.getProperty("user.dir")
	        + "\\Resources\\USER_ACTION_SUMMARY.txt";
	public static final String	HITDETAILSTATUSVIEWPTH		= System.getProperty("user.dir")
	        + "\\Resources\\HIT_DETAIL_STATUS_VIEW.txt";
	public static final String	HITSUMMARYSTATUSVIEWPTH		= System.getProperty("user.dir")
	        + "\\Resources\\HIT_SUMMARY_STATUS_VIEW.txt";
	public static final String	SCRHITDETAILSTATUSVIEWPTH	= System.getProperty("user.dir")
	        + "\\Resources\\SCR_HIT_DETAIL_STATUS_VIEW.txt";
	public static final String	SCRHITSUMMARYSTATUSVIEWPTH	= System.getProperty("user.dir")
	        + "\\Resources\\SCR_HIT_SUMMARY_STATUS_VIEW.txt";
	public static final String	alertIDpth					= System.getProperty("user.dir")
	        + "\\Resources\\ALERT_ID.txt";
	public static final String	CHECKPATH					= System.getProperty("user.dir")
	        + "\\Resources\\CHECK_FLAG.txt";
	public static final String	CLOSEPATH					= System.getProperty("user.dir")
	        + "\\Resources\\CLOSE_FLAG.txt";
	public static final String	DRIVER						= "DRIVER";
	public static final String	DRIVERPATH					= "DRIVERPATH";
	public static final String	CHROMEDRIVER				= "CHROMEDRIVER";
	public static final String	LOGLEVELATT					= "LOGLEVELATT";
	public static final String	LOGLEVEL					= "LOGLEVEL";
	public static final String	LOGFILEATT					= "LOGFILEATT";
	public static final String	LOGFILE						= "LOGFILE";
	public static final String	pageLoadStrategy_key		= "pageLoadStrategy";
	public static final String	pageLoadStrategy_val		= "eager";

	public static final String	clickLinkByXPath_XPath1		= "//*[contains(text(),'";
	public static final String	clickLinkByXPath_XPath3		= "')]";

	public static final String	MenuFrame_Val				= "MenuFrame";
	public static final String	SearchFrame_Val				= "SearchFrame";

	// Sayali Hande Start
	public static final String	IwMiddleFrame_Val			= "IwMiddleFrame";
	public static final String	IwTopFrame_Val				= "IwTopFrame";
	public static final String	IwBottomFrame_Val			= "IwBottomFrame";
	public static final String	IwEntityTopFrame_Val		= "IwEntityTopFrame";
	public static final String	IwEntityBottomFrame_Val		= "IwEntityBottomFrame";
	// End
	// Login page
	public static final String	UserName_label				= "ArmorTicket";

	// Workspace Picklist
	public static final String	Workspace_window			= "Workspace Picklist";

	// BPS page
	public static final String	TabFrame_Val				= "TabFrame";

	// File Inquiry-Search criteria
	public static final String	fileName_ID					= "S1_F9";
	public static final String	fileToDate_ID				= "S1_F11";
	public static final String	fileFromDate_ID				= "S1_F12";
	public static final String	fileSearch_ID				= "srchButton";

	// Physical File Details
	public static final String	logicalFile_ID				= "linkV14_R54";

	// file Inquiry Result
	public static final String	tableXPathResult_Xpath		= "S1_F9";

	public static final String	tableXPathResult			= "//table[@id='result_tbl']";
	public static final String	tableXpath1					= "//*[@id='pagehdr']/tbody/tr[1]/td";

	// Logical File Details
	public static final String	logicalFileDetails_winhdl	= "Logical File Details";
	public static final String	tranCount_ID				= "linkV14_R53";
	public static final String	batchCount_ID				= "linkV13_R22";

	// Batch Details
	public static final String	batchstatus_ID				= "textV23_RB1105";
	public static final String	batchref_ID					= "textV23_RB1101";

	// REbulk constant
	public static String		JSPURL						= "JSPURL";
	public static String		JSP1URL						= "JSP1URL";
	public static String		JSP2URL						= "JSP2URL";
	public static String		JSP3URL						= "JSP3URL";
	public static String		JSP4URL						= "JSP4URL";
	public static String		JSP5URL						= "JSP5URL";
	public static String		JSP6URL						= "JSP6URL";

	public static String		strRebulkPathFROMSTUB		= "strRebulkPathFROMSTUB";
	public static String		strEFTStubPathTOPSH			= "strEFTStubPathTOPSH";
	public static String		strACHStubPathTOPSH			= "strACHStubPathTOPSH";
	public static String		CNRUploadLatestFile			= "CNRUploadLatestFile";
	public static String		strCNRStubPathTOPSH			= "strCNRStubPathTOPSH";
	public static String		strRebulkPathTOSTUB			= "strRebulkPathTOSTUB";
	public static String		strEFTStubPathFROMPSH		= "strEFTStubPathFROMPSH";
	public static String		strACHStubPathFROMPSH		= "strACHStubPathFROMPSH";
	public static String		strCHKStubPathFROMPSH		= "strCHKStubPathFROMPSH";
	public static String		strSARStubPathFROMPSH		= "strSARStubPathFROMPSH";
	public static String		dbURLFGT					= "dbURLFGT";
	public static String		userNameFGT					= "userNameFGT";
	public static String		passwordFGT					= "passwordFGT";
	public static String		dbURLOLDFGT					= "dbURLOLDFGT";
	public static String		userNameOLDFGT				= "userNameOLDFGT";
	public static String		passwordOLDFGT				= "passwordOLDFGT";
	public static String		ASYNCJSP					= "ASYNCJSP";
	public static String		MT900JSP					= "MT900JSP";
	public static String		MT199JSP					= "MT199JSP";
	public static String		GOWSTUBJSP					= "GOWSTUBJSP";
	public static String		SCHEDULER					= "SCHEDULER";
	public static String		dbURLs						= "dbURLs";
	public static String		userNames					= "userNames";
	public static String		passwords					= "passwords";
	public static String		dbURL						= "dbURL";
	public static String		userName					= "userName";
	public static String		password					= "password";
	public static String		BBD_URL						= "BBD_URL";
	public static String		BBD_URL_DELETE				= "BBD_URL_DELETE";
	public static String		DB_QUERY					= "Select * From Txn_Proc_Opn Where file_workitem_id in ( select workitemid From File_Proc_Opn where file_name like ? ) and cust_ref_num= ? ";
	public static String		WireExtRefNum				= "wire_ext_ref_num";
	public static String		Env2_HOST_NAME				= "Env2_HOST_NAME";
	public static String		Env2_LOGIN					= "Env2_LOGIN";
	public static String		Env2_PASSWORD				= "Env2_PASSWORD";

}

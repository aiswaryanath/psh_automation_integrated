package rebulkandResponse;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Utilities.FileUtilities;
import Utilities.PropertyUtils;

public class GetRubulkID
{
	public static ArrayList<String> GetRebulKidOrgId(String WORKITEMID,String type)
	{
		ArrayList<String> GetRebulKidOrgId = null;
		try
		{
			
			Class.forName("oracle.jdbc.driver.OracleDriver");
			String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
			String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
			String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
			Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			ResultSet ResultSet = getResultSet(connection, "handoff_file_opn", WORKITEMID,type);
			try
			{
				while (ResultSet.next())
				{
					GetRebulKidOrgId = new ArrayList<String>();
					System.out.println("REBULKID:" + ResultSet.getString("HFO_REBULKID"));
					GetRebulKidOrgId.add(ResultSet.getString("HFO_REBULKID"));
					System.out.println("ORIGINATOR_ID:" + ResultSet.getString("HFO_ORIGINATOR_ID"));
					GetRebulKidOrgId.add(ResultSet.getString("HFO_ORIGINATOR_ID"));
					System.out.println("FILE_NAME:" + ResultSet.getString("HFO_FILE_NAME"));
					GetRebulKidOrgId.add(ResultSet.getString("HFO_FILE_NAME"));
					System.out.println("fcn:" + ResultSet.getString("HFO_FCN"));
					GetRebulKidOrgId.add(ResultSet.getString("HFO_FCN"));


				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				connection.close();
				ResultSet.close();
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return GetRebulKidOrgId;
	}

	public static ArrayList<String> ConnectToFileProcOpn(String FileName)
	{
		ArrayList<String> GetRebulKidOrgId = null;
		try
		{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
			String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
			String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
			Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			String workitemid = FileUtilities.GetFilevalue("VAR_PSHFILEWORKITEMID");
			ResultSet ResultSet = getResultSetTestToolFileName(connection, "TEST_TOOL_FILE_NAME", workitemid);
			try
			{
				while (ResultSet.next())
				{
					GetRebulKidOrgId = new ArrayList<String>();
					System.out.println("WORKITEMID:" + ResultSet.getString("WORKITEMID"));
					GetRebulKidOrgId.add(ResultSet.getString("WORKITEMID"));
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				connection.close();
				ResultSet.close();
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return GetRebulKidOrgId;
	}

	public static ArrayList<String> GetWorkItemIDList(String FileName)
	{
		ArrayList<String> GetRebulKidOrgId = new ArrayList<String>();
		try
		{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
			String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
			String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
			Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			ResultSet ResultSet = getResultSetTestToolFileName(connection, "TEST_TOOL_FILE_NAME", FileName);
			try
			{
				while (ResultSet.next())
				{

					System.out.println("WORKITEMID:" + ResultSet.getString("WORKITEMID"));
					GetRebulKidOrgId.add(ResultSet.getString("WORKITEMID"));
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				connection.close();
				ResultSet.close();
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return GetRebulKidOrgId;
	}

	public static ResultSet getResultSet(final Connection connection, final String tableName, String file_workitemid,String type)
	{
		String query = "select HFO_REBULKID,HFO_ORIGINATOR_ID,HFO_FILE_NAME,HFO_FCN from handoff_file_opn where"
				+ " hfo_file_workitemid ='"
		        + file_workitemid + "'"+ " and hfo_rebulkid= '"+ type + "'";
		      
		System.out.println(query);
		return executeQuery(connection, query);
	}

	public static ResultSet getResultSetCNR(final Connection connection, final String tableName, String file_workitemid,
	        String CUST_REF_NUM)
	{
		
		String workitemid = FileUtilities.GetFilevalue("VAR_PSHFILEWORKITEMID");
		String query = "Select FILE_WORKITEM_ID from TXN_PROC_OPN where FILE_WORKITEM_ID IN ('" + workitemid
		        + "') and CUST_REF_NUM = '" + CUST_REF_NUM + "'";
		System.out.println(query);
		return executeQuery(connection, query);
	}

	public static ResultSet getResultSetTestToolFileName(final Connection connection, final String tableName,
	        String file_workitemid)
	{
		String query = "SELECT WORKITEMID, MESSAGE_ID, FILE_STATUS, FILE_NAME FROM FILE_PROC_OPN where WORKITEMID like '%"
		        + file_workitemid + "%'";
		System.out.println(query);
		return executeQuery(connection, query);
	}

	private static final ResultSet executeQuery(final Connection connection, final String query)
	{
		try
		{
			return connection.createStatement().executeQuery(query);
		}
		catch (SQLException e)
		{
			throw new RuntimeException(e);
		}
	}

	public static ArrayList<String> GetRebulKidOrgIdFileNameCNR(String WORKITEMID, String CUST_REF_NUM)
	{
		ArrayList<String> GetRebulKidOrgId = null;
		try
		{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
			String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
			String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
			Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			ResultSet ResultSet = getResultSetCNR(connection, "TXN_PROC_OPN", WORKITEMID, CUST_REF_NUM);
			try
			{
				while (ResultSet.next())
				{
					GetRebulKidOrgId = new ArrayList<String>();
					System.out.println("FILE_WORKITEM_ID:" + ResultSet.getString("FILE_WORKITEM_ID"));
					GetRebulKidOrgId.add(ResultSet.getString("FILE_WORKITEM_ID"));

				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				connection.close();
				ResultSet.close();
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return GetRebulKidOrgId;
	}

	public static ArrayList<String> DBConnect(String SQL, String COLUMN, String ID, String Condition)
	{
		ArrayList<String> GetRebulKidOrgId = null;
		try
		{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
			String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
			String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
			Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			        
			ResultSet ResultSet = _getResult(connection, SQL, ID, Condition);
			try
			{
				while (ResultSet.next())
				{
					GetRebulKidOrgId = new ArrayList<String>();
					System.out.println("COLUMN:" + ResultSet.getString(COLUMN));
					GetRebulKidOrgId.add(ResultSet.getString(COLUMN));
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				connection.close();
				ResultSet.close();
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return GetRebulKidOrgId;
	}

	public static ResultSet _getResult(final Connection connection, final String SQL, String ID, String Condition)
	{
		String query = SQL + " like '%" + ID + "%'" + Condition;
		System.out.println(query);
		return executeQuery(connection, query);
	}

	public static void main(String[] args)
	{
		String HFO_ID_sar = GetRubulkID.DBConnect(
		        "Select hfo_file_workitemid,HFO_REBULKID,HFO_ORIGINATOR_ID,HFO_FILE_NAME from handoff_file_opn where hfo_file_workitemid",
		        "HFO_REBULKID", "8164035", " and HFO_PAY_TYPE= 'SAR'").get(0);
		System.out.println(HFO_ID_sar);

		ArrayList<String> _FileName_sar = new ArrayList<String>();
		_FileName_sar = GetRubulkID.DBConnect(
		        "Select hfo_file_workitemid,HFO_REBULKID,HFO_ORIGINATOR_ID,HFO_FILE_NAME from handoff_file_opn where hfo_file_workitemid",
		        "HFO_FILE_NAME", "8164035", " and HFO_PAY_TYPE= 'SAR'");

		String _FileNameString_sar = _FileName_sar.get(0);
		System.out.println(_FileNameString_sar);

	}

}

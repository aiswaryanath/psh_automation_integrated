package rebulkandResponse;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;


/*import org.CIBC_Automation.VO.HandOffVO;
import org.CIBC_Automation.VO.RespRebulkVO;
import org.CIBC_Automation.driver.Action_Keywords;
import org.CIBC_Automation.driver.CIBC_XPATH_Constants;
import org.CIBC_Automation.driver.IOUtilities;
import org.CIBC_Automation.driver.PSHDAO;
import org.CIBC_Automation.functionlib.Utilities;*/
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import Logger.LoggerUtils;
import Utilities.FileUtilities;
import Utilities.PropertyUtils;
import rebulkandResponse.GetRubulkID;
//import Utility_FileMoveMovement.GetRubulkID;
import file_Driver.SFTPConnect;
import file_Driver.SFTP_FileUpload;
import file_Driver.SFTP_PSH_exe;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Action_Keywords 
{

	public static WebDriver					driver;
	public static WebDriverWait				wait				= null;
	//private static Utilities				util;
	private static String					checkFlag			= "Y";
	private static String					closeFlag			= "Y";
	private static boolean					result				= false;
	private static String					errorDesc			= "NA";
	private static HashMap					physical_details	= new HashMap();
	private static HashMap<String, String>	manualActionIds		= new HashMap();
	private static List<String>				wids				= new ArrayList();
	private static int						nPayInquiryCnt		= 0;
	private static int						nBatchInqCnt		= 0;
	private static int						nresultCnt			= 0;
	private static final String				ORPATH				= System.getProperty("user.dir")
	        + "\\Object_Repository\\OR.properties";
	private static final String				Key					= null;
	private static String					configpath			= System.getProperty("user.dir")
	        + "\\Config\\config.properties";
	public static boolean					CustRefSimilar		= true;
	private static List						lManualActionIDS	= null;
	private static List<String>				bvIds				= null;
	private static String					strbatchno			= null;
	private final String					strWorkItemIds		= null;
	private static HashMap					rebulk_details		= new HashMap();
	public static HashMap					iwsMap				= new HashMap<String, String>();
	public static HashMap					data				= new HashMap<String, String>();
	public static String					r1					= null;
	public static String					Description1		= null;
	public static String					r					= null;
	public static String					Description			= null;
	public static HashMap<String, HashMap>	sdata				= new HashMap<String, HashMap>();
	public static HashMap<String, HashMap>	sbdata				= new HashMap<String, HashMap>();
	public static HashMap<String, String>	record				= new HashMap<String, String>();
	public static ArrayList					fileNameRecords		= new ArrayList();
	public static Logger logger = LoggerUtils.getLogger();
	public static boolean					focus_flg			= true;
	private static List						exceldata			= null;
	private static ArrayList<String>		groupExcelData		= new ArrayList();
	private static ArrayList<String>		gowExcelData		= new ArrayList();
	private static ArrayList				finalList			= new ArrayList();
	public static ArrayList<String>			objBulkExcelData	= new ArrayList();
	public static boolean					filestatusflg;
	private static HashMap<String, String>	precheckmap			= new <String, String> HashMap();
	private static HashMap<String, String>	precheckmap_report	= new <String, String> HashMap();
	private static ArrayList<String>		orgId				= new ArrayList();
	private static ArrayList<String>		accountNums			= new ArrayList();
	public static ArrayList<String>			Wiid_array			= new ArrayList<String>();
	
	
	public static void _purgeDirectory(String _path)
	{
		File dir = new File(_path);

		for (File file : dir.listFiles())
		{
			if (file.isDirectory())
				_purgeDirectory(_path);
			file.delete();
		}
	}
	
	
	
	public static void clearStubEntries(String sTestCaseID) throws Exception

	{

		StringBuffer strQuery = new StringBuffer();

		Connection conn = null;

		PreparedStatement pstmt = null;
	
		String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
		String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
		String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
		logger.info("clearStubEntries---->");
		System.out.println("clearStubEntries---->");
		try
		{
			strQuery.append("DELETE FROM STUB_CONFIG_RESPNSE_FILES");

			Class.forName("oracle.jdbc.driver.OracleDriver");
		    conn=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
	
			pstmt = conn.prepareStatement(strQuery.toString());
			int n = pstmt.executeUpdate();

			logger.info("Number of records deleted:" + n);
			System.out.println("Number of records deleted:" + n);
			try{
			conn.commit();
			}
			catch(Exception e)
			{
			System.out.println("Auto commit on");
			}
		}

		catch (Exception ex)

		{
			System.out.println("exception in clearstub entries:" + ex.getStackTrace());
			logger.fatal("exception in clearstub entries", ex);

			if (pstmt != null)

				pstmt.close();

			if (conn != null)

				conn.close();

		}

		finally

		{

			if (pstmt != null)

				pstmt.close();

			if (conn != null)

				conn.close();

		}

		logger.info("++++++++++++++++exit from getRebulkId++++++++++++++++++++");
		System.out.println("exit in clearstub entries");
	}
	

	
	
	public static void respRebulk_CSV_ACH(String tcid, String testdata)
	{
		String value = null;
		
			logger.info("-----Insided respRebulk--------------- " + testdata);
			ArrayList<String> orgList = new ArrayList<String>();
			RespRebulkVO objRespRebulkVO = null;
			HandOffVO objHandOffVO = null;
			//PSHDAO objPSHDAO = new PSHDAO();
			StringBuffer StrBuffVal = new StringBuffer();
			try
			{
				logger.info("Testdata " + testdata);
				System.out.println("first data");
				// step 1.code to read CSV from readRebulkResponseCSV
				lManualActionIDS = IOUtilities.readRebulkResponseData(tcid, testdata);
				logger.info("lManualActionIDS-->" + lManualActionIDS.size());

				for (int i = 0; i < lManualActionIDS.size(); i++)
				{
					System.out.println(lManualActionIDS.get(i));
				}
				if (lManualActionIDS != null)
				{
					// ArrayList nTemp = (ArrayList) lManualActionIDS;

					String strAction = "";
					String strResult = "";
					WebElement elementInput = null;
					// access via new for-loop
					String ORIGINATOR_ID = null;
					String FILE_NAME = null;
					String FCN = null;
					for (Object object : lManualActionIDS)
					{
						objRespRebulkVO = (RespRebulkVO) object;
						// Pass the logical file name and get the Hand off data
						// ****************code commented
						// 21-08-2017**********************
						// strbatchno = objRespRebulkVO.getStrEODRep();
						System.out.println(" ---1 ---" + objRespRebulkVO.getStrFileNm() + " ---2 ---"
						        + objRespRebulkVO.getStrStatus() + " ---3 ---" + objRespRebulkVO.getStrBatchRes()
						        + " --- 4 ---" + objRespRebulkVO.getStrOrgIDResp() + "----5-------"
						        + objRespRebulkVO.getStrEODRep());
						orgList = objRespRebulkVO.getStrOrgIDResp();
						logger.info(" ---1 ---" + objRespRebulkVO.getStrFileNm() + " ---2 ---"
						        + objRespRebulkVO.getStrStatus() + " ---3 ---" + objRespRebulkVO.getStrBatchRes()
						        + " --- 4 ---" + objRespRebulkVO.getStrOrgIDResp() + "----5-------"
						        + objRespRebulkVO.getStrEODRep());

						String[] orgStringArr = null;
						System.out.println(orgList.get(0));
						logger.info(orgList.get(0));
						orgStringArr = orgList.get(0).split("\\,");
						String desOrg= orgStringArr[0];
						String paytype = orgStringArr[1];
						System.out.println(orgStringArr[1]);
						logger.info(orgStringArr[1]);
						// Step2.getHandOffData call to get rebulk id and
						// other data
						// Sayali Change

						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\ACH");
						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\EFT");
						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FILEStubResp");
						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FILEStubRespEFT");
						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FileStubRespLatest");
						Action_Keywords
						        ._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FileStubRespLatestEFT");
						Action_Keywords
						        ._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FileStubRespLatestEFT");

						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FILEStubRespCNR");

						Action_Keywords
						        ._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FILEStubRespCNRLatest");

						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\CNR\\Pain001");

						Action_Keywords
						        ._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FILEStubRespSARLatest");

						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FILEStubRespSAR");

						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\CNR\\ISO_REMIT");
						SFTP_PSH_exe.clearRemotedirectory(PropertyUtils.readProperty("strRebulkPathFROMSTUB"));
						if (orgStringArr[1].equals("ACH"))
						{
							System.out.println("***********ACH**********");
							System.out.println("GetHandoffData for ACH" + objRespRebulkVO.getStrFileNm());
							logger.info("GetHandoffData for ACH" + objRespRebulkVO.getStrFileNm());
							ArrayList<String> ob_wid = new ArrayList<String>();
							ob_wid = GetRubulkID.ConnectToFileProcOpn(objRespRebulkVO.getStrFileNm());
							ArrayList<String> _wid = new ArrayList<String>();
							FileUtilities Fileutil = new FileUtilities();
							String rebulkId=Fileutil.GetFilevalue("VAR_REBULKID");
							_wid = GetRubulkID.GetRebulKidOrgId(ob_wid.get(0),rebulkId);
							String REBULKID = _wid.get(0);
							ORIGINATOR_ID = _wid.get(1);
							FILE_NAME = _wid.get(2);
							FCN = REBULKID.substring(2);
							String Server_PathACH = PropertyUtils.readProperty(CIBC_XPATH_Constants.strACHStubPathFROMPSH);
							       

							// SFTPConnect.SFTPfile_DownloadToLocal(
							// "/usr1/SIR18360/GTB_HOME/IPSH_HOME/filearea/outgoing/EFT_Mainframe/ACH/outgoing",
							// "D:/PSH_Automation/LOCAL_PAYMENT_FILE/ACH/",
							// FILE_NAME);

							System.out.println("File download for ACH" + FILE_NAME);
							logger.info("File download for ACH" + FILE_NAME);
							SFTPConnect.SFTPfile_DownloadToLocal(Server_PathACH,
									System.getProperty("user.dir")+"/src/test/resources/LOCAL_PAYMENT_FILE/ACH/", FILE_NAME);


							System.out.println("Check:" + testdata);

							for (int j = 0; j < orgStringArr.length; j++)
							{
								System.out.println("CHECK:" + orgStringArr[j]);
								logger.info("CHECK:" + orgStringArr[j]);
								
							}

							String OriginatorID = orgStringArr[0];
							String TXN_Responses = orgStringArr[3];
							clearStubEntries("SS");
							//System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\Driver\\chromedriver.exe");
							WebDriverManager.chromedriver().setup();
							ChromeOptions options = new ChromeOptions();
							options.setProxy(null);
							driver = new ChromeDriver(options);
	
			
							try
							{
								driver.get(PropertyUtils.readProperty(CIBC_XPATH_Constants.JSP1URL));
								driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
								driver.manage().window().maximize();

								driver.findElement(By.xpath("//*[@value='ACH']")).click();

								driver.findElement(By.id("originid")).sendKeys(OriginatorID);

								String TX_RESP = TXN_Responses.replace(" ", ",");

								driver.findElement(By.name("transaction_responses")).sendKeys(TX_RESP);
								Thread.sleep(7000);

								driver.findElement(By.name("save")).click();
								Thread.sleep(5000);
								// driver.close();
								// driver.findElement(By.xpath("//*[text()='Go
								// to Response Generator Page']")).click();
								String fileprocess = PropertyUtils.readProperty(CIBC_XPATH_Constants.JSP2URL);
								        
								driver.get(fileprocess);
								driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
								driver.manage().window().maximize();
								// FILE UPLOAD
								String strRebulkPathTOSTUB = PropertyUtils.readProperty(CIBC_XPATH_Constants.strRebulkPathTOSTUB);
								       
								// SFTP_FileUpload.UtilitySFTPUpload("D:/PSH_Automation/LOCAL_PAYMENT_FILE/ACH",
								// "/usr1/SIR18360/GTB_HOME/IPSH_HOME/filearea/FILESTUBS/incomingfiles");

								SFTP_FileUpload.UtilitySFTPUpload(System.getProperty("user.dir")+"/src/test/resources/LOCAL_PAYMENT_FILE/ACH",
								        strRebulkPathTOSTUB);

								Thread.sleep(45000);

								// String str = orgStringArr[4];

								// char[] cArray = str.toCharArray();

								// for (int i = 0; i < cArray.length; i++)
								// {
								// if (cArray[i] == 'E')
								// {
								Thread.sleep(1000);
								driver.findElement(By.name("OID")).clear();
								// Originator ID
								driver.findElement(By.name("OID")).sendKeys(OriginatorID);
								driver.findElement(By.name("FILEID")).sendKeys(FCN);
								driver.findElement(By.id("ACH_FILE")).click();
								driver.findElement(By.id("echoback")).click();
								driver.findElement(By.id("buttons")).click();
								logger.info("ACH Echoback clicked");
								Thread.sleep(10000);
								// }

								// if (cArray[i] == 'P')
								// {
								Thread.sleep(1000);
								driver.findElement(By.name("OID")).clear();
								driver.findElement(By.name("OID")).sendKeys(OriginatorID);
								driver.findElement(By.name("FILEID")).sendKeys(FCN);
								driver.findElement(By.id("ACH_FILE")).click();
								driver.findElement(By.id("prepsr_oicf")).click();
								driver.findElement(By.id("buttons")).click();
								logger.info("ACH prepsr clicked");
								Thread.sleep(10000);
								// }

								// if (cArray[i] == 'O')
								// {
								Thread.sleep(1000);
								driver.findElement(By.name("OID")).clear();
								driver.findElement(By.name("OID")).sendKeys(OriginatorID);
								driver.findElement(By.name("FILEID")).sendKeys(FCN);
								driver.findElement(By.id("ACH_FILE")).click();
								driver.findElement(By.id("psr_oicf")).click();
								driver.findElement(By.id("buttons")).click();
								logger.info("ACH psr clicked");
								Thread.sleep(10000);

								// }

								// if (cArray[i] == 'R')
								// {
								Thread.sleep(1000);
								driver.findElement(By.name("OID")).clear();
								driver.findElement(By.name("OID")).sendKeys(OriginatorID);
								driver.findElement(By.name("FILEID")).sendKeys(FCN);
								driver.findElement(By.id("ACH_FILE")).click();
								driver.findElement(By.id("return_reject")).click();
								driver.findElement(By.id("buttons")).click();
								logger.info("ACH return reject clicked");
								Thread.sleep(10000);

								// }

								// if (cArray[i] == 'N')
								// {
								Thread.sleep(1000);
								driver.findElement(By.name("OID")).clear();
								driver.findElement(By.name("OID")).sendKeys(OriginatorID);
								driver.findElement(By.name("FILEID")).sendKeys(FCN);
								driver.findElement(By.id("ACH_FILE")).click();
								driver.findElement(By.id("noc")).click();
								driver.findElement(By.id("buttons")).click();
								logger.info("ACH NOC clicked");
								Thread.sleep(10000);

								// }
								// }
							}
							catch (Exception e)
							{
								e.printStackTrace();
								logger.debug(e.toString());
							}

							// Downoad and get Latest File

							String ServerPath_ach = PropertyUtils.readProperty(CIBC_XPATH_Constants.strRebulkPathFROMSTUB);
							        
							String LocalPath_ach = System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FILEStubResp\\";
							String LatestFilesLocation_ach = System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FileStubRespLatest\\";
							int TopFiles_ach = 4;

							SFTP_PSH_exe.UtilityDownloadLatestFiles(ServerPath_ach, LocalPath_ach,
							        LatestFilesLocation_ach, TopFiles_ach);

							// Upload to Server ....strACHStubPathTOPSH
							// strACHStubPathTOPSH.File_Operation_SFTPUpload();
							String UploadFilesToServerach = PropertyUtils.readProperty(CIBC_XPATH_Constants.strACHStubPathTOPSH);
							       
							SFTP_FileUpload.UtilitySFTPUpload(LatestFilesLocation_ach, UploadFilesToServerach);

							// Launch
							try
							{
								WebDriverManager.chromedriver().setup();
								//System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\Driver\\chromedriver.exe");
								ChromeOptions options1 = new ChromeOptions();
								options1.setProxy(null);
								driver = new ChromeDriver(options1);
							   
								driver.get(PropertyUtils.readProperty(CIBC_XPATH_Constants.JSP3URL));
								driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
								driver.manage().window().maximize();
								for (int i = 6; i <= 9; i++)
								{
									System.out.println("fileType" + i);
									logger.info("fileType" + i);
									driver.findElement(By.id("fileType" + i)).click();
									driver.findElement(By.xpath("//input[@type='button']")).click();
									Thread.sleep(10000);
									driver.findElement(By.xpath("//*[text()='Return to response file upload']"))
									        .click();
									Thread.sleep(3000);
								}
								driver.quit();
								System.out.println("***********ACH ENDS**********");
								logger.info("***********ACH ENDS**********");
							}
							catch (Exception e)
							{
								e.printStackTrace();
							}
						}

						}
				}
			}
			catch (Exception ex)
			{
				logger.fatal("Exception in respRebulk_CSV", ex);
			}
		

		}
	

	public static void respRebulk_CSV_EFT(String tcid, String testdata)
	{
		String value = null;
		
			logger.info("-----Insided respRebulk--------------- " + testdata);
			ArrayList<String> orgList = new ArrayList<String>();
			RespRebulkVO objRespRebulkVO = null;
			HandOffVO objHandOffVO = null;
			//PSHDAO objPSHDAO = new PSHDAO();
			StringBuffer StrBuffVal = new StringBuffer();
			try
			{
				logger.info("Testdata " + testdata);
				System.out.println("first data");
				// step 1.code to read CSV from readRebulkResponseCSV
				lManualActionIDS = IOUtilities.readRebulkResponseData(tcid, testdata);
				logger.info("lManualActionIDS-->" + lManualActionIDS.size());

				for (int i = 0; i < lManualActionIDS.size(); i++)
				{
					System.out.println(lManualActionIDS.get(i));
				}
				if (lManualActionIDS != null)
				{
					// ArrayList nTemp = (ArrayList) lManualActionIDS;

					String strAction = "";
					String strResult = "";
					WebElement elementInput = null;
					// access via new for-loop
					String ORIGINATOR_ID = null;
					String FILE_NAME = null;
					String FCN=null;
					for (Object object : lManualActionIDS)
					{
						objRespRebulkVO = (RespRebulkVO) object;
						// Pass the logical file name and get the Hand off data
						// ****************code commented
						// 21-08-2017**********************
						// strbatchno = objRespRebulkVO.getStrEODRep();
						System.out.println(" ---1 ---" + objRespRebulkVO.getStrFileNm() + " ---2 ---"
						        + objRespRebulkVO.getStrStatus() + " ---3 ---" + objRespRebulkVO.getStrBatchRes()
						        + " --- 4 ---" + objRespRebulkVO.getStrOrgIDResp() + "----5-------"
						        + objRespRebulkVO.getStrEODRep());
						logger.info(" ---1 ---" + objRespRebulkVO.getStrFileNm() + " ---2 ---"
						        + objRespRebulkVO.getStrStatus() + " ---3 ---" + objRespRebulkVO.getStrBatchRes()
						        + " --- 4 ---" + objRespRebulkVO.getStrOrgIDResp() + "----5-------"
						        + objRespRebulkVO.getStrEODRep());
						orgList = objRespRebulkVO.getStrOrgIDResp();

						String[] orgStringArr = null;
						System.out.println(orgList.get(0));
						logger.info(orgList.get(0));
						orgStringArr = orgList.get(0).split("\\,");
						String paytype = orgStringArr[1];
						System.out.println(orgStringArr[1]);
						logger.info(orgStringArr[1]);
						// Step2.getHandOffData call to get rebulk id and
						// other data
						// Sayali Change

						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\ACH");
						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\EFT");
						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FILEStubResp");
						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FILEStubRespEFT");
						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FileStubRespLatest");
						Action_Keywords
						        ._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FileStubRespLatestEFT");
						Action_Keywords
						        ._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FileStubRespLatestEFT");

						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FILEStubRespCNR");

						Action_Keywords
						        ._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FILEStubRespCNRLatest");

						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\CNR\\Pain001");

						Action_Keywords
						        ._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FILEStubRespSARLatest");

						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FILEStubRespSAR");

						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\CNR\\ISO_REMIT");
						SFTP_PSH_exe.clearRemotedirectory(PropertyUtils.readProperty("strRebulkPathFROMSTUB"));
						clearStubEntries("SS");
						if (orgStringArr[1].equals("EFT"))
						{
							System.out.println("***********EFT**********");
							logger.info("getHandoffData for EFT" + objRespRebulkVO.getStrFileNm());
							ArrayList<String> ob_wid = new ArrayList<String>();
							ob_wid = GetRubulkID.ConnectToFileProcOpn(objRespRebulkVO.getStrFileNm());
							ArrayList<String> _wid = new ArrayList<String>();
							FileUtilities Fileutil = new FileUtilities();
							String rebulkId=Fileutil.GetFilevalue("VAR_REBULKID");
							_wid = GetRubulkID.GetRebulKidOrgId(ob_wid.get(0),rebulkId);
							String REBULKID = _wid.get(0);
							ORIGINATOR_ID = _wid.get(1);
							FILE_NAME = _wid.get(2);
							FCN = _wid.get(3);

							System.out.println("Download Sinle File in:" + FILE_NAME);
							logger.info("Download Sinle File in:" + FILE_NAME);
							String strEFTStubPathFROMPSH_EFT = PropertyUtils.readProperty("strEFTStubPathFROMPSH");

							SFTPConnect.SFTPfile_DownloadToLocal(strEFTStubPathFROMPSH_EFT,
							       System.getProperty("user.dir")+"/src/test/resources/LOCAL_PAYMENT_FILE/EFT/", FILE_NAME);

							logger.info("getHandoffData for EFT" + FILE_NAME);

							System.out.println("Check:" + testdata);

							for (int j = 0; j < orgStringArr.length; j++)
							{
								System.out.println("CHECK:" + orgStringArr[j]);
							}

							String OriginatorID = orgStringArr[0];
							String TXN_Responses = orgStringArr[3];
							WebDriverManager.chromedriver().setup();
							//System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\Driver\\chromedriver.exe");
							ChromeOptions options = new ChromeOptions();
							options.setProxy(null);
							driver = new ChromeDriver(options);
	
			
							try
							{
								driver.get(PropertyUtils.readProperty(CIBC_XPATH_Constants.JSP1URL));
								driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
								driver.manage().window().maximize();

								driver.findElement(By.xpath("//*[@value='EFT']")).click();

								driver.findElement(By.id("originid")).sendKeys(OriginatorID);

								String TX_RESP = TXN_Responses.replace(" ", ",");

								driver.findElement(By.name("transaction_responses")).sendKeys(TX_RESP);
								Thread.sleep(7000);

								driver.findElement(By.name("save")).click();
								Thread.sleep(6000);


								String fileprocess = PropertyUtils.readProperty("JSP2URL");
								driver.get(fileprocess);
								driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
								driver.manage().window().maximize();

								System.out.println("SFTP_FileUpload.File_Operation_SFTPUploadEFT()");

								String strRebulkPathTOSTUB = PropertyUtils.readProperty("strRebulkPathTOSTUB");
								 

								SFTP_FileUpload.UtilitySFTPUpload(System.getProperty("user.dir")+"/src/test/resources/LOCAL_PAYMENT_FILE/EFT",
								        strRebulkPathTOSTUB);

								Thread.sleep(45000);

								Thread.sleep(1000);
								driver.findElement(By.name("OID")).clear();
								// Originator ID
								driver.findElement(By.name("OID")).sendKeys(OriginatorID);
								driver.findElement(By.name("FILEID")).sendKeys(FCN);
								driver.findElement(By.id("EFT_FILE")).click();
								driver.findElement(By.id("echoback")).click();
								driver.findElement(By.id("buttons")).click();
								logger.info("clicked on echoback");
								Thread.sleep(10000);
							
								Thread.sleep(1000);
								driver.findElement(By.name("OID")).clear();
								driver.findElement(By.name("OID")).sendKeys(OriginatorID);
								driver.findElement(By.name("FILEID")).sendKeys(FCN);
								driver.findElement(By.id("EFT_FILE")).click();
								driver.findElement(By.id("prepsr_oicf")).click();
								driver.findElement(By.id("buttons")).click();
								logger.info("clicked on prepsr");
								Thread.sleep(10000);
								// }

								// if (cArray[i] == 'O')
								// {
								Thread.sleep(1000);
								driver.findElement(By.name("OID")).clear();
								driver.findElement(By.name("OID")).sendKeys(OriginatorID);
								driver.findElement(By.name("FILEID")).sendKeys(FCN);
								driver.findElement(By.id("EFT_FILE")).click();
								driver.findElement(By.id("psr_oicf")).click();
								driver.findElement(By.id("buttons")).click();
								logger.info("clicked on psr");
								Thread.sleep(10000);

								// }

								// if (cArray[i] == 'R')
								// {
								Thread.sleep(1000);
								driver.findElement(By.name("OID")).clear();
								driver.findElement(By.name("OID")).sendKeys(OriginatorID);
								driver.findElement(By.name("FILEID")).sendKeys(FCN);
								driver.findElement(By.id("EFT_FILE")).click();
								driver.findElement(By.id("return_reject")).click();
								driver.findElement(By.id("buttons")).click();
								logger.info("clicked on returnreject");
								Thread.sleep(10000);

								// }

								// if (cArray[i] == 'N')
								// {
								Thread.sleep(1000);
								driver.findElement(By.name("OID")).clear();
								driver.findElement(By.name("OID")).sendKeys(OriginatorID);
								driver.findElement(By.name("FILEID")).sendKeys(FCN);
								driver.findElement(By.id("EFT_FILE")).click();
								driver.findElement(By.id("noc")).click();
								driver.findElement(By.id("buttons")).click();
								logger.info("clicked on noc");
								Thread.sleep(10000);

								// }
								// }
							}
							catch (Exception e)
							{
								e.printStackTrace();
								logger.debug(e.toString(),e.getCause());
							}

							Thread.sleep(60000);
							// Downoad and get Latest File
							// SFTP_PSH_exe.GetLatestFileToMovethemEFT();
							driver.quit();
							String ServerPath_EFT = PropertyUtils.readProperty("strRebulkPathFROMSTUB");
							       
							String LocalPath_EFT = System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FILEStubRespEFT\\";
							String LatestFilesLocation_EFT = System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FileStubRespLatestEFT\\";
							int TopFiles_EFT = 4;
							SFTP_PSH_exe.UtilityDownloadLatestFiles(ServerPath_EFT, LocalPath_EFT,
							        LatestFilesLocation_EFT, TopFiles_EFT);

							// Upload to Server ....strACHStubPathTOPSH
							// strACHStubPathTOPSH.File_Operation_SFTPUploadEFT();

							String strEFTStubPathTOPSH = PropertyUtils.readProperty("strEFTStubPathTOPSH");
							     
							SFTP_FileUpload.UtilitySFTPUpload(LatestFilesLocation_EFT, strEFTStubPathTOPSH);

							// Launch
							try
							{
								WebDriverManager.chromedriver().setup();
							//	System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\Driver\\chromedriver.exe");
							ChromeOptions options2	 = new ChromeOptions();
							options2.setProxy(null);
							driver = new ChromeDriver(options2);
	
			
							try
							{
								driver.get(PropertyUtils.readProperty(CIBC_XPATH_Constants.JSP3URL));
								driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
								driver.manage().window().maximize();

								// EFT Echoback
								driver.findElement(By.id("fileType1")).click();

								driver.findElement(By.xpath("//input[@type='button']")).click();

								Thread.sleep(10000);
								driver.findElement(By.xpath("//*[text()='Return to response file upload']")).click();
								Thread.sleep(5000);

								// Eft Payment ACRJ

								driver.findElement(By.id("fileType3")).click();

								driver.findElement(By.xpath("//input[@type='button']")).click();

								Thread.sleep(10000);
								driver.findElement(By.xpath("//*[text()='Return to response file upload']")).click();
								Thread.sleep(5000);

								// Eft Payment RCRT

								driver.findElement(By.id("fileType4")).click();

								driver.findElement(By.xpath("//input[@type='button']")).click();

								Thread.sleep(10000);
								driver.findElement(By.xpath("//*[text()='Return to response file upload']")).click();
								Thread.sleep(5000);

								// fileType2 Eft Return

								driver.findElement(By.id("fileType2")).click();

								driver.findElement(By.xpath("//input[@type='button']")).click();

								Thread.sleep(10000);
								driver.findElement(By.xpath("//*[text()='Return to response file upload']")).click();
								Thread.sleep(5000);

								// Eft Noc

								driver.findElement(By.id("fileType5")).click();

								driver.findElement(By.xpath("//input[@type='button']")).click();

								Thread.sleep(10000);
								driver.findElement(By.xpath("//*[text()='Return to response file upload']")).click();
								Thread.sleep(5000);
								driver.quit();
								System.out.println("***********EFT ENDS**********");
								logger.info("***********EFT ENDS**********");
							
							}
							catch (Exception e)
							{
								e.printStackTrace();
								logger.debug(e.toString(),e.getCause());
							}
							}
							catch (Exception e)
							{
								e.printStackTrace();
								logger.debug(e.toString(),e.getCause());
							}
						}

						}
				}
			}
			catch (Exception ex)
			{
				logger.fatal("Exception in respRebulk_CSV", ex);
			}
		

		}
	
	public static void respRebulk_CSV_CNR(String tcid, String testdata)
	{
		String value = null;
		
			logger.info("-----Insided respRebulk--------------- " + testdata);
			ArrayList<String> orgList = new ArrayList<String>();
			RespRebulkVO objRespRebulkVO = null;
			HandOffVO objHandOffVO = null;
			//PSHDAO objPSHDAO = new PSHDAO();
			StringBuffer StrBuffVal = new StringBuffer();
			try
			{
				logger.info("Testdata " + testdata);
				System.out.println("first data");
				// step 1.code to read CSV from readRebulkResponseCSV
				lManualActionIDS = IOUtilities.readRebulkResponseData(tcid, testdata);
				logger.info("lManualActionIDS-->" + lManualActionIDS.size());

				for (int i = 0; i < lManualActionIDS.size(); i++)
				{
					System.out.println(lManualActionIDS.get(i));
				}
				if (lManualActionIDS != null)
				{
					// ArrayList nTemp = (ArrayList) lManualActionIDS;

					String strAction = "";
					String strResult = "";
					WebElement elementInput = null;
					// access via new for-loop
					String ORIGINATOR_ID = null;
					String FILE_NAME = null;
					for (Object object : lManualActionIDS)
					{
						objRespRebulkVO = (RespRebulkVO) object;
						// Pass the logical file name and get the Hand off data
						// ****************code commented
						// 21-08-2017**********************
						// strbatchno = objRespRebulkVO.getStrEODRep();
						System.out.println(" ---1 ---" + objRespRebulkVO.getStrFileNm() + " ---2 ---"
						        + objRespRebulkVO.getStrStatus() + " ---3 ---" + objRespRebulkVO.getStrBatchRes()
						        + " --- 4 ---" + objRespRebulkVO.getStrOrgIDResp() + "----5-------"
						        + objRespRebulkVO.getStrEODRep());
						orgList = objRespRebulkVO.getStrOrgIDResp();
						logger.info(" ---1 ---" + objRespRebulkVO.getStrFileNm() + " ---2 ---"
						        + objRespRebulkVO.getStrStatus() + " ---3 ---" + objRespRebulkVO.getStrBatchRes()
						        + " --- 4 ---" + objRespRebulkVO.getStrOrgIDResp() + "----5-------"
						        + objRespRebulkVO.getStrEODRep());
						String[] orgStringArr = null;
						System.out.println(orgList.get(0));
						logger.info(orgList.get(0));
						orgStringArr = orgList.get(0).split("\\,");
						String paytype = orgStringArr[1];
						System.out.println(orgStringArr[1]);
						logger.info(orgStringArr[1]);
						// Step2.getHandOffData call to get rebulk id and
						// other data
						// Sayali Change

						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\ACH");
						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\EFT");
						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FILEStubResp");
						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FILEStubRespEFT");
						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FileStubRespLatest");
						Action_Keywords
						        ._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FileStubRespLatestEFT");
						Action_Keywords
						        ._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FileStubRespLatestEFT");

						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FILEStubRespCNR");

						Action_Keywords
						        ._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FILEStubRespCNRLatest");

						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\CNR\\Pain001");

						Action_Keywords
						        ._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FILEStubRespSARLatest");

						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FILEStubRespSAR");

						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\CNR\\ISO_REMIT");
						SFTP_PSH_exe.clearRemotedirectory(PropertyUtils.readProperty("strRebulkPathFROMSTUB"));
						clearStubEntries("SS");
						if (orgStringArr[1].equals("CNR"))
						{

							System.out.println("orgStringArr[2]:" + orgStringArr[2]);
							System.out.println("tx response:" + orgStringArr[3]);
							System.out.println("orgStringArr[4]:" + orgStringArr[4]);
							System.out.println("orgStringArr[5]:" + orgStringArr[5]);
							System.out.println("orgStringArr[6]:" + orgStringArr[6]);
							System.out.println("orgStringArr[7]:" + orgStringArr[7]);
							System.out.println("orgStringArr[8]:" + orgStringArr[8]);

							System.out.println("***********CNR**********");
							logger.info("getHandoffData for CNR" + objRespRebulkVO.getStrFileNm());
							ArrayList<String> ob_wid2 = new ArrayList<String>();
							ob_wid2 = GetRubulkID.GetWorkItemIDList(objRespRebulkVO.getStrFileNm());

							System.out.println("orgStringArr[2]:" + orgStringArr[2]);
							System.out.println("txn response:" + orgStringArr[3]);
							System.out.println("orgStringArr[4]:" + orgStringArr[4]);
							System.out.println("orgStringArr[5]:" + orgStringArr[5]);
							System.out.println("orgStringArr[6]:" + orgStringArr[6]);
							System.out.println("orgStringArr[7]:" + orgStringArr[7]);
							System.out.println("orgStringArr[8]:" + orgStringArr[8]);

							System.out.println("SIZE OF ARRAY:" + ob_wid2.size());
							logger.info("SIZE OF ARRAY:" + ob_wid2.size());
							for (int i = 0; i < ob_wid2.size(); i++)
							{
								Wiid_array.add(ob_wid2.get(i));
							}

							String delimiter = ",";
							String result = "", prefix = "";
							for (String s : Wiid_array)
							{
								result += prefix + s;
								prefix = "'" + delimiter + "'";
							}
							System.out.println(result);
							logger.info(result);
							String WorkitemID_LIST = "'" + result + "'";
							System.out.println(WorkitemID_LIST);
							logger.info(WorkitemID_LIST);

							ArrayList<String> _wid1 = new ArrayList<String>();
							_wid1 = GetRubulkID.GetRebulKidOrgIdFileNameCNR(result, orgStringArr[0]);
							FileUtilities Fileutil = new FileUtilities();
							String rebulkId=Fileutil.GetFilevalue("VAR_REBULKID");
							String FILE_WORKITEM_ID = _wid1.get(0);

							System.out.println(FILE_WORKITEM_ID);
							logger.info(FILE_WORKITEM_ID);
							ArrayList<String> _FileName = new ArrayList<String>();
							_FileName = GetRubulkID.DBConnect(
							        "Select hfo_file_workitemid,HFO_REBULKID,HFO_ORIGINATOR_ID,HFO_FILE_NAME from handoff_file_opn where hfo_file_workitemid",
							        "HFO_FILE_NAME", FILE_WORKITEM_ID, " and HFO_REBULKID= '"+rebulkId+"'");

							String _FileNameString = _FileName.get(0);

							// CNR File Name
							System.out.println(_FileNameString);
							logger.info(_FileNameString);
							// SFTPConnect.file_DownloadToLocalCNR(_FileNameString);

							String CNR_ServerPath =PropertyUtils.readProperty("strCHKStubPathFROMPSH");
							  
							SFTPConnect.SFTPfile_DownloadToLocal(CNR_ServerPath,
							        System.getProperty("user.dir")+"/src/test/resources/LOCAL_PAYMENT_FILE/CNR/Pain001", _FileNameString);

							String HFO_ID = GetRubulkID.DBConnect(
							        "Select hfo_file_workitemid,HFO_PAY_TYPE,HFO_REBULKID,HFO_ORIGINATOR_ID,HFO_FILE_NAME from handoff_file_opn where hfo_file_workitemid",
							        "HFO_REBULKID", FILE_WORKITEM_ID, " and HFO_PAY_TYPE= 'CHK'").get(0);
							System.out.println(HFO_ID);
							logger.info(HFO_ID);
							WebDriverManager.chromedriver().setup();
							//System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\Driver\\chromedriver.exe");
							ChromeOptions options4 = new ChromeOptions();
							options4.setProxy(null);
							driver = new ChromeDriver(options4);
	
			
							try
							{
								driver.get(PropertyUtils.readProperty(CIBC_XPATH_Constants.JSP1URL));
							driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
							driver.manage().window().maximize();

							driver.findElement(By.id("r3")).click();

							driver.findElement(By.id("originid")).sendKeys(HFO_ID);

							// driver.findElement(By.id("bRespSeq")).sendKeys("A");
							driver.findElement(By.id("bRespSeq")).sendKeys(orgStringArr[2]);

							// driver.findElement(By.name("transaction_responses")).sendKeys("A,R,C");
							String txnres = orgStringArr[3];
							txnres = txnres.replaceAll("\\s", ",");
							driver.findElement(By.name("transaction_responses")).sendKeys(txnres);

							// driver.findElement(By.name("trans_error_codes")).sendKeys("DU03");
							driver.findElement(By.name("trans_error_codes")).sendKeys(orgStringArr[7]);

							Thread.sleep(7000);

							driver.findElement(By.name("save")).click();

							Thread.sleep(6000);

							// driver.findElement(By.xpath("//*[text()='Go to
							// Response Generator Page']")).click();
							String fileprocess = PropertyUtils.readProperty("JSP2URL");
							    
							driver.get(fileprocess);
							driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
							driver.manage().window().maximize();
							String strRebulkPathTOSTUBCNR =PropertyUtils.readProperty("strRebulkPathTOSTUB");
							    
							SFTP_FileUpload.UtilitySFTPUpload(System.getProperty("user.dir")+"/src/test/resources/LOCAL_PAYMENT_FILE/CNR/Pain001",
							        strRebulkPathTOSTUBCNR);
							Thread.sleep(45000);

							driver.findElement(By.name("OID")).sendKeys(HFO_ID);

							driver.findElement(By.id("CNR_FILE")).click();

							driver.findElement(By.id("echoback")).click();

							driver.findElement(By.id("buttons")).click();
							logger.info("clicked on echoback");
							Thread.sleep(10000);

							driver.findElement(By.name("OID")).sendKeys(HFO_ID);

							driver.findElement(By.id("CNR_FILE")).click();

							driver.findElement(By.id("prepsr_oicf")).click();

							driver.findElement(By.id("buttons")).click();
							logger.info("clicked on psr");
							Thread.sleep(10000);
							driver.quit();

							// Downoad and get Latest File
							String ServerPath = PropertyUtils.readProperty("strRebulkPathFROMSTUB");
							  
							String LocalPath = System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FILEStubRespCNR\\";
							String LatestFilesLocation = System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FILEStubRespCNRLatest\\";
							int TopFiles = 1;

							SFTP_PSH_exe.UtilityDownloadLatestFiles(ServerPath, LocalPath, LatestFilesLocation,
							        TopFiles);

							// Upload to Server ....strACHStubPathTOPSH
							String UploadFilesToServer = PropertyUtils.readProperty("CNRUploadLatestFile");
						
							SFTP_FileUpload.UtilitySFTPUpload(LatestFilesLocation, UploadFilesToServer);
							//System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\Driver\\chromedriver.exe");
							WebDriverManager.chromedriver().setup();
							ChromeOptions options5 = new ChromeOptions();
							options5.setProxy(null);
							driver = new ChromeDriver(options5);
	
			
							try
							{
								driver.get(PropertyUtils.readProperty(CIBC_XPATH_Constants.JSP3URL));
							driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

							driver.manage().window().maximize();
							driver.findElement(By.id("fileType10")).click();
							driver.findElement(By.xpath("//input[@type='button']")).click();

							Thread.sleep(10000);
							driver.findElement(By.xpath("//*[text()='Return to response file upload']")).click();
							driver.findElement(By.id("fileType11")).click();
							driver.findElement(By.xpath("//input[@type='button']")).click();
							Thread.sleep(10000);

							driver.findElement(By.xpath("//*[text()='Return to response file upload']")).click();
							Thread.sleep(10000);
							driver.quit();
							System.out.println("***********CNR ENDS**********");
							logger.info("***********CNR ENDS**********");
						}
							catch (Exception e)
							{
								e.printStackTrace();
								logger.debug(e.toString(),e.getCause());
							}
						}
							catch (Exception e)
							{
								e.printStackTrace();
								logger.debug(e.toString(),e.getCause());
							}
						}
				}
			}
			}
			catch (Exception ex)
			{
				logger.fatal("Exception in respRebulk_CSV", ex);
			}
		

		}
			
	public static void respRebulk_CSV_SAR(String tcid, String testdata)
	{
		String value = null;
			System.out.println("Ash 1");
			logger.info("-----Insided respRebulk--------------- " + testdata);
			ArrayList<String> orgList = new ArrayList<String>();
			RespRebulkVO objRespRebulkVO = null;
			HandOffVO objHandOffVO = null;
			//PSHDAO objPSHDAO = new PSHDAO();
			StringBuffer StrBuffVal = new StringBuffer();
			try
			{
				logger.info("Testdata " + testdata);
				System.out.println("Testdata " + testdata);
				// step 1.code to read CSV from readRebulkResponseCSV
				lManualActionIDS = IOUtilities.readRebulkResponseData(tcid, testdata);
				logger.info("lManualActionIDS-->" + lManualActionIDS.size());

				for (int i = 0; i < lManualActionIDS.size(); i++)
				{
					System.out.println("ash1"+lManualActionIDS.get(i));
					logger.info("ash1"+lManualActionIDS.get(i));
				}
				if (lManualActionIDS != null)
				{
					// ArrayList nTemp = (ArrayList) lManualActionIDS;

					String strAction = "";
					String strResult = "";
					WebElement elementInput = null;
					// access via new for-loop
					String ORIGINATOR_ID = null;
					String FILE_NAME = null;
					for (Object object : lManualActionIDS)
					{
						objRespRebulkVO = (RespRebulkVO) object;
						// Pass the logical file name and get the Hand off data
						// ****************code commented
						// 21-08-2017**********************
						// strbatchno = objRespRebulkVO.getStrEODRep();
						System.out.println(" ---1 ---" + objRespRebulkVO.getStrFileNm() + " ---2 ---"
						        + objRespRebulkVO.getStrStatus() + " ---3 ---" + objRespRebulkVO.getStrBatchRes()
						        + " --- 4 ---" + objRespRebulkVO.getStrOrgIDResp() + "----5-------"
						        + objRespRebulkVO.getStrEODRep());
						logger.info(" ---1 ---" + objRespRebulkVO.getStrFileNm() + " ---2 ---"
						        + objRespRebulkVO.getStrStatus() + " ---3 ---" + objRespRebulkVO.getStrBatchRes()
						        + " --- 4 ---" + objRespRebulkVO.getStrOrgIDResp() + "----5-------"
						        + objRespRebulkVO.getStrEODRep());
						orgList = objRespRebulkVO.getStrOrgIDResp();
						
						String[] orgStringArr = null;
						System.out.println(orgList.get(0));
						logger.info(orgList.get(0));
						orgStringArr = orgList.get(0).split("\\,");
						String paytype = orgStringArr[1];
						System.out.println("orgStringArr[1]"+orgStringArr[1]);
						logger.info("orgStringArr[1]"+orgStringArr[1]);
						// Step2.getHandOffData call to get rebulk id and
						// other data
						// Sayali Change
						System.out.println("Ash purging");
						logger.info("Ash purging");
						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\ACH");
						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\EFT");
						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FILEStubResp");
						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FILEStubRespEFT");
						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FileStubRespLatest");
						Action_Keywords
						        ._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FileStubRespLatestEFT");
						Action_Keywords
						        ._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FileStubRespLatestEFT");

						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FILEStubRespCNR");

						Action_Keywords
						        ._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FILEStubRespCNRLatest");

						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\CNR\\Pain001");

						Action_Keywords
						        ._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FILEStubRespSARLatest");

						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FILEStubRespSAR");

						Action_Keywords._purgeDirectory(System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\CNR\\ISO_REMIT");
						SFTP_PSH_exe.clearRemotedirectory(PropertyUtils.readProperty("strRebulkPathFROMSTUB"));
						clearStubEntries("SS");
						
						if (orgStringArr[1].equals("SAR"))
						{

							System.out.println("orgStringArr[2]:" + orgStringArr[2]);
							System.out.println("orgStringArr[3]:" + orgStringArr[3]);
							System.out.println("orgStringArr[4]:" + orgStringArr[4]);
							System.out.println("orgStringArr[5]:" + orgStringArr[5]);
							System.out.println("orgStringArr[6]:" + orgStringArr[6]);
							System.out.println("orgStringArr[7]:" + orgStringArr[7]);
							System.out.println("orgStringArr[8]:" + orgStringArr[8]);

							System.out.println("**********************SAR*************************");
							logger.info("getHandoffData for CNR" + objRespRebulkVO.getStrFileNm());
							ArrayList<String> ob_widSAR = new ArrayList<String>();
							FileUtilities Fileutil = new FileUtilities();
							String FileworkitemiId=Fileutil.GetFilevalue("VAR_PSHFILEWORKITEMID");
							ob_widSAR = GetRubulkID.GetWorkItemIDList(FileworkitemiId);

							System.out.println("SIZE OF ARRAY:" + ob_widSAR.size());
							logger.info("SIZE OF ARRAY:" + ob_widSAR.size());
							for (int i = 0; i < ob_widSAR.size(); i++)
							{
								Wiid_array.add(ob_widSAR.get(i));
							}

							String delimiter_sar = ",";
							String result_sar = "", prefix_sar = "";
							for (String s : Wiid_array)
							{
								result_sar += prefix_sar + s;
								prefix_sar = "'" + delimiter_sar + "'";
							}
							System.out.println(result_sar);
							logger.info(result_sar);
							String WorkitemID_LIST_sar = "'" + result_sar + "'";
							System.out.println(WorkitemID_LIST_sar);
							logger.info(WorkitemID_LIST_sar);
							ArrayList<String> _wid_sar = new ArrayList<String>();
							_wid_sar = GetRubulkID.GetRebulKidOrgIdFileNameCNR(result_sar, orgStringArr[0]);
							String FILE_WORKITEM_ID_sar = _wid_sar.get(0);

							System.out.println(FILE_WORKITEM_ID_sar);
							logger.info(FILE_WORKITEM_ID_sar);
							ArrayList<String> _FileName_sar = new ArrayList<String>();
							
							String REBULKID=Fileutil.GetFilevalue("VAR_REBULKID");
							_FileName_sar = GetRubulkID.DBConnect(
							        "Select hfo_file_workitemid,HFO_REBULKID,HFO_ORIGINATOR_ID,HFO_FILE_NAME from handoff_file_opn where hfo_file_workitemid",
							        "HFO_FILE_NAME", FILE_WORKITEM_ID_sar, " and HFO_REBULKID='"+REBULKID+"'");

							String _FileNameString_sar = _FileName_sar.get(0);

							// CNR File Name
							System.out.println(_FileNameString_sar);
							logger.info(_FileNameString_sar);
							String strSARStubPathFROMPSH = PropertyUtils.readProperty("strSARStubPathFROMPSH");
							       
							SFTPConnect.SFTPfile_DownloadToLocal(strSARStubPathFROMPSH,
							        System.getProperty("user.dir")+"/LOCAL_PAYMENT_FILE/CNR/ISO_REMIT", _FileNameString_sar);

							String HFO_ID_sar = Fileutil.GetFilevalue("VAR_REBULKID");
							System.out.println("HFO_ID_sar"+HFO_ID_sar);
							WebDriverManager.chromedriver().setup();
							//System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\Driver\\chromedriver.exe");
							ChromeOptions options = new ChromeOptions();
							options.setProxy(null);
							driver = new ChromeDriver(options);
	
			
						
								driver.get(PropertyUtils.readProperty(CIBC_XPATH_Constants.JSP1URL));
							driver.manage().window().maximize();

							driver.findElement(By.id("r3")).click();

							driver.findElement(By.id("originid")).sendKeys(HFO_ID_sar);

							// _driver.findElement(By.id("bRespSeq")).sendKeys("A");
							driver.findElement(By.id("bRespSeq")).sendKeys(orgStringArr[2]);

							// _driver.findElement(By.name("transaction_responses")).sendKeys("A,R,C");
							String txnres1 = orgStringArr[3];
							txnres1 = txnres1.replaceAll("\\s", ",");
							driver.findElement(By.name("transaction_responses")).sendKeys(txnres1);

							// _driver.findElement(By.name("transaction_responses")).sendKeys(orgStringArr[3]);

							// _driver.findElement(By.name("trans_error_codes")).sendKeys("DU03");
							driver.findElement(By.name("trans_error_codes")).sendKeys(orgStringArr[7]);

							Thread.sleep(7000);

							driver.findElement(By.name("save")).click();

							Thread.sleep(6000);

							// _driver.findElement(By.xpath("//*[text()='Go to
							// Response Generator Page']")).click();
							String fileprocess1 = PropertyUtils.readProperty("JSP2URL");
							  
							driver.get(fileprocess1);
							driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
							driver.manage().window().maximize();
							// Checking!!

							// SFTP_FileUpload.UtilitySFTPUpload("D:/PSH_Automation/LOCAL_PAYMENT_FILE/CNR/ISO_REMIT",
							// "/usr1/SIR18360/GTB_HOME/IPSH_HOME/filearea/FILESTUBS/incomingfiles");

							SFTP_FileUpload.UtilitySFTPUpload(System.getProperty("user.dir")+"/src/test/resources/LOCAL_PAYMENT_FILE/CNR/ISO_REMIT",
									PropertyUtils.readProperty("strRebulkPathTOSTUB"
							  ));

							Thread.sleep(45000);

							driver.findElement(By.name("OID")).sendKeys(HFO_ID_sar);

							driver.findElement(By.id("CNR_FILE")).click();

							driver.findElement(By.id("echoback")).click();

							driver.findElement(By.id("buttons")).click();

							Thread.sleep(10000);

							driver.findElement(By.name("OID")).sendKeys(HFO_ID_sar);

							driver.findElement(By.id("CNR_FILE")).click();

							driver.findElement(By.id("prepsr_oicf")).click();

							driver.findElement(By.id("buttons")).click();

							Thread.sleep(10000);
							driver.quit();

							// Downoad and get Latest File
							String ServerPath_cnr = PropertyUtils.readProperty("strRebulkPathFROMSTUB");
						
							String LocalPath_cnr = System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FILEStubRespSAR\\";
							String LatestFilesLocation_cnr = System.getProperty("user.dir")+"\\src\\test\\resources\\LOCAL_PAYMENT_FILE\\FILEStubRespSARLatest\\";
							int TopFiles_cnr = 1;

							SFTP_PSH_exe.UtilityDownloadLatestFiles(ServerPath_cnr, LocalPath_cnr,
							        LatestFilesLocation_cnr, TopFiles_cnr);

							// Upload to Server ....strACHStubPathTOPSH
							String UploadFilesToServercnr =PropertyUtils.readProperty("strCNRStubPathTOPSH");
							    
							SFTP_FileUpload.UtilitySFTPUpload(LatestFilesLocation_cnr, UploadFilesToServercnr);
							WebDriverManager.chromedriver().setup();
							//System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\Driver\\chromedriver.exe");
							ChromeOptions options8 = new ChromeOptions();
							options8.setProxy(null);
							driver = new ChromeDriver(options8);
	
			
						
								driver.get(PropertyUtils.readProperty(CIBC_XPATH_Constants.JSP3URL));
							driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

							driver.manage().window().maximize();
							driver.findElement(By.id("fileType10")).click();
							driver.findElement(By.xpath("//input[@type='button']")).click();

							Thread.sleep(10000);
							driver.findElement(By.xpath("//*[text()='Return to response file upload']")).click();
							driver.findElement(By.id("fileType11")).click();
							driver.findElement(By.xpath("//input[@type='button']")).click();
							Thread.sleep(10000);

							driver.findElement(By.xpath("//*[text()='Return to response file upload']")).click();
							Thread.sleep(10000);
							driver.quit();
						}
						System.out.println("*******************SAR ENDS *******************");
						logger.info("*******************SAR ENDS *******************");
			
					}
						}
						}
				
			catch (Exception ex)
			{
				logger.fatal("Exception in respRebulk_CSV", ex);
			}
		

		}
		
			public static void main(String[] args) throws Exception
			{

				HashMap mapIni=new HashMap();
				System.out.println("respRebulk_CSV WILL BE LAUNCHED");
				//Action_Keywords.respRebulk_CSV_ACH("SANITY_TC020","3~Accept~A A R~9708234614,ACH,N,A AR AN R,5");
				//Action_Keywords.respRebulk_CSV_EFT("SANITY_TC021","3~Accept~A A R~0100777006,EFT,N,AN R C AR,5");
				//Action_Keywords.respRebulk_CSV_CNR("SANITY_TC011","3~Accept~A~ANSIxSC11xP010,CNR,A,A R C,2,NA,NA,DU03,N");
				Action_Keywords.respRebulk_CSV_SAR("SANITY_TC011","3~Accept~A~ANSIxSC11xP003,SAR,A,A R C,2,NA,NA,DU03,N");
			}
}
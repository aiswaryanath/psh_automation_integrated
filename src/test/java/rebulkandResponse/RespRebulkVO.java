/****************************************************************************/
/* Copyright �  2015 Intellect Design Area Ltd. All rights reserved      	*/
/*                                                                      	*/
/****************************************************************************/
/*  Application  : Intellect Payment Engine 								*/
/*                            												*/
/*  Module Name  : Automation Application               	     			*/
/*  File Name    : RespRebulkVO.java               		  					*/
/*                                            	                          	*/
/*  Description  :															*/
/*                 	                                                		*/
/*  Author       : Nitin Nandwana       										*/
/****************************************************************************/
/* Version Control Block                                              		*/
/* ~~~~~~~ ~~~~~~~ ~~~~~                                              		*/
/*                                                                    		*/
/*    Date    	Version    Author          	Ref.            Description 	*/
/*    ====    	=======    ======          ====             ===========
 / * 18-Jul-2017   1.0    Sayali Hande  				Initial Version
 / *
 /****************************************************************************/
package rebulkandResponse;

import java.util.ArrayList;

import org.apache.log4j.Logger;

/**
 * @author nikita.shirodkar this function is used to store data from CSV in
 *         getter,setter methods
 */
public class RespRebulkVO
{
	@Override
	public String toString() {
		return "RespRebulkVO [strFileNm=" + strFileNm + ", strRebulkOpn=" + strRebulkOpn + ", strEODRep=" + strEODRep
				+ ", strStatus=" + strStatus + ", strBatchRes=" + strBatchRes + ", strTxnResp=" + strTxnResp
				+ ", strEFTChk=" + strEFTChk + ", strTcId=" + strTcId + ", strStat=" + strStat + ", strWrkitm="
				+ strWrkitm + ", strSysNm=" + strSysNm + ", strIntNm=" + strIntNm + ", strTrnDes=" + strTrnDes
				+ ", strIntResCod=" + strIntResCod + ", orgID=" + orgID + "]";
	}

	private static Logger	        logger	     = Logger.getLogger("CIBC");
	private String	                strFileNm	 = null;
	private String	                strRebulkOpn	= null;
	private String	                strEODRep	 = null;
	private String	                strStatus	 = null;
	private String	                strBatchRes	 = null;
	private String	                strTxnResp	 = null;
	private String	                strEFTChk	 = null;
	private String	                strTcId	     = null;
	private String	                strStat	     = null;
	private String	                strWrkitm	 = null;
	private String	                strSysNm	 = null;
	private String	                strIntNm	 = null;
	private String	                strTrnDes	 = null;
	private String	                strIntResCod	= null;
	// private String strResMsg = null;
	private final ArrayList<String>	orgID	     = new ArrayList<String>();

	public void setStrFileNm(String nStrFileNm)
	{
		this.strFileNm = nStrFileNm;
	}

	public String getStrFileNm()
	{
		return this.strFileNm;
	}

	public void setStrRebulkOpn(String nStrRebulkOpn)
	{
		this.strRebulkOpn = nStrRebulkOpn;
	}

	public String getStrRebulkOpn()
	{
		return this.strRebulkOpn;
	}

	public void setStrStatus(String nStrStatus)
	{
		this.strStatus = nStrStatus;
	}

	public String getStrStatus()
	{
		return this.strStatus;
	}

	public void setStrBatchRes(String nStrBatchRes)
	{
		this.strBatchRes = nStrBatchRes;
	}

	public String getStrBatchRes()
	{
		return this.strBatchRes;
	}

	public void setStrTxnResp(String nStrTxnResp)
	{
		this.strTxnResp = nStrTxnResp;
	}

	public ArrayList<String> getStrOrgIDResp()
	{
		return orgID;
	}

	public void setStrOrgIDResp(String nStrOrgResp)
	{

		orgID.add(nStrOrgResp);
	}

	public String getStrTxnResp()
	{
		return this.strTxnResp;
	}

	public void setStrEODRep(String nStrEODRep)
	{
		this.strEODRep = nStrEODRep;
	}

	public String getStrEODRep()
	{
		return this.strEODRep;
	}

	public void setStrEFTChk(String nstrEFTChk)
	{
		this.strEFTChk = nstrEFTChk;
	}

	public String getStrEFTChk()
	{
		return this.strEFTChk;
	}

	public void setStrTestcaseId(String nstrTcId)
	{
		this.strTcId = nstrTcId;
	}

	public String getStrTestcaseId()
	{
		return this.strTcId;
	}

	public void setStatus(String nstrStat)
	{
		this.strStat = nstrStat;
	}

	public String getStatus()
	{
		return this.strStat;
	}

	public void setWorkitem(String nstrWrkitm)
	{
		this.strWrkitm = nstrWrkitm;
	}

	public String getWorkitem()
	{
		return this.strWrkitm;
	}

	public void setSystemName(String nstrSysNm)
	{
		this.strSysNm = nstrSysNm;
	}

	public String getSystemName()
	{
		return this.strSysNm;
	}

	public void setInterfaceName(String nstrIntNm)
	{
		this.strIntNm = nstrIntNm;
	}

	public String getInterfaceName()
	{
		return this.strIntNm;
	}

	public void setTransDesc(String nstrTrnDes)
	{
		this.strTrnDes = nstrTrnDes;
	}

	public String getTransDesc()
	{
		return this.strTrnDes;
	}

	public void setIntrfcRespoCode(String nstrIntResCod)
	{
		this.strIntResCod = nstrIntResCod;
	}

	public String getIntrfcRespoCode()
	{
		return this.strIntResCod;
	}

	// public void setResMsg(String nstrResMsg)
	// {
	// this.strResMsg = nstrResMsg;
	// }
	//
	// public String getResMsg()
	// {
	// return this.strResMsg;
	// }

}

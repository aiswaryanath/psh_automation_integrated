/****************************************************************************/
/* Copyright �  2015 Intellect Design Area Ltd. All rights reserved      	*/
/*                                                                      	*/
/****************************************************************************/
/*  Application  : Intellect Payment Engine 								*/
/*                            												*/
/*  Module Name  : Automation Application               	     			*/
/*  File Name    : IOUtilities.java                 						*/
/*                                                                      	*/
/*  Description  :															*/
/*                 	                                                		*/
/*  Author       : Nitin Chourey       										*/
/****************************************************************************/
/* Version Control Block                                              		*/
/* ~~~~~~~ ~~~~~~~ ~~~~~                                              		*/
/*                                                                    		*/
/*    Date    	Version    Author          	Ref.            Description 	*/
/*    ====    	=======    ======          ====             ===========
 / * 18-Jul-2017   1.0      Nitin C  						Initial Version
 / *
 /****************************************************************************/
package rebulkandResponse;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//import org.CIBC_Automation.VO.RespRebulkVO;
import org.apache.log4j.Logger;

import UIOperations.FGTExecution;

public class IOUtilities
{

	private static List		gManualActionCSV	= null;
	public static Logger	logger				= Logger.getLogger("CIBC");

	public static ArrayList readRebulkResponseData(String tcid, String testData) throws FileNotFoundException
	{
		ArrayList objWOrkitemData = new ArrayList();
		try
		{
			logger.info("+++++++++++++++++++Inside CSV read++++++");
			logger.info("safsgdggfbgfbfbf" + testData);
			System.out.println("second data");
			RespRebulkVO objRespRebulkVO = null;
			String[] result = null;
			objRespRebulkVO = new RespRebulkVO();
			result = testData.split("~");
			logger.info("result length---" + result.length);
			for (int i = 0; i < result.length; i++)
			{
				logger.info("result[" + i + "] :" + result[i]);
				System.out.println("result[" + i + "] :" + result[i]);
			}
			//String filename = Action_Keywords.getFileName(tcid);
			FGTExecution fe= new FGTExecution();
			HashMap<String, String> filenameinMap = fe.GetFilename(tcid);
			String filename = filenameinMap.get("FILE_NAME");
			
			objRespRebulkVO.setStrEODRep(result[0]);
			objRespRebulkVO.setStrStatus(result[1]);
			objRespRebulkVO.setStrBatchRes(result[2]);
			objRespRebulkVO.setStrFileNm(filename);
			for (int i = 3; i < result.length; i++)
			{
				logger.info(" Hai ===result[i]--->" + result[i]);
				System.out.println("Org result[" + i + "] :" + result[i]);
				objRespRebulkVO.setStrOrgIDResp(result[i]);
			}

			try
			{
				objWOrkitemData.add(objRespRebulkVO);
				//objRespRebulkVO = null;
				System.out.println("objWOrkitemData"+objRespRebulkVO.toString());
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

			// }

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		logger.info("+++++++++++Exsisting csv++++++++++++++++++++++++++");
		return objWOrkitemData;

	}

}
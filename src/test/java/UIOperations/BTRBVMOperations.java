package UIOperations;

import org.apache.log4j.Logger;

import Logger.LoggerUtils;
import Utilities.PSHBetCall;
import Utilities.PSHCC2CallBV;

public class BTRBVMOperations {
	static Logger log = LoggerUtils.getLogger();
	
	public String PSHCBTRBVMAPI(String TestCaseNo, String TestData) throws Exception {
		// TODO Auto-generated method stub
		PSHCC2CallBV bvmcall=new PSHCC2CallBV();
		PSHBetCall betcall = new PSHBetCall();
		Keywordoperations key=new Keywordoperations();
		String input1=null;
		String input2 = null;
		String status = null;
		String CurrentDate=key._DateFormat();
		System.out.println(CurrentDate);
		String PaymentExpiryDate = betcall.AddDate(CurrentDate,30);
		String[] Data = TestData.split("\\|");
		
		switch(TestCaseNo) 
		{
		case "BTRxBVxTC1xF1xTXN8":
			
			try {
				input1 = "{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T01:57:55\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"47.33\",\"InitgPty\":{\"Nm\":\"CBX 02 Client02\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"CBXBATCH001\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"47.33\",\"PmtTpInf\":{\"SvcLvl\":{\"Prtry\":\"REGULAR_PAYMENT\"},\"LclInstrm\":{\"Prtry\":\"BTR\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"CBX 02 Client02\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}},\"DbtrAcct\":{\"Ccy\":\"CAD\",\"Nm\":\"etransfer Account\",\"Id\":{\"Othr\":{\"Id\":\"6402409\"}}},\"DbtrAgt\":{\"FinInstnId\":{\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"2001\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"092286-0801202kr01\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"47.33\"}},\"Cdtr\":{\"Nm\":\"CONTACT LEGAL NM\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"CtctDtls\":{\"MobNb\":\"09890800908\",\"EmailAdr\":\"guiii@gmail.com\",\"Othr\":\"EN\"}},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"L\",\"securityQuestion\":\"What's the password I gave you?\",\"securityAnswer\":\"43abd74f52900c169131a4646d069cd2d24bfc1344bdc2b45f5e26d8a7491a4a\",\"hashType\":\"SHA2\",\"hashSalt\":\"1W\",\"ChnlPymtId\":\"#VAR4\",\"DbAmt\":\"47.33\",\"Ccy\":\"CAD\",\"ProductCd\":\"DOMESTIC\",\"DbtrPuId\":\"99000080\",\"PymtExpDt\":\"#VAR5T01:57:55.862Z\",\"AliasHandleType\":\"EMAIL\",\"AliasHandleId\":\"guiii@gmail.com\",\"AcctCretDt\":\"2020-01-29\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"9900008099000001\",\"ChnlIndctr\":\"ONLINE\",\"CustIpAddr\":\"000.000.000.000\",\"CustAuthMtd\":\"PASSWORD\",\"AuthTp\":\"PAYMENT_LEVEL\"}}}}}}";
				String VAR_messageid = key.getSaltString("Y", 10,"PSHBETAUT");
				System.out.println(VAR_messageid);
				key.StoreValue("VAR_MESSAGEID",VAR_messageid);
				String VAR_chnlid = key.getSaltString("Y", 10,"BETchn");
				System.out.println(VAR_chnlid);
					
				key.StoreValue("VAR_PSHBTRCHANLID",VAR_chnlid);
				 input2 = input1.replace("#VAR1",VAR_messageid);
				 input2 = input2.replace("#VAR2",key._DateFormat());
				 input2 = input2.replace("#VAR3",key._DateFormat());
				 input2 = input2.replace("#VAR4",VAR_chnlid);
				 input2 = input2.replace("#VAR5", PaymentExpiryDate);
				System.out.println("First API is:"+input2);	
				String status1=betcall.POSTToAPI(input2);
				Thread.sleep(10000);
			String input3 = input2.replace("#VAR4", key.getSaltString("Y", 10,"BETchn"));
			System.out.println("Second API with same message id:" + input3);
			status = betcall.POSTToAPI(input3);
			}
						catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case "BTRxBVxTC1xF1xTXN9":
			
			try {
	            input1=	"{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T08:35:45\",\"NbOfTxs\":\"2\",\"CtrlSum\":\"82.82\",\"InitgPty\":{\"Nm\":\"CBX Client 002\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"CBXBATCH001\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"41.41\",\"PmtTpInf\":{\"SvcLvl\":{\"Prtry\":\"ACCOUNT_DEPOSIT_PAYMENT\"},\"LclInstrm\":{\"Prtry\":\"BTR\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"CBX Client 002\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}},\"DbtrAcct\":{\"Ccy\":\"CAD\",\"Nm\":\"etransfer Account\",\"Id\":{\"Othr\":{\"Id\":\"6402409\"}}},\"DbtrAgt\":{\"FinInstnId\":{\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"2001\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"610548495728-7030220\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"41.41\"}},\"CdtrAgt\":{\"FinInstnId\":{\"ClrSysMmbId\":{\"MmbId\":\"00100002\",\"ClrSysId\":{\"Cd\":\"CACPA\"}}}},\"Cdtr\":{\"Nm\":\"Contact  Interac\",\"PstlAdr\":{\"Ctry\":\"CA\"}},\"CdtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"9969911\"}}},\"RmtInf\":{\"Ustrd\":[\"sdfdffds\"]},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"L\",\"ChnlPymtId\":\"#VAR4\",\"DbAmt\":\"41.41\",\"Ccy\":\"CAD\",\"ProductCd\":\"DOMESTIC\",\"DbtrPuId\":\"99000080\",\"PymtExpDt\":\"#VAR6T08:35:45.277Z\",\"AcctCretDt\":\"2020-01-29\"}}}}]},{\"PmtInfId\":\"CBXBATCH001\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"41.41\",\"PmtTpInf\":{\"SvcLvl\":{\"Prtry\":\"ACCOUNT_DEPOSIT_PAYMENT\"},\"LclInstrm\":{\"Prtry\":\"BTR\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"CBX Client 002\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}},\"DbtrAcct\":{\"Ccy\":\"CAD\",\"Nm\":\"etransfer Account\",\"Id\":{\"Othr\":{\"Id\":\"6402409\"}}},\"DbtrAgt\":{\"FinInstnId\":{\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"2001\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"610548495728-7030220\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"41.41\"}},\"CdtrAgt\":{\"FinInstnId\":{\"ClrSysMmbId\":{\"MmbId\":\"00100002\",\"ClrSysId\":{\"Cd\":\"CACPA\"}}}},\"Cdtr\":{\"Nm\":\"Contact  Interac\",\"PstlAdr\":{\"Ctry\":\"CA\"}},\"CdtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"9969911\"}}},\"RmtInf\":{\"Ustrd\":[\"sdfdffds\"]},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"L\",\"ChnlPymtId\":\"#VAR5\",\"DbAmt\":\"41.41\",\"Ccy\":\"CAD\",\"ProductCd\":\"DOMESTIC\",\"DbtrPuId\":\"99000080\",\"PymtExpDt\":\"#VAR6T08:35:45.277Z\",\"AcctCretDt\":\"2020-01-29\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"9900008099000001\",\"ChnlIndctr\":\"ONLINE\",\"CustIpAddr\":\"000.000.000.000\",\"CustAuthMtd\":\"PASSWORD\",\"AuthTp\":\"NOT_REQUIRED\"}}}}}}";
				String VAR_messageid = key.getSaltString("Y", 10,"PSHBETAUT");
				System.out.println(VAR_messageid);
				log.info(VAR_messageid);
				key.StoreValue("VAR_MESSAGEID",VAR_messageid);
				String VAR_chnlid = key.getSaltString("Y", 10,"BETchn");
				System.out.println(VAR_chnlid);
				String VAR_chnlid2 = key.getSaltString("Y", 10,"BETchn");
				System.out.println(VAR_chnlid2);
				key.StoreValue("VAR_PSHBTRCHANLID",VAR_chnlid);
				 input2 = input1.replace("#VAR1",VAR_messageid);
				 input2 = input2.replace("#VAR2",key._DateFormat());
				 input2 = input2.replace("#VAR3",key._DateFormat());
				 input2 = input2.replace("#VAR4",VAR_chnlid);
				 input2 = input2.replace("#VAR5",VAR_chnlid2);
				 input2 = input2.replace("#VAR6", PaymentExpiryDate);
				System.out.println("The final API is:"+input2);	
				log.info("The final API is:"+input2);
					status = betcall.POSTToAPI(input2);
			}
						catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;	
		case "BTRxBVxTC1xF1xTXN10":
		
			try {
				input1 = "{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T01:57:55\",\"NbOfTxs\":\"5\",\"CtrlSum\":\"47.33\",\"InitgPty\":{\"Nm\":\"CBX 02 Client02\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"CBXBATCH001\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"47.33\",\"PmtTpInf\":{\"SvcLvl\":{\"Prtry\":\"REGULAR_PAYMENT\"},\"LclInstrm\":{\"Prtry\":\"BTR\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"CBX 02 Client02\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}},\"DbtrAcct\":{\"Ccy\":\"CAD\",\"Nm\":\"etransfer Account\",\"Id\":{\"Othr\":{\"Id\":\"6402409\"}}},\"DbtrAgt\":{\"FinInstnId\":{\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"2001\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"092286-0801202kr01\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"47.33\"}},\"Cdtr\":{\"Nm\":\"CONTACT LEGAL NM\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"CtctDtls\":{\"MobNb\":\"09890800908\",\"EmailAdr\":\"guiii@gmail.com\",\"Othr\":\"EN\"}},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"L\",\"securityQuestion\":\"What's the password I gave you?\",\"securityAnswer\":\"43abd74f52900c169131a4646d069cd2d24bfc1344bdc2b45f5e26d8a7491a4a\",\"hashType\":\"SHA2\",\"hashSalt\":\"1W\",\"ChnlPymtId\":\"#VAR4\",\"DbAmt\":\"47.33\",\"Ccy\":\"CAD\",\"ProductCd\":\"DOMESTIC\",\"DbtrPuId\":\"99000080\",\"PymtExpDt\":\"#VAR5T01:57:55.862Z\",\"AliasHandleType\":\"EMAIL\",\"AliasHandleId\":\"guiii@gmail.com\",\"AcctCretDt\":\"2020-01-29\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"9900008099000001\",\"ChnlIndctr\":\"ONLINE\",\"CustIpAddr\":\"000.000.000.000\",\"CustAuthMtd\":\"PASSWORD\",\"AuthTp\":\"PAYMENT_LEVEL\"}}}}}}";
				String VAR_messageid = key.getSaltString("Y", 10,"PSHBETAUT");
				System.out.println(VAR_messageid);
				key.StoreValue("VAR_MESSAGEID",VAR_messageid);
				String VAR_chnlid = key.getSaltString("Y", 10,"BETchn");
				System.out.println(VAR_chnlid);
				key.StoreValue("VAR_PSHBTRCHANLID",VAR_chnlid);
				 input2 = input1.replace("#VAR1",VAR_messageid);
				 input2 = input2.replace("#VAR2",key._DateFormat());
				 input2 = input2.replace("#VAR3",key._DateFormat());
				 input2 = input2.replace("#VAR4",VAR_chnlid);
				 input2 = input2.replace("#VAR5",PaymentExpiryDate);
				System.out.println("The final API is:"+input2);
				log.info("The final API is:"+input2);
					status = betcall.POSTToAPI(input2);
			}
						catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
			case "BTRxBVxTC1xF1xTXN11":
			
			try {
				input1=	"{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T01:57:55\",\"NbOfTxs\":\"2\",\"CtrlSum\":\"94.66\",\"InitgPty\":{\"Nm\":\"CBX 02 Client02\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"CBXBATCH001\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"2\",\"CtrlSum\":\"47.33\",\"PmtTpInf\":{\"SvcLvl\":{\"Prtry\":\"REGULAR_PAYMENT\"},\"LclInstrm\":{\"Prtry\":\"BTR\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"CBX 02 Client02\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}},\"DbtrAcct\":{\"Ccy\":\"CAD\",\"Nm\":\"etransfer Account\",\"Id\":{\"Othr\":{\"Id\":\"6402409\"}}},\"DbtrAgt\":{\"FinInstnId\":{\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"2001\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"092286-0801202kr01\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"47.33\"}},\"Cdtr\":{\"Nm\":\"CONTACT LEGAL NM\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"CtctDtls\":{\"MobNb\":\"09890800908\",\"EmailAdr\":\"guiii@gmail.com\",\"Othr\":\"EN\"}},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"L\",\"securityQuestion\":\"What's the password I gave you?\",\"securityAnswer\":\"43abd74f52900c169131a4646d069cd2d24bfc1344bdc2b45f5e26d8a7491a4a\",\"hashType\":\"SHA2\",\"hashSalt\":\"1W\",\"ChnlPymtId\":\"#VAR4\",\"DbAmt\":\"47.33\",\"Ccy\":\"CAD\",\"ProductCd\":\"DOMESTIC\",\"DbtrPuId\":\"99000080\",\"PymtExpDt\":\"#VAR6T01:57:55.862Z\",\"AliasHandleType\":\"EMAIL\",\"AliasHandleId\":\"guiii@gmail.com\",\"AcctCretDt\":\"2020-01-29\"}}}},{\"PmtId\":{\"EndToEndId\":\"092286-0801202kr01\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"47.33\"}},\"Cdtr\":{\"Nm\":\"CONTACT LEGAL NM\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"CtctDtls\":{\"MobNb\":\"09890800908\",\"EmailAdr\":\"guiii@gmail.com\",\"Othr\":\"EN\"}},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"L\",\"securityQuestion\":\"What's the password I gave you?\",\"securityAnswer\":\"43abd74f52900c169131a4646d069cd2d24bfc1344bdc2b45f5e26d8a7491a4a\",\"hashType\":\"SHA2\",\"hashSalt\":\"1W\",\"ChnlPymtId\":\"#VAR5\",\"DbAmt\":\"47.33\",\"Ccy\":\"CAD\",\"ProductCd\":\"DOMESTIC\",\"DbtrPuId\":\"99000080\",\"PymtExpDt\":\"#VAR6T01:57:55.862Z\",\"AliasHandleType\":\"EMAIL\",\"AliasHandleId\":\"guiii@gmail.com\",\"AcctCretDt\":\"2020-01-29\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"9900008099000001\",\"ChnlIndctr\":\"ONLINE\",\"CustIpAddr\":\"000.000.000.000\",\"CustAuthMtd\":\"PASSWORD\",\"AuthTp\":\"PAYMENT_LEVEL\"}}}}}}";
				String VAR_messageid = key.getSaltString("Y", 10,"PSHBETAUT");
				System.out.println(VAR_messageid);
				key.StoreValue("VAR_MESSAGEID",VAR_messageid);
				String VAR_chnlid = key.getSaltString("Y", 10,"BETchn");
				System.out.println(VAR_chnlid);
				String VAR_chnlid2 = key.getSaltString("Y", 10,"BETchn");
				System.out.println(VAR_chnlid2);
				key.StoreValue("VAR_PSHBTRCHANLID",VAR_chnlid);
				 input2 = input1.replace("#VAR1",VAR_messageid);
				 input2 = input2.replace("#VAR2",key._DateFormat());
				 input2 = input2.replace("#VAR3",key._DateFormat());
				 input2 = input2.replace("#VAR4",VAR_chnlid);
				 input2 = input2.replace("#VAR5",VAR_chnlid2);
				 input2 = input2.replace("#VAR6", PaymentExpiryDate);
				System.out.println("The final API is:"+input2);	
					status = betcall.POSTToAPI(input2);
			}
						catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;	
	      case "BTRxBVxTC1xF1xTXN12":
		try {
			input1 ="{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T01:57:55\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"57.33\",\"InitgPty\":{\"Nm\":\"CBX 02 Client02\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"CBXBATCH001\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"47.33\",\"PmtTpInf\":{\"SvcLvl\":{\"Prtry\":\"REGULAR_PAYMENT\"},\"LclInstrm\":{\"Prtry\":\"BTR\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"CBX 02 Client02\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}},\"DbtrAcct\":{\"Ccy\":\"CAD\",\"Nm\":\"etransfer Account\",\"Id\":{\"Othr\":{\"Id\":\"6402409\"}}},\"DbtrAgt\":{\"FinInstnId\":{\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"2001\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"092286-0801202kr01\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"47.33\"}},\"Cdtr\":{\"Nm\":\"CONTACT LEGAL NM\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"CtctDtls\":{\"MobNb\":\"09890800908\",\"EmailAdr\":\"guiii@gmail.com\",\"Othr\":\"EN\"}},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"L\",\"securityQuestion\":\"What's the password I gave you?\",\"securityAnswer\":\"43abd74f52900c169131a4646d069cd2d24bfc1344bdc2b45f5e26d8a7491a4a\",\"hashType\":\"SHA2\",\"hashSalt\":\"1W\",\"ChnlPymtId\":\"#VAR4\",\"DbAmt\":\"47.33\",\"Ccy\":\"CAD\",\"ProductCd\":\"DOMESTIC\",\"DbtrPuId\":\"99000080\",\"PymtExpDt\":\"#VAR5T01:57:55.862Z\",\"AliasHandleType\":\"EMAIL\",\"AliasHandleId\":\"guiii@gmail.com\",\"AcctCretDt\":\"2020-01-29\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"9900008099000001\",\"ChnlIndctr\":\"ONLINE\",\"CustIpAddr\":\"000.000.000.000\",\"CustAuthMtd\":\"PASSWORD\",\"AuthTp\":\"PAYMENT_LEVEL\"}}}}}}";
			 
			String VAR_messageid = key.getSaltString("Y", 10,"PSHBETAUT");
			System.out.println("Message id"+VAR_messageid);
			key.StoreValue("VAR_MESSAGEID",VAR_messageid);
			String VAR_chnlid = key.getSaltString("Y", 10,"BETchn");
			System.out.println("channel id:"+VAR_chnlid);
			key.StoreValue("VAR_PSHBTRCHANLID",VAR_chnlid);
			 input2 = input1.replace("#VAR1",VAR_messageid);
			 input2 = input2.replace("#VAR2",key._DateFormat());
			 input2 = input2.replace("#VAR3",key._DateFormat());
			 input2 = input2.replace("#VAR4",VAR_chnlid);
			 input2 = input2.replace("#VAR5",PaymentExpiryDate);
			System.out.println("The final API is:"+input2);	
				status = betcall.POSTToAPI(input2);
				}
				catch (Exception e) {
						// TODO Auto-generated catch block
				e.printStackTrace();
		}
			break;
	      case "BTRxBVxTC1xF1xTXN13":
	  		try {
	  			input1 ="{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T01:57:55\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"47.33\",\"InitgPty\":{\"Nm\":\"CBX 02 Client02\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"CBXBATCH001\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"5\",\"CtrlSum\":\"47.33\",\"PmtTpInf\":{\"SvcLvl\":{\"Prtry\":\"REGULAR_PAYMENT\"},\"LclInstrm\":{\"Prtry\":\"BTR\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"CBX 02 Client02\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}},\"DbtrAcct\":{\"Ccy\":\"CAD\",\"Nm\":\"etransfer Account\",\"Id\":{\"Othr\":{\"Id\":\"6402409\"}}},\"DbtrAgt\":{\"FinInstnId\":{\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"2001\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"092286-0801202kr01\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"47.33\"}},\"Cdtr\":{\"Nm\":\"CONTACT LEGAL NM\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"CtctDtls\":{\"MobNb\":\"09890800908\",\"EmailAdr\":\"guiii@gmail.com\",\"Othr\":\"EN\"}},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"L\",\"securityQuestion\":\"What's the password I gave you?\",\"securityAnswer\":\"43abd74f52900c169131a4646d069cd2d24bfc1344bdc2b45f5e26d8a7491a4a\",\"hashType\":\"SHA2\",\"hashSalt\":\"1W\",\"ChnlPymtId\":\"#VAR4\",\"DbAmt\":\"47.33\",\"Ccy\":\"CAD\",\"ProductCd\":\"DOMESTIC\",\"DbtrPuId\":\"99000080\",\"PymtExpDt\":\"#VAR5T01:57:55.862Z\",\"AliasHandleType\":\"EMAIL\",\"AliasHandleId\":\"guiii@gmail.com\",\"AcctCretDt\":\"2020-01-29\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"9900008099000001\",\"ChnlIndctr\":\"ONLINE\",\"CustIpAddr\":\"000.000.000.000\",\"CustAuthMtd\":\"PASSWORD\",\"AuthTp\":\"PAYMENT_LEVEL\"}}}}}}";
				 
				String VAR_messageid = key.getSaltString("Y", 10,"PSHBETAUT");
				System.out.println("message id: "+VAR_messageid);
				key.StoreValue("VAR_MESSAGEID",VAR_messageid);
				String VAR_chnlid = key.getSaltString("Y", 10,"BETchn");
				System.out.println("Channel id: "+VAR_chnlid);
				key.StoreValue("VAR_PSHBTRCHANLID",VAR_chnlid);
				 input2 = input1.replace("#VAR1",VAR_messageid);
				 input2 = input2.replace("#VAR2",key._DateFormat());
				 input2 = input2.replace("#VAR3",key._DateFormat());
				 input2 = input2.replace("#VAR4",VAR_chnlid);
				 input2 = input2.replace("#VAR5",PaymentExpiryDate);
				System.out.println("The final API is:"+input2);	
					status = betcall.POSTToAPI(input2);
	  		}
	  							catch (Exception e) {
	  						// TODO Auto-generated catch block
	  						e.printStackTrace();
	  						}
	  		break;
	      case "BTRxBVxTC1xF1xTXN14":
		  		try {
		  			input1 ="{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T01:57:55\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"47.33\",\"InitgPty\":{\"Nm\":\"CBX 02 Client02\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"CBXBATCH001\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"107.33\",\"PmtTpInf\":{\"SvcLvl\":{\"Prtry\":\"REGULAR_PAYMENT\"},\"LclInstrm\":{\"Prtry\":\"BTR\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"CBX 02 Client02\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}},\"DbtrAcct\":{\"Ccy\":\"CAD\",\"Nm\":\"etransfer Account\",\"Id\":{\"Othr\":{\"Id\":\"6402409\"}}},\"DbtrAgt\":{\"FinInstnId\":{\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"2001\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"092286-0801202kr01\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"47.33\"}},\"Cdtr\":{\"Nm\":\"CONTACT LEGAL NM\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"CtctDtls\":{\"MobNb\":\"09890800908\",\"EmailAdr\":\"guiii@gmail.com\",\"Othr\":\"EN\"}},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"L\",\"securityQuestion\":\"What's the password I gave you?\",\"securityAnswer\":\"43abd74f52900c169131a4646d069cd2d24bfc1344bdc2b45f5e26d8a7491a4a\",\"hashType\":\"SHA2\",\"hashSalt\":\"1W\",\"ChnlPymtId\":\"#VAR4\",\"DbAmt\":\"47.33\",\"Ccy\":\"CAD\",\"ProductCd\":\"DOMESTIC\",\"DbtrPuId\":\"99000080\",\"PymtExpDt\":\"#VAR5T01:57:55.862Z\",\"AliasHandleType\":\"EMAIL\",\"AliasHandleId\":\"guiii@gmail.com\",\"AcctCretDt\":\"2020-01-29\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"9900008099000001\",\"ChnlIndctr\":\"ONLINE\",\"CustIpAddr\":\"000.000.000.000\",\"CustAuthMtd\":\"PASSWORD\",\"AuthTp\":\"PAYMENT_LEVEL\"}}}}}}";
					 
					String VAR_messageid = key.getSaltString("Y", 10,"PSHBETAUT");
					System.out.println("message id: "+VAR_messageid);
					key.StoreValue("VAR_MESSAGEID",VAR_messageid);
					String VAR_chnlid = key.getSaltString("Y", 10,"BETchn");
					System.out.println("Channel id: "+VAR_chnlid);
					key.StoreValue("VAR_PSHBTRCHANLID",VAR_chnlid);
					 input2 = input1.replace("#VAR1",VAR_messageid);
					 input2 = input2.replace("#VAR2",key._DateFormat());
					 input2 = input2.replace("#VAR3",key._DateFormat());
					 input2 = input2.replace("#VAR4",VAR_chnlid);
					 input2 = input2.replace("#VAR5",PaymentExpiryDate);
					System.out.println("The final API is:"+input2);	
						status = betcall.POSTToAPI(input2);
		  		}
		  							catch (Exception e) {
		  						// TODO Auto-generated catch block
		  						e.printStackTrace();
		  						}
		  		break;
	      case "BTRxBVxTC1xF1xTXN15":
	    	  try {
		  			input1 ="{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T01:57:55\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"47.33\",\"InitgPty\":{\"Nm\":\"CBX 02 Client02\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"CBXBATCH001\",\"PmtMtd\":\"CHK\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"47.33\",\"PmtTpInf\":{\"SvcLvl\":{\"Prtry\":\"REGULAR_PAYMENT\"},\"LclInstrm\":{\"Prtry\":\"BTR\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"CBX 02 Client02\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}},\"DbtrAcct\":{\"Ccy\":\"CAD\",\"Nm\":\"etransfer Account\",\"Id\":{\"Othr\":{\"Id\":\"6402409\"}}},\"DbtrAgt\":{\"FinInstnId\":{\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"2001\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"092286-0801202kr01\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"47.33\"}},\"Cdtr\":{\"Nm\":\"CONTACT LEGAL NM\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"CtctDtls\":{\"MobNb\":\"09890800908\",\"EmailAdr\":\"guiii@gmail.com\",\"Othr\":\"EN\"}},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"L\",\"securityQuestion\":\"What's the password I gave you?\",\"securityAnswer\":\"43abd74f52900c169131a4646d069cd2d24bfc1344bdc2b45f5e26d8a7491a4a\",\"hashType\":\"SHA2\",\"hashSalt\":\"1W\",\"ChnlPymtId\":\"#VAR4\",\"DbAmt\":\"47.33\",\"Ccy\":\"CAD\",\"ProductCd\":\"DOMESTIC\",\"DbtrPuId\":\"99000080\",\"PymtExpDt\":\"#VAR5T01:57:55.862Z\",\"AliasHandleType\":\"EMAIL\",\"AliasHandleId\":\"guiii@gmail.com\",\"AcctCretDt\":\"2020-01-29\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"9900008099000001\",\"ChnlIndctr\":\"ONLINE\",\"CustIpAddr\":\"000.000.000.000\",\"CustAuthMtd\":\"PASSWORD\",\"AuthTp\":\"PAYMENT_LEVEL\"}}}}}}";
					String VAR_messageid = key.getSaltString("Y", 10,"PSHBETAUT");
					System.out.println("message id: "+VAR_messageid);
					key.StoreValue("VAR_MESSAGEID",VAR_messageid);
					String VAR_chnlid = key.getSaltString("Y", 10,"BETchn");
					System.out.println("Channel id: "+VAR_chnlid);
					key.StoreValue("VAR_PSHBTRCHANLID",VAR_chnlid);
					 input2 = input1.replace("#VAR1",VAR_messageid);
					 input2 = input2.replace("#VAR2",key._DateFormat());
					 input2 = input2.replace("#VAR3",key._DateFormat());
					 input2 = input2.replace("#VAR4",VAR_chnlid);
					 input2 = input2.replace("#VAR5",PaymentExpiryDate);
					System.out.println("The final API is:"+input2);	
						status = betcall.POSTToAPI(input2);
		  		}
		  							catch (Exception e) {
		  						// TODO Auto-generated catch block
		  						e.printStackTrace();
		  						}
		  		break;
	      case "BTRxBVxTC1xF1xTXN16":
		  		try {
		  			input1 ="{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T01:57:55\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"47.33\",\"InitgPty\":{\"Nm\":\"CBX 02 Client02\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"CBXBATCH001\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"47.33\",\"PmtTpInf\":{\"SvcLvl\":{\"Prtry\":\"REGULAR_PAYMENT\"},\"LclInstrm\":{\"Prtry\":\"BTR\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"CBX 02 Client02\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}},\"DbtrAcct\":{\"Ccy\":\"CAD\",\"Nm\":\"etransfer Account\",\"Id\":{\"Othr\":{\"Id\":\"6402409\"}}},\"DbtrAgt\":{\"FinInstnId\":{\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"2001\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"092286-0801202kr01\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"47.33\"}},\"Cdtr\":{\"Nm\":\"CONTACT LEGAL NM\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"CtctDtls\":{\"MobNb\":\"09890800908\",\"EmailAdr\":\"guiii@gmail.com\",\"Othr\":\"EN\"}},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"L\",\"securityQuestion\":\"What's the password I gave you?\",\"securityAnswer\":\"43abd74f52900c169131a4646d069cd2d24bfc1344bdc2b45f5e26d8a7491a4a\",\"hashType\":\"SHA2\",\"hashSalt\":\"1W\",\"ChnlPymtId\":\"#VAR4\",\"DbAmt\":\"47.33\",\"Ccy\":\"CAD\",\"ProductCd\":\"DOMESTIC\",\"DbtrPuId\":\"99000080\",\"PymtExpDt\":\"#VAR5T01:57:55.862Z\",\"AliasHandleType\":\"EMAIL\",\"AliasHandleId\":\"guiii@gmail.com\",\"AcctCretDt\":\"2020-01-29\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"9900008099000001\",\"ChnlIndctr\":\"ONLINE\",\"CustIpAddr\":\"000.000.000.000\",\"CustAuthMtd\":\"PASSWORD\",\"AuthTp\":\"PAYMENT_LEVEL\"}}}}}}";
					String VAR_messageid = key.getSaltString("Y", 10,"PSHBETAUT");
					System.out.println("message id: "+VAR_messageid);
					key.StoreValue("VAR_MESSAGEID",VAR_messageid);
					String VAR_chnlid = key.getSaltString("Y", 10,"BETchn");
					System.out.println("Channel id: "+VAR_chnlid);
					key.StoreValue("VAR_PSHBTRCHANLID",VAR_chnlid);
					 input2 = input1.replace("#VAR1",key.getSaltString("Y", 10,"PSHBETAUT"));
					 input2 = input2.replace("#VAR2",key._DateFormat());
					 input2 = input2.replace("#VAR3",key._DateFormat());
					 input2 = input2.replace("#VAR4",VAR_chnlid);
					 input2 = input2.replace("#VAR5",PaymentExpiryDate);
					 System.out.println("First API is:"+input2);	
						String status1=betcall.POSTToAPI(input2);
						Thread.sleep(10000);
					String input3 = input1.replace("#VAR1", VAR_messageid);
					input3 = input3.replace("#VAR2",key._DateFormat());
					input3 = input3.replace("#VAR3",key._DateFormat());
					input3 = input3.replace("#VAR4",VAR_chnlid);
					input3 = input3.replace("#VAR5",PaymentExpiryDate);
					System.out.println("Second API with same message id:" + input3);
					status = betcall.POSTToAPI(input3);
		  		}
		  							catch (Exception e) {
		  						// TODO Auto-generated catch block
		  						e.printStackTrace();
		  						}
		  		break;
	      case "BTRxBVxTC1xF1xTXN18":
		  		try {
		  			input1 ="{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T01:57:55\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"47.33\",\"InitgPty\":{\"Nm\":\"CBX 02 Client02\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"CBXBATCH001\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"47.33\",\"PmtTpInf\":{\"SvcLvl\":{\"Prtry\":\"REGULAR_PAYMENT\"},\"LclInstrm\":{\"Prtry\":\"CHK\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"CBX 02 Client02\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}},\"DbtrAcct\":{\"Ccy\":\"CAD\",\"Nm\":\"etransfer Account\",\"Id\":{\"Othr\":{\"Id\":\"6402409\"}}},\"DbtrAgt\":{\"FinInstnId\":{\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"2001\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"092286-0801202kr01\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"47.33\"}},\"Cdtr\":{\"Nm\":\"CONTACT LEGAL NM\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"CtctDtls\":{\"MobNb\":\"09890800908\",\"EmailAdr\":\"guiii@gmail.com\",\"Othr\":\"EN\"}},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"L\",\"securityQuestion\":\"What's the password I gave you?\",\"securityAnswer\":\"43abd74f52900c169131a4646d069cd2d24bfc1344bdc2b45f5e26d8a7491a4a\",\"hashType\":\"SHA2\",\"hashSalt\":\"1W\",\"ChnlPymtId\":\"#VAR4\",\"DbAmt\":\"47.33\",\"Ccy\":\"CAD\",\"ProductCd\":\"DOMESTIC\",\"DbtrPuId\":\"99000080\",\"PymtExpDt\":\"#VAR5T01:57:55.862Z\",\"AliasHandleType\":\"EMAIL\",\"AliasHandleId\":\"guiii@gmail.com\",\"AcctCretDt\":\"2020-01-29\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"9900008099000001\",\"ChnlIndctr\":\"ONLINE\",\"CustIpAddr\":\"000.000.000.000\",\"CustAuthMtd\":\"PASSWORD\",\"AuthTp\":\"PAYMENT_LEVEL\"}}}}}}";
					String VAR_messageid = key.getSaltString("Y", 10,"PSHBETAUT");
					System.out.println("message id: "+VAR_messageid);
					key.StoreValue("VAR_MESSAGEID",VAR_messageid);
					String VAR_chnlid = key.getSaltString("Y", 10,"BETchn");
					System.out.println("Channel id: "+VAR_chnlid);
					key.StoreValue("VAR_PSHBTRCHANLID",VAR_chnlid);
					 input2 = input1.replace("#VAR1",VAR_messageid);
					 input2 = input2.replace("#VAR2",key._DateFormat());
					 input2 = input2.replace("#VAR3",key._DateFormat());
					 input2 = input2.replace("#VAR4",VAR_chnlid);
					 input2 = input2.replace("#VAR5",PaymentExpiryDate);
					System.out.println("The final API is:"+input2);	
						status = betcall.POSTToAPI(input2);
		  		}
		  							catch (Exception e) {
		  						// TODO Auto-generated catch block
		  						e.printStackTrace();
		  						}
		  		break;
	      case "BTRxBVxTC1xF1xTXN19":
		  		try {
		  			input1 ="{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T01:57:55\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"47.33\",\"InitgPty\":{\"Nm\":\"CBX 02 Client02\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"CBXBATCH001\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"47.33\",\"PmtTpInf\":{\"SvcLvl\":{\"Prtry\":\"REGULAR_PAYMENT\"},\"LclInstrm\":{\"Prtry\":\"BTR\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"CBX 02 Client02\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}},\"DbtrAcct\":{\"Ccy\":\"CAD\",\"Nm\":\"etransfer Account\",\"Id\":{\"Othr\":{\"Id\":\"6402409\"}}},\"DbtrAgt\":{\"FinInstnId\":{\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"2001\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"092286-0801202kr01\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"47.33\"}},\"Cdtr\":{\"Nm\":\"CONTACT LEGAL NM\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"CtctDtls\":{\"MobNb\":\"09890800908\",\"EmailAdr\":\"guiii@gmail.com\",\"Othr\":\"EN\"}},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"L\",\"securityQuestion\":\"What's the password I gave you?\",\"securityAnswer\":\"43abd74f52900c169131a4646d069cd2d24bfc1344bdc2b45f5e26d8a7491a4a\",\"hashType\":\"SHA2\",\"hashSalt\":\"1W\",\"ChnlPymtId\":\"#VAR4\",\"DbAmt\":\"47.33\",\"Ccy\":\"CAD\",\"ProductCd\":\"DOMESTIC\",\"DbtrPuId\":\"99000080\",\"PymtExpDt\":\"#VAR5T01:57:55.862Z\",\"AliasHandleType\":\"EMAIL\",\"AliasHandleId\":\"guiii@gmail.com\",\"AcctCretDt\":\"2020-01-29\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"9900008099000001\",\"ChnlIndctr\":\"ONLINE\",\"CustIpAddr\":\"000.000.000.000\",\"CustAuthMtd\":\"PASSWORD\",\"AuthTp\":\"PAYMENT_LEVEL\"}}}}}}";
					String VAR_messageid = key.getSaltString("Y", 10,"PSHBETAUT");
					System.out.println("message id: "+VAR_messageid);
					key.StoreValue("VAR_MESSAGEID",VAR_messageid);
					String VAR_chnlid = key.getSaltString("Y", 10,"BETchn");
					System.out.println("Channel id: "+VAR_chnlid);
					key.StoreValue("VAR_PSHBTRCHANLID",VAR_chnlid);
					PSHBetCall betcall1 = new PSHBetCall();
					input2 = input1.replace("#VAR1",VAR_messageid);
					 input2 = input2.replace("#VAR2",key._DateFormat());
					 input2 = input2.replace("#VAR3",betcall1.AddDate(CurrentDate,-1));
					 input2 = input2.replace("#VAR4",VAR_chnlid);
					 input2 = input2.replace("#VAR5",PaymentExpiryDate);
					 //input2 = input2.replace("#VAR5",PaymentExpiryDate);
					System.out.println("The final API is:"+input2);	
						status = betcall.POSTToAPI(input2);
		  		}
		  							catch (Exception e) {
		  						// TODO Auto-generated catch block
		  						e.printStackTrace();
		  						}
		  		break;
	      case "BTRxBVxTC1xF1xTXN20":
		  		try {
		  			input1 ="{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T01:57:55\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"47.33\",\"InitgPty\":{\"Nm\":\"CBX 02 Client02\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"CBXBATCH001\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"47.33\",\"PmtTpInf\":{\"SvcLvl\":{\"Prtry\":\"REGULAR_PAYMENT\"},\"LclInstrm\":{\"Prtry\":\"BTR\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"CBX 02 Client02\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}},\"DbtrAcct\":{\"Ccy\":\"CAD\",\"Nm\":\"etransfer Account\",\"Id\":{\"Othr\":{\"Id\":\"6402409\"}}},\"DbtrAgt\":{\"FinInstnId\":{\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"2001\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"092286-0801202kr01\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"47.33\"}},\"Cdtr\":{\"Nm\":\"CONTACT LEGAL NM\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"CtctDtls\":{\"MobNb\":\"09890800908\",\"EmailAdr\":\"guiii@gmail.com\",\"Othr\":\"EN\"}},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"L\",\"securityQuestion\":\"What's the password I gave you?\",\"securityAnswer\":\"43abd74f52900c169131a4646d069cd2d24bfc1344bdc2b45f5e26d8a7491a4a\",\"hashType\":\"SHA2\",\"hashSalt\":\"1W\",\"ChnlPymtId\":\"#VAR4\",\"DbAmt\":\"47.33\",\"Ccy\":\"CAD\",\"ProductCd\":\"DOMESTIC\",\"DbtrPuId\":\"99000080\",\"PymtExpDt\":\"#VAR5T01:57:55.862Z\",\"AliasHandleType\":\"EMAIL\",\"AliasHandleId\":\"guiii@gmail.com\",\"AcctCretDt\":\"2020-01-29\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"9900008099000001\",\"ChnlIndctr\":\"ONLINE\",\"CustIpAddr\":\"000.000.000.000\",\"CustAuthMtd\":\"PASSWORD\",\"AuthTp\":\"PAYMENT_LEVEL\"}}}}}}";
					String VAR_messageid = key.getSaltString("Y", 10,"PSHBETAUT");
					System.out.println("message id: "+VAR_messageid);
					key.StoreValue("VAR_MESSAGEID",VAR_messageid);
					String VAR_chnlid = key.getSaltString("Y", 10,"BETchn");
					System.out.println("Channel id: "+VAR_chnlid);
					key.StoreValue("VAR_PSHBTRCHANLID",VAR_chnlid);
					PSHBetCall betcall1 = new PSHBetCall();
					input2 = input1.replace("#VAR1",VAR_messageid);
					 input2 = input2.replace("#VAR2",key._DateFormat());
					 input2 = input2.replace("#VAR3",betcall1.AddDate(CurrentDate,101));
					 input2 = input2.replace("#VAR4",VAR_chnlid);
					 input2 = input2.replace("#VAR5",PaymentExpiryDate);
					 //input2 = input2.replace("#VAR5",PaymentExpiryDate);
					System.out.println("The final API is:"+input2);	
						status = betcall.POSTToAPI(input2);
		  		}
		  							catch (Exception e) {
		  						// TODO Auto-generated catch block
		  						e.printStackTrace();
		  						}
		  		break;
	      case "BTRxBVxTC1xF1xTXN7":
		  		try {
		  			input1 = "{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T01:57:55\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"47.33\",\"InitgPty\":{\"Nm\":\"CBX 02 Client02\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"CBXBATCH001\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"47.33\",\"PmtTpInf\":{\"SvcLvl\":{\"Prtry\":\"REGULAR_PAYMENT\"},\"LclInstrm\":{\"Prtry\":\"BTR\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"CBX 02 Client02\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}},\"DbtrAcct\":{\"Ccy\":\"CAD\",\"Nm\":\"etransfer Account\",\"Id\":{\"Othr\":{\"Id\":\"6402409\"}}},\"DbtrAgt\":{\"FinInstnId\":{\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"2001\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"092286-0801202kr01\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"47.33\"}},\"Cdtr\":{\"Nm\":\"CONTACT LEGAL NM\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"CtctDtls\":{\"MobNb\":\"09890800908\",\"EmailAdr\":\"guiii@gmail.com\",\"Othr\":\"EN\"}},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"L\",\"securityQuestion\":\"What's the password I gave you?\",\"securityAnswer\":\"43abd74f52900c169131a4646d069cd2d24bfc1344bdc2b45f5e26d8a7491a4a\",\"hashType\":\"SHA2\",\"hashSalt\":\"1W\",\"ChnlPymtId\":\"#VAR4\",\"DbAmt\":\"47.33\",\"Ccy\":\"CAD\",\"ProductCd\":\"DOMESTIC\",\"DbtrPuId\":\"99000080\",\"PymtExpDt\":\"#VAR5T01:57:55.862Z\",\"AliasHandleType\":\"EMAIL\",\"AliasHandleId\":\"guiii@gmail.com\",\"AcctCretDt\":\"2020-01-29\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"9900008099000001\",\"ChnlIndctr\":\"ONLINE\",\"CustIpAddr\":\"000.000.000.000\",\"CustAuthMtd\":\"PASSWORD\",\"AuthTp\":\"PAYMENT_LEVEL\"}}}}}}";
		  			System.out.println("The final API is:"+input1);			
		  			status = betcall.POSTToAPI2(input1, TestData);
		  			System.out.println(status);
		  		}
		  							catch (Exception e) {
		  						// TODO Auto-generated catch block
		  						e.printStackTrace();
		  						}
		  		break;
	      case "BTRxBVxTC1xF1xTXN6":
	    	  try {
	    		  input1 = "{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"CreDtTm\":\"VAR2T01:57:55\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"47.33\",\"InitgPty\":{\"Nm\":\"CBX 02 Client02\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"CBXBATCH001\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"47.33\",\"PmtTpInf\":{\"SvcLvl\":{\"Prtry\":\"REGULAR_PAYMENT\"},\"LclInstrm\":{\"Prtry\":\"BTR\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"CBX 02 Client02\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}},\"DbtrAcct\":{\"Ccy\":\"CAD\",\"Nm\":\"etransfer Account\",\"Id\":{\"Othr\":{\"Id\":\"6402409\"}}},\"DbtrAgt\":{\"FinInstnId\":{\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"2001\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"092286-0801202kr01\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"47.33\"}},\"Cdtr\":{\"Nm\":\"CONTACT LEGAL NM\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"CtctDtls\":{\"MobNb\":\"09890800908\",\"EmailAdr\":\"guiii@gmail.com\",\"Othr\":\"EN\"}},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"L\",\"securityQuestion\":\"What's the password I gave you?\",\"securityAnswer\":\"43abd74f52900c169131a4646d069cd2d24bfc1344bdc2b45f5e26d8a7491a4a\",\"hashType\":\"SHA2\",\"hashSalt\":\"1W\",\"ChnlPymtId\":\"#VAR4\",\"DbAmt\":\"47.33\",\"Ccy\":\"CAD\",\"ProductCd\":\"DOMESTIC\",\"DbtrPuId\":\"99000080\",\"PymtExpDt\":\"#VAR5T01:57:55.862Z\",\"AliasHandleType\":\"EMAIL\",\"AliasHandleId\":\"guiii@gmail.com\",\"AcctCretDt\":\"2020-01-29\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"9900008099000001\",\"ChnlIndctr\":\"ONLINE\",\"CustIpAddr\":\"000.000.000.000\",\"CustAuthMtd\":\"PASSWORD\",\"AuthTp\":\"PAYMENT_LEVEL\"}}}}}}";
		  			System.out.println("The final API is:"+input1);			
		  			status = betcall.POSTToAPI2(input1, TestData);
		  			System.out.println(status);
		  			log.info("Status:"+status);
		  		}
		  							catch (Exception e) {
		  						// TODO Auto-generated catch block
		  						e.printStackTrace();
		  						}
		  		break;
	      case "BTRxBVxTC1xF1xTXN1":
		  		try {
		  		input1=	"{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T01:57:55\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"47.33\",\"InitgPty\":{\"Nm\":\"CBX 02 Client02\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"CBXBATCH001\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"47.33\",\"PmtTpInf\":{\"SvcLvl\":{\"Prtry\":\"REGULAR_PAYMENT\"},\"LclInstrm\":{\"Prtry\":\"BTR\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"CBX 02 Client02\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}},\"DbtrAcct\":{\"Ccy\":\"CAD\",\"Nm\":\"etransfer Account\",\"Id\":{\"Othr\":{\"Id\":\"6402409\"}}},\"DbtrAgt\":{\"FinInstnId\":{\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"2001\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"092286-0801202kr01\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"47.33\"}},\"Cdtr\":{\"Nm\":\"CONTACT LEGAL NM\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"CtctDtls\":{\"MobNb\":\"09890800908\",\"EmailAdr\":\"guiii@gmail.com\",\"Othr\":\"EN\"}},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"L\",\"securityQuestion\":\"What's the password I gave you?\",\"securityAnswer\":\"43abd74f52900c169131a4646d069cd2d24bfc1344bdc2b45f5e26d8a7491a4a\",\"hashType\":\"SHA2\",\"hashSalt\":\"1W\",\"ChnlPymtId\":\"#VAR4\",\"DbAmt\":\"47.33\",\"Ccy\":\"CAD\",\"ProductCd\":\"DOMESTIC\",\"DbtrPuId\":\"99000080\",\"PymtExpDt\":\"#VAR5T01:57:55.862Z\",\"AliasHandleType\":\"EMAIL\",\"AliasHandleId\":\"guiii@gmail.com\",\"AcctCretDt\":\"2020-01-29\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"9900008099000001\",\"ChnlIndctr\":\"ONLINE\",\"CustIpAddr\":\"000.000.000.000\",\"CustAuthMtd\":\"PASSWORD\",\"AuthTp\":\"PAYMENT_LEVEL\"}}}}}}";		  		
		  		System.out.println("The final API is:"+input1);			
	  			status = betcall.POSTToAPI2(input1, TestData);
	  			System.out.println(status);
		  		}
		  							catch (Exception e) {
		  						// TODO Auto-generated catch block
		  						e.printStackTrace();
		  						}
		  		break;
	      case "BTRxBVxTC1xF1xTXN2":
		  		try {
		  		input1=	"{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T01:57:55\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"47.33\",\"InitgPty\":{\"Nm\":\"CBX 02 Client02\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"CBXBATCH001\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"47.33\",\"PmtTpInf\":{\"SvcLvl\":{\"Prtry\":\"REGULAR_PAYMENT\"},\"LclInstrm\":{\"Prtry\":\"BTR\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"CBX 02 Client02\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}},\"DbtrAcct\":{\"Ccy\":\"CAD\",\"Nm\":\"etransfer Account\",\"Id\":{\"Othr\":{\"Id\":\"6402409\"}}},\"DbtrAgt\":{\"FinInstnId\":{\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"2001\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"092286-0801202kr01\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"47.33\"}},\"Cdtr\":{\"Nm\":\"CONTACT LEGAL NM\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"CtctDtls\":{\"MobNb\":\"09890800908\",\"EmailAdr\":\"guiii@gmail.com\",\"Othr\":\"EN\"}},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"L\",\"securityQuestion\":\"What's the password I gave you?\",\"securityAnswer\":\"43abd74f52900c169131a4646d069cd2d24bfc1344bdc2b45f5e26d8a7491a4a\",\"hashType\":\"SHA2\",\"hashSalt\":\"1W\",\"ChnlPymtId\":\"#VAR4\",\"DbAmt\":\"47.33\",\"Ccy\":\"CAD\",\"ProductCd\":\"DOMESTIC\",\"DbtrPuId\":\"99000080\",\"PymtExpDt\":\"#VAR5T01:57:55.862Z\",\"AliasHandleType\":\"EMAIL\",\"AliasHandleId\":\"guiii@gmail.com\",\"AcctCretDt\":\"2020-01-29\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"9900008099000001\",\"ChnlIndctr\":\"ONLINE\",\"CustIpAddr\":\"000.000.000.000\",\"CustAuthMtd\":\"PASSWORD\",\"AuthTp\":\"PAYMENT_LEVEL\"}}}}}}";		  		
		  		System.out.println("The final API is:"+input1);			
	  			status = betcall.POSTToAPI2(input1, TestData);
	  			System.out.println(status);
		  		}
		  		catch (Exception e) {
		  		// TODO Auto-generated catch block
		  		e.printStackTrace();
		  						}
		  		break;
	      case "BTRxBVxTC1xF1xTXN5":
		  		try {
		  			input1= "{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T01:57:55\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"47.33\",\"InitgPty\":{\"Nmr\":\"CBX 02 Client02\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"CBXBATCH001\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"47.33\",\"PmtTpInf\":{\"SvcLvl\":{\"Prtry\":\"REGULAR_PAYMENT\"},\"LclInstrm\":{\"Prtry\":\"BTR\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"CBX 02 Client02\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}},\"DbtrAcct\":{\"Ccy\":\"CAD\",\"Nm\":\"etransfer Account\",\"Id\":{\"Othr\":{\"Id\":\"6402409\"}}},\"DbtrAgt\":{\"FinInstnId\":{\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"2001\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"092286-0801202kr01\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"47.33\"}},\"Cdtr\":{\"Nm\":\"CONTACT LEGAL NM\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"CtctDtls\":{\"MobNb\":\"09890800908\",\"EmailAdr\":\"guiii@gmail.com\",\"Othr\":\"EN\"}},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"L\",\"securityQuestion\":\"What's the password I gave you?\",\"securityAnswer\":\"43abd74f52900c169131a4646d069cd2d24bfc1344bdc2b45f5e26d8a7491a4a\",\"hashType\":\"SHA2\",\"hashSalt\":\"1W\",\"ChnlPymtId\":\"#VAR4\",\"DbAmt\":\"47.33\",\"Ccy\":\"CAD\",\"ProductCd\":\"DOMESTIC\",\"DbtrPuId\":\"99000080\",\"PymtExpDt\":\"#VAR5T01:57:55.862Z\",\"AliasHandleType\":\"EMAIL\",\"AliasHandleId\":\"guiii@gmail.com\",\"AcctCretDt\":\"2020-01-29\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"9900008099000001\",\"ChnlIndctr\":\"ONLINE\",\"CustIpAddr\":\"000.000.000.000\",\"CustAuthMtd\":\"PASSWORD\",\"AuthTp\":\"PAYMENT_LEVEL\"}}}}}}";
		  			String VAR_messageid = key.getSaltString("Y", 10,"PSHBETAUT");
					System.out.println("message id: "+VAR_messageid);
					key.StoreValue("VAR_MESSAGEID",VAR_messageid);
					String VAR_chnlid = key.getSaltString("Y", 10,"BETchn");
					System.out.println("Channel id: "+VAR_chnlid);
					key.StoreValue("VAR_PSHBTRCHANLID",VAR_chnlid);
					 input2 = input1.replace("#VAR1",VAR_messageid);
					 input2 = input2.replace("#VAR2",key._DateFormat());
					 input2 = input2.replace("#VAR3",key._DateFormat());
					 input2 = input2.replace("#VAR4",VAR_chnlid);
					 input2 = input2.replace("#VAR5",PaymentExpiryDate);
					System.out.println("The final API is:"+input2);	
						status = betcall.POSTToAPI(input2);
		  		}
		  						catch (Exception e) {
		  						// TODO Auto-generated catch block
		  						e.printStackTrace();
		  						}
		  		break;
	      case "BTRxBVxTC1xF1xTXN4":
		  		try {
		  			input1 = "{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T01:57:55\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"47.33\",\"InitgPty\":{\"Nm\":\"CBX 02 Client02\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"CBXBATCH001\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"47.33\",\"PmtTpInf\":{\"SvcLvl\":{\"Prtry\":\"REGULAR_PAYMENT\"},\"LclInstrm\":{\"Prtry\":\"BTR\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"CBX 02 Client02\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}},\"DbtrAcct\":{\"Ccy\":\"CAD\",\"Nm\":\"etransfer Account\",\"Id\":{\"Othr\":{\"Id\":\"6402409\"}}},\"DbtrAgt\":{\"FinInstnId\":{\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"2001\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"092286-0801202kr01\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"47.33\"}},\"Cdtr\":{\"Nm\":\"CONTACT LEGAL NM\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"CtctDtls\":{\"MobNb\":\"09890800908\",\"EmailAdr\":\"guiii@gmail.com\",\"Othr\":\"EN\"}},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"L\",\"securityQuestion\":\"What's the password I gave you?\",\"securityAnswer\":\"43abd74f52900c169131a4646d069cd2d24bfc1344bdc2b45f5e26d8a7491a4a\",\"hashType\":\"SHA2\",\"hashSalt\":\"1W\",\"ChnlPymtId\":\"#VAR4\",\"DbAmt\":\"47.33\",\"Ccy\":\"CAD\",\"ProductCd\":\"DOMESTIC\",\"DbtrPuId\":\"99000080\",\"PymtExpDt\":\"#VAR5T01:57:55.862Z\",\"AliasHandleType\":\"EMAIL\",\"AliasHandleId\":\"guiii@gmail.com\",\"AcctCretDt\":\"2020-01-29\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"9900008099000001\",\"ChnlIndctr\":\"ONLINE\",\"CustIpAddr\":\"000.000.000.000\",\"CustAuthMtd\":\"PASSWORD\",\"AuthTp\":\"PAYMENT_LEVEL\"}}}}}}";

		  			System.out.println("The final API is:"+input1);			
		  			status = betcall.POSTToAPI2(input1, TestData);
		  			System.out.println(status);
		  		}
		  						catch (Exception e) {
		  						// TODO Auto-generated catch block
		  						e.printStackTrace();
		  						}
		  		break;
	      case "BTRxBVxTC1xF1xTXN17":
		  		try {
		  			input1 = "{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T01:57:55\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"47.33\",\"InitgPty\":{\"Nm\":\"CBX 02 Client02\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"CBXBATCH001\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"47.33\",\"PmtTpInf\":{\"SvcLvl\":{\"Prtry\":\"REGULAR_PAYMENT\"},\"LclInstrm\":{\"Prtry\":\"BTR\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"CBX 02 Client02\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"99000080\"}}}},\"DbtrAcct\":{\"Ccy\":\"CAD\",\"Nm\":\"etransfer Account\",\"Id\":{\"Othr\":{\"Id\":\"6402409\"}}},\"DbtrAgt\":{\"FinInstnId\":{\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"2001\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"092286-0801202kr01\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"47.33\"}},\"Cdtr\":{\"Nm\":\"CONTACT LEGAL NM\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"CtctDtls\":{\"MobNb\":\"09890800908\",\"EmailAdr\":\"guiii@gmail.com\",\"Othr\":\"EN\"}},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"L\",\"securityQuestion\":\"What's the password I gave you?\",\"securityAnswer\":\"43abd74f52900c169131a4646d069cd2d24bfc1344bdc2b45f5e26d8a7491a4a\",\"hashType\":\"SHA2\",\"hashSalt\":\"1W\",\"ChnlPymtId\":\"#VAR4\",\"DbAmt\":\"47.33\",\"Ccy\":\"CAD\",\"ProductCd\":\"DOMESTIC\",\"DbtrPuId\":\"99000080\",\"PymtExpDt\":\"#VAR5T01:57:55.862Z\",\"AliasHandleType\":\"EMAIL\",\"AliasHandleId\":\"guiii@gmail.com\",\"AcctCretDt\":\"2020-01-29\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"9900008099000001\",\"ChnlIndctr\":\"ONLINE\",\"CustIpAddr\":\"000.000.000.000\",\"CustAuthMtd\":\"PASSWORD\",\"AuthTp\":\"PAYMENT_LEVEL\"}}}}}}";
		  			String VAR_messageid = key.getSaltString("Y", 10,"PSHBETAUT");
					System.out.println("message id: "+VAR_messageid);
					key.StoreValue("VAR_MESSAGEID",VAR_messageid);
					String VAR_chnlid = key.getSaltString("Y", 10,"BETchn");
					System.out.println("Channel id: "+VAR_chnlid);
					key.StoreValue("VAR_PSHBTRCHANLID",VAR_chnlid);
					 input2 = input1.replace("#VAR1",VAR_messageid);
					 input2 = input2.replace("#VAR2",key._DateFormat());
					 input2 = input2.replace("#VAR3",key._DateFormat());
					 input2 = input2.replace("#VAR4",VAR_chnlid);
					 input2 = input2.replace("#VAR5",PaymentExpiryDate);
					System.out.println("The final API is:"+input2);	
						status = betcall.POSTToAPI(input2);
		  		}
		  						catch (Exception e) {
		  						// TODO Auto-generated catch block
		  						e.printStackTrace();
		  						}
		  		break;
	      case "CC2_BV_TC014F01":
		  		try {
		  			input1 = "{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T11:51:01\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"100\",\"Authstn\":{\"Prtry\":\"N\"},\"InitgPty\":{\"Nm\":\"BILL PAY CEU Auto\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"12332423\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"ACHRMTBATCH1\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"100\",\"PmtTpInf\":{\"SvcLvl\":{\"Cd\":\"URGP\"},\"LclInstrm\":{\"Cd\":\"TRF\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"PSH - C\",\"PstlAdr\":{\"StrtNm\":\"Near Ekta Vihar\",\"BldgNb\":\"C-2\",\"PstCd\":\"400614\",\"TwnNm\":\"Seawood\",\"CtrySubDvsn\":\"ON\",\"Ctry\":\"CA\",\"AdrLine\":[\"Nilagiri Garden\",\"Nerul\",\"Navi Mumbai\"]},\"Id\":{\"OrgId\":{\"AnyBIC\":\"CIBCCATTXXX\"}}},\"DbtrAcct\":{\"Ccy\":\"USD\",\"Nm\":\"CIBC LTD\",\"Id\":{\"Othr\":{\"Id\":\"1208001\"}}},\"DbtrAgt\":{\"FinInstnId\":{\"BICFI\":\"CIBCCATAXXX\",\"Nm\":\"CIBC\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"12081\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"InstrId\":\"ACHREMIT\",\"EndToEndId\":\"#VAR4\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"USD\",\"Amt\":\"100\"}},\"XchgRateInf\":{\"XchgRate\":\"1.2931\",\"RateTp\":\"AGRD\",\"CtrctId\":\"213887431\"},\"ChrgBr\":\"DEBT\",\"IntrmyAgt1\":{\"FinInstnId\":{\"BICFI\":\"BUKBGB22\",\"ClrSysMmbId\":{\"ClrSysId\":{\"Cd\":\"CACPA\"},\"MmbId\":\"12345\"}}},\"CdtrAgt\":{\"FinInstnId\":{\"BICFI\":\"NWBKGB21\",\"ClrSysMmbId\":{\"ClrSysId\":{\"Cd\":\"USABA\"},\"MmbId\":\"011000015\"},\"Nm\":\"FEDERAL RESERVE BANK\",\"PstlAdr\":{\"StrtNm\":\"ELM ST\",\"BldgNb\":\"420\",\"PstCd\":\"1R1R1R\",\"TwnNm\":\"NEW YORK\",\"CtrySubDvsn\":\"NY\",\"Ctry\":\"US\",\"AdrLine\":[\"sasadaead\",\"dsfgs\",\"dsfsdf\"]}}},\"Cdtr\":{\"Nm\":\"SAM PVT LTD\",\"PstlAdr\":{\"StrtNm\":\"PISCATWAY\",\"BldgNb\":\"201\",\"PstCd\":\"12345\",\"TwnNm\":\"NEW YORK\",\"CtrySubDvsn\":\"NY\",\"Ctry\":\"US\",\"AdrLine\":[\"dsd\",\"sadlkl\",\"kllklk\"]}},\"CdtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"123456789\"}}},\"RmtInf\":{\"Ustrd\":\"Remitance Info Line01 DataXXXXXXX01Remitance Info Line02 DataXXXXXXX02Remitance Info Line03 DataXXXXXXX03Remitance Info Line04 DataXXXXXXX04\"},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"L\",\"Ccy\":\"CAD\",\"DbAmt\":\"100\"}}},\"Purp\":{\"Prtry\":\"SAL\"},\"InstrForDbtrAgt\":\"EN\"}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"1208000000000001\",\"FXId\":\"CMO   16072\"}}}}}}";

		  			String valueDate=Data[0];
		  			 input2 = input1.replace("#VAR1",bvmcall.messageID());
		  			 input2 = input2.replace("#VAR2",key._DateFormat());
		  			 input2 = input2.replace("#VAR3",bvmcall.Dateissue(valueDate));
		  			 input2 = input2.replace("#VAR4",bvmcall.EndtoEndID());
		  			 
		  			System.out.println("The final API is:"+input2);			
		  		status = bvmcall.getresponse(input2);
		  		}
		  						catch (Exception e) {
		  						// TODO Auto-generated catch block
		  						e.printStackTrace();
		  						}
		  		break;
	      case "CC2_BV_TC015F01":
		  		try {
		  			input1 = "{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T11:51:01\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"100\",\"Authstn\":{\"Prtry\":\"N\"},\"InitgPty\":{\"Nm\":\"BILL PAY CEU Auto\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"12332423\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"ACHRMTBATCH1\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"100\",\"PmtTpInf\":{\"SvcLvl\":{\"Cd\":\"URGP\"},\"LclInstrm\":{\"Cd\":\"TRF\"}},\"ReqdExctnDt\":\"#VAR3\",\"ChrgsAcct\":{\"Id\":{\"Othr\":{\"Id\":\"120811208001\"}},\"Ccy\":\"CAD\"},\"Dbtr\":{\"Nm\":\"PSH - C\",\"PstlAdr\":{\"StrtNm\":\"Near Ekta Vihar\",\"BldgNb\":\"C-2\",\"PstCd\":\"400614\",\"TwnNm\":\"Seawood\",\"CtrySubDvsn\":\"ON\",\"Ctry\":\"CA\",\"AdrLine\":[\"Nilagiri Garden\",\"Nerul\",\"Navi Mumbai\"]},\"Id\":{\"OrgId\":{\"AnyBIC\":\"CIBCCATTXXX\"}}},\"DbtrAcct\":{\"Ccy\":\"USD\",\"Nm\":\"CIBC LTD\",\"Id\":{\"Othr\":{\"Id\":\"1208001\"}}},\"DbtrAgt\":{\"FinInstnId\":{\"BICFI\":\"CIBCCATAXXX\",\"Nm\":\"CIBC\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"12081\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"InstrId\":\"ACHREMIT\",\"EndToEndId\":\"#VAR4\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"USD\",\"Amt\":\"100\"}},\"XchgRateInf\":{\"XchgRate\":\"1.2931\",\"RateTp\":\"AGRD\",\"CtrctId\":\"213887431\"},\"ChrgBr\":\"CRED\",\"IntrmyAgt1\":{\"FinInstnId\":{\"BICFI\":\"BUKBGB22\",\"ClrSysMmbId\":{\"ClrSysId\":{\"Cd\":\"CACPA\"},\"MmbId\":\"12345\"}}},\"CdtrAgt\":{\"FinInstnId\":{\"BICFI\":\"NWBKGB21\",\"ClrSysMmbId\":{\"ClrSysId\":{\"Cd\":\"USABA\"},\"MmbId\":\"011000015\"},\"Nm\":\"FEDERAL RESERVE BANK\",\"PstlAdr\":{\"StrtNm\":\"ELM ST\",\"BldgNb\":\"420\",\"PstCd\":\"1R1R1R\",\"TwnNm\":\"NEW YORK\",\"CtrySubDvsn\":\"NY\",\"Ctry\":\"US\",\"AdrLine\":[\"sasadaead\",\"dsfgs\",\"dsfsdf\"]}}},\"Cdtr\":{\"Nm\":\"SAM PVT LTD\",\"PstlAdr\":{\"StrtNm\":\"PISCATWAY\",\"BldgNb\":\"201\",\"PstCd\":\"12345\",\"TwnNm\":\"NEW YORK\",\"CtrySubDvsn\":\"NY\",\"Ctry\":\"US\",\"AdrLine\":[\"dsd\",\"sadlkl\",\"kllklk\"]}},\"CdtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"123456789\"}}},\"RmtInf\":{\"Ustrd\":\"Remitance Info Line01 DataXXXXXXX01Remitance Info Line02 DataXXXXXXX02Remitance Info Line03 DataXXXXXXX03Remitance Info Line04 DataXXXXXXX04\"},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"L\",\"Ccy\":\"USD\",\"DbAmt\":\"100\"}}},\"Purp\":{\"Prtry\":\"SAL\"},\"InstrForDbtrAgt\":\"EN\"}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"1208000000000001\",\"FXId\":\"CMO   16072\"}}}}}}";
		  			String valueDate=Data[0];
		  			 input2 = input1.replace("#VAR1",bvmcall.messageID());
		  			 input2 = input2.replace("#VAR2",key._DateFormat());
		  			 input2 = input2.replace("#VAR3",bvmcall.Dateissue(valueDate));
		  			 input2 = input2.replace("#VAR4",bvmcall.EndtoEndID()+"ALPHA");
		  			 
		  			System.out.println("The final API is:"+input2);			
		  		status = bvmcall.getresponse(input2);
		  		}
		  						catch (Exception e) {
		  						// TODO Auto-generated catch block
		  						e.printStackTrace();
		  						}
		  		break;
		  		
	      default:
				System.out.println("BVM API");
				break;
		}
		return status;
			}
}

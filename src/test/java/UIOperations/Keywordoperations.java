package UIOperations;
import java.util.Random;
import java.util.Set;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.formula.functions.Index;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import Logger.LoggerUtils;
import Modules.Modules;
import UI_CC2_ManualActionInquiry.PSH_Inqcontroller;
import Utilities.*;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Keywordoperations {
	FileUtilities Fileutil = new FileUtilities();
	PropertyUtils Prop = new PropertyUtils();
	ApiCall apisteve = new ApiCall();
	static Logger log = LoggerUtils.getLogger();
	public String status;
	  public ArrayList<String> arrayList1;  
	public void concatenateString(String excelInput)
	{ String temp="";
		String ExcelValue[] =excelInput.split("\\|");
		
		for(int i=0;i<ExcelValue.length-1;i++)
		{
			try {
				if (ExcelValue[i].contains("VAR_"))
				{
					ExcelValue[i]=Fileutil.GetFilevalue(ExcelValue[i]).trim();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			temp=temp+ExcelValue[i];
			
		}
		
		Fileutil.writeUsingFileWriter(ExcelValue[ExcelValue.length-1]+"="+temp);			
		
	}
	

	public void selectAndEnter(String XPATH, String TestData, WebDriver driver) {

		WebElement element = driver.findElement(By.xpath(XPATH));

		// Added by jyoti --pickvariable from values.properties

		if (TestData.contains("VAR_"))
			TestData = Fileutil.GetFilevalue(TestData);

		
		if (TestData.contains("|")) {
			int index = TestData.indexOf("|");
			String addinfo = TestData.substring(index + 1);
			TestData = TestData.substring(0, index);

			System.out.println("this is additional info" + addinfo);
			System.out.println("this is input" + TestData);

			element.clear();

			element.sendKeys(TestData);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			List<WebElement> options = driver
					.findElements(By.xpath("//div[@class='row flex-items-xs-between ng-star-inserted']"));

			int size = options.size();
			if (size > 1 & !addinfo.isEmpty())

			{
				int i = 0;
				for (WebElement webElement1 : options) {
					i++;
					
					String txt = webElement1.getText();
					System.out.println("this is text " + txt);
					boolean flag = false;
					if (txt.contains(addinfo)) {

						flag = true;

					}
					System.out.println(flag);
					if (flag) {
						System.out.println(TestData + "was found at " + i);
						element.clear();
						element.sendKeys(TestData);
						driver.findElement(By.xpath("//li[@class='ng-star-inserted'][" + i
								+ "]//div[@class='row flex-items-xs-between ng-star-inserted']//span")).click();

						break;
					}

				}

			} else {
				// System.out.println("ths is sne text " + text.trim());

				element.clear();
				element.sendKeys(TestData);

				driver.findElement(By.xpath("//div[@class='intellect-lookup-dropdown-options ng-star-inserted']"))
						.click();
				// element.sendKeys(Keys.TAB);

			}
		}

		else {

			element.clear();
			element.sendKeys(TestData);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String opt = driver.findElement(By.xpath("//div[@role='option']")).getText();
			System.out.println(opt + "this is the option string");

			driver.findElement(By.xpath("//div[@role='option']")).click();
		}

		System.out.println("Exiting Select and enter");

	}

	public String VerifyText(String XPATH, String elementType, String TestData, WebDriver driver) {
		/*
		 * if(log.isDebugEnabled()) log.debug("Entering");
		 */
		try {
			if (TestData.contains(Prop.readProperty("Variablesname"))) {
				TestData = Fileutil.GetFilevalue(TestData).trim();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement tabname = driver.findElement(By.xpath(XPATH));
		// Utility.highLighterMethod(driver, tabname);
		String txtLegalName = tabname.getText(); // getAttribute("value");
		txtLegalName = txtLegalName.replace("\n", " ");

		// log.debug("Text "+ txtLegalName);
		System.out.println("Element UI:" +txtLegalName+"| Length of element ui: "+txtLegalName.length());
		log.info("Element UI:" +txtLegalName+"| Length of element ui: "+txtLegalName.length());
		System.out.println("Element Testdata:" +TestData+"| Length of element Testdata: "+TestData.length());
		log.info("Element Testdata:" +TestData+"| Length of element Testdata: "+TestData.length());
		// changed by jyoti
		if (elementType.equals("VerifyBoxPartial")) {

			if (txtLegalName.contains(TestData))
				txtLegalName = TestData;

		}

		if (txtLegalName.equals(TestData))
			return "PASS";
		else
			return "FAIL";

		/*
		 * if(log.isDebugEnabled()) log.debug("Leaving after click");
		 */
	}

	public void SendKeys(String XPATH, String TestData, WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(XPATH));

		element.sendKeys(TestData);
		element.clear();
		element.sendKeys(TestData);
		element.sendKeys(Keys.TAB);

	}
	
	public void SendKeysie(String XPATH, String TestData, WebDriver driver)
	{	 
		if(TestData.contains("VAR"))
		{
			TestData=Fileutil.GetFilevalue(TestData);
			System.out.println("Actual value:"+TestData);
			log.info("Actual value:"+TestData);
		}
	((JavascriptExecutor) driver).executeScript("arguments[0].value='"+TestData+"';", driver.findElement(By.xpath(XPATH)));
	}



	public void ClearTextBox(String XPATH, String TestData, WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(XPATH));
		element.clear();

	}

	public void SelectDiv(String XPATH, String TestData, WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(XPATH));

		// Added by jyoti --pickvariable from values.properties

		/*
		 * if(sInputValues.contains("VAR_")) sInputValues =
		 * GetFilevalue(sInputValues).trim();
		 */

		// System.out.println("in Div Select");
		// System.out.println(sInputValues);

		element.click();

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			System.out.println(e.toString());
			log.debug(e.toString());
		}
		List<WebElement> options = driver.findElements(By.xpath("//div[@role='option']"));
		for (WebElement option : options) {
			// System.out.println(option.getText());
			if (option.getText().trim().equals(TestData)) {

				option.click();
				break;
			}
			// System.out.println("Exiting div select");
		}

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			System.out.println(e.toString());
			log.debug(e.toString());
		}

		// System.out.println("Exiting ListBox Div Select");

	}
  public void SelectIE(String XPATH,String TestData, WebDriver driver)
  {  WebElement dropdwn= driver.findElement(By.xpath(XPATH));
	  
	  Select sel = new Select(dropdwn);

		//sel.selectByVisibleText(TestData);
		sel.selectByValue(TestData);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  
  }
  public void SelectByVisibleText(String XPATH,String TestData, WebDriver driver)
  {  WebElement dropdwn= driver.findElement(By.xpath(XPATH));
  	dropdwn.click();
	  
	  Select sel = new Select(dropdwn);
	  System.out.println("entered 1");
		sel.selectByVisibleText(TestData);
		dropdwn.click();
		//sel.selectByValue(TestData);
		System.out.println("entered 2");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  
  }
  public String SelectIEOPTIONVALUES(String XPATH,String TestData, WebDriver driver)
  {  WebElement dropdwn= driver.findElement(By.xpath(XPATH));
      String Result="FAIL";
      List<String> sheetvalues=new ArrayList();
      List<String> Comparevalues=new ArrayList();
  if(TestData!=null)
	{
		String[] Data = TestData.split("\\|");
		sheetvalues = Arrays.asList(Data);
		//Collections.sort(sheetvalues); 
		  for (int j1 = 0; j1 < sheetvalues.size(); j1++) {
		        System.out.println("Values from sheet:"+sheetvalues.get(j1)+" Size:"+sheetvalues.get(j1).length());
		    }
	}
	  Select sel = new Select(dropdwn);
	  	List<WebElement> uivalues=new ArrayList();
	  	uivalues=sel.getOptions();
	    
	    System.out.println(uivalues.size());
		log.debug(uivalues.size());
	    // Loop to print one by one
	    for (int j = 0; j < uivalues.size(); j++) {
	        //System.out.println("Values from UI:"+uivalues.get(j).getText()+" Size:"+sheetvalues.get(j).length());
	        Comparevalues.add(uivalues.get(j).getText());
	    }
	    
	    for (int j2 = 0; j2 <  Comparevalues.size(); j2++) {
	        System.out.println("Values from UI list:"+ Comparevalues.get(j2)+" Size:"+sheetvalues.get(j2).length());
	        
	    }
		
	    if(Comparevalues.equals(sheetvalues))
	    {
	    	   System.out.println("PASS");
	    	   Result="PASS";
	    }
	    else
	    {
	    	System.out.println("FAIL");
	    	   Result="FAIL";
	    }
	    return Result;
  }
	public String GetRow(String XPATH, String elementType, String TestData, WebDriver driver) {
		

		/*
		 * WebElement element= driver.findElement(By.xpath(XPATH));
		 * element.sendKeys(TestData);
		 */

		String count = getRowCount(XPATH, elementType, driver);// this is the no
																// of rows on 1
																// page
		String Rowpropxpath, Cellpropxpath = "";
		boolean flag = false;
		int i = 0, rowNo = 0;

		String[] xlsArray = TestData.split("\\|");

		if (xlsArray[1].contains("VAR_")) {
			xlsArray[1] = Fileutil.GetFilevalue(xlsArray[1]);
			xlsArray[1] = xlsArray[1].trim();

		}

		int rowcount = Integer.parseInt(count);
		int spantextlength = 0;
		int index = 0;
		int Pagesdisp = 1;
		// Getting the total no of rows dispalyed
		// String
		// spantext=driver.findElement(By.xpath("//div[@class='datagrid-foot-description']//span[contains(text(),'structures')]")).getText();
		/*if (rowcount > 12) {
			String spantext = driver
					.findElement(By.xpath("//div[@class='datagrid-foot-description']//span[contains(text(),'s')]"))
					.getText();

			String[] strngs = spantext.split("\\s+");
			spantextlength = strngs.length;
			index = spantextlength - 2;
			System.out.println("This is index" + index + "this is length" + spantextlength);

			// spantext.subSequence(0, index);
			System.out.println(strngs[index]);
			Pagesdisp = Integer.parseInt(strngs[index]);

		}*/

		// int Pagesdisp=62;//assuming the total no of rows displayed on one
		// page
		// String colno="3";
		String colno = String.valueOf((Integer.parseInt(xlsArray[0]) + 1));
		System.out.println("This is colmn no" + colno);

		double NosPages = (double) (Pagesdisp) / (double) (rowcount);
		System.out.println(NosPages);
		double NoofPages = Math.ceil(NosPages);
		int Nos = (int) NoofPages;
		System.out.println(Nos);

		while (Nos != 0 && flag == false) {
			int row_count = Integer.parseInt(getRowCount(XPATH, elementType, driver));

			try {
				if (driver.findElement(By.xpath(XPATH + "//button[@class='pagination-next']")).isEnabled())
					Nos = 6;
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("No button found");
			}

			for (i = 1; i <= row_count && flag == false; i++) {
				try {

					// Cellpropxpath=Cellpropxpath.replaceFirst("cellnum",
					// colno);

					if (elementType.equalsIgnoreCase("getDataRow")) {
						Rowpropxpath = Prop.readProperty("DATARowval");
						Cellpropxpath = "";
					} else {
						Rowpropxpath = Prop.readProperty("Rowval");
						Cellpropxpath = Prop.readProperty("Cellval");
						Cellpropxpath = Cellpropxpath.replace("cellnum", colno);
						System.out.println(Cellpropxpath);
					}
					// Rowpropxpath=Rowpropxpath.replaceFirst("rownum",String.valueOf(i));
					Rowpropxpath = Rowpropxpath.replace("rownum", String.valueOf(i));
					System.out.println(Rowpropxpath);

					String FinalXpath = XPATH + Rowpropxpath + Cellpropxpath;
					System.out.println(FinalXpath);
					String text = driver.findElement(By.xpath(FinalXpath)).getText();
					System.out.println("this is Text" + text);
					System.out.println(xlsArray[1]);
					if (text.contains(xlsArray[1])) {
						rowNo = i;
						flag = true;
						break;

					}

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			Nos--;
			if (Nos > 0 && !flag) {
				// driver.findElement(By.xpath("//button[@class='pagination-next']")).click();
				driver.findElement(By.xpath(XPATH + "//button[@class='pagination-next']")).click();
				;
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			else
				break;

		}

		// System.out.println(Nos);

		if (flag == true) {
			System.out.println("VAR_ROW=" + rowNo);
			Fileutil.writeUsingFileWriter(xlsArray[2] + "=" + rowNo);
			return "PASS";
		} else {
			System.out.println("Row not found");
			return "FAIL";
		}

	
	}

	public String getRowCount(String XPATH, String elementType, WebDriver driver) {
		String Rowpropxpath;
		List<WebElement> rows = null;

		try {

			if (elementType.equalsIgnoreCase("getDataRow"))
				Rowpropxpath = Prop.readProperty("DATARowval");
			else
				Rowpropxpath = Prop.readProperty("Rowval");
			// Rowpropxpath = readProperty("Rowval");

			Rowpropxpath = Rowpropxpath.replace("[rownum]", "");
			System.out.println(Rowpropxpath);
			String FinalXpath = XPATH + Rowpropxpath;
			rows = driver.findElements(By.xpath(FinalXpath));
			int size = rows.size();
			System.out.println(rows.size());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (String.valueOf(rows.size()));
	}

	public void ClickCell(String XPATH, String elementType, String TestData, WebDriver driver) {
		String[] ArrayxlsValue = TestData.split("\\|");
		String FinalXpath = "";
		String Cellpropxpath;
		String Rowpropxpath;
		try {

			if (ArrayxlsValue[0].contains("VAR_")) {
				// read property from cofig file
				ArrayxlsValue[0] = Fileutil.GetFilevalue(ArrayxlsValue[0]);

			}

			if (elementType.equalsIgnoreCase("ClickDataCell")) {
				Rowpropxpath = Prop.readProperty("DATARowval");
				Cellpropxpath = "";
			}

			else {
				Cellpropxpath = Prop.readProperty("Cellval");
				Rowpropxpath = Prop.readProperty("Rowval");

			}

			Cellpropxpath = Cellpropxpath.replace("cellnum", ArrayxlsValue[1]);
			System.out.println(Cellpropxpath);
			// Rowpropxpath=Rowpropxpath.replaceFirst("rownum",ArrayxlsValue[0]);
			Rowpropxpath = Rowpropxpath.replace("rownum", ArrayxlsValue[0]);
			System.out.println(Rowpropxpath);

			if (ArrayxlsValue.length > 2) {
				// string button="\\button\span["+ArrayxlsValue[2]+"]";

				if (!ArrayxlsValue[2].equalsIgnoreCase("0")) {
					FinalXpath = XPATH + Rowpropxpath + Cellpropxpath + "//button[" + ArrayxlsValue[2] + "]";
					System.out.println("fin xpath" + FinalXpath);
				} else {
					FinalXpath = XPATH + Rowpropxpath + Cellpropxpath;

				}

			} else {
				System.out.println(XPATH + Rowpropxpath + Cellpropxpath + "//button");
				FinalXpath = XPATH + Rowpropxpath + Cellpropxpath + "//button";
				System.out.println(FinalXpath);
			}
			driver.findElement(By.xpath(FinalXpath)).click();
		}

		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void selectDate(String XPATH, String elementType, String TestData, WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(XPATH));
		element.click();

		// Added by jyoti --pickvariable from values.properties

		if (TestData.contains("VAR_"))
			TestData = Fileutil.GetFilevalue(TestData);

		// userdateval = 20182711
		int noofdaysmore = 0;
		WebElement nextyear;
		WebElement nextmonth;
		int useryear = 0;
		int usermonth = 0;
		int userdate = 0;
		String refdate = "";
		int refmonth = 0;

		boolean newdate = false;
		newdate = TestData.contains("+");
		String finaldate = "";

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date currentDate = new Date();
		// System.out.println(dateFormat.format(currentDate));
		Calendar c = Calendar.getInstance();
		c.setTime(currentDate);

		// if newdate is true then it means user has given sysdate + some days
		// value else
		if (newdate) {
			noofdaysmore = Integer.parseInt(TestData.substring(TestData.indexOf('+'), TestData.length()));
			c.add(Calendar.DATE, noofdaysmore);
			Date finaldatevalue = c.getTime();
			finaldate = dateFormat.format(finaldatevalue).toString();
			useryear = Integer.parseInt(finaldate.substring(0, 4));
			usermonth = Integer.parseInt(finaldate.substring(5, 7));
			userdate = Integer.parseInt(finaldate.substring(8, 10));
		} else// take user given date as it is and set it
		{
			finaldate = TestData;
			useryear = Integer.parseInt(finaldate.substring(0, 4));
			usermonth = Integer.parseInt(finaldate.substring(4, 6));
			userdate = Integer.parseInt(finaldate.substring(6, 8));
		}

		if (elementType.equals("DatePickerRelative")) {
			System.out.println("In relative");

			try {
				refdate = Prop.readProperty("EOD").trim();

				refmonth = Integer.parseInt(refdate.substring(4, 6));
				// userdate=Integer.parseInt(refdate.substring(6,8));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (usermonth > refmonth) {
				driver.findElement(By.xpath("//button[@aria-label='Next Month']")).click();
				driver.findElement(By.xpath("//*[@class='datevalue currmonth']/span[text()='" + userdate + "']"))
						.click();
			}

			else
				driver.findElement(By.xpath("//*[@class='datevalue currmonth']/span[text()='" + userdate + "']"))
						.click();

		} else {
			// System.out.println("Date value: "+finaldate);

			/* This below will hold true for any value of date */
			LocalDate curdate = java.time.LocalDate.now();
			String currentdatestr = curdate.toString();
			int currentyear = Integer.parseInt(currentdatestr.substring(0, 4));
			int currentmonth = Integer.parseInt(currentdatestr.substring(5, 7));
			int currentdate = Integer.parseInt(currentdatestr.substring(8, 10));

			Calendar cal = Calendar.getInstance();
			cal.set(useryear, usermonth, userdate);
			int maxdate = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
			// System.out.println("Maximum date in the month given : "+maxdate);

			int yeardiff = 0;
			int monthdiff = 0;
			int datediff = 0;

			String openmonth = driver
					.findElement(By
							.xpath("//div[@class='selector selectorarrow selectorarrowleft']//div[@class='headermonthtxt']/button"))
					.getText();
			String openyear = driver.findElement(By.xpath("//div[@class='headeryeartxt']/button")).getText();

			if (currentyear == useryear) {
				// System.out.println("Matching year");

				if (currentmonth == usermonth) {
					// System.out.println("Matching month");

					if (currentdate == userdate) {
						// System.out.println("Matching date");
					} else if (currentdate > userdate) {
						System.out
								.println("Invalid selection, Please select a date equal or greater than current date");
						// jo change here

						// monthdiff=currentmonth-;

					} else if (currentdate < userdate) {
						// System.out.println("User provided date is greater
						// than current date");
						datediff = userdate - currentdate;
						// System.out.println("Date difference: "+datediff);
					}
					driver.findElement(By.xpath("//*[@class='datevalue currmonth']/span[text()='" + userdate + "']"))
							.click();
				} else if (currentmonth > usermonth) {
					System.out.println(
							"Invalid selection, Please select a monthh equal or greater than current month ,still proceeding");
					// new change
					monthdiff = currentmonth - usermonth;
					for (int i = 0; i < monthdiff; i++) // click on right arrow
														// as many times as
														// months difference
					{
						driver.findElement(By.xpath("//button[@aria-label='Previous Month']")).click();
					}

					driver.findElement(By.xpath("//*[@class='datevalue currmonth']/span[text()='" + userdate + "']"))
							.click();

				} else if (currentmonth < usermonth) {
					// System.out.println("User provided month is greater than
					// current month");
					monthdiff = usermonth - currentmonth;
					System.out.println("Month difference: " + monthdiff);

					for (int i = 0; i < monthdiff; i++) // click on right arrow
														// as many times as
														// months difference
					{
						driver.findElement(By.xpath("//button[@aria-label='Next Month']")).click();
					}

					driver.findElement(By.xpath("//*[@class='datevalue currmonth']/span[text()='" + userdate + "']"))
							.click();
				}

			} else if (currentyear > useryear) {
				System.out.println("Invalid selection, date is less than current year");

				yeardiff = currentyear - useryear;
				int totaldiff = yeardiff * 12;

				if (usermonth > currentmonth) {
					monthdiff = usermonth - currentmonth;
					int actualmonthdiff = totaldiff - monthdiff;

					for (int i = 0; i < actualmonthdiff; i++) // click on right
																// arrow as many
																// times as
																// months
																// difference
					{
						driver.findElement(By.xpath("//button[@aria-label='Previous Month']")).click();
					}

					driver.findElement(By.xpath("//*[@class='datevalue currmonth']/span[text()='" + userdate + "']"))
							.click();

				}

			} // if year is not same check if greater or lesser
			else if (currentyear < useryear) {
				// System.out.println("User has given date that has year greater
				// than current year");
				yeardiff = useryear - currentyear;
				System.out.println("Date difference: " + yeardiff);
				for (int i = 0; i < yeardiff; i++) // click on right arrow as
													// many times as months
													// difference
				{
					driver.findElement(By.xpath("//button[@aria-label='Next Year']")).click();
				}

				if (usermonth == currentmonth) {
					driver.findElement(By.xpath("//*[@class='datevalue currmonth']/span[text()='" + userdate + "']"))
							.click();
				} else if (usermonth < currentmonth) {
					monthdiff = currentmonth - usermonth;
					for (int i = 0; i < monthdiff; i++) {
						driver.findElement(By.xpath("//button[@aria-label='Previous Month']")).click();
					}
					// logic below is to check if user provided date is within
					// max date of the month
					if (userdate < maxdate)
						driver.findElement(
								By.xpath("//*[@class='datevalue currmonth']/span[text()='" + userdate + "']")).click();
					else
						System.out.println("The date provided by user isnt present in provided month");
				} else if (usermonth > currentmonth) {
					monthdiff = usermonth - currentmonth;
					for (int i = 0; i < monthdiff; i++) {
						driver.findElement(By.xpath("//button[@aria-label='Next Month']")).click();
					}
					// logic below is to check if user provided date is within
					// max date of the month
					if (userdate < maxdate)
						driver.findElement(
								By.xpath("//*[@class='datevalue currmonth']/span[text()='" + userdate + "']")).click();
					else
						System.out.println("The date provided by user isnt present in provided month");
				}

			}

		}

	}

	public String VerifyDisabledDate(String XPATH, String elementType, String TestData, WebDriver driver) {
		boolean disabled = false;
		WebElement element = driver.findElement(By.xpath(XPATH));
		element.click();

		// Added by jyoti --pickvariable from values.properties

		if (TestData.contains("VAR_"))
			TestData = Fileutil.GetFilevalue(TestData);

		// userdateval = 20182711
		int noofdaysmore = 0;
		WebElement nextyear;
		WebElement nextmonth;
		int useryear = 0;
		int usermonth = 0;
		int userdate = 0;
		String refdate = "";
		int refmonth = 0;

		boolean newdate = false;

		if (TestData.contains("-")) {
			newdate = TestData.contains("-");

		} else if (TestData.contains("+")) {
			newdate = TestData.contains("+");
		}
		String finaldate = "";

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date currentDate = new Date();
		// System.out.println(dateFormat.format(currentDate));
		Calendar c = Calendar.getInstance();
		c.setTime(currentDate);

		// if newdate is true then it means user has given sysdate + some days
		// value else
		if (newdate) {

			if (TestData.contains("+")) {
				noofdaysmore = Integer.parseInt(TestData.substring(TestData.indexOf('+'), TestData.length()));
				c.add(Calendar.DATE, noofdaysmore);
			}

			if (TestData.contains("-")) {
				noofdaysmore = Integer.parseInt(TestData.substring(TestData.indexOf('-'), TestData.length()));
				c.add(Calendar.DATE, noofdaysmore);
			}

			Date finaldatevalue = c.getTime();
			finaldate = dateFormat.format(finaldatevalue).toString();
			useryear = Integer.parseInt(finaldate.substring(0, 4));
			usermonth = Integer.parseInt(finaldate.substring(5, 7));
			userdate = Integer.parseInt(finaldate.substring(8, 10));
		} else// take user given date as it is and set it
		{
			finaldate = TestData;
			useryear = Integer.parseInt(finaldate.substring(0, 4));
			usermonth = Integer.parseInt(finaldate.substring(4, 6));
			userdate = Integer.parseInt(finaldate.substring(6, 8));
		}

		if (elementType.equals("DatePickerRelative")) {
			System.out.println("In relative");

			try {
				refdate = Prop.readProperty("EOD").trim();

				refmonth = Integer.parseInt(refdate.substring(4, 6));
				// userdate=Integer.parseInt(refdate.substring(6,8));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (usermonth > refmonth) {
				driver.findElement(By.xpath("//button[@aria-label='Next Month']")).click();
				driver.findElement(By.xpath("//*[@class='datevalue currmonth']/span[text()='" + userdate + "']"))
						.click();
			}

			else
				driver.findElement(By.xpath("//*[@class='datevalue currmonth']/span[text()='" + userdate + "']"))
						.click();

		} else {
			// System.out.println("Date value: "+finaldate);

			/* This below will hold true for any value of date */
			LocalDate curdate = java.time.LocalDate.now();
			String currentdatestr = curdate.toString();
			int currentyear = Integer.parseInt(currentdatestr.substring(0, 4));
			int currentmonth = Integer.parseInt(currentdatestr.substring(5, 7));
			int currentdate = Integer.parseInt(currentdatestr.substring(8, 10));

			Calendar cal = Calendar.getInstance();
			cal.set(useryear, usermonth, userdate);
			int maxdate = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
			// System.out.println("Maximum date in the month given : "+maxdate);

			int yeardiff = 0;
			int monthdiff = 0;
			int datediff = 0;

			String openmonth = driver
					.findElement(By
							.xpath("//div[@class='selector selectorarrow selectorarrowleft']//div[@class='headermonthtxt']/button"))
					.getText();
			String openyear = driver.findElement(By.xpath("//div[@class='headeryeartxt']/button")).getText();

			if (currentyear == useryear) {
				// System.out.println("Matching year");

				if (currentmonth == usermonth) {
					// System.out.println("Matching month");

					if (currentdate == userdate) {
						// System.out.println("Matching date");
					} else if (currentdate > userdate) {

						System.out
								.println("Invalid selection, Please select a date equal or greater than current date");

						// check disability for past DATE HERE

						disabled = driver.findElement(By
								.xpath("//td[@class='daycell disabled ng-star-inserted']//*[@class='datevalue currmonth']/span[text()='"
										+ userdate + "']"))
								.isDisplayed();

						System.out.println("disable dispaly" + disabled);

						/*
						 * boolean disabled1=driver.findElement(By.
						 * xpath("//td[@class='daycell disabled ng-star-inserted']//*[@class='datevalue currmonth']/span[text()='"
						 * +userdate+"']")).isEnabled();
						 * System.out.println("enable check"+disabled1);
						 */
					} else if (currentdate < userdate) {
						// System.out.println("User provided date is greater
						// than current date");
						datediff = userdate - currentdate;
						// System.out.println("Date difference: "+datediff);
					}
					// driver.findElement(By.xpath("//*[@class='datevalue
					// currmonth']/span[text()='"+userdate+"']")).click();
					try {
						disabled = driver.findElement(By
								.xpath("//td[@class='daycell disabled ng-star-inserted']//*[@class='datevalue currmonth']/span[text()='"
										+ userdate + "']"))
								.isDisplayed();
					} catch (Exception e) {
						disabled = false;
						// element.click();
					}
					// CHECK DISABILITY OF FUTURE DATE HERE SAME MONTH
				} else if (currentmonth > usermonth) {
					System.out.println(
							"Invalid selection, Please select a monthh equal or greater than current month ,still proceeding");
					// new change
					monthdiff = currentmonth - usermonth;
					for (int i = 0; i < monthdiff; i++) // click on right arrow
														// as many times as
														// months difference
					{
						driver.findElement(By.xpath("//button[@aria-label='Previous Month']")).click();
					}

					// driver.findElement(By.xpath("//*[@class='datevalue
					// currmonth']/span[text()='"+userdate+"']")).click();
					try {
						disabled = driver.findElement(By
								.xpath("//td[@class='daycell disabled ng-star-inserted']//*[@class='datevalue currmonth']/span[text()='"
										+ userdate + "']"))
								.isDisplayed();
					} catch (Exception e) {
						disabled = false;
						// element.click();
					}

					// CHECK DATE DISBILITY

				} else if (currentmonth < usermonth) {
					// System.out.println("User provided month is greater than
					// current month");
					monthdiff = usermonth - currentmonth;
					System.out.println("Month difference: " + monthdiff);

					for (int i = 0; i < monthdiff; i++) // click on right arrow
														// as many times as
														// months difference
					{
						driver.findElement(By.xpath("//button[@aria-label='Next Month']")).click();
					}

					// driver.findElement(By.xpath("//*[@class='datevalue
					// currmonth']/span[text()='"+userdate+"']")).click();
					try {
						disabled = driver.findElement(By
								.xpath("//td[@class='daycell disabled ng-star-inserted']//*[@class='datevalue currmonth']/span[text()='"
										+ userdate + "']"))
								.isDisplayed();
					} catch (Exception e) {
						disabled = false;
						// element.click();
					}
					// CHECK DATE DISABILITY
				}

			} else if (currentyear > useryear) {
				System.out.println("Invalid selection, date is less than current year");

				yeardiff = currentyear - useryear;
				int totaldiff = yeardiff * 12;

				if (usermonth > currentmonth) {
					monthdiff = usermonth - currentmonth;
					int actualmonthdiff = totaldiff - monthdiff;

					for (int i = 0; i < actualmonthdiff; i++) // click on right
																// arrow as many
																// times as
																// months
																// difference
					{
						driver.findElement(By.xpath("//button[@aria-label='Previous Month']")).click();
					}

					// driver.findElement(By.xpath("//*[@class='datevalue
					// currmonth']/span[text()='"+userdate+"']")).click();
					try {
						disabled = driver.findElement(By
								.xpath("//td[@class='daycell disabled ng-star-inserted']//*[@class='datevalue currmonth']/span[text()='"
										+ userdate + "']"))
								.isDisplayed();
					} catch (Exception e) {
						disabled = false;
						// element.click();
					}
					// CHECK DATE DISABILITY

				}

			} // if year is not same check if greater or lesser
			else if (currentyear < useryear) {
				// System.out.println("User has given date that has year greater
				// than current year");
				yeardiff = useryear - currentyear;
				System.out.println("Date difference: " + yeardiff);

				// changed code since year next is disabled hence handling by
				// clicking next month
				/*
				 * for(int i=0;i<yeardiff;i++) // click on right arrow as many
				 * times as months difference { driver.findElement(By.
				 * xpath("//button[@aria-label='Next Year']")).click(); }
				 */

				if (usermonth == currentmonth) {
					// driver.findElement(By.xpath("//*[@class='datevalue
					// currmonth']/span[text()='"+userdate+"']")).click();
					try {
						disabled = driver.findElement(By
								.xpath("//td[@class='daycell disabled ng-star-inserted']//*[@class='datevalue currmonth']/span[text()='"
										+ userdate + "']"))
								.isDisplayed();
					} catch (Exception e) {
						disabled = false;
						// element.click();
					}
				} else if (usermonth < currentmonth) { // changed code since
														// year next is disabled
														// hence handling by
														// clicking next month
					monthdiff = 12 - currentmonth + usermonth;

					for (int i = 0; i < monthdiff; i++) {
						// driver.findElement(By.xpath("//button[@aria-label='Previous
						// Month']")).click();

						driver.findElement(By.xpath("//button[@aria-label='Next Month']")).click();
						// changed code since year next is disabled hence
						// handling by clicking next month
					}
					// logic below is to check if user provided date is within
					// max date of the month
					if (userdate < maxdate)
						// driver.findElement(By.xpath("//*[@class='datevalue
						// currmonth']/span[text()='"+userdate+"']")).click();
						try {
						disabled = driver.findElement(By.xpath("//td[@class='daycell disabled ng-star-inserted']//*[@class='datevalue nextmonth']/span[text()='" + userdate + "']")).isDisplayed();
						} catch (Exception e) {
						disabled = false;
						// element.click();
						}
					else
						System.out.println("The date provided by user isnt present in provided month");
				} else if (usermonth > currentmonth) {// TBD
					monthdiff = usermonth - currentmonth;
					for (int i = 0; i < monthdiff; i++) {
						driver.findElement(By.xpath("//button[@aria-label='Next Month']")).click();
					}
					// logic below is to check if user provided date is within
					// max date of the month
					if (userdate < maxdate)
						// driver.findElement(By.xpath("//*[@class='datevalue
						// currmonth']/span[text()='"+userdate+"']")).click();
						try {
						disabled = driver.findElement(By.xpath("//td[@class='daycell disabled ng-star-inserted']//*[@class='datevalue currmonth']/span[text()='" + userdate + "']")).isDisplayed();
						} catch (Exception e) {
						disabled = false;
						// element.click();
						}
					else
						System.out.println("The date provided by user isnt present in provided month");
				}

			}

		}
		element.click();

		if (disabled)
			return "PASS";

		else
			return "FAIL";
	}

	public void GetText(String xPathNavigation, String elementType, String excelInput, WebDriver driver) {
		String text = driver.findElement(By.xpath(xPathNavigation)).getText();
		System.out.println("Text: " + text);
		text = text.replace("\n", " ");

		if (excelInput.contains("VAR")) {
			Fileutil.writeUsingFileWriter(excelInput + "=" + text);
		}
		if (excelInput.contains("VAR_QUOTEID")) {
			Keywordoperations _Storevalue=new Keywordoperations();
			_Storevalue.StoreValue("VAR_QUOTEID",text);

		}
	}

	public String VerifyCell(String xPathNavigation, String elementType, String excelInput, WebDriver driver)

	{

		String rowncol = "";
		String UIvalue = "";
		String Excelvalue = "";
		boolean datevalidation = false;
		rowncol = excelInput.substring(0, excelInput.indexOf('/'));// gets the
																	// row
		System.out.println("Rowncolumn values from excel in verifyCellValue: " + rowncol);
		UIvalue = getCellValue(xPathNavigation, rowncol, elementType, driver).trim();
		UIvalue = UIvalue.replace("\n", " ");
		UIvalue = UIvalue.replace("    ", " ");
		System.out.println("UI value of getCellValue: " + UIvalue);

		Excelvalue = excelInput.substring(excelInput.indexOf('/') + 1, excelInput.length());
		try {
			if (Excelvalue.contains("Date")) {
				datevalidation = true;
			}
			if (Excelvalue.contains(Prop.readProperty("Variablesname"))) {
				Excelvalue = Fileutil.GetFilevalue(Excelvalue).trim();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (!(Excelvalue.equalsIgnoreCase("SKIP"))) {
			if (UIvalue.contains(",") && !(Excelvalue.contains(",")))
				UIvalue = UIvalue.replace(",", "");

			/*
			 * if(Excelvalue.contains(".00")) { String
			 * Excelvalues[]=Excelvalue.split("\\."); Excelvalue=Excelvalues[0];
			 * }
			 */
			else if (Excelvalue.contains("|"))

			{
				String ExcelValues[] = Excelvalue.split("\\|");
				Excelvalue = ExcelValues[0];
			}

			if (datevalidation == true && UIvalue.contains("/") && !(Excelvalue.contains("/"))
					&& !Excelvalue.isEmpty()) {
				String useryear = Excelvalue.substring(0, 4);
				String usermonth = Excelvalue.substring(4, 6);
				String userdate = Excelvalue.substring(6, 8);
				Excelvalue = usermonth + "/" + userdate + "/" + useryear;
				System.out.println(Excelvalue);
			}
			// changed by jyoti
			if (elementType.equals("VerifyCellPartial") || elementType.equals("VerifyDataCellPartial")) {

				if (UIvalue.contains(Excelvalue))
					UIvalue = Excelvalue;
			}
			// Assert.assertEquals(UIvalue, Excelvalue);
			/*
			 * SoftAssert a= new SoftAssert(); a.assertEquals(UIvalue,
			 * Excelvalue);
			 */

			if (UIvalue.equals(Excelvalue))
				return "PASS";

			else
				return "FAIL";
		} else
			return "SKIP";

	}

	public String getCellValue(String xPathNavigation, String excelInput, String elementType, WebDriver driver) {
		// //clr-datagrid[@class="intellect-datagrid
		// datagrid-host"][1]//*[contains(@class,'datagrid-row
		// ng-star-inserted')][1]//clr-dg-cell[contains(@class,"datagrid-cell")][3]
		// rownum colnum frm 2
		String rownum = "";
		int colnum = 0;
		String value = "";
		String xpath = "";
		String Cellpropxpath = "";
		String Rowpropxpath = "";
		String propertyxpath = "";
		// excelInput.g
		rownum = excelInput.substring(0, excelInput.indexOf('|'));// gets the
																	// row
		System.out.println("Row num of getCellValue: " + rownum);
		colnum = Integer.parseInt(excelInput.substring(excelInput.indexOf('|') + 1, excelInput.length()));// gets
																											// the
																											// column
		System.out.println("Col num of getCellValue: " + colnum);
		// code to fetch row from file properties

		if (rownum.contains("VAR_"))
			rownum = Fileutil.GetFilevalue(rownum);

		// colnum = colnum + 1;//to make column index as 1 more since for
		// Instructions screen it starts with 2
		String colnumstr = String.valueOf(colnum);
		System.out.println("colnumstr of getCellValue is: " + colnumstr);
		try {

			if (elementType.equalsIgnoreCase("VerifyDataCellPartial")
					|| elementType.equalsIgnoreCase("VerifyDataCell")) {
				Rowpropxpath = Prop.readProperty("DATARowval");
				Cellpropxpath = "";
			} else {
				Cellpropxpath = Prop.readProperty("Cellval");

				Cellpropxpath = Cellpropxpath.replace("cellnum", colnumstr);
				System.out.println(Cellpropxpath);
				Rowpropxpath = Prop.readProperty("Rowval");
			}
			// Rowpropxpath=Rowpropxpath.replaceFirst("rownum",rownum);
			Rowpropxpath = Rowpropxpath.replace("rownum", rownum);
			System.out.println(Rowpropxpath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Exception caught in getCellValue: " + e.toString());
		}

		propertyxpath = Rowpropxpath + Cellpropxpath;
		System.out.println(propertyxpath);
		xpath = xPathNavigation + propertyxpath;
		System.out.println("xpath : " + xpath);
		// String xpath = "//div[@class='row
		// flex-items-lg-center']//div[@class='row intellect-full-width
		// ng-star-inserted']//div[@class='col-lg-12']//*[contains(@class,'datagrid-row
		// ng-star-inserted')]["+rownum+"]"+"//*[contains(@class,'datagrid-cell
		// ng-star-inserted')]["+colnum+"]";

		// System.out.println("xpath is: "+xpath);
		value = driver.findElement(By.xpath(xpath)).getText();
		System.out.println("value is: " + value);
		return value;
	}

	public void OpenURL(String TestData, WebDriver driver) {
		try {
			// driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL
			// +"t");

			Robot rb = new Robot();
			rb.keyPress(KeyEvent.VK_CONTROL);
			rb.keyPress(KeyEvent.VK_T);

			rb.keyRelease(KeyEvent.VK_CONTROL);
			rb.keyRelease(KeyEvent.VK_T);

			ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs.get(1));
			driver.get("https://www.guru99.com");

		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}
	
	public String ValidateDropdownValues(String XPATH, String ElementType, String TestData, WebDriver driver)
	{
		List<String> selvalues = new ArrayList<String>();
		List<String> Actvalues = new ArrayList<String>();

		while (TestData.contains("|")) {
			int index = TestData.indexOf("|");
			String curroption = TestData.substring(0, index);
			System.out.println(curroption + "this is expected string");
			selvalues.add(curroption);
			TestData = TestData.substring(index + 1);
		}

		selvalues.add(TestData);
		System.out.println(selvalues + "this is expected");
		WebElement element = driver.findElement(By.xpath(XPATH));
		element.click();
		List<WebElement> options = driver.findElements(By.xpath("//select//option"));
		for (WebElement option : options) {

			System.out.println(option.getText());

			Actvalues.add(option.getText());
		
	}
		if (selvalues.equals(Actvalues)) {

			return "PASS";
		}

		else {
			return "FAIL";

		}
	}

	public String ValidateSelectValues(String XPATH, String ElementType, String TestData, WebDriver driver) {

		/*
		 * if(log.isDebugEnabled())
		 * log.debug("In ValidateSelectValues method: ");
		 */
		System.out.println("ValidateSelectValues");
		System.out.println(TestData);
		// sInputValues="1|2|3|4";
		// String [] arr1=sInputValues.split("|");
		List<String> selvalues = new ArrayList<String>();
		List<String> Actvalues = new ArrayList<String>();

		while (TestData.contains("|")) {
			int index = TestData.indexOf("|");
			String curroption = TestData.substring(0, index);
			System.out.println(curroption + "this is expected string");
			selvalues.add(curroption);
			TestData = TestData.substring(index + 1);
		}

		selvalues.add(TestData);
		System.out.println(selvalues + "this is expected");
		WebElement element = driver.findElement(By.xpath(XPATH));
		element.click();

		if (ElementType.contains("dropdown")) {
			List<WebElement> options = driver.findElements(By.xpath("//select//option"));
			for (WebElement option : options) {

				System.out.println(option.getText());

				Actvalues.add(option.getText());

			}
		}

		else if (ElementType.contains("Select")) {
			List<WebElement> options = driver.findElements(By.xpath("//div[@role='option']"));
			for (WebElement option : options) {

				System.out.println(option.getText());

				Actvalues.add(option.getText());

			}
		}
		System.out.println(Actvalues + "this is actual");

		XPATH = XPATH + "//span[@class='ng-arrow']";
		driver.findElement(By.xpath(XPATH)).click();

		if (selvalues.equals(Actvalues)) {

			return "PASS";
		}

		else {
			return "FAIL";

		}

	}

	public String VerifySortNumber(String XPATH_NAV, String ElementType, String TestData, WebDriver driver) {
		boolean matchflag = false;
		boolean nextpage = false;
		String XathpageNav = "";
		int nextpagecount = 0;
		int previouspagecount = 0;

		ArrayList<BigDecimal> UITableInt = new ArrayList<BigDecimal>();
		// ArrayList<Integer> UITableInt=new ArrayList<Integer>();
		ArrayList<BigDecimal> SortedTableInt;
		do {
			List<WebElement> Webel = driver.findElements(By.xpath(XPATH_NAV));
			nextpage = false;

			if (nextpagecount > 2)
				break;
			int size = Webel.size();
			if (size >= 10) {
				try {
					int index = StringUtils.ordinalIndexOf(XPATH_NAV, "/", 3);
					// *[@class='intellect-stack-view
					// ng-star-inserted'][1]//*[contains(@class,'datagrid-cell
					// ng-star-inserted')][5]:VerifySortNumber
					XathpageNav = XPATH_NAV.substring(0, index);
					System.out.println(XathpageNav);
					if (driver.findElement(By.xpath(XathpageNav + "//button[@class='pagination-next']")).isEnabled()) {
						nextpage = true;
						nextpagecount++;
					}
				} catch (Exception e) {
					// result flag
				}

			}

			for (WebElement e : Webel) {

				String text = e.getText();
				String textvalues[] = text.split(" ");
				System.out.println(textvalues[0]);
				textvalues[0] = textvalues[0].replaceAll(",", "");
				// UITableInt.add(Integer.valueOf());

				UITableInt.add(new BigDecimal(textvalues[0]));

			}
			if (nextpage && nextpagecount < 2) {
				driver.findElement(By.xpath(XathpageNav + "//button[@class='pagination-next']")).click();
				System.out.println("here");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block

					// result flag
					e1.printStackTrace();
				}
				previouspagecount++;
			}
		} while (nextpage);

		SortedTableInt = new ArrayList<BigDecimal>(UITableInt);

		if (TestData.contains("|DESC")) {

			Collections.reverse(SortedTableInt);
		} else {
			Collections.sort(SortedTableInt);
		}
		for (int i = 0; i < UITableInt.size(); i++) {
			if ((UITableInt.get(i)).equals(SortedTableInt.get(i))) {
				matchflag = true;
				System.out.println(UITableInt.get(i));
				System.out.println(SortedTableInt.get(i));
			}

			else
				matchflag = false;
		}
		for (int k = 0; k < previouspagecount; k++) {
			driver.findElement(By.xpath(XathpageNav + "//button[@class='pagination-previous']")).click();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// stacktrace
		if (matchflag)
			return "PASS";
		else
			return "FAIL";
	}

	public String VerifySortValues(String XPATH_NAV, String ElementType, String TestData, WebDriver driver) {

		boolean matchflag = false;
		boolean nextpage = false;
		String XathpageNav = "";
		int nextpagecount = 0;
		int previouspagecount = 0;
		/*
		 * if(ElementType.equals("VerifySortValues")) {
		 */

		ArrayList<String> InputValuesString = new ArrayList<String>();
		String TestDataSet[] = TestData.split("\\|");
		for (String a : TestDataSet) {
			a = a.trim();
			InputValuesString.add(a);

		}

		// ArrayList<Integer> UITableInt=new ArrayList<Integer>();
		ArrayList<String> UITableString = new ArrayList<String>();
		do {
			List<WebElement> Webel1 = driver.findElements(By.xpath(XPATH_NAV));
			nextpage = false;
			if (nextpagecount > 2)
				break;
			int size = Webel1.size();
			if (size >= 10) {
				try {
					int index = StringUtils.ordinalIndexOf(XPATH_NAV, "/", 3);
					// *[@class='intellect-stack-view
					// ng-star-inserted'][1]//*[contains(@class,'datagrid-cell
					// ng-star-inserted')][5]:VerifySortNumber
					XathpageNav = XPATH_NAV.substring(0, index);
					System.out.println(XathpageNav);
					if (driver.findElement(By.xpath(XathpageNav + "//button[@class='pagination-next']")).isEnabled()) {
						nextpage = true;
						nextpagecount++;
					}
				} catch (Exception e) {

				}

			}

			for (WebElement e : Webel1) {

				String text = e.getText();
				text = text.replaceAll("[0-9]", "");
				text = text.replace("\n", " ");
				text = text.replace("  ", " ").trim();

				UITableString.add(text);

			}

			if (nextpage && nextpagecount < 2) {
				driver.findElement(By.xpath(XathpageNav + "//button[@class='pagination-next']")).click();
				System.out.println("here");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block

					// result flag
					e1.printStackTrace();
				}
				previouspagecount++;
			}
		} while (nextpage);

		// Collections.sort(UITableString);
		for (int i = 0; i < UITableString.size(); i++) {
			if ((UITableString.get(i)).equals(InputValuesString.get(i))) {
				matchflag = true;
			}

			else
				matchflag = false;
		}

		for (int k = 0; k < previouspagecount; k++) {
			driver.findElement(By.xpath(XathpageNav + "//button[@class='pagination-previous']")).click();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// }

		if (matchflag)
			return "PASS";
		else
			return "FAIL";
	}

	public String VerifySortAlpha(String XPATH_NAV, String ElementType, String TestData, WebDriver driver) {

		boolean matchflag = false;
		boolean nextpage = false;
		String XathpageNav = "";
		int nextpagecount = 0;
		int previouspagecount = 0;

		/*
		 * if(ElementType.equals("VerifySortAlpha")) {
		 */

		ArrayList<String> DataValuesString = new ArrayList<String>();

		// ArrayList<Integer> UITableInt=new ArrayList<Integer>();
		ArrayList<String> SortedTableString;
		do {
			List<WebElement> Webel1 = driver.findElements(By.xpath(XPATH_NAV));

			nextpage = false;

			if (nextpagecount > 2)
				break;

			int size = Webel1.size();
			if (size >= 10) {
				try {
					int index = StringUtils.ordinalIndexOf(XPATH_NAV, "/", 3);
					// *[@class='intellect-stack-view
					// ng-star-inserted'][1]//*[contains(@class,'datagrid-cell
					// ng-star-inserted')][5]:VerifySortNumber
					XathpageNav = XPATH_NAV.substring(0, index);
					System.out.println(XathpageNav);
					if (driver.findElement(By.xpath(XathpageNav + "//button[@class='pagination-next']")).isEnabled()) {
						nextpage = true;
						nextpagecount++;
					}
				} catch (Exception e) {
					// result flag
				}

			}

			for (WebElement e : Webel1) {

				String text = e.getText();
				text = text.replaceAll("[0-9]", "");

				DataValuesString.add(text);

			}

			if (nextpage && nextpagecount < 2) {
				driver.findElement(By.xpath(XathpageNav + "//button[@class='pagination-next']")).click();
				System.out.println("here");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block

					// result flag
					e1.printStackTrace();
				}
				previouspagecount++;
			}
		} while (nextpage);

		SortedTableString = new ArrayList<String>(DataValuesString);

		if (TestData.contains("|DESC")) {

			Collections.reverse(SortedTableString);
		} else {
			Collections.sort(SortedTableString);
		}
		for (int i = 0; i < DataValuesString.size(); i++) {
			if ((DataValuesString.get(i)).equals(SortedTableString.get(i))) {
				matchflag = true;
			}

			else
				matchflag = false;
		}

		for (int k = 0; k < previouspagecount; k++) {
			driver.findElement(By.xpath(XathpageNav + "//button[@class='pagination-previous']")).click();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// }

		if (matchflag)
			return "PASS";
		else
			return "FAIL";
	}

	public String VerifyDisabled(String XPATH_NAV, String ElementType, String TestData, WebDriver driver) {
		/*
		 * if(log.isDebugEnabled())
		 * log.debug("Verifying if element is enabled in UI");
		 */

		boolean status = driver.findElement(By.xpath(XPATH_NAV)).isEnabled();

		if (!status)
			return "PASS";

		else
			return "FAIL";

	}

	public String VerifyEnabled(String XPATH_NAV, String ElementType, String TestData, WebDriver driver) {
		/*
		 * if(log.isDebugEnabled())
		 * log.debug("Verifying if element is enabled in UI");
		 */

		boolean status = driver.findElement(By.xpath(XPATH_NAV)).isEnabled();

		if (status)
			return "PASS";

		else
			return "FAIL";

	}

	public String VerifyDisplayed(String XPATH_NAV, String ElementType, String TestData, WebDriver driver) {
		/*
		 * if(log.isDebugEnabled())
		 * log.debug("Verifying if element is enabled in UI");
		 */

		boolean status = driver.findElement(By.xpath(XPATH_NAV)).isDisplayed();

		if (status)
			return "PASS";

		else
			return "FAIL";

	}

	public String VerifyNotDisplayed(String XPATH_NAV, String ElementType, String TestData, WebDriver driver) {
		/*
		 * if(log.isDebugEnabled())
		 * log.debug("Verifying if element is enabled in UI");
		 */

		boolean status = driver.findElement(By.xpath(XPATH_NAV)).isDisplayed();

		if (!status)
			return "PASS";

		else
			return "FAIL";

	}
	public String PSHCBETAPI(String TestData) throws Exception {
		// TODO Auto-generated method stub
		if (TestData != null) {
			String[] Data = TestData.split("\\|");
			String Type=  Data[0];
			String Valdate = Data[1];
			String Dbtraccno= Data[2];
			String Dbtrtransitno= Data[3];
			String Panno= Data[4];
			String SettType= Data[5];
			System.out.println("Data from testdata:"+Type+Valdate + Dbtraccno + Dbtrtransitno + Panno + SettType);
			PSHBetCall bet=new PSHBetCall();
			status = bet.getresponse(Type,Valdate,Dbtraccno,Dbtrtransitno,Panno,SettType);
		}
		return status;
	}
	public void SuccessfullClickActions(String XPATH_NAV, String ElementType, String TestData, WebDriver driver) {
		try {
			int count = 0;
			while (count < 10) {
				if (driver.findElement(By.xpath(XPATH_NAV)).isDisplayed()) {
					// System.out.println("JS Button Clicked Successsfully!!");
					Actions actions = new Actions(driver);
					actions.moveToElement(driver.findElement(By.xpath(XPATH_NAV))).click().perform();
					break;
				} else {
					System.out.println(count);
					count++;
				}
			}
		} catch (Exception e) {
			System.out.println("Exception checking again!!" + e.toString());
			// HandleWebElements.SuccessfullClickJS(driver, Locator);
		}
	}

	public String VerifyColumnValues(String XPATH_NAV, String ElementType, String TestData, WebDriver driver) {
		boolean matchflag = false;
		boolean nextpage = false;
		String XathpageNav = "";
		int nextpagecount = 0;
		int previouspagecount = 0;

		if (ElementType.equals("VerifyColumnValues")) {
			ArrayList<String> UITableInt = new ArrayList<String>();

			do {
				List<WebElement> Webel = driver.findElements(By.xpath(XPATH_NAV));
				nextpage = false;
				if (nextpagecount > 2)
					break;

				int size = Webel.size();
				if (size >= 10) {
					try {
						int index = StringUtils.ordinalIndexOf(XPATH_NAV, "/", 3);
						// *[@class='intellect-stack-view
						// ng-star-inserted'][1]//*[contains(@class,'datagrid-cell
						// ng-star-inserted')][5]:VerifySortNumber
						XathpageNav = XPATH_NAV.substring(0, index);
						System.out.println(XathpageNav);
						if (driver.findElement(By.xpath(XathpageNav + "//button[@class='pagination-next']"))
								.isEnabled()) {
							nextpage = true;
							nextpagecount++;

						}
					} catch (Exception e) {
						// TODO: handle exception
					}

				}

				for (WebElement e : Webel) {

					String text = e.getText();
					/*
					 * String textvalues[]= text.split(" ");
					 * System.out.println(textvalues[0]);
					 * //UITableInt.add(Integer.valueOf());
					 */
					UITableInt.add(text);

				}
				if (nextpage && nextpagecount < 2) {
					driver.findElement(By.xpath(XathpageNav + "//button[@class='pagination-next']")).click();
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					previouspagecount++;
				}
			} while (nextpage);

			for (int i = 0; i < UITableInt.size(); i++) {
				if ((UITableInt.get(i)).contains(TestData)) {
					matchflag = true;
				}

				else
					matchflag = false;
			}
			for (int k = 0; k < previouspagecount; k++)
				driver.findElement(By.xpath(XathpageNav + "//button[@class='pagination-previous']")).click();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (matchflag)
			return "PASS";
		else
			return "FAIL";
	}

	public void GetCellText(String xPathNavigation, String elementType, String excelInput, WebDriver driver)

	{

		String rowncol = "";
		String UIvalue = "";
		String Excelvalue = "";
		boolean datevalidation = false;
		int index = StringUtils.ordinalIndexOf(excelInput, "|", 2);
		// rowncol = excelInput.substring(0,excelInput.indexOf('/'));//gets the
		// row
		rowncol = excelInput.substring(0, index);// gets the row
		System.out.println("Rowncolumn values from excel in verifyCellValue: " + rowncol);
		UIvalue = getCellValue(xPathNavigation, rowncol, elementType, driver).trim();
		UIvalue = UIvalue.replace("\n", " ");
		UIvalue = UIvalue.replace("    ", " ");

		String excelInputs[] = excelInput.split("\\|");

		if (excelInputs[2].contains("VAR")) {
			Fileutil.writeUsingFileWriter(excelInputs[2] + "=" + UIvalue);
		}

	}

	public String VerifyCellButton(String XPATH_NAV, String ElementType, String TestData, WebDriver driver) {
		boolean Displayflag = false;
		boolean nextpage = false;
		String XathpageNav = "";
		String buttonXpath = "";
		String Rowpropxpath = "";
		int nextpagecount = 0;
		int previouspagecount = 0;

		if (ElementType.equals("VerifyCellButton")) {

			ArrayList<String> UITableInt = new ArrayList<String>();

			do {
				List<WebElement> Webel = driver.findElements(By.xpath(XPATH_NAV));

				nextpage = false;

				if (nextpagecount > 2)
					break;

				int i = 0;
				int size = Webel.size();
				if (size >= 10) {
					try {
						int index = StringUtils.ordinalIndexOf(XPATH_NAV, "/", 3);
						// *[@class='intellect-stack-view
						// ng-star-inserted'][1]//*[contains(@class,'datagrid-cell
						// ng-star-inserted')][5]:VerifySortNumber
						XathpageNav = XPATH_NAV.substring(0, index);
						System.out.println(XathpageNav);
						if (driver.findElement(By.xpath(XathpageNav + "//button[@class='pagination-next']"))
								.isEnabled()) {
							nextpage = true;
							nextpagecount++;
						}
					} catch (Exception e) {
						// TODO: handle exception
					}

				}

				for (WebElement e : Webel) {
					i = i + 1;
					int index = StringUtils.ordinalIndexOf(XPATH_NAV, "/", 3);

					try {
						Rowpropxpath = Prop.readProperty("Rowval");
						Rowpropxpath = Rowpropxpath.replace("rownum", String.valueOf(i));
					} catch (IOException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
					String xpath1 = XathpageNav + Rowpropxpath + XPATH_NAV.substring(index, XPATH_NAV.length());
					buttonXpath = xpath1 + "//button//span[text()='" + TestData + "']";
					try {

						System.out.println(buttonXpath + "button is displYED");
						driver.findElement(By.xpath(buttonXpath)).isDisplayed();
					} catch (Exception e1) {
						Displayflag = false;
						break;
					}

				}
				if (nextpage && nextpagecount < 2) {
					driver.findElement(By.xpath(XathpageNav + "//button[@class='pagination-next']")).click();
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					previouspagecount++;
				}
			} while (nextpage);
		}

		for (int k = 0; k < previouspagecount; k++)
			driver.findElement(By.xpath(XathpageNav + "//button[@class='pagination-previous']")).click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (Displayflag)
			return "PASS";

		else
			return "FAIL";

	}

	public String VerifySortDates(String XPATH_NAV, String ElementType, String TestData, WebDriver driver) {
		boolean matchflag = false;
		boolean nextpage = false;
		String XathpageNav = "";
		int nextpagecount = 0;
		int previouspagecount = 0;
		ArrayList<Date> SortlistDates1;

		if (ElementType.equals("VerifySortDates")) {
			ArrayList<Date> UIlistDates1 = new ArrayList<Date>();
			DateFormat dateFormatter1 = new SimpleDateFormat("yyyy-MM-dd");

			do {
				List<WebElement> Webel = driver.findElements(By.xpath(XPATH_NAV));
				nextpage = false;

				if (nextpagecount > 2)
					break;
				int size = Webel.size();
				if (size >= 10) {
					try {
						int index = StringUtils.ordinalIndexOf(XPATH_NAV, "/", 3);
						// *[@class='intellect-stack-view
						// ng-star-inserted'][1]//*[contains(@class,'datagrid-cell
						// ng-star-inserted')][5]:VerifySortNumber
						XathpageNav = XPATH_NAV.substring(0, index);
						System.out.println(XathpageNav);
						if (driver.findElement(By.xpath(XathpageNav + "//button[@class='pagination-next']"))
								.isEnabled()) {
							nextpage = true;
							nextpagecount++;
						}
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				for (WebElement e : Webel) {
					String text = e.getText();
					String[] xlsArray = TestData.split("\\|");
					if (xlsArray[0].equals("YYYY/MM/DD")) {
						text = text.replaceAll("/", "-");

						try {
							UIlistDates1.add(dateFormatter1.parse(text));
						} catch (ParseException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
					else {
						text = text.replaceAll("/", "-");
						try {
							UIlistDates1.add(dateFormatter1.parse(text));
						} catch (ParseException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}
				if (nextpage && nextpagecount < 2) {
					driver.findElement(By.xpath(XathpageNav + "//button[@class='pagination-next']")).click();
					System.out.println("here");
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					previouspagecount++;
				}
			} while (nextpage);
			// ArrayList<Date> listDates1 = new ArrayList<Date>();
			SortlistDates1 = new ArrayList<Date>(UIlistDates1);
			// SortedTableString=new ArrayList<String>(DataValuesString);

			if (TestData.contains("|DESC")) {
				System.out.println("Before sort" + SortlistDates1);

				Collections.reverse(SortlistDates1);
				Collections.sort(SortlistDates1);
				System.out.println(SortlistDates1 + "Sorted list");
				System.out.println(UIlistDates1 + "UI list");
				int i;
				int k;
				for (i = 0, k = UIlistDates1.size() - 1; i < UIlistDates1.size() && k >= 0; i++, k--) {
					if ((UIlistDates1.get(i)).equals(SortlistDates1.get(k))) {
						matchflag = true;
						System.out.println(UIlistDates1.get(i));
						System.out.println(SortlistDates1.get(k));
						System.out.println(k + "this is k");
						System.out.println(i + "this is i");
					} else {
						matchflag = false;
						break;
					}
				}
			} else {
				Collections.sort(SortlistDates1);
				for (int i = 0; i < UIlistDates1.size(); i++) {
					if ((UIlistDates1.get(i)).equals(SortlistDates1.get(i))) {
						matchflag = true;
						System.out.println(UIlistDates1.get(i));
						System.out.println(SortlistDates1.get(i));
					}

					else
						matchflag = false;
					break;
				}

			}
			for (int k = 0; k < previouspagecount; k++) {
				driver.findElement(By.xpath(XathpageNav + "//button[@class='pagination-previous']")).click();

				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}

		if (matchflag)
			return "PASS";
		else
			return "FAIL";
	}
	
	public void SelectFromContactLookup(String XPATH, String elementType, String TestData, WebDriver driver) {
		// driver.findElement(By.xpath(XPATH)).clear();
		driver.findElement(By.xpath(XPATH)).click();
		String[] Testdataval = TestData.split("\\|");

		try {

			driver.findElement(By.xpath("//span[contains(text(),'Lookup from contacts list')]")).click();
		} catch (Exception e) {

		}
		String getRowdata = "1|" + Testdataval[0] + "|" + Testdataval[1];
		GetRow("//div[@class='datagrid-body']", "GetRow", getRowdata, driver);
		String Rowpropxpath = "";
		try {
			Rowpropxpath = Prop.readProperty("Rowval");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String rownum = Fileutil.GetFilevalue(Testdataval[1]);
		ClickCell("//div[@class='datagrid-body']", "ClickCell", rownum + "|" + "1", driver);
		String xpathnew = Rowpropxpath + "//clr-dg-row-detail";

		xpathnew = xpathnew.replace("rownum", rownum);
		driver.findElement(By.xpath(xpathnew)).click();

	}

	public static void GetSubStringofMessageSEQID(String TestData) throws Exception {
		FileUtilities FileUtilities = new FileUtilities();
		String substr = FileUtilities.GetFilevalue(Keywordoperations.get_DataValue(TestData).get(2));

		int Start = Integer.parseInt(Keywordoperations.get_DataValue(TestData).get(0));
		int End = Integer.parseInt(Keywordoperations.get_DataValue(TestData).get(1));
		System.out.println(substr.substring(Start, End));

		String _SubStirngMessageSeqID = getSubStringMsgID(substr, Start, End);
		Keywordoperations Keywordoperations = new Keywordoperations();
		Keywordoperations.StoreValue(Keywordoperations.get_DataValue(TestData).get(3), _SubStirngMessageSeqID);
	}

	public static String getSubStringMsgID(String substr, int Start, int End) {
		String _SubStirngMessageSeqID = substr.substring(Start, End);
		return _SubStirngMessageSeqID;
	}

	public static ArrayList<String> get_DataValue(String User_Name) {
		ArrayList<String> _arr_str = new ArrayList<String>();
		StringTokenizer st = new StringTokenizer(User_Name);
		while (st.hasMoreTokens()) {
			_arr_str.add(st.nextToken("|"));
		}
		return _arr_str;
	}

	public void StoreValue(String key, String Value) {
		Value = Value.replace("\n", " ");
		if (key.contains("VAR")) {
			Fileutil.writeUsingFileWriter(key + "=" + Value);
		}
	}

	public static Response GetResponse(String ReferrenceID, RequestSpecification httpRequest) {
		Response response = RestAssured.given().spec(httpRequest).when().request(io.restassured.http.Method.GET,
				getReferrenceID(ReferrenceID));
		return response;
	}

	public static String getReferrenceID(String ReferrenceID) {
		return ReferrenceID + "?pretty";
	}

	public static JsonPath GetJSONPathvalue(Response response) {
		JsonPath jsonPathEvaluator = response.jsonPath();
		return jsonPathEvaluator;
	}

	public static String toStringID(String getVal, JsonPath jsonPathEvaluator) {
		String _messageID;
		_messageID = jsonPathEvaluator.get(getVal);
		return _messageID;
	}

	public String STEVEAPI(String TestData) throws Exception {
		// TODO Auto-generated method stub
		if (TestData != null) {
			String[] Data = TestData.split("\\|");
			String requestcode = Data[0];
			System.out.println("requestcode:" + requestcode);
			String EndtoEndId = Data[1];
			System.out.println("EndtoEndId:" + EndtoEndId);
			status = apisteve.getresponse(requestcode, EndtoEndId);
		}
		return status;
	}

	public static String GetValueFromElasticDB(String TestData) throws Exception {
		FileUtilities FileUtilities = new FileUtilities();
		Keywordoperations _StoreMessageID = new Keywordoperations();
		String _TestData = FileUtilities.GetFilevalue(Keywordoperations.get_DataValue(TestData).get(0));
		String API_Url = PropertyUtils.readProperty("API_URL");
		System.out.println("Elastic url" + API_Url);
		String ReferrenceID = _TestData;
		String getVal = PropertyUtils.readProperty("MsgId");
		System.out.println("Elastic msgid" + getVal);
		String _messageID = null;
		try {
			RestAssured.baseURI = API_Url;
			RequestSpecification httpRequest = RestAssured.given();
			Response response = GetResponse(ReferrenceID, httpRequest);
			JsonPath jsonPathEvaluator = GetJSONPathvalue(response);
			_messageID = toStringID(getVal, jsonPathEvaluator);
			System.out.println("Elastic resposne" + _messageID);
			_StoreMessageID.StoreValue(Keywordoperations.get_DataValue(TestData).get(1), _messageID);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return _messageID;
	}

	public void QuitDriver(WebDriver driver) throws Exception {
		if (driver != null) {
			driver.quit();
		}
	}

	//Varun FUll Fill RTP API
	public static void TriggerFulfillRTPAPI(String TestData) throws Exception {
		FileUtilities FileUtilities=new  FileUtilities();
		String VAR_DATE=null;
		String FullFill_RTPAPI = PropertyUtils.readProperty("FullFillRTP_API");
		try {
			String VAR_AMOUNT = Keywordoperations.get_DataValue(TestData).get(0);
			VAR_DATE = Keywordoperations.get_DataValue(TestData).get(1);
			
			if (VAR_DATE.equalsIgnoreCase("date")) {
				VAR_DATE = _DateFormat();
			}else{
				VAR_DATE = Keywordoperations.get_DataValue(TestData).get(1);
			}
			
			String MESSAGE_ID = Keywordoperations.get_DataValue(TestData).get(2);
			
			String VAL_STOREFULFILLRTP = getSaltString("Y", 10,MESSAGE_ID);			
			Keywordoperations Keywordoperations = new Keywordoperations();		
			Keywordoperations.StoreValue("VAR_FULFILLRTP", VAL_STOREFULFILLRTP);
			
			URL url = new URL(FullFill_RTPAPI);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("x-et-api-signature", "CA000003");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("x-et-channel-indicator", "ONLINE");
			conn.setRequestProperty("x-et-participant-id", "CA000010");
			conn.setRequestProperty("x-et-request-id","GSPR_" + getSaltString("N", 5,MESSAGE_ID));
			conn.setRequestProperty("x-et-retry-indicator", "TRUE");
			conn.setRequestProperty("x-et-transaction-time",getSimpleDateFormat());
			conn.setRequestProperty("x-et-participant-user-id", "00000006");
			conn.setRequestProperty("X-et-api-signature-type","PAYLOAD_DIGEST_SHA256");

			String input = ReplaceValueJSON(VAR_AMOUNT,VAR_DATE,VAL_STOREFULFILLRTP);

			System.out.println(input);
			OutputStream os = conn.getOutputStream();
			os.write(input.getBytes());
			os.flush();

			System.out.println(conn.getResponseCode());
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			String output;
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}
			conn.disconnect();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	
	
	public String VerifyValue(String XPATH, String elementType,String ElementName, String TestData, WebDriver driver) {
		/*
		 * if(log.isDebugEnabled()) log.debug("Entering");
		 */
		try {
			if (TestData.contains(Prop.readProperty("Variablesname"))) {
				TestData = Fileutil.GetFilevalue(TestData).trim();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		
		String txtLegalName = driver.findElement(By.xpath(XPATH)).getAttribute("value");
		
		
		txtLegalName = txtLegalName.replace("\n", " ");

		System.out.println("Element in UI:" +txtLegalName+"| Length of element ui: "+txtLegalName.length());
		System.out.println("Element in Testdata:" +TestData+"| Length of element Testdata: "+TestData.length());
        log.info("Element UI:" +txtLegalName+"| Length of element ui: "+txtLegalName.length());
        log.info("Element Testdata:" +TestData+"| Length of element Testdata: "+TestData.length());

		// changed by jyoti
		if (elementType.equals("VerifyValuePartial")) {

			if (txtLegalName.contains(TestData))
				txtLegalName = TestData;

		}

		if (txtLegalName.equals(TestData))
			return "PASS";
		else
			return "FAIL";

		/*
		 * if(log.isDebugEnabled()) log.debug("Leaving after click");
		 */
	}	
	
	public static String ReplaceValueJSON(String VAR_AMOUNT,String VAR_DATE,String VAR_MESSAGEID) throws Exception {
		String ReqPayload_Create_Domain = PropertyUtils.readProperty("FullFillRTP_REQ");
		String finalstring = ReqPayload_Create_Domain
				.replace("VAR_AMOUNT", VAR_AMOUNT)
				.replace("VAR_DATE", VAR_DATE)
				.replace("VARMASAGEID", VAR_MESSAGEID);
		return finalstring;
	}
	
	public static String getSaltString(String Flag, int _NumLength,String VAR_MESSAGEID)  throws Exception {
		String SALTCHARS = "1234567890";
		String Result = null;
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < _NumLength) {
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String saltStr = salt.toString();
		if (Flag.equals("Y")) {
			Result = VAR_MESSAGEID + saltStr;
		} else {
			Result = saltStr;
		}
		return Result;
	}
	public static String getSimpleDateFormat() throws Exception {
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'20:53:23.234'Z'");
		String dateAsISOString = df.format(date);
		System.out.println(dateAsISOString);
		log.info(dateAsISOString);
		return dateAsISOString;
	}
	public static String getRandomNumber(String Flag, int _NumLength,String VAR_MESSAGEID)  throws Exception {
		//String SALTCHARS = "1234567890";
		String SALTCHARS = CurrentDateTime("yyyyMMdd")+"00"+CurrentDateTime("hhmmss");
		String Result = null;
				if (Flag.equals("Y")) {
			Result = VAR_MESSAGEID + SALTCHARS;
		} else {
			Result = SALTCHARS;
		}
		return Result;
	}
public static String CurrentDateTime(String formate) throws Exception {
		
		DateFormat dateFormat = new SimpleDateFormat(formate);
		Date date = new Date();
		String dates = dateFormat.format(date);
		return dates;
	}
	
	public static String _DateFormat() throws Exception {
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String dateAsISOString = df.format(date);
		System.out.println(dateAsISOString);
		return dateAsISOString;
	}
	
	public static String GetValueFromFAEIndicatorElasticDB(String TestData) throws Exception {
		FileUtilities FileUtilities = new FileUtilities();
		Keywordoperations _StoreMessageID = new Keywordoperations();
		String _TestData = FileUtilities.GetFilevalue(Keywordoperations.get_DataValue(TestData).get(0));
		String API_Url = PropertyUtils.readProperty("API_URL");
		System.out.println("Elastic url" + API_Url);
		String ReferrenceID = _TestData;
		String getVal = PropertyUtils.readProperty("FAE_CALLPath");
		System.out.println("Elastic msgid" + getVal);
		String Result=null;
		String _messageID = null;
		try {
			RestAssured.baseURI = API_Url;
			RequestSpecification httpRequest = RestAssured.given();
			Response response = GetResponse(ReferrenceID, httpRequest);
			JsonPath jsonPathEvaluator = GetJSONPathvalue(response);
			_messageID = toStringID(getVal, jsonPathEvaluator);
			System.out.println("Elastic resposne" + _messageID);
//			_StoreMessageID.StoreValue(Keywordoperations.get_DataValue(TestData).get(1), _messageID);
			if (_messageID.equalsIgnoreCase(Keywordoperations.get_DataValue(TestData).get(1))) {
				Result = "PASS";
			}else{
				Result = "FAIL";
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Result;
	}


	public String PSHCWiresCall(String testData) {
		// TODO Auto-generated method stub
		if (testData != null) {
		String[] Data = testData.split("\\|");
		String Type=  Data[0];
		String Valdate = Data[1];
		String Dbtraccno= Data[2];
		String Dbtrtransitno= Data[3];
		String Panno= Data[4];
		String FXId= Data[5];
		String ctrctid=Data[6];
		String InstrucCurrency=Data[7];
		String AcctType=Data[8];
		System.out.println("Data from testdata:"+Type+Valdate + Dbtraccno + Dbtrtransitno + Panno + FXId + AcctType + ctrctid + InstrucCurrency);
		PSHWiresCall bet=new PSHWiresCall();
		try {
		status = bet.getresponse(Type,Valdate,Dbtraccno,Dbtrtransitno,Panno,FXId,AcctType,ctrctid,InstrucCurrency);
		} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		}
		return status;
		}



	public String PSHCWiresCancellation(String testData) {
		// TODO Auto-generated method stub
		if (testData != null) {
			String[] Data = testData.split("\\|");
			String Custid=  Data[0];
			String custrefnum = Data[1];
			String criteria = Data[2];

			System.out.println("Data from testdata:"+Custid+custrefnum);
			PSHWiresCancellation bet=new PSHWiresCancellation();
			try {
				status = bet.getresponse(Custid,custrefnum,criteria);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return status;
	}
	public String GetColumnValues(String XPATH_NAV, String ElementType, String TestData, WebDriver driver)
	{

		boolean matchflag = false;
		boolean nextpage = false;
		String XathpageNav = "";
		int nextpagecount = 0;
		int previouspagecount = 0;
		String TestDataval[]=TestData.split("\\|");
		if (ElementType.equals("GetColumnValues")) {
			ArrayList<String> UITableInt = new ArrayList<String>();

			do {
				List<WebElement> Webel = driver.findElements(By.xpath(XPATH_NAV));
				nextpage = false;
				if (nextpagecount > 2)
					break;

				int size = Webel.size();
				if (size >= Integer.valueOf(TestDataval[1])) {
					try {
						int index = StringUtils.ordinalIndexOf(XPATH_NAV, "/", 3);
						// *[@class='intellect-stack-view
						// ng-star-inserted'][1]//*[contains(@class,'datagrid-cell
						// ng-star-inserted')][5]:VerifySortNumber
						XathpageNav = XPATH_NAV.substring(0, index);
						System.out.println(XathpageNav);
						if (driver.findElement(By.xpath(XathpageNav + "//button[@class='pagination-next']"))
								.isEnabled()) {
							nextpage = true;
							nextpagecount++;

						}
					} catch (Exception e) {
						// TODO: handle exception
					}

				}

				for (WebElement e : Webel) {

					String text = e.getText();
					
					System.out.println(text+"text here");
					
					
					
					text=text.replace("\n", " ");
					
					//UIvalue = UIvalue.replace("\n", " ");
					//text = text.replace("    ", " ");
					//text=text.replace(oldChar, newChar)
					/*
					 * String textvalues[]= text.split(" ");
					 * System.out.println(textvalues[0]);
					 * //UITableInt.add(Integer.valueOf());
					 */
					UITableInt.add(text);

				}
				if (nextpage && nextpagecount < 2) {
					driver.findElement(By.xpath(XathpageNav + "//button[@class='pagination-next']")).click();
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					previouspagecount++;
				}
			} while (nextpage);
			
			String newval="";
			String currva="";
			Collections.sort(UITableInt);
			for (int i = 0; i < UITableInt.size(); i++) 
				
			{
				String Currval[] =UITableInt.get(i).split(" ");
				
				currva=Currval[2];
				 newval=newval.concat(currva.replace(" ", "")+"|");
			}
			System.out.println(newval);
			newval=newval.substring(0, newval.length()-1);
			if (TestDataval[0].contains("VAR")) {
				Fileutil.writeUsingFileWriter(TestDataval[0] + "=" + newval);
			}
			
			for (int k = 0; k < previouspagecount; k++)
				driver.findElement(By.xpath(XathpageNav + "//button[@class='pagination-previous']")).click();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (matchflag)
			return "PASS";
		else
			return "FAIL";
	
	}

	  public ArrayList<String> sortAscending() {         
		    Collections.sort(this.arrayList1);         
		    return this.arrayList1;     
		  }  

	public String GetElasticValuesPayTo(String testData) {
		//GraphqBeneCall gcall=new GraphqBeneCall();
		ElasticSearchBenePayTo gcall=new ElasticSearchBenePayTo();
		  ArrayList<String> namesW= new ArrayList();
		  ArrayList<String> namesW1= new ArrayList();
		  ArrayList<String> namesW2= new ArrayList();
		  ArrayList<String> namesg=new ArrayList();
		  namesW=null;
		String ElasticDbvalue=null;
		String compare=null;
		 String name1 = "";
		if (testData != null) {
			//String[] Data = testData.split("\\|");
			String token=  testData;
			System.out.println("corporate id from sheet"+token);
		try {
			ElasticDbvalue = PropertyUtils.readProperty("API2_URL");
			System.out.println("Req from "+ElasticDbvalue);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			namesW=null;
			namesW1=gcall.POSTToAPI(ElasticDbvalue,token);
			
		     Collections.sort(namesW1);   
			 namesg = (ArrayList<String>) namesW1.stream().distinct() .collect(Collectors.toList());
              namesW2=namesg;
			    
			System.out.println("size :"+namesW2.size());
			
			  for(int i=0;i<namesW2.size();i++){  
					 
				   System.out.println("Names"+namesW2.get(i)); 
				   name1=name1+namesW2.get(i).toString()+"|";
				   //compare=itr.next()+"|";
				   System.out.println("values from elastic:"+name1);
				  }
		
		        String Finalmsg=name1;
		        String output=Finalmsg.substring(0, Finalmsg.length()-1);
		        System.out.println("Value to be stored"+output);   
				   Keywordoperations _Storevalue=new Keywordoperations();
					_Storevalue.StoreValue("VAR_PAYTO", output);
					if(output!=null)
					{
						compare="PASS";
					}
					else
					{
						compare="FAIL";
					}
						
					
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
		return compare;
	
	}


	public String GetElasticValuesPayFrom(String testData) {
		System.out.println("GetElasticValuesPayFrom method");
		String ElasticDbvalue1=null;
		String compare=null;
		ElasticSearchAccountsPayFrom e=new ElasticSearchAccountsPayFrom();
		
		if (testData != null) {
			String[] Data = testData.split("\\|");
			String name=  Data[1];
			String type=  Data[2];
			System.out.println("from sheet:"+name+" "+type);

		try {
			ElasticDbvalue1 = PropertyUtils.readProperty("API3_URL");
			System.out.println("Req from "+ElasticDbvalue1);
			compare=e.getData(ElasticDbvalue1,name,type);
			System.out.println("COMAPRE VARIABLE"+compare);
		} catch (IOException e1) {

			e1.printStackTrace();
		} catch (Exception e1) {

			e1.printStackTrace();
		}

		Keywordoperations _Storevalue1=new Keywordoperations();
		_Storevalue1.StoreValue("VAR_PAYFROM", compare);
		

	}
		return compare;	
}
	public void SendSequenceofKeys(String XPATH, String TestData, WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(XPATH));
		element.sendKeys(TestData);
		element.clear();
		element.sendKeys(TestData);
	}

	public static void getSumMoneyOut() throws JSONException{
		Keywordoperations Keywordoperations =new Keywordoperations();
		double _Sum = 0;
		try{
		RestAssured.baseURI = "http://10.198.12.2:9200/quest.pay/paymentInstructions";
		 RequestSpecification httpRequest = RestAssured.given();
		 Response response = httpRequest.request(Method.GET, "_search?q=status:PENDING ACCEPTANCE AND paymentTypesObj.code:fulfillrtp");
		 String responseBody = response.getBody().asString();
		 JSONObject _objRESP = new JSONObject(responseBody);
		 String hits_Obj = _objRESP.getString("hits");
		 JSONObject Obj_Arr = new JSONObject(hits_Obj);
		 JSONArray StsRsnInf_arr = Obj_Arr.getJSONArray("hits");
		 System.out.println(StsRsnInf_arr.length());
		 
		 for (int i = 0; i < StsRsnInf_arr.length(); i++) {
			 JSONObject _obj=new JSONObject(StsRsnInf_arr.get(i).toString());
			 String _SourceObj = _obj.get("_source").toString();
			 JSONObject _EquivalentObj = new JSONObject(_SourceObj);
				 System.out.println(_EquivalentObj.get("instructedAmount"));
				 String _EquivalentValue = _EquivalentObj.get("instructedAmount").toString();
				 JSONObject _EquivalentValueObj = new JSONObject(_EquivalentValue); 
				 System.out.println(_EquivalentValueObj.get("value"));
				 _Sum = _Sum + Double.parseDouble(_EquivalentValueObj.get("value").toString());
		 }
		 	System.out.println("Sum = " + _Sum);
		 	String s=String.valueOf(_Sum);
		 	System.out.println(s);
		 	String strArr [] = s.split("\\.");
		 	if(strArr[1].equals("00") || strArr[1].equals("0")) {
		 		s = strArr[0];
		 	}
		 	System.out.println("After Split : " + s);
		 	
		 	Keywordoperations.StoreValue("VAR_MONEY_OUTSUM", s);
		}catch (Exception e) {
			e.printStackTrace();
		}
		 
	}


	public static void getSumPaymentAndReqInWorkFlow() throws JSONException{
		Keywordoperations Keywordoperations=new Keywordoperations();
		try{
		RestAssured.baseURI = "http://10.198.12.2:9200/quest.pay/paymentInstructions";
		 RequestSpecification httpRequest = RestAssured.given();
		 Response response = httpRequest.request(Method.GET, "_search?q=status:pending approval");
		 String responseBody = response.getBody().asString();
		 JSONObject _objRESP = new JSONObject(responseBody);
		 String hits_Obj = _objRESP.getString("hits");
//		 System.out.println(hits_Obj);
		 JSONObject Obj_Arr = new JSONObject(hits_Obj);
		 JSONArray StsRsnInf_arr = Obj_Arr.getJSONArray("hits");
		 System.out.println(StsRsnInf_arr.length());
		 
		 double _Sum = 0;
		 for (int i = 0; i < StsRsnInf_arr.length(); i++) {
			 JSONObject _obj=new JSONObject(StsRsnInf_arr.get(i).toString());
			 String _SourceObj = _obj.get("_source").toString();
			 JSONObject _EquivalentObj = new JSONObject(_SourceObj);
				 System.out.println(_EquivalentObj.get("instructedAmount"));
				 String _EquivalentValue = _EquivalentObj.get("instructedAmount").toString();
				 JSONObject _EquivalentValueObj = new JSONObject(_EquivalentValue); 
				 System.out.println(_EquivalentValueObj.get("value"));
				 _Sum = _Sum + Double.parseDouble(_EquivalentValueObj.get("value").toString());
		 }
		 	System.out.println("Sum = " + _Sum);
		 	
		 	String s=String.valueOf(_Sum);
		 	System.out.println(s);
		 	Keywordoperations.StoreValue("VAR_MONEY_OUTPMTREQSUM", s);
	
		 	
		}catch (Exception e) {
			e.printStackTrace();
		}
	}	
	
		public static void getSumMoneyIn() throws JSONException{
		Keywordoperations Keywordoperations=new Keywordoperations();
		try{
		RestAssured.baseURI = "http://10.198.12.2:9200/quest.pay/paymentInstructions";
		 RequestSpecification httpRequest = RestAssured.given();
		 Response response = httpRequest.request(Method.GET, "_search?q=status:PENDING APPROVAL");
		 String responseBody = response.getBody().asString();
		 JSONObject _objRESP = new JSONObject(responseBody);
		 String hits_Obj = _objRESP.getString("hits");
//		 System.out.println(hits_Obj);
		 JSONObject Obj_Arr = new JSONObject(hits_Obj);
		 JSONArray StsRsnInf_arr = Obj_Arr.getJSONArray("hits");
		 System.out.println(StsRsnInf_arr.length());
		 
		 double _Sum = 0;
		 for (int i = 0; i < StsRsnInf_arr.length(); i++) {
			 JSONObject _obj=new JSONObject(StsRsnInf_arr.get(i).toString());
			 String _SourceObj = _obj.get("_source").toString();
			 JSONObject _EquivalentObj = new JSONObject(_SourceObj);
//			 if(_EquivalentObj.get("paymentMethod").equals("TRF")){
				 System.out.println(_EquivalentObj.get("instructedAmount"));
				 String _EquivalentValue = _EquivalentObj.get("instructedAmount").toString();
				 JSONObject _EquivalentValueObj = new JSONObject(_EquivalentValue); 
				 System.out.println(_EquivalentValueObj.get("value"));
				 _Sum = _Sum + Double.parseDouble(_EquivalentValueObj.get("value").toString());
//			 }
		 }
		 	System.out.println("Sum = " + _Sum);
		 	String s=String.valueOf(_Sum);
		 	System.out.println(s);
		 	Keywordoperations.StoreValue("VAR_MONEY_IN", s);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
		public String Validate_MoneyOutRequestToFulFillSumUI(String XPATH, String elementType, String TestData, WebDriver driver) {
			/*
			 * if(log.isDebugEnabled()) log.debug("Entering");
			 */
			try {
				if (TestData.contains(Prop.readProperty("Variablesname"))) {
					TestData = Fileutil.GetFilevalue(TestData).trim();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			WebElement tabname = driver.findElement(By.xpath(XPATH));
			// Utility.highLighterMethod(driver, tabname);
			String txtLegalName = tabname.getText(); // getAttribute("value");
			txtLegalName = txtLegalName.replace("\n", " ");

			// log.debug("Text "+ txtLegalName);
			System.out.println("Element getText is: " + txtLegalName);
			String substr;
			FileUtilities FileUtilities=new FileUtilities();
			substr = FileUtilities.GetFilevalue("VAR_MONEY_OUTSUM");
			if (substr.equalsIgnoreCase(txtLegalName)) {
				return "PASS";
			}
			else
				return "FAIL";
		} 
	
		public static void getSumSendRTP() throws JSONException{
		Keywordoperations Keywordoperations=new Keywordoperations();
		try{
		RestAssured.baseURI = "http://10.198.12.2:9200/quest.pay/paymentInstructions";
		 RequestSpecification httpRequest = RestAssured.given();
		 Response response = httpRequest.request(Method.GET, "_search?q=status:SENT TO RECEIVER AND paymentTypesObj.code:sendrtp");
		 String responseBody = response.getBody().asString();
		 JSONObject _objRESP = new JSONObject(responseBody);
		 String hits_Obj = _objRESP.getString("hits");
//		 System.out.println(hits_Obj);
		 JSONObject Obj_Arr = new JSONObject(hits_Obj);
		 JSONArray StsRsnInf_arr = Obj_Arr.getJSONArray("hits");
		 System.out.println(StsRsnInf_arr.length());
		 
		 double _Sum = 0;
		 for (int i = 0; i < StsRsnInf_arr.length(); i++) {
			 JSONObject _obj=new JSONObject(StsRsnInf_arr.get(i).toString());
			 String _SourceObj = _obj.get("_source").toString();
			 JSONObject _EquivalentObj = new JSONObject(_SourceObj);
				 System.out.println(_EquivalentObj.get("instructedAmount"));
				 String _EquivalentValue = _EquivalentObj.get("instructedAmount").toString();
				 JSONObject _EquivalentValueObj = new JSONObject(_EquivalentValue); 
				 System.out.println(_EquivalentValueObj.get("value"));
				 _Sum = _Sum + Double.parseDouble(_EquivalentValueObj.get("value").toString());
		 }
		 	System.out.println("Sum = " + _Sum);
		 	
		 	System.out.println("Sum = " + _Sum);
		 	String s=String.valueOf(_Sum);
		 	System.out.println(s);
		 	Keywordoperations.StoreValue("VAR_SENDRTP_SUM", s);

		}catch (Exception e) {
			e.printStackTrace();
		}
	}
		public void RandomClickCell(String XPATH, String elementType, String TestData, WebDriver driver) {
			String[] ArrayxlsValue = TestData.split("\\|");
			String FinalXpath = "";
			String Cellpropxpath;
			String Rowpropxpath;
			int rowException=1;
			try {


			//button[@aria-label='Payments']

			try {
			Thread.sleep(8000);
			} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			}


			/* String nofpgs=driver.findElement(By.xpath("//li[@class='ng-star-inserted'][3]//button")).getText();
			int totalno=Integer.valueOf(nofpgs)*10;*/

			String noOfPayments=driver.findElement(By.xpath("//button[@aria-label='Payments']/span[2]")).getText();

			noOfPayments=noOfPayments.substring(1,(noOfPayments.length()-1));

			System.out.println(noOfPayments);

			int totalno=Integer.valueOf(noOfPayments);




			if (ArrayxlsValue[0].contains("VAR_")) {
			// read property from cofig file
			ArrayxlsValue[0] = Fileutil.GetFilevalue(ArrayxlsValue[0]);

			}

			if (elementType.equalsIgnoreCase("ClickDataCell")) {
			Rowpropxpath = Prop.readProperty("DATARowval");
			Cellpropxpath = "";
			}

			else {
			Cellpropxpath = Prop.readProperty("Cellval");
			Rowpropxpath = Prop.readProperty("Rowval");

			}


			Cellpropxpath = Cellpropxpath.replace("cellnum", ArrayxlsValue[1]);
			System.out.println(Cellpropxpath);
			// Rowpropxpath=Rowpropxpath.replaceFirst("rownum",ArrayxlsValue[0]);
			System.out.println(Rowpropxpath);
			//Rowpropxpath = Rowpropxpath.replace("rownum", ArrayxlsValue[0]);

			System.out.println(Rowpropxpath);

			//FinalXpath = XPATH + Rowpropxpath + Cellpropxpath;

			//driver.findElement(By.xpath(FinalXpath));
			System.out.println("*****"+FinalXpath+"BBB");

			int toBeIterate=totalno;
			for(int i=0;i<totalno;i++)
			{
			if( rowException< toBeIterate && i<9 ) {
			if((i%2) ==0)
			{
			//call approve



			try {
			Thread.sleep(3000);
			} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			}
			Rowpropxpath = Prop.readProperty("Rowval");
			Rowpropxpath=Rowpropxpath.replace("rownum",rowException+"");
			System.out.println("*****"+FinalXpath+"AAA");
			//FinalXpath = XPATH + rowException + Cellpropxpath + "//button[" + ArrayxlsValue[2] + "]";
			FinalXpath= XPATH + Rowpropxpath + Cellpropxpath + "//button[1]";

			System.out.println("*****"+FinalXpath+"AAA");

			driver.findElement(By.xpath(FinalXpath)).click();


			//code here to do steps after approve
			WebElement rsaPin = driver.findElement(By.xpath("//*[@id='rsaPin']"));

			//rsaPin.sendKeys("0000");
			rsaPin.clear();
			rsaPin.sendKeys("0000");
			rsaPin.sendKeys(Keys.TAB);

			WebElement rsaToken = driver.findElement(By.xpath("//*[@id='rsaToken']"));


			rsaToken.clear();
			rsaToken.sendKeys("ABCD");
			rsaToken.sendKeys(Keys.TAB); 
			//button[@class='btn btn-block btn-primary rsa-verify-button']
			try {
			Thread.sleep(3000);

			driver.findElement(By.xpath("//button[@class='btn btn-block btn-primary rsa-verify-button']")).click();
			Thread.sleep(8000);
			//if Sorry shweta
			//div[@class='text-center ng-star-inserted']//strong


			WebElement successMessage = driver.findElement(By.xpath("//div[@class='modal-content']//h3"));
			//Utility.highLighterMethod(driver, tabname);
			String varResult = successMessage.getText(); 
			System.out.println("\n Success Msg is +++++"+ varResult);
			if(varResult.equalsIgnoreCase("Sorry!")) {
			rowException++;
			System.out.println("true****************");
			}

			driver.findElement(By.xpath("//clr-icon[@shape='close']")).click();

			}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}

			}
			else 

			{

			try {
			Thread.sleep(3000);
			} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			}

			Rowpropxpath = Prop.readProperty("Rowval");
			Rowpropxpath=Rowpropxpath.replace("rownum",rowException+""); 
			FinalXpath= XPATH + Rowpropxpath + Cellpropxpath + "//button[2]";
			driver.findElement(By.xpath(FinalXpath)).click();


			try {
			Thread.sleep(3000);


			//WebElement rejectReason = driver.findElement(By.xpath("//*[@id='rejectReason']"));
			SelectDiv("//*[@id='rejectReason']", "Others", driver);

			Thread.sleep(3000);

			WebElement notes = driver.findElement(By.xpath("//*[@id='addNotes']"));


			notes.clear();
			notes.sendKeys("ABCD");
			notes.sendKeys(Keys.TAB); 


			Thread.sleep(8000);

			driver.findElement(By.xpath("//button[@class='btn btn-danger ng-star-inserted']/span[text()=\"Reject\"]")).click();

			Thread.sleep(3000);
			WebElement successMessage = driver.findElement(By.xpath("//div[@class='modal-content']//h3"));
			//Utility.highLighterMethod(driver, tabname);
			String varResult = successMessage.getText(); 
			System.out.println("\n Success Msg is +++++"+ varResult);
			if(varResult.equalsIgnoreCase("Sorry!")) {
			rowException++;
			System.out.println("True");
			}

			driver.findElement(By.xpath("//clr-icon[@shape='close']")).click();
			}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}

			}
			}
			//call reject

			toBeIterate--;

			}



			if (ArrayxlsValue.length > 2) {
			// string button="\\button\span["+ArrayxlsValue[2]+"]";

			if (!ArrayxlsValue[2].equalsIgnoreCase("0")) {
			FinalXpath = XPATH + Rowpropxpath + Cellpropxpath + "//button[" + ArrayxlsValue[2] + "]";
			System.out.println("fin xpath" + FinalXpath);
			} else {


			}

			} else {
			System.out.println(XPATH + Rowpropxpath + Cellpropxpath + "//button");
			FinalXpath = XPATH + Rowpropxpath + Cellpropxpath + "//button";
			System.out.println(FinalXpath);
			}
			driver.findElement(By.xpath(FinalXpath)).click();
			}

			catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			}
			}


		public String GetPSHLogs(String testData) throws Exception {
			String ResultPSH=null;
			FTPExample f=new FTPExample();
			String ServerPath=PropertyUtils.readProperty("PSHSERVERLOG");
			String LocalPath=PropertyUtils.readProperty("PSHLOGDOWNLOADPATH");
			try {
				ResultPSH=f.SFTPfile_DownloadToLocal(ServerPath, LocalPath, testData);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				ResultPSH="FAIL";
				e.printStackTrace();
				log.debug(e.toString());
			}
			return ResultPSH;
		}


		public String ReadPSHLogs(String testData) throws IOException {
			String Result="FAIL";
			String[] Data = testData.split("\\|");
			String filename=Data[0];
			String searchstring=Data[1];
			String LocalPath=PropertyUtils.readProperty("PSHREADLOGPATH");
			filename=LocalPath+filename;
			ReadPSHLogs ir=new ReadPSHLogs();
			int result=ir.checkdata(filename, searchstring);
			if (result>0)
			{
				Result="PASS";
				System.out.println("Pass "+result);
				log.info("Pass "+result);
			}
			else{Result="FAIL";}
			return Result;
		}
		public  String AsyncMessageComparison(String compare) throws JSONException {
			FileUtilities Fileutil = new FileUtilities();
			String jsonbody=Fileutil.GetFilevalue("VAR_OUTGOINGMSG");
			System.out.println(jsonbody);
			log.info(jsonbody);
			JSONObject obj = new JSONObject(jsonbody);
			JSONObject obj1=obj.getJSONObject("context");
			JSONObject obj2=obj1.getJSONObject("eventType");
			String status=obj2.getString("status");
			System.out.println(status);
			log.info(status);
			if(status.equals(compare))
			{
			status="PASS";
			System.out.println(status);
			}
			else
			{
				status="FAIL";
				System.out.println(status);
			}
			return status;
		}


		public String WiresMT103ACK(String testData) {
			String Result="PASS";
			WiresAckMT103MT199 w=new WiresAckMT103MT199();
			w.dbconnect();
			try {
				w.ackMt103("d");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Result="FAIL";
				e.printStackTrace();
				log.debug(e.toString());
			}
			return Result;
		}
		
		public String WiresMT199(String testData) {
			String Result="PASS";
			WiresAckMT103MT199 w=new WiresAckMT103MT199();
			w.dbconnect();
			try {
				w.MT199(testData);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Result="FAIL";
				e.printStackTrace();
				log.debug(e.toString());
			}
			return Result;
		}
		
		public String WiresMT900(String testData) {
			String Result="PASS";
			WiresAckMT103MT199 w=new WiresAckMT103MT199();
			w.dbconnect();
			try {
				w.MT900(testData);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Result="FAIL";
				e.printStackTrace();
				log.debug(e.toString());
			}
			return Result;
		}


		public String IWSFeedUpload(String testData) throws Exception {
		   String status="PASS";
			Random rand = new Random(); 
			Date date = new Date();
			DateFormat df = new SimpleDateFormat("YYYYMMdd");
			String dateAsISOString = df.format(date);
			String ICNdate=dateAsISOString.substring(2);
			String host = PropertyUtils.readProperty("PSHAPP");
			String username=PropertyUtils.readProperty("PSHAPP_username");
			String pwd=PropertyUtils.readProperty("PSHAPP_Password");
			String remotedir=PropertyUtils.readProperty("IWSRemotedir");
			String localdir=System.getProperty("user.dir")+"/src/test/resources/upload/";
			FTPExample f=new FTPExample(host,22,username,pwd);
			
			WiresAckMT103MT199 w=new WiresAckMT103MT199();
			String wirerefnum=w.dbconnect();
			String IWSFeed="FH@@@@@@@@200000IWS                                                                                                                    \r\n"+
"FD*******                         TO!!!!!!000063000201706220501170#                                xxxxxxxx-xxxx-4xxx-8xxx-xxxxxxxxxxxx\r\n"+
"FT00000001                                                                                                                             ";
			System.out.println(IWSFeed);
			IWSFeed=IWSFeed.replace("@@@@@@@@", dateAsISOString);
			IWSFeed=IWSFeed.replace("!!!!!!", ICNdate);
			IWSFeed=IWSFeed.replace("#", testData);
			IWSFeed=IWSFeed.replace("*******", wirerefnum);
			System.out.println(IWSFeed);
			String filename="IWSPSH160117096401513_With_SWIFT_UETR_";
			int rand_int1 = rand.nextInt(10000); 
	        String uniqueid=String.valueOf(rand_int1);
	        filename=filename+uniqueid;
	        deletefile();
	        String filenameupload=localdir+filename+".txt";
	        System.out.println("filenameupload:"+filenameupload);
	        log.info("filenameupload:"+filenameupload);
			FileWriter writer = new FileWriter(filenameupload);  
		    BufferedWriter buffer = new BufferedWriter(writer);  
		    buffer.write(IWSFeed);  
		    buffer.close();  
		    f.upload(filenameupload,remotedir );
			return status;
		}
		   public void deletefile() {
		        String path=System.getProperty("user.dir")+"\\src\\test\\resources\\upload\\"; 
		        File file = new File(path);
		        File[] files = file.listFiles(); 
		        for (File f:files) 
		        {if (f.isFile() && f.exists()) 
		            { f.delete();
		System.out.println("successfully deleted");
		log.info("successfully deleted");
		            }else{
		System.out.println("cant delete a file due to open or error");
		log.info("cant delete a file due to open or error");
		} }  }


		   public String ClickManualActionQ(String testData, WebDriver driver) throws InterruptedException {
			   String Status="PASS";
			   driver.switchTo().frame("TabFrame");
				Modules md = new Modules();
				JavascriptExecutor jsExecutor = (JavascriptExecutor)driver;
				String currentFrame = (String) jsExecutor.executeScript("return self.name");
				System.out.println("CurrentFrame:"+currentFrame); 
				WebDriverWait wait= new WebDriverWait(driver,30);
			
				/*Thread.sleep(5000);
				driver.switchTo().defaultContent();
				System.out.println(driver.getTitle());
				driver.switchTo().frame("TabFrame");*/
				String[] Data = testData.split("\\|");
				for(String dat:Data)
				{
				System.out.println("data from array"+dat);
				log.info("data from array"+dat);
				}
				String tabFrameMenuButton=  Data[0];//WiresMenu
				String xpth1 = Data[1];//
				System.out.println("submenu"+xpth1);
				log.info("submenu"+xpth1);
				xpth1.trim();
				String xpth2 = Data[2];
				xpth2.trim();
				System.out.println("queuename"+xpth2);
				log.info("queuename"+xpth2);
				String xpth3 = Data[3];
				xpth3.trim();
				System.out.println("queue"+xpth3);
				log.info("queue"+xpth3);
				try {
				WebElement tabFrameButton = driver.findElement(By.xpath("//*[text()='"+tabFrameMenuButton+"']"));
				System.out.println(tabFrameButton);
				log.info(tabFrameButton);
				tabFrameButton.click();
				String currentFrame2 = (String) jsExecutor.executeScript("return self.name");
				System.out.println("currentFrame2");
				System.out.println("CurrentFrame:"+currentFrame2); 
				log.info("CurrentFrame:"+currentFrame2);
				driver.switchTo().defaultContent();
				//JavascriptExecutor jsExecutor = (JavascriptExecutor)driver;
				String currentFrame3 = (String) jsExecutor.executeScript("return self.name");
				System.out.println("CurrentFrame:"+currentFrame3); 
				log.info("CurrentFrame:"+currentFrame3); 
				System.out.println(driver.getTitle());
				driver.switchTo().frame("MenuFrame");//MenuFrame
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()='"+xpth1+"']")));
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//*[text()='"+xpth1+"']")));
				WebElement subMenuFrameButton = driver.findElement(By.xpath("//div[text()='"+xpth1+"']"));//WiresSubmenu
				System.out.println("subMenuFrameButton:"+subMenuFrameButton);
				log.info("subMenuFrameButton:"+subMenuFrameButton);
				//subMenuFrameButton.click();
				//Thread.sleep(8000);
			
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()='"+xpth2+"']")));
				WebElement subMenuFrameButton2 = driver.findElement(By.xpath("//*[text()='"+xpth2+"']"));//WarehouseSubmenu
				System.out.println("subMenuFrameButton2:"+subMenuFrameButton2);
				log.info("subMenuFrameButton:"+subMenuFrameButton);
				subMenuFrameButton2.click();
				//((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//*[text()='"+xpth2+"']")));
				if(!xpth3.equals("-"))
				{	
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()='"+xpth3+"']")));
				//Thread.sleep(8000);
				WebElement subMenuFrameButton3 = driver.findElement(By.xpath("//*[text()='"+xpth3+"']"));//warehoused Production
				System.out.println("subMenuFrameButton3:"+subMenuFrameButton3);
			    log.info("subMenuFrameButton3:"+subMenuFrameButton3);
				JavascriptExecutor js = (JavascriptExecutor) driver;
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//*[text()='"+xpth3+"']")));
				}
				//((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//*[text()='k']")));
				System.out.println("success");
				log.info("success");
				}
				catch(Exception e)
				{
					Status="FAIL";
					System.out.println(e.getLocalizedMessage());
					log.debug(e.toString());
				}
				return Status;
			}
		   public String ClickManualActionDQ(String testData, WebDriver driver) throws InterruptedException {
			   String Status="PASS";
			   driver.switchTo().frame("TabFrame");
				Modules md = new Modules();
				JavascriptExecutor jsExecutor = (JavascriptExecutor)driver;
				String currentFrame = (String) jsExecutor.executeScript("return self.name");
				System.out.println("CurrentFrame:"+currentFrame); 
				WebDriverWait wait= new WebDriverWait(driver,30);
			

				String[] Data = testData.split("\\|");
				for(String dat:Data)
				{
				System.out.println("data from array"+dat);
				log.info("data from array"+dat);
				}
				String tabFrameMenuButton=  Data[0];//WiresMenu
				String xpth1 = Data[1];//
				System.out.println("submenu"+xpth1);
				log.info("submenu"+xpth1);
				xpth1.trim();
				String xpth2 = Data[2];
				xpth2.trim();
				System.out.println("queuename"+xpth2);
				log.info("queuename"+xpth2);
				String xpth3 = Data[3];
				xpth3.trim();
				System.out.println("queue"+xpth3);
				log.info("queue"+xpth3);
				try {
				WebElement tabFrameButton = driver.findElement(By.xpath("//*[text()='"+tabFrameMenuButton+"']"));
				System.out.println(tabFrameButton);
				log.info(tabFrameButton);
				tabFrameButton.click();
				String currentFrame2 = (String) jsExecutor.executeScript("return self.name");
				System.out.println("currentFrame2");
				System.out.println("CurrentFrame:"+currentFrame2); 
				log.info("CurrentFrame:"+currentFrame2);
				driver.switchTo().defaultContent();
				//JavascriptExecutor jsExecutor = (JavascriptExecutor)driver;
				String currentFrame3 = (String) jsExecutor.executeScript("return self.name");
				System.out.println("CurrentFrame:"+currentFrame3); 
				log.info("CurrentFrame:"+currentFrame3); 
				System.out.println(driver.getTitle());
				driver.switchTo().frame("MenuFrame");//MenuFrame
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()='"+xpth1+"']")));
			
				WebElement subMenuFrameButton = driver.findElement(By.xpath("//*[text()='"+xpth1+"']"));//WiresSubmenu
				System.out.println("subMenuFrameButton:"+subMenuFrameButton);
				log.info("subMenuFrameButton:"+subMenuFrameButton);
				
				subMenuFrameButton.click();
				
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//*[text()='"+xpth1+"']")));
				//Thread.sleep(8000);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()='"+xpth2+"']")));
				WebElement subMenuFrameButton2 = driver.findElement(By.xpath("//*[text()='"+xpth2+"']"));//WarehouseSubmenu
				System.out.println("subMenuFrameButton2:"+subMenuFrameButton2);
				log.info("subMenuFrameButton:"+subMenuFrameButton);
				//subMenuFrameButton2.click();
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//*[text()='"+xpth2+"']")));
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()='"+xpth3+"']")));
				//Thread.sleep(8000);
				WebElement subMenuFrameButton3 = driver.findElement(By.xpath("//*[text()='"+xpth3+"']"));//warehoused Production
				System.out.println("subMenuFrameButton3:"+subMenuFrameButton3);
			    log.info("subMenuFrameButton3:"+subMenuFrameButton3);
				JavascriptExecutor js = (JavascriptExecutor) driver;
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//*[text()='"+xpth3+"']")));
				//((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//*[text()='k']")));
				System.out.println("success");
				log.info("success");
				}
				catch(Exception e)
				{
					Status="FAIL";
					System.out.println(e.getLocalizedMessage());
					log.debug(e.toString());
				}
				return Status;
			}



		public String PSHCCC2API(String testData) {
			// TODO Auto-generated method stub
			if (testData != null) {
				log.info("cc2 call keyword");
				String[] Data = testData.split("\\|");
				String Type=  Data[0];
				String Valdate = Data[1];
				String Dbtraccno= Data[2];
				String Dbtrtransitno= Data[3];
				String Panno= Data[4];
				String SettType= Data[5];
				String debitcurrency= Data[6];
				String creditcurrency= Data[7];
				String chargetype= Data[8];
				System.out.println("Data from testdata:"+Type+Valdate + Dbtraccno + Dbtrtransitno + Panno + SettType+debitcurrency+creditcurrency+chargetype);
				log.info("Data from testdata:"+Type+Valdate + Dbtraccno + Dbtrtransitno + Panno + SettType);
				PSHCC2Call bet=new PSHCC2Call();
				try {
					status = bet.getresponse(Type,Valdate,Dbtraccno,Dbtrtransitno,Panno,SettType,debitcurrency,creditcurrency,chargetype);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					log.debug(e.toString());
				}
			}
			return status;
		}




public String SearchAccWarehouseGow(String testData, WebDriver driver) throws Exception {
log.info("SearchAccWarehouse -------");
String makerAction="Mark as Completed";
String makerRemark="to be completed";
String AccStatus="Pending UMM Generation";
   String Status="PASS";
if (testData != null) {
String[] Data = testData.split("\\|");
AccStatus =Data[2];
makerAction =  Data[0];
makerRemark = Data[1];
}
String TxWorkItemID = Fileutil.GetFilevalue("VAR_PSHTXNWORKITEMID");
System.out.println("TxWorkItemID:"+TxWorkItemID);
String expAlertmesg="NA";
try
{

driver.switchTo().defaultContent();
driver.switchTo().frame("SearchFrame");
DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
Date myDate = new Date(System.currentTimeMillis());
String From = dateFormat.format(myDate);
Calendar cal = Calendar.getInstance();
cal.setTime(myDate);
cal.add(Calendar.DATE, 4);
String To = dateFormat.format(cal.getTime());
Thread.sleep(5000);
String toYear = new SimpleDateFormat("yyyy").format(cal.getTime());
String fromYear = new SimpleDateFormat("yyyy").format(myDate);
String toMonth = new SimpleDateFormat("MM").format(cal.getTime());
String fromMonth = new SimpleDateFormat("MM").format(myDate);
int toyear = Integer.parseInt(toYear);
int fromyear = Integer.parseInt(fromYear);
int tomonth = Integer.parseInt(toMonth);
int frommonth = Integer.parseInt(fromMonth);
System.out.println("DateFrom--->" + From);
log.info("DateFrom--->" + From);
System.out.println("DateTo--->" + To);
log.info("DateFrom--->" + To);
System.out.println(toYear+" "+ fromyear+" "+ tomonth+" "+ frommonth);
log.info(toYear+" "+ fromyear+" "+ tomonth+" "+ frommonth);
int tomonthforjust = tomonth-1;
String str1 = Integer.toString(tomonthforjust);
driver.findElement(By.xpath("//td[@id='ALeCLoT2']/img")).click();

WebElement month = driver.findElement(By.xpath("//select[@class='ui-datepicker-month']"));
try {
month.click();
Select sel2 = new Select(month);
 System.out.println(str1);
sel2.selectByValue(str1);
//month.click();
//sel.selectByValue(TestData);
System.out.println("entered 2");
Thread.sleep(5000);
}
catch (InterruptedException e)
{
// TODO Auto-generated catch block
e.printStackTrace();
log.debug(e.toString());
}

WebElement year = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']"));
try {
year.click();
Select sel = new Select(year);
 System.out.println("entered 1");
sel.selectByVisibleText(toYear);
//year.click();
//sel.selectByValue(TestData);
System.out.println("entered 2");
Thread.sleep(5000);
}
catch (InterruptedException e) {
// TODO Auto-generated catch block
e.printStackTrace();
}

//Date
String Todate = new SimpleDateFormat("dd").format(cal.getTime());

char x = Todate.charAt(0);
char y = '0';
if(x==y)
{
Todate = Todate.substring(1);
System.out.println(Todate);
}
System.out.println("DateFrom--->" + From);
log.info("DateFrom--->" + From+"DateTo--->" + To+"Date To:" +Todate);
System.out.println("DateTo--->" + To);
System.out.println("Date To:" +Todate);
Thread.sleep(3000);

List<WebElement> allDates=driver.findElements(By.xpath("//table[@class='ui-datepicker-calendar']//td"));

for(WebElement ele:allDates)
{

String date=ele.getText();
System.out.println(date);
log.info(date);
if(date.equalsIgnoreCase(Todate))
{
ele.click();
break;
}

}
driver.findElement(By.id("paymentworkitemid")).sendKeys(TxWorkItemID);
((JavascriptExecutor) driver).executeScript("arguments[0].value='"+TxWorkItemID+"';", driver.findElement(By.id("paymentworkitemid")));
driver.findElement(By.xpath("//span[text()='Search']")).click();
driver.findElement(By.id("paymentworkitemid")).clear();
driver.switchTo().defaultContent();
driver.switchTo().frame("SearchFrame").switchTo().frame("iframe1");
Thread.sleep(5000);

WebElement PaymentUMMStatus = driver.findElement(
       By.xpath("//*[@id='WorkItemsList']/div/div/div[2]/div/div/div[2]//table/tbody/tr[1]/td[11]"));

String actualPaymentUMMStatus = "";
Thread.sleep(5000);
actualPaymentUMMStatus = PaymentUMMStatus.getText();
Thread.sleep(5000);
if (AccStatus.equals(actualPaymentUMMStatus))
{
System.out.println("Actual and Expected Statuses matched: " + actualPaymentUMMStatus);
log.info("Actual and Expected Statuses matched: " + actualPaymentUMMStatus);
}
else
{
System.out.println("Actual and Expected Statuses mis-matched. Actual payment status was: "
       + actualPaymentUMMStatus);
log.info("Actual and Expected Statuses mis-matched. Actual payment status was: "
       + actualPaymentUMMStatus);
Status="FAIL";
throw new Exception("Status Mismatch AccWarehouse/Gow with expected status");
}
if(!makerAction.equalsIgnoreCase("NA"))
{
System.out.println("Changing value to present date");
DBConnector DBConnector2=new DBConnector();
int i=DBConnector2.Update_PSH_Dates("PSH|update txn_proc_opn set value_date =? where workitemid IN (?)|VAR_CURRDATE|VAR_PSHTXNWORKITEMID");
if(i>=1)
{
log.info("Value Date updated to current date for the txn");
System.out.println("Value Date updated to current date for the txn");

}
driver.findElement(By.xpath("//*[@id='ext-gen22']/div[1]/table/tbody/tr[1]/td[1]/div/div")).click();
log.info("Check Box For The WKID Selected" + TxWorkItemID);
System.out.println("Check Box For The WKID Selected" + TxWorkItemID);
if (makerAction.equalsIgnoreCase("Mark as Ack Received"))
{
log.info("Click Mark as Ack Received");
driver.findElement(By.xpath("//*[text()='Mark as Ack Received']")).click();
}
if (makerAction.equalsIgnoreCase("Authorize"))
{
log.info("Click on Authorize");
driver.findElement(By.xpath("//*[text()='Authorize']")).click();
}
else if (makerAction.equalsIgnoreCase("Mark as Completed"))
{
log.info("Click Mark as Completed");
System.out.println("Click Mark as Completed");
driver.findElement(By.xpath("//*[text()='Mark as Completed']")).click();
}
else
{
log.info("Click Submit Buttom");
System.out.println("Click Submit Button");
driver.findElement(By.xpath("//*[text()='" + makerAction + "']")).click();
}
Thread.sleep(10000);
Set<String> Windows = driver.getWindowHandles();
for (String win : Windows)
{
driver.switchTo().window(win);
log.info(driver.getTitle());
if (!"Intellect Suite - Enterprise Platform for Boundaryless Banking from Intellect Design Arena-EN"
       .equalsIgnoreCase(driver.getTitle()) && !"BPS".equalsIgnoreCase(driver.getTitle()))
{
break;
}
}
driver.switchTo().frame("IwMiddleFrame");

if (!makerRemark.equals(""))
{
((JavascriptExecutor) driver).executeScript("arguments[0].value='"+makerRemark+"';", driver.findElement(By.xpath("//*[@name='Text Area']")));
driver.findElement(By.xpath("//*[@name='Text Area']")).sendKeys(makerRemark);
System.out.println("Sending maker remarks");
driver.switchTo().defaultContent();
Thread.sleep(5000);
driver.switchTo().frame("IwBottomFrame");
((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath(("//*[@id='bottomBtnsDivId']/table/tbody/tr/td[4]/a"))));
driver.findElement(By.xpath("//*[@id='bottomBtnsDivId']/table/tbody/tr/td[4]/a")).click();
Alert alert = driver.switchTo().alert();
String alerttext = alert.getText();
System.out.println("Alert text: " + alerttext);
if (alerttext.contains("Bulk Action Successful"))
System.out.println("Alert message matched!: " + alerttext);
log.info("Alert message matched!: " + alerttext);
alert.accept();
}
else

{
driver.findElement(By.xpath("//*[@name='Text Area']")).sendKeys(makerRemark);
driver.switchTo().defaultContent();
driver.switchTo().frame("IwBottomFrame");
driver.findElement(By.xpath("//*[@id='bottomBtnsDivId']/table/tbody/tr/td[4]/a")).click();
Alert alert = driver.switchTo().alert();
String alerttext = alert.getText();
System.out.println("Alert text: " + alerttext);
if (alerttext.contains(expAlertmesg))
System.out.println("Alert message matched!: " + alerttext);
log.info("Alert message matched!: " + alerttext);
alert.accept();
}

}

}
catch (Exception e)
{
Status="FAIL";
e.printStackTrace();
System.out.println("Exception caught in SearchAccWarehouse:" + e.toString());
Status="FAIL";
throw e;
}

return Status;
}

		public String GowNotification(String testData, WebDriver driver) {
			log.info("SearchGowNotification -------");

		    String Status="PASS"; 
			if (testData != null) {
		
				String GowStatus =testData;
				WiresAckMT103MT199 w=new WiresAckMT103MT199();
				try {
					w.dbconnect();
					w.GOW1ACK(GowStatus);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					log.debug(e.toString());
				}
			
			}
			String TxWorkItemID = Fileutil.GetFilevalue("VAR_PSHTXNWORKITEMID");
			System.out.println("TxWorkItemID:"+TxWorkItemID);

			
			return Status;
		
		}

		public String PSHCCC2BVMAPI(String TestCaseNo, String TestData) {
			// TODO Auto-generated method stub
			CC2BVMOperations bvm=new CC2BVMOperations();
			if (TestData != null)
			{
				try {
					status = bvm.PSHCCC2BVMAPI(TestCaseNo,TestData);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return status;
		}
		public void SearchClientID(String XPATH,String testData, WebDriver driver) throws Exception {
			StringBuffer returnstring=new StringBuffer("");
			try {
	
			
			Thread.sleep(5000);
	
			driver.findElement(By.xpath(XPATH)).click();
			Thread.sleep(10000);
			Set<String> Windows = driver.getWindowHandles();
			for (String win : Windows)
			{
				driver.switchTo().window(win);
				log.info(driver.getTitle());
				if (!"Intellect Suite - Enterprise Platform for Boundaryless Banking from Intellect Design Arena-EN"
				        .equalsIgnoreCase(driver.getTitle()) && !"BPS".equalsIgnoreCase(driver.getTitle()))
				{
					break;
				}
			}
			String TestData[] = testData.split("\\|");
			
			Thread.sleep(5000);
			driver.switchTo().frame("IFrameWindow");
			driver.switchTo().frame("IwSearchPickList");
			((JavascriptExecutor) driver).executeScript("arguments[0].value='"+TestData[0]+"';", driver.findElement(By.xpath("//*[@name='SearchParameter1']")));
			driver.findElement(By.xpath("//*[@name='SearchParameter1']")).sendKeys(TestData[0]);
			System.out.println("sending client ID");
			((JavascriptExecutor) driver).executeScript("arguments[0].value='"+TestData[1]+"';", driver.findElement(By.xpath("//*[@name='SearchParameter2']")));
			driver.findElement(By.xpath("//*[@name='SearchParameter1']")).sendKeys(TestData[1]);
			System.out.println("sending client Name");
					
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath(("//*[@id='submitSearch']"))));
			System.out.println("clicking search1");
			driver.findElement(By.xpath("//*[@id='submitSearch']")).click();
			System.out.println("clicking search2");
			Thread.sleep(15000);
			System.out.println("shifting to Iwpicklist");
			driver.switchTo().parentFrame().switchTo().frame("IwPickList");
			/*System.out.println("going to click on number");
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath(("//a[@class='ST168']"))));
			System.out.println("clicked on number");*/
			System.out.println("searching to click on number");
			if(!driver.findElements(By.xpath("//a[@class='ST168']")).isEmpty()){
		        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath(("//a[@class='ST168']"))));
				System.out.println("clicked on number");
		    }else{
		        
		    	System.out.println("Client absent");
		    	driver.close();
		    			    }
			Robot rb = new Robot();
			rb.keyPress(KeyEvent.VK_TAB);
			rb.keyRelease(KeyEvent.VK_TAB);
			
			focusOnSingleWindow(driver);
			}
			catch (InterruptedException e) 
			{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	
		}
		public void focusOnSingleWindow(WebDriver driver)
		{
			log.info("----Entering in focusOnSingleWindow----");
			try
			{

				// Thread.sleep(10000);
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

				Set<String> window = driver.getWindowHandles();
				log.info("SIZE=" + window.size());
				Iterator<String> itr = window.iterator();
				while (itr.hasNext())
				{
					log.info("I am here 1");
					driver.switchTo().window(itr.next());
				}

				if ("BPS".equalsIgnoreCase(driver.getTitle()))
				{
					driver.manage().window().maximize();
					driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
					// Thread.sleep(15000);
				}

			}
			catch (Exception ex)
			{
				log.fatal("Exception in focusOnSingleWindow " + ex.toString());

			}
			log.info("----leaving from focusOnSingleWindow----");
			//return c;
		}

		

		public String PSHBTRBVMAPI(String TestCaseNo, String TestData) {
			// TODO Auto-generated method stub
			String Result_Status = null;
			BTRBVMOperations bvm=new BTRBVMOperations();
			String Data[] = TestData.split("|");
			if (TestData != null)
			{
				try {
					status = bvm.PSHCBTRBVMAPI(TestCaseNo,TestData);
					if(status.contains("\"TxSts\":\"TXVA\""))
					{
						Result_Status="PASS";
					}
					else if(status.contains("\"TxSts\":\"ACSP\""))
					{
						Result_Status="PASS";
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Result_Status="FAIL";
				}
			}
			return Result_Status;
		}
		public String PSHBTRBVMAPI2(String TestCaseNo, String TestData) {
			// TODO Auto-generated method stub
			String Result_Status = null;
			BTRBVMOperations bvm=new BTRBVMOperations();
			if (TestData != null)
			{
				String[] Data = TestData.split("\\|");
				String channelID=  Data[0];
				String Flow_ID = Data[1];
				String ExceptionCode= Data[2];
				String ExceptionDesc= Data[3];
				try {
					status = bvm.PSHCBTRBVMAPI(TestCaseNo,TestData);
					if(status.contains(ExceptionCode)&&status.contains(ExceptionDesc))
					{
						Result_Status="PASS";
						System.out.println("Output has Exception code :"+ExceptionCode);
						log.info("Output has Exception code :"+ExceptionCode);
						System.out.println("Output has Exception desc:" +ExceptionDesc);
						log.info("Output has Exception desc:" +ExceptionDesc);
					}
					else
					{
						Result_Status="FAIL";
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					log.debug(e.toString());
					Result_Status="FAIL";
				}
			}
			return Result_Status;
		}
	

		public void rapidManualAction(WebDriver driver,String testData) throws InterruptedException {
			String parentWinHandle=driver.getWindowHandle();
			System.out.println(driver.getTitle());
		        Set<String> winHandles = driver.getWindowHandles();
		        for(String handle: winHandles){
		        	
		            if(!handle.equals(parentWinHandle)){
		            driver.switchTo().window(handle);
		            
		            if(driver.getTitle().equals("Label Entry Window"))
		            break;
		        	System.out.println(driver.getTitle());
		            }
		            else
		            {
		            	System.out.println("Widow not found");
		            }}
		        
		        driver.switchTo().defaultContent();
		        Thread.sleep(6000);
		           
		}


		public WebDriver directLogin(WebDriver driver) throws Exception {
			System.setProperty("webdriver.ie.driver",System.getProperty("user.dir")+"\\src\\test\\resources\\Driver\\IEDriverServer.exe");
			//WebDriverManager.iedriver().setup();
			DesiredCapabilities capability = DesiredCapabilities.internetExplorer();
			capability.setCapability("pageLoadStrategy", "eager");
			
			//commented
			//capability.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION,true); 
			capability.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL,false);
			capability.setCapability("nativeEvents", false); 
			//capability.setCapability(InternetExplorerDriver.ENABLE_ELEMENT_CACHE_CLEANUP, true);
			capability.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
			driver = new InternetExplorerDriver(capability);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
			//driver.get("http://10.10.8.62:10062/BPS/");
			String PSHURL = PropertyUtils.readProperty("PSH_URL");
			driver.get(PSHURL);
			

			driver.manage().window().maximize();
		
			
			
		

			return driver;
		}
	
}

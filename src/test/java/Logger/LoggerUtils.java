package Logger;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;
import java.util.Calendar;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class LoggerUtils extends SecurityManager{
	private static ConcurrentHashMap<String, org.apache.log4j.Logger> loggerMap = new ConcurrentHashMap<String, org.apache.log4j.Logger>();
    public static Level level=Level.ALL;
    private final Logger logger = Logger.getLogger(LoggerUtils.class
            .getName());
    private FileHandler fh = null;
    
    public LoggerUtils()
    {
    	
    	        //just to make our log file nicer :)
    	       /* SimpleDateFormat format = new SimpleDateFormat("M-d_HHmmss");
    	        try {
    	            fh = new FileHandler("D:\\CBX_Automation\\Log\\log4j-application.log");
    	               // + format.format(Calendar.getInstance().getTime()) + ".log");
    	        } catch (Exception e) {
    	            e.printStackTrace();
    	        }

    	        fh.setFormatter(new SimpleFormatter());
    	        */
    	        String log4jConfigFile = System.getProperty("user.dir")
    	                +"\\src\\test\\resources\\"+ "log4j.properties";
    	        PropertyConfigurator.configure(log4jConfigFile);
    	 
    }
  
        public static Logger getLogger()
        {
            String className = new LoggerUtils().getClassName();
            Logger logger = Logger.getLogger(className);
          
            return logger;
        }

        private String getClassName()
        {
            return getClassContext()[2].getName();
        }
    }

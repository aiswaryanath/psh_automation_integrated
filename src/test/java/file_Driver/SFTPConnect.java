package file_Driver;

import java.util.Vector;

//import org.CIBC_Automation.driver.CIBC_XPATH_Constants;
//import org.CIBC_Automation.functionlib.Utilities;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import Utilities.PropertyUtils;
import rebulkandResponse.CIBC_XPATH_Constants;

public class SFTPConnect
{
	public static void file_DownloadToLocal(String FileName) throws Exception
	{

		//String hostname = "10.10.8.62";
	//	String login = "SIR18360";
	//	String password = "PSHRetro@Joyo123";
	//	String directory = "/usr1/SIR18360/GTB_HOME/IPSH_HOME/filearea/outgoing/EFT_Mainframe/ACH/outgoing";

		
		
		String hostname =  PropertyUtils.readProperty("PSHAPP");
		String login = PropertyUtils.readProperty("PSHAPP_username");
		String password =  PropertyUtils.readProperty("PSHAPP_Password");
		String directory = PropertyUtils.readProperty("strACHStubPathFROMPSH");
		// String directory = "the directory";
		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		JSch ssh = new JSch();
		String envdetail=PropertyUtils.readProperty("ENV_DETAIL");
		if(envdetail.contains("CLOUD"))
		{	
		String ppk=	PropertyUtils.readProperty("ENV_PPK");
		String privateKey = System.getProperty("user.dir")+"\\src\\test\\resources\\ppk\\"+ppk;
		//String privateKey = "D:\\CBX_Automation\\ppk\\"+ppk;
		ssh.addIdentity(privateKey);
		}
		Session session;
		try
		{
			// GETTING sESSION FROM SERVER
			session = ssh.getSession(login, hostname, 22);
			session.setConfig(config);
			session.setConfig("PreferredAuthentications", "publickey,keyboard-interactive,password");
			session.setPassword(password);
			session.connect();

			// CONNECTED TO CHANNEL
			ChannelSftp channel = (ChannelSftp) session.openChannel("sftp");
			channel.connect();

			if (channel.isConnected())
			{
				System.out.println("Connected!!");
				Channel sftp = channel;
				((ChannelSftp) sftp).cd(directory);
				try
				{
					((ChannelSftp) sftp).cd(directory);
					Vector<ChannelSftp.LsEntry> list = ((ChannelSftp) sftp).ls(directory);
					for (int i = 0; i < list.size(); i++)
					{
						if (!(list.get(i).getFilename().equalsIgnoreCase("..")
						        || (list.get(i).getFilename().equalsIgnoreCase("."))))
						{
							if (list.get(i).getFilename().equalsIgnoreCase(FileName))
							{
								System.out.println(list.get(i).getFilename());
								System.out.println(directory + "/" + list.get(i).getFilename());
								String filepath=System.getProperty("user.dir")+"/LOCAL_PAYMENT_FILE/ACH/";
								//channel.get(directory + "/" + list.get(i).getFilename(),"D:/CBX_Automation/LOCAL_PAYMENT_FILE/ACH/");
								channel.get(directory + "/" + list.get(i).getFilename(),filepath);
							}
						}

					}

				}
				catch (Exception e)
				{
					System.out.println(e.toString());
				}
			}
			else
			{
				System.out.println("Not Connected To Server!!");
			}
			channel.disconnect();
			session.disconnect();
		}
		catch (JSchException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void file_DownloadToLocalEFT(String FileName) throws Exception
	{
	//	String hostname = "10.10.8.62";
	//	String login = "SIR18360";
	//	String password = "PSHRetro@Joyo123";
	//	String directory = "/usr1/SIR18360/GTB_HOME/IPSH_HOME/filearea/outgoing/EFT_Mainframe/EFT/outgoing";
		
		
		String hostname =  PropertyUtils.readProperty("PSHAPP");
		String login = PropertyUtils.readProperty("PSHAPP_username");
		String password =  PropertyUtils.readProperty("PSHAPP_Password");
		String directory = PropertyUtils.readProperty("strEFTStubPathFROMPSH");
		// String directory = "the directory";
		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		JSch ssh = new JSch();
		String envdetail=PropertyUtils.readProperty("ENV_DETAIL");
		if(envdetail.contains("CLOUD"))
		{	
		String ppk=	PropertyUtils.readProperty("ENV_PPK");
		String privateKey = System.getProperty("user.dir")+"\\src\\test\\resources\\ppk\\"+ppk;
	//	String privateKey = "D:\\CBX_Automation\\ppk\\"+ppk;
		ssh.addIdentity(privateKey);
		}
		Session session;
		try
		{
			// GETTING sESSION FROM SERVER
			session = ssh.getSession(login, hostname, 22);
			session.setConfig(config);
			session.setConfig("PreferredAuthentications", "publickey,keyboard-interactive,password");
			session.setPassword(password);
			session.connect();

			// CONNECTED TO CHANNEL
			ChannelSftp channel = (ChannelSftp) session.openChannel("sftp");
			channel.connect();

			if (channel.isConnected())
			{
				System.out.println("Connected!!");
				Channel sftp = channel;
				((ChannelSftp) sftp).cd(directory);
				try
				{
					((ChannelSftp) sftp).cd(directory);
					Vector<ChannelSftp.LsEntry> list = ((ChannelSftp) sftp).ls(directory);
					for (int i = 0; i < list.size(); i++)
					{
						if (!(list.get(i).getFilename().equalsIgnoreCase("..")
						        || (list.get(i).getFilename().equalsIgnoreCase("."))))
						{
							if (list.get(i).getFilename().equalsIgnoreCase(FileName))
							{
								System.out.println(list.get(i).getFilename());
								System.out.println(directory + "/" + list.get(i).getFilename());
								String filepath=System.getProperty("user.dir")+"/LOCAL_PAYMENT_FILE/EFT/";
								//channel.get(directory + "/" + list.get(i).getFilename(), "D:/CBX_Automation/LOCAL_PAYMENT_FILE/EFT/");
								channel.get(directory + "/" + list.get(i).getFilename(),filepath);
							}
						}

					}

				}
				catch (Exception e)
				{
					System.out.println(e.toString());
				}
			}
			else
			{
				System.out.println("Not Connected To Server!!");
			}
			channel.disconnect();
			session.disconnect();
		}
		catch (JSchException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void file_DownloadToLocalCNR(String FileName) throws Exception
	{
		//String hostname = "10.10.8.62";
		//String login = "SIR18360";
		//String password = "PSHRetro@Joyo123";
		//String directory = "/usr1/SIR18360/GTB_HOME/IPSH_HOME/filearea/outgoing/CNR/print/email/fax/courier/Pain001";
		String hostname =  PropertyUtils.readProperty("PSHAPP");
		String login = PropertyUtils.readProperty("PSHAPP_username");
		String password =  PropertyUtils.readProperty("PSHAPP_Password");
		String directory = PropertyUtils.readProperty("strCHKStubPathFROMPSH");
		// String directory = "the directory";
		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		JSch ssh = new JSch();
		String envdetail=PropertyUtils.readProperty("ENV_DETAIL");
		if(envdetail.contains("CLOUD"))
		{	
		String ppk=	PropertyUtils.readProperty("ENV_PPK");
		//String privateKey = "D:\\CBX_Automation\\ppk\\"+ppk;
		String privateKey = System.getProperty("user.dir")+"\\src\\test\\resources\\ppk\\"+ppk;
		ssh.addIdentity(privateKey);
		}
		Session session;
		try
		{
			// GETTING sESSION FROM SERVER
			session = ssh.getSession(login, hostname, 22);
			session.setConfig(config);
			session.setConfig("PreferredAuthentications", "publickey,keyboard-interactive,password");
			session.setPassword(password);
			session.connect();

			// CONNECTED TO CHANNEL
			ChannelSftp channel = (ChannelSftp) session.openChannel("sftp");
			channel.connect();

			if (channel.isConnected())
			{
				System.out.println("Connected!!");
				Channel sftp = channel;
				((ChannelSftp) sftp).cd(directory);
				try
				{
					((ChannelSftp) sftp).cd(directory);
					Vector<ChannelSftp.LsEntry> list = ((ChannelSftp) sftp).ls(directory);
					for (int i = 0; i < list.size(); i++)
					{
						if (!(list.get(i).getFilename().equalsIgnoreCase("..")
						        || (list.get(i).getFilename().equalsIgnoreCase("."))))
						{
							if (list.get(i).getFilename().equalsIgnoreCase(FileName))
							{
								System.out.println(list.get(i).getFilename());
								System.out.println(directory + "/" + list.get(i).getFilename());
								String filepath=System.getProperty("user.dir")+"/LOCAL_PAYMENT_FILE/CNR/Pain001";
								//channel.get(directory + "/" + list.get(i).getFilename(),"D:/CBX_Automation/LOCAL_PAYMENT_FILE/CNR/Pain001");
								channel.get(directory + "/" + list.get(i).getFilename(),filepath);
							}
						}

					}

				}
				catch (Exception e)
				{
					System.out.println(e.toString());
				}
			}
			else
			{
				System.out.println("Not Connected To Server!!");
			}
			channel.disconnect();
			session.disconnect();
		}
		catch (JSchException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void SFTPfile_DownloadToLocal(String ServerPath, String LocalPath, String FileName) throws Exception
	{
		// String hostname = "10.10.8.62";
		// String login = "SIR18360";
		// String password = "PSHRetro@Joyo123";
		String hostname = PropertyUtils.readProperty("PSHAPP");
		String login = PropertyUtils.readProperty("PSHAPP_username");
		String password = PropertyUtils.readProperty("PSHAPP_Password");

		// String directory =
		// "/usr1/SIR18360/GTB_HOME/IPSH_HOME/filearea/outgoing/CNR/print/email/fax/courier/Pain001";
		String directory = ServerPath;

		// String directory = "the directory";
		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		JSch ssh = new JSch();
		String envdetail=PropertyUtils.readProperty("ENV_DETAIL");
		if(envdetail.contains("CLOUD"))
		{	
		String ppk=	PropertyUtils.readProperty("ENV_PPK");
		//String privateKey = "D:\\CBX_Automation\\ppk\\"+ppk;
		String privateKey = System.getProperty("user.dir")+"\\src\\test\\resources\\ppk\\"+ppk;
		ssh.addIdentity(privateKey);
		}
		Session session;
		try
		{
			// GETTING sESSION FROM SERVER
			session = ssh.getSession(login, hostname, 22);
			session.setConfig(config);
			session.setConfig("PreferredAuthentications", "publickey,keyboard-interactive,password");
			session.setPassword(password);
			session.connect();

			// CONNECTED TO CHANNEL
			ChannelSftp channel = (ChannelSftp) session.openChannel("sftp");
			channel.connect();

			if (channel.isConnected())
			{
				System.out.println("Connected!!");
				Channel sftp = channel;
				((ChannelSftp) sftp).cd(directory);
				try
				{
					((ChannelSftp) sftp).cd(directory);
					Vector<ChannelSftp.LsEntry> list = ((ChannelSftp) sftp).ls(directory);
					for (int i = 0; i < list.size(); i++)
					{
						if (!(list.get(i).getFilename().equalsIgnoreCase("..")
						        || (list.get(i).getFilename().equalsIgnoreCase("."))))
						{
							if (list.get(i).getFilename().equalsIgnoreCase(FileName))
							{
								System.out.println(list.get(i).getFilename());
								System.out.println(directory + "/" + list.get(i).getFilename());
								channel.get(directory + "/" + list.get(i).getFilename(), LocalPath);
							}
						}

					}

				}
				catch (Exception e)
				{
					System.out.println(e.toString());
				}
			}
			else
			{
				System.out.println("Not Connected To Server!!");
			}
			channel.disconnect();
			session.disconnect();
		}
		catch (JSchException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws Exception
	{
		//file_DownloadToLocal("");
		String Server_PathACH = PropertyUtils.readProperty(CIBC_XPATH_Constants.strACHStubPathFROMPSH);
		
		SFTPfile_DownloadToLocal(Server_PathACH, "D:/CBX_Automation/LOCAL_PAYMENT_FILE/ACH/", "20000001012020527486.0094");
	}

}

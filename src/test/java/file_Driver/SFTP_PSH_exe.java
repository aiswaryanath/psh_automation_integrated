package file_Driver;

import org.apache.log4j.Logger;
import Logger.LoggerUtils;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.apache.commons.io.filefilter.FileFileFilter;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;

import Utilities.PropertyUtils;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;

public class SFTP_PSH_exe {
	
	public static ArrayList<String>	_ob					= null;
	public static ArrayList<String>	_ob_filename		= null;
	public static ArrayList<String>	_ob_filename_seize	= new ArrayList<String>();
	static Logger log = LoggerUtils.getLogger();
	
public static void file_DownloadToLocal() throws Exception{
		String hostname = PropertyUtils.readProperty("FGT_HOSTNAME");
		String login = PropertyUtils.readProperty("FGT_LOGIN");
		String password = PropertyUtils.readProperty("FGT_PWD");
		String directory = PropertyUtils.readProperty("FGT_DIRECTORY");
		
		// String directory = "the directory";
		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		JSch ssh = new JSch();
		Session session;
		try {
			//GETTING sESSION FROM SERVER
			session = ssh.getSession(login, hostname, 22);
			session.setConfig(config);
			session.setConfig("PreferredAuthentications","publickey,keyboard-interactive,password");
			session.setPassword(password);
			session.connect();
			
			//CONNECTED TO CHANNEL
			ChannelSftp channel = (ChannelSftp) session.openChannel("sftp");
			channel.connect();
			
			if (channel.isConnected()) {
				System.out.println("Connected!!");
				log.info("FGT SFTP Connected!!");
				Channel sftp = (ChannelSftp) channel;
				((ChannelSftp) sftp).cd(directory);
				try{
					((ChannelSftp) sftp).cd(directory);
					Vector<ChannelSftp.LsEntry> list = ((ChannelSftp) sftp).ls(directory);
					for (int i = 0; i < list.size(); i++) {
						if(!(list.get(i).getFilename().equalsIgnoreCase("..") || (list.get(i).getFilename().equalsIgnoreCase(".")))){
							Vector<ChannelSftp.LsEntry> listA = ((ChannelSftp) sftp).ls(directory+""+list.get(i).getFilename());
							for(int j=0;j<listA.size();j++){
								if(!(listA.get(j).getFilename().equalsIgnoreCase("..") || (listA.get(j).getFilename().equalsIgnoreCase(".")))){
									  Vector<ChannelSftp.LsEntry> listB = ((ChannelSftp) sftp).ls(directory+""+list.get(i).getFilename()+"/"+listA.get(j).getFilename());
									   for(int k=0;k<listB.size();k++){
										   if(!(listB.get(k).getFilename().equalsIgnoreCase("..") || (listB.get(k).getFilename().equalsIgnoreCase(".")))){
											   Vector<ChannelSftp.LsEntry> listC = ((ChannelSftp) sftp).ls(directory+""+list.get(i).getFilename()+"/"+listA.get(j).getFilename()+"/"+listB.get(k).getFilename());
											     for(int L=0;L<listC.size();L++){
											    	 if(!(listC.get(L).getFilename().equalsIgnoreCase("..") || (listC.get(L).getFilename().equalsIgnoreCase(".")))){
											    		if (listB.get(k).getFilename().equalsIgnoreCase("SCA")) {
											    			String filepath=System.getProperty("user.dir")+"//src/test/resources/LOCAL_PAYMENT_FILE/SCA/";
											    			 channel.get(directory+""+list.get(i).getFilename()+"/"+listA.get(j).getFilename()+"/"+listB.get(k).getFilename()+"/"+listC.get(L).getFilename(),filepath);
														 }
											    		if (listB.get(k).getFilename().equalsIgnoreCase("NDM")) {
											    			String filepath=System.getProperty("user.dir")+"//src/test/resources/LOCAL_PAYMENT_FILE/NDM/";
											    			 channel.get(directory+""+list.get(i).getFilename()+"/"+listA.get(j).getFilename()+"/"+listB.get(k).getFilename()+"/"+listC.get(L).getFilename(),filepath);
														}
											    		if (listB.get(k).getFilename().equalsIgnoreCase("FTS")) {
											    			String filepath=System.getProperty("user.dir")+"//src/test/resources/LOCAL_PAYMENT_FILE/FTS/";
											    			 channel.get(directory+""+list.get(i).getFilename()+"/"+listA.get(j).getFilename()+"/"+listB.get(k).getFilename()+"/"+listC.get(L).getFilename(), filepath);
											    				
														}
											    		//Added FTT
											    		if (listB.get(k).getFilename().equalsIgnoreCase("FTT")) {
											    			System.out.println(directory+""+list.get(i).getFilename()+"/"+listA.get(j).getFilename()+"/"+listB.get(k).getFilename()+"/"+listC.get(L).getFilename());
											    			log.info(directory+""+list.get(i).getFilename()+"/"+listA.get(j).getFilename()+"/"+listB.get(k).getFilename()+"/"+listC.get(L).getFilename());
											    			String filepath=System.getProperty("user.dir")+"//src/test/resources/LOCAL_PAYMENT_FILE/FTT/";
											    			 channel.get(directory+""+list.get(i).getFilename()+"/"+listA.get(j).getFilename()+"/"+listB.get(k).getFilename()+"/"+listC.get(L).getFilename(),filepath);
														}
												}
											 }
										  }
									   }
									} 
								}
							}
						}
				}catch (Exception e) {
					System.out.println(e.toString());
					log.debug(e.toString(),e.getCause());
				}
			}else{
				System.out.println("Not Connected To Server!!");
				log.info("Not Connected To Server!!");
			}
			channel.disconnect();
			session.disconnect();
		} catch (JSchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.debug(e.toString(),e.getCause());
		}
	}

public static void UtilityDownloadLatestFiles(String ServerPath, String LocalPath, String LatestFilesLocation,
        int TopFiles)
{
	try
	{
		UtilityDownloadToLocalPSH(ServerPath, LocalPath);

		// File directory = new
		// File("D:\\PSH_Automation\\LOCAL_PAYMENT_FILE\\FILEStubResp\\");
		File directory = new File(LocalPath);
		File[] files = directory.listFiles((FileFilter) FileFileFilter.FILE);

		Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
		displayFiles(files);
		int Count = 5;
		for (int i = 0; i < _ob.size(); i++)
		{
			if (i <= TopFiles)
			{
				System.out.println(_ob.get(i));
				File Source_file = new File(_ob.get(i));
				File File_dest = new File(LatestFilesLocation + "\\" + _ob_filename.get(i));
				try
				{
					copyFileUsingApacheCommonsIO(Source_file, File_dest);
				}
				catch (Exception e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
					log.debug(e.toString(),e.getCause());
				}
			}
		}
	}
	catch (Exception e)
	{
		e.printStackTrace();
	}
}

public static void UtilityDownloadToLocal(String ServerPath, String LocalPath) throws Exception
{
	// String hostname = "10.10.8.62";
	// String login = "SIR18360";
	// String password = "PSHRetro@Joyo123";
	//String hostname = Utilities.getProp(CIBC_XPATH_Constants.Env2_HOST_NAME, CIBC_XPATH_Constants.CONFIGPATH);
	//String login = Utilities.getProp(CIBC_XPATH_Constants.Env2_LOGIN, CIBC_XPATH_Constants.CONFIGPATH);
	//String password = Utilities.getProp(CIBC_XPATH_Constants.Env2_PASSWORD, CIBC_XPATH_Constants.CONFIGPATH);

	String hostname = PropertyUtils.readProperty("FGT_HOSTNAME");
	String login = PropertyUtils.readProperty("FGT_LOGIN");
	String password = PropertyUtils.readProperty("FGT_PWD");
	// String directory =
	// "/usr1/SIR18360/GTB_HOME/IPSH_HOME/filearea/FILESTUBS/responsefiles";
	String directory = ServerPath;

	// String directory = "the directory";
	java.util.Properties config = new java.util.Properties();
	config.put("StrictHostKeyChecking", "no");
	JSch ssh = new JSch();
	Session session;
	try
	{
		// GETTING sESSION FROM SERVER
		session = ssh.getSession(login, hostname, 22);
		session.setConfig(config);
		session.setConfig("PreferredAuthentications", "publickey,keyboard-interactive,password");
		session.setPassword(password);
		session.connect();

		// CONNECTED TO CHANNEL
		ChannelSftp channel = (ChannelSftp) session.openChannel("sftp");
		channel.connect();

		if (channel.isConnected())
		{
			System.out.println("Connected!!");
		log.info("Connected!!");
			Channel sftp = channel;
			((ChannelSftp) sftp).cd(directory);
			try
			{
				((ChannelSftp) sftp).cd(directory);
				Vector<ChannelSftp.LsEntry> list = ((ChannelSftp) sftp).ls(directory);
				for (int i = 0; i < list.size(); i++)
				{
					if (!(list.get(i).getFilename().equalsIgnoreCase("..")
					        || (list.get(i).getFilename().equalsIgnoreCase("."))))
					{
						System.out.println(list.get(i).getFilename());
						log.info(list.get(i).getFilename());
						String remoteFilePath = directory + "/" + list.get(i).getFilename();
						SftpATTRS attrs = channel.lstat(remoteFilePath);
						SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
						Date modDate = format.parse(attrs.getMtimeString());
						// channel.get(directory + "/" +
						// list.get(i).getFilename(),"D:\\PSH_Automation\\LOCAL_PAYMENT_FILE\\FILEStubResp\\");
						// File downloadedFile = new
						// File("D:\\PSH_Automation\\LOCAL_PAYMENT_FILE\\FILEStubResp\\"+
						// "\\" + list.get(i).getFilename());

						channel.get(directory + "/" + list.get(i).getFilename(), LocalPath);
						File downloadedFile = new File(LocalPath + "\\" + list.get(i).getFilename());
						downloadedFile.setLastModified(modDate.getTime());
					}
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				log.debug(e.toString(),e.getCause());
			}
			channel.disconnect();
			session.disconnect();
		}
	}
	catch (JSchException e)
	{
		e.printStackTrace();
		log.debug(e.toString(),e.getCause());
	}
}

public static void UtilityDownloadToLocalPSH(String ServerPath, String LocalPath) throws Exception
{
	// String hostname = "10.10.8.62";
	// String login = "SIR18360";
	// String password = "PSHRetro@Joyo123";
	//String hostname = Utilities.getProp(CIBC_XPATH_Constants.Env2_HOST_NAME, CIBC_XPATH_Constants.CONFIGPATH);
	//String login = Utilities.getProp(CIBC_XPATH_Constants.Env2_LOGIN, CIBC_XPATH_Constants.CONFIGPATH);
	//String password = Utilities.getProp(CIBC_XPATH_Constants.Env2_PASSWORD, CIBC_XPATH_Constants.CONFIGPATH);

	String hostname =  PropertyUtils.readProperty("PSHAPP");
	String login = PropertyUtils.readProperty("PSHAPP_username");
	String password =  PropertyUtils.readProperty("PSHAPP_Password");
	// String directory =
	// "/usr1/SIR18360/GTB_HOME/IPSH_HOME/filearea/FILESTUBS/responsefiles";
	String directory = ServerPath;

	// String directory = "the directory";
	java.util.Properties config = new java.util.Properties();
	config.put("StrictHostKeyChecking", "no");
	JSch ssh = new JSch();
	String envdetail=PropertyUtils.readProperty("ENV_DETAIL");
	if(envdetail.contains("CLOUD"))
	{	
	String ppk=	PropertyUtils.readProperty("ENV_PPK");
	//String privateKey = "D:\\CBX_Automation\\ppk\\"+ppk;
	String privateKey = System.getProperty("user.dir")+"\\src\\test\\resources\\ppk\\"+ppk;
	ssh.addIdentity(privateKey);
	}
	Session session;
	try
	{
		// GETTING sESSION FROM SERVER
		session = ssh.getSession(login, hostname, 22);
		session.setConfig(config);
		session.setConfig("PreferredAuthentications", "publickey,keyboard-interactive,password");
		session.setPassword(password);
		session.connect();

		// CONNECTED TO CHANNEL
		ChannelSftp channel = (ChannelSftp) session.openChannel("sftp");
		channel.connect();

		if (channel.isConnected())
		{
			System.out.println("Connected!!");
			Channel sftp = channel;
			((ChannelSftp) sftp).cd(directory);
			try
			{
				((ChannelSftp) sftp).cd(directory);
				Vector<ChannelSftp.LsEntry> list = ((ChannelSftp) sftp).ls(directory);
				for (int i = 0; i < list.size(); i++)
				{
					if (!(list.get(i).getFilename().equalsIgnoreCase("..")
					        || (list.get(i).getFilename().equalsIgnoreCase("."))))
					{
						System.out.println(list.get(i).getFilename());
						String remoteFilePath = directory + "/" + list.get(i).getFilename();
						SftpATTRS attrs = channel.lstat(remoteFilePath);
						SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
						Date modDate = format.parse(attrs.getMtimeString());
						// channel.get(directory + "/" +
						// list.get(i).getFilename(),"D:\\PSH_Automation\\LOCAL_PAYMENT_FILE\\FILEStubResp\\");
						// File downloadedFile = new
						// File("D:\\PSH_Automation\\LOCAL_PAYMENT_FILE\\FILEStubResp\\"+
						// "\\" + list.get(i).getFilename());

						channel.get(directory + "/" + list.get(i).getFilename(), LocalPath);
						File downloadedFile = new File(LocalPath + "\\" + list.get(i).getFilename());
						downloadedFile.setLastModified(modDate.getTime());
					}
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				log.debug(e.toString(),e.getCause());
			}
			channel.disconnect();
			session.disconnect();
		}
	}
	catch (JSchException e)
	{
		e.printStackTrace();
	}
}

public static void displayFiles(File[] files)
{
	_ob = new ArrayList<String>();
	_ob_filename = new ArrayList<String>();
	for (File file : files)
	{
		// System.out.printf("File: %-20s Last Modified:" + new
		// Date(file.lastModified()) + "\n", file.getName());
		_ob.add(file.getAbsolutePath());
		_ob_filename.add(file.getName());
	}
}
public static void copyFileUsingApacheCommonsIO(File source, File dest) throws Exception
{
	FileUtils.copyFile(source, dest);
}

public static void clearRemotedirectory(String ServerPath) throws Exception
{
	// String hostname = "10.198.12.23";
	// String login = "SIR18763";
	// String password = "test123";

	String hostname =  PropertyUtils.readProperty("PSHAPP");
	String login = PropertyUtils.readProperty("PSHAPP_username");
	String password =  PropertyUtils.readProperty("PSHAPP_Password");

	// String directory =
	// "/opt/SIR18763/GTB_HOME/IPSH_HOME/filearea/FILESTUBS/responsefiles/";
	String directory = ServerPath;

	// String directory = "the directory";
	java.util.Properties config = new java.util.Properties();
	config.put("StrictHostKeyChecking", "no");
	JSch ssh = new JSch();
	Session session;
	String envdetail=PropertyUtils.readProperty("ENV_DETAIL");
	if(envdetail.contains("CLOUD"))
	{	
	String ppk=	PropertyUtils.readProperty("ENV_PPK");
	//String privateKey = "D:\\CBX_Automation\\ppk\\"+ppk;
	String privateKey = System.getProperty("user.dir")+"\\src\\test\\resources\\ppk\\"+ppk;
	try {
		ssh.addIdentity(privateKey);
	} catch (JSchException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
	
	session = ssh.getSession(login, hostname, 22);
	session.setConfig(config);
	session.setConfig("PreferredAuthentications", "publickey,keyboard-interactive,password");
	session.setPassword(password);
	try
	{
		// GETTING sESSION FROM SERVER

		session.connect();

		// CONNECTED TO CHANNEL
		ChannelSftp channel = (ChannelSftp) session.openChannel("sftp");
		channel.connect();

		if (channel.isConnected())
		{
			System.out.println("Connected!!");
			log.info("Connected!!");

			recursiveFolderDelete(channel, directory);

			/*
			 * File f = new File(directory);
			 * System.out.println(f.getPath()); File[] files =
			 * f.listFiles((FileFilter) FileFileFilter.FILE); if (files ==
			 * null) { System.out.println("bad luck"); }
			 * 
			 * // System.out.println(files.toString());
			 * System.out.println("in here 0"); for (int i = 0; i <=
			 * f.length(); i++) { System.out.println(files[i].getName());
			 * files[i].delete(); }
			 */
			session.disconnect();
		}
	}
	catch (Exception e)
	{
		e.printStackTrace();
		session.disconnect();
		log.debug(e.toString(),e.getCause());
	}

}

private static void recursiveFolderDelete(ChannelSftp channelSftp, String path) throws SftpException
{

	// List source directory structure.
	Collection<ChannelSftp.LsEntry> fileAndFolderList = channelSftp.ls(path);

	// Iterate objects in the list to get file/folder names.
	for (ChannelSftp.LsEntry item : fileAndFolderList)
	{
		if (!item.getAttrs().isDir())
		{
			channelSftp.rm(path + "/" + item.getFilename()); // Remove file.
		}
		else if (!(".".equals(item.getFilename()) || "..".equals(item.getFilename())))
		{ // If it is a subdir.
			try
			{
				// removing sub directory.
				channelSftp.rmdir(path + "/" + item.getFilename());
			}
			catch (Exception e)
			{ // If subdir is not empty and error occurs.
			  // Do lsFolderRemove on this subdir to enter it and clear
			  // its contents.
				recursiveFolderDelete(channelSftp, path + "/" + item.getFilename());
			}
		}
	}
	// channelSftp.rmdir(path); // delete the parent directory after empty
}



	public static void main(String[] args) throws Exception{
		//file_DownloadToLocal();
		
	clearRemotedirectory(PropertyUtils.readProperty("strRebulkPathFROMSTUB"));
	}

}

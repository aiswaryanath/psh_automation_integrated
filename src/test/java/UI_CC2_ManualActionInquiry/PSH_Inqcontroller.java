package UI_CC2_ManualActionInquiry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.IAssert;

import com.codoid.products.exception.FilloException;

import Logger.LoggerUtils;
import UIOperations.Keywordoperations;
import Utilities.DBConnector;
import Utilities.FileUtilities;
import Utilities.PropertyUtils;

public class PSH_Inqcontroller {

	static String BPS_window;

	static FileUtilities Fileutil = new FileUtilities();
	static PropertyUtils Prop = new PropertyUtils();
	static Logger log = LoggerUtils.getLogger();

	public static void SearchWorkitemID(WebDriver driver, String TestData, String xPathNav) throws Exception {
		try {

			String Workitemid = Fileutil.GetFilevalue(TestData);
			Thread.sleep(6000);

			driver.switchTo().defaultContent();
			driver.switchTo().frame("TabFrame");
			String currentFrameName = (String) ((JavascriptExecutor) driver)
					.executeScript("return window.frameElement.name");
			System.out.println("TabFrame::" + currentFrameName);
			driver.switchTo().defaultContent();

			driver.switchTo().frame("MenuFrame");
			String currentFrameName1 = (String) ((JavascriptExecutor) driver)
					.executeScript("return window.frameElement.name");
			System.out.println("MenuFrame::" + currentFrameName1);
			driver.switchTo().defaultContent();

			driver.switchTo().frame("SearchFrame");
			String currentFrameName2 = (String) ((JavascriptExecutor) driver)
					.executeScript("return window.frameElement.name");
			System.out.println("SearchFrame::" + currentFrameName2);

			/*
			 * WebElement Search_WitemId = driver .findElement(By.
			 * xpath("(//*[@class=' x-form-text x-form-field x-box-item'])[last()-23]"
			 * ));
			 */
			WebElement Search_WitemId = driver.findElement(By.xpath(xPathNav));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].value='" + Workitemid + "';", Search_WitemId);

			((JavascriptExecutor) driver).executeScript("arguments[0].click();",
					driver.findElement(By.xpath("(//*[@class='x-grid3-hd-btn'])[last()-25]")));
//ASH 25
			WebElement ele = driver.findElement(By.xpath("(//*[@class='x-menu-list-item'])[last()-0]"));
			// WebElement ele =
			// driver.findElement(By.xpath("//*[@class='x-menu-item
			// x-menu-item-arrow']"));
			Actions ref = new Actions(driver);
			ref.moveToElement(ele);
			ref.click().build().perform();
			Thread.sleep(5000);
			WebElement ele1 = driver.findElement(By.xpath("(//*[@class='x-menu-item-icon '])[last()-7]"));
			Actions ref1 = new Actions(driver);
			ref1.moveToElement(ele1);
			ref1.click().build().perform();
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",
					driver.findElement(By.xpath("(//*[@class='x-menu-item-icon '])[last()-7]")));

			Thread.sleep(3000);
			/*
			 * driver.findElement(By.xpath(
			 * "(//*[@class='x-menu-list-item'])[last()-0]")).sendKeys(Keys.
			 * ENTER); ((JavascriptExecutor)driver).executeScript(
			 * "arguments[0].click();",driver.findElement(By.
			 * xpath("//*[@class='x-menu-item x-menu-item-arrow']")));
			 * Thread.sleep(3000); ((JavascriptExecutor)driver).executeScript(
			 * "arguments[0].click();",driver.findElement(By.
			 * xpath("(//*[@class='x-menu-item-icon '])[last()-7]")));
			 * Thread.sleep(3000);
			 */
			driver.findElement(By.xpath("(//*[contains(text(),'" + Workitemid + "')])[last()-1]")).click();
			driver.switchTo().defaultContent();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void SearchWorkitemIDEFT(WebDriver driver, String TestData, String xPathNav) throws Exception {
		try {

			String Workitemid = Fileutil.GetFilevalue(TestData);
			Thread.sleep(6000);

			driver.switchTo().defaultContent();
			driver.switchTo().frame("TabFrame");
			String currentFrameName = (String) ((JavascriptExecutor) driver)
					.executeScript("return window.frameElement.name");
			System.out.println("TabFrame::" + currentFrameName);
			driver.switchTo().defaultContent();

			driver.switchTo().frame("MenuFrame");
			String currentFrameName1 = (String) ((JavascriptExecutor) driver)
					.executeScript("return window.frameElement.name");
			System.out.println("MenuFrame::" + currentFrameName1);
			driver.switchTo().defaultContent();

			driver.switchTo().frame("SearchFrame");
			String currentFrameName2 = (String) ((JavascriptExecutor) driver)
					.executeScript("return window.frameElement.name");
			System.out.println("SearchFrame::" + currentFrameName2);

			/*
			 * WebElement Search_WitemId = driver .findElement(By.
			 * xpath("(//*[@class=' x-form-text x-form-field x-box-item'])[last()-23]"
			 * ));
			 */
			WebElement Search_WitemId = driver.findElement(By.xpath(xPathNav));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].value='" + Workitemid + "';", Search_WitemId);

			((JavascriptExecutor) driver).executeScript("arguments[0].click();",
					driver.findElement(By.xpath("(//*[@class='x-grid3-hd-btn'])[last()-27]")));
			
			//((JavascriptExecutor) driver).executeScript("arguments[0].click();",
				//	driver.findElement(By.xpath("(//*[@id='ext-gen181']")));
			WebElement ele = driver.findElement(By.xpath("(//*[@class='x-menu-list-item'])[last()-0]"));
			// WebElement ele =
			// driver.findElement(By.xpath("//*[@class='x-menu-item
			// x-menu-item-arrow']"));
			Actions ref = new Actions(driver);
			ref.moveToElement(ele);
			ref.click().build().perform();
			Thread.sleep(5000);
			WebElement ele1 = driver.findElement(By.xpath("(//*[@class='x-menu-item-icon '])[last()-7]"));
			Actions ref1 = new Actions(driver);
			ref1.moveToElement(ele1);
			ref1.click().build().perform();
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",
					driver.findElement(By.xpath("(//*[@class='x-menu-item-icon '])[last()-7]")));
			Thread.sleep(3000);
			/*
			 * driver.findElement(By.xpath(
			 * "(//*[@class='x-menu-list-item'])[last()-0]")).sendKeys(Keys.
			 * ENTER); ((JavascriptExecutor)driver).executeScript(
			 * "arguments[0].click();",driver.findElement(By.
			 * xpath("//*[@class='x-menu-item x-menu-item-arrow']")));
			 * Thread.sleep(3000); ((JavascriptExecutor)driver).executeScript(
			 * "arguments[0].click();",driver.findElement(By.
			 * xpath("(//*[@class='x-menu-item-icon '])[last()-7]")));
			 * Thread.sleep(3000);
			 */
			driver.findElement(By.xpath("(//*[contains(text(),'" + Workitemid + "')])[last()-1]")).click();
			driver.switchTo().defaultContent();
		//	driver.switchTo().window(C)

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String SearchWorkitemIDACH(WebDriver driver, String TestData, String xPathNav) throws Exception {
		String status="PASS";
		try {
			String Data[]=TestData.split("\\|");
			//System.out.println("CustRefnum:"+Data[0]);
			//System.out.println("Action:"+Data[1]);
			int size=Data.length;
			System.out.println("Noof txns: "+size);
			Keywordoperations K=new Keywordoperations();
			for(int i=1;i<=size;i++)
			{
				System.out.println("Txn number: "+i);
				if(i>1)
				{
					System.out.println("Txn number from second onwards: "+i);
					String queue=FileUtilities.GetFilevalue("VAR_QUEUE");
					K.ClickManualActionDQ(queue,driver);	
				}
				String Meta[]=Data[i-1].split(":");
				String custrefnum=Meta[0];
				System.out.println("CustRefnum:"+Meta[0]);
				System.out.println("Action:"+Meta[1]);
				DBConnector DBConnect=new DBConnector();
				String Workitemid=DBConnect.GetTXNWORKITEMID(custrefnum);
				driver.getCurrentUrl();

				String CurrentWindow=driver.getWindowHandle();
				WebDriverWait wait = new WebDriverWait(driver, 30);
				Keywordoperations Keywordoperations=new Keywordoperations();
				Actions ref = new Actions(driver);
				//String Workitemid = Fileutil.GetFilevalue("VAR_PSHTXNWORKITEMID");
				Thread.sleep(3000);
				driver.switchTo().defaultContent();

				driver.switchTo().frame("TabFrame");
				String currentFrameName = (String) ((JavascriptExecutor) driver)
						.executeScript("return window.frameElement.name");
				System.out.println("TabFrame::" + currentFrameName);
				driver.switchTo().defaultContent();

				driver.switchTo().frame("MenuFrame");
				String currentFrameName1 = (String) ((JavascriptExecutor) driver)
						.executeScript("return window.frameElement.name");
				System.out.println("MenuFrame::" + currentFrameName1);
				driver.switchTo().defaultContent();

				driver.switchTo().frame("SearchFrame");
				String currentFrameName2 = (String) ((JavascriptExecutor) driver)
						.executeScript("return window.frameElement.name");
				System.out.println("SearchFrame::" + currentFrameName2);
				//enter workitemid into serachbox
				xPathNav="//*[@id='ext-comp-1003']";
				WebElement Search_WitemId = driver.findElement(By.xpath(xPathNav));
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].value='" + Workitemid + "';", Search_WitemId);

				WebElement ele7 = driver.findElement(By.xpath("//*[text()='Workitem Id']/a"));

				List<WebElement> de1=driver.findElements(By.xpath("(//*[@class='x-grid3-hd-btn'])"));
				int count=0;
				System.out.println(de1.size());


				((JavascriptExecutor) driver).executeScript("arguments[0].click();",
						driver.findElement(By.xpath("(//*[@class='x-grid3-hd-btn'])[last()-28]")));

				//driver.findElement(By.xpath("//*[text()='Workitem Id']/a")));
				WebElement ele = driver.findElement(By.xpath("(//*[@class='x-menu-list-item'])[last()-0]"));

				ref.moveToElement(ele);
				ref.click().build().perform();
				Thread.sleep(1500);

				List<WebElement> filtermenu1 = driver
						.findElements(By.xpath("//*[@class='x-menu-item x-menu-check-item filteroption-undefined']"));

				WebElement item = filtermenu1.get(0);


				item.click();

				Thread.sleep(2000);


				//SWITCH TO TXN SCREEN
				try{
					try
					{
						//driver.findElement(By.xpath("(//[contains(text(),'" + Workitemid + "')])[last()-1]")).click();
						driver.findElement(By.xpath("(//a[contains(text(),'" + Workitemid + "')])")).click();
					}
					catch(Exception e)
					{
						return "Workitemid not available in Txn queue";
					}
					driver.switchTo().defaultContent();
					boolean flag=false;
					String parentWinHandle=driver.getWindowHandle();
					System.out.println(driver.getTitle());
					Set<String> winHandles = driver.getWindowHandles();
					for(String handle: winHandles){

						if(!handle.equals(parentWinHandle)){
							driver.switchTo().window(handle);

							if(driver.getTitle().equals("Label Entry Window"))
								break;
							System.out.println(driver.getTitle());
							log.info(driver.getTitle());
						}
						else
						{
							System.out.println("traversing through windows");
							log.info("Window not found");

						}}
					//SWITCH TO INFRAME IN TXN SCRREN
					driver.switchTo().defaultContent();
					WebElement frame_1;
					if(Meta[1].equals("Authorize"))
					{	
						frame_1 = driver.findElement(By.xpath("//div[@id='TAB_0']//iframe"));
					}
					else
					{
						frame_1 = driver.findElement(By.xpath("//div[@id='TAB_0-body']//iframe"));
					}
					driver.switchTo().frame(frame_1);

					driver.switchTo().frame("IwMiddleFrame");

					//click on user remarks
					String  XPATH_NAV="//*[(text()='User Remark')]";
					WebElement clickable = wait.until(
							ExpectedConditions.visibilityOfElementLocated(By.xpath(XPATH_NAV)));

					((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath(XPATH_NAV)));

					//click on expand all in user remarks
					XPATH_NAV="(//*[@id='PEAL'])[last()]";
					WebElement clic = wait.until(
							ExpectedConditions.visibilityOfElementLocated(By.xpath(XPATH_NAV)));

					((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath(XPATH_NAV)));

					//enter user remarks
					if(Meta[1].equals("Authorize"))
					{	

						XPATH_NAV="//*[@id='P162943174-331294617']";
						Keywordoperations.SendKeysie(XPATH_NAV,"default automation remarks checker ",driver);
						//click on user remarks
						XPATH_NAV="//a[@id='P162943174-565944340']/b";
						Thread.sleep(2500);
						((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath(XPATH_NAV)));

					}
					else
					{
						XPATH_NAV="//*[@id='P162943174-335820275']";
						Keywordoperations.SendKeysie(XPATH_NAV,"default automation remarks maker ",driver);
						//click on user remarks
						XPATH_NAV="//a[@id='P162943174-565944470']/b";
						Thread.sleep(2500);
						((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath(XPATH_NAV)));

					}

					//click on user remarks
					//click on bottom frame
					driver.switchTo().defaultContent();
					WebElement frame_2;
					if(Meta[1].equals("Authorize"))
					{	
						frame_2 = driver.findElement(By.xpath("//div[@id='TAB_0']//iframe"));
					}
					else
					{
						frame_2 = driver.findElement(By.xpath("//div[@id='TAB_0-body']//iframe"));
					}
					driver.switchTo().frame(frame_2);

					driver.switchTo().frame("IwBottomFrame");

					//click on submit button
					if(!(Meta[1].equals("Authorize")))
					{
						XPATH_NAV="//span[text()='"+Meta[1]+"']";
						System.out.println(XPATH_NAV);
						//WebElement cl= wait.until(
						//      ExpectedConditions.visibilityOfElementLocated(By.xpath(XPATH_NAV)));
						Thread.sleep(2000);
						getvalidxpath(driver,XPATH_NAV);
						Thread.sleep(3000);
					}
					else
					{
						//
						XPATH_NAV="//span[text()='Authorize']";
						//WebElement cl= wait.until(
						//       ExpectedConditions.visibilityOfElementLocated(By.xpath(XPATH_NAV)));
						getvalidxpath(driver,XPATH_NAV);
					}

					Thread.sleep(3000);
					//	 driver.quit();

				}
				catch (Exception e) {

					e.printStackTrace();
					log.debug(e.toString());
					System.out.println("in this exception 2:"+e.toString());
					return e.toString();
				}
				Set<String> winHandles = driver.getWindowHandles();
				for(String handle: winHandles){


					driver.switchTo().window(handle);

					if(driver.getTitle().equals("BPS"))
					{
						CurrentWindow=driver.getWindowHandle();
						driver.switchTo().window(handle);
						break;
					}

				}
				driver.switchTo().window(CurrentWindow);
				System.out.println(driver.getTitle());
			//	return "PASS";
			}

		}catch (Exception e) {
			e.printStackTrace();
			System.out.println("in this exception:"+e.toString());
			return e.toString();
		}

		finally {
			Thread.sleep(3000);
			driver.close();

			Runtime.getRuntime().exec("taskkill /F /IM chromedriver.exe /T");
			Runtime.getRuntime().exec("taskkill /F /IM IEDriverServer.exe /T");
			Thread.sleep(6000);
		}
		return status;

	}
	
	public static String getvalidxpath(WebDriver driver,String XpathNav)
	{
		List<WebElement> el=new ArrayList<WebElement>();
		el=driver.findElements(By.xpath(XpathNav));
		for(WebElement e:el)
		{
			int x=e.getLocation().getX();
			int y=e.getLocation().getY();
			System.out.println("X:"+x+"Y:"+y);
			if(x>0 && y>0)
			{
				System.out.println("successfully clicked "+XpathNav+ "button");
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", e);
			}
		}
		
		return "";
	}
	
	public static String SearchWorkitemIDGen(WebDriver driver, String TestData, String xPathNav) throws Exception {
		StringBuffer returnstring=new StringBuffer("");
		try {
			String Data[]=TestData.split("\\|");
			
			int size=Data.length;
			System.out.println("Noof txns: "+size);
			Keywordoperations K=new Keywordoperations();
			for(int i=1;i<=size;i++)
			{
				System.out.println("Txn number: "+i);
				if(i>1)
				{
					System.out.println("Txn number from second onwards: "+i);
					String queue=FileUtilities.GetFilevalue("VAR_QUEUE");
					K.ClickManualActionDQ(queue,driver);	
					returnstring.append("|");
				}
				String Meta[]=Data[i-1].split(":");
			String queue=FileUtilities.GetFilevalue("VAR_QUEUE");
				int select=25;
				//ach,legacyach -28, eft-27,wires,legacy eft,account transfer,bulk e transfer bet 25,cheques-26
				if(queue.contains("ACH"))
				{
					select=28;
				}
				else if(queue.startsWith("EFT"))
				{
					select=27;
				}
		 
		    String custrefnum=Meta[0];
			System.out.println("CustRefnum:"+Meta[0]);
			System.out.println("Action:"+Meta[1]);
			DBConnector DBConnect=new DBConnector();
			String Workitemid=DBConnect.GetTXNWORKITEMID(custrefnum);
			returnstring.append(Workitemid);
			driver.getCurrentUrl();
			
			String CurrentWindow=driver.getWindowHandle();
			WebDriverWait wait = new WebDriverWait(driver, 30);
			Keywordoperations Keywordoperations=new Keywordoperations();
			Actions ref = new Actions(driver);
			//String Workitemid = Fileutil.GetFilevalue("VAR_PSHTXNWORKITEMID");
			Thread.sleep(3000);
			driver.switchTo().defaultContent();
			
			driver.switchTo().frame("TabFrame");
			String currentFrameName = (String) ((JavascriptExecutor) driver)
					.executeScript("return window.frameElement.name");
			System.out.println("TabFrame::" + currentFrameName);
			driver.switchTo().defaultContent();

			driver.switchTo().frame("MenuFrame");
			String currentFrameName1 = (String) ((JavascriptExecutor) driver)
					.executeScript("return window.frameElement.name");
			System.out.println("MenuFrame::" + currentFrameName1);
			driver.switchTo().defaultContent();

			driver.switchTo().frame("SearchFrame");
			String currentFrameName2 = (String) ((JavascriptExecutor) driver)
					.executeScript("return window.frameElement.name");
			System.out.println("SearchFrame::" + currentFrameName2);
//enter workitemid into serachbox
			xPathNav="//*[@id='ext-comp-1003']";
			WebElement Search_WitemId = driver.findElement(By.xpath(xPathNav));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].value='" + Workitemid + "';", Search_WitemId);
	

			List<WebElement> de1=driver.findElements(By.xpath("(//*[@class='x-grid3-hd-btn'])"));
			int count=0;
			System.out.println(de1.size());

			if(queue.contains("ACH"))
			{
				((JavascriptExecutor) driver).executeScript("arguments[0].click();",
						driver.findElement(By.xpath("(//*[@class='x-grid3-hd-btn'])[last()-28]")));
			}
			else if(queue.startsWith("EFT"))
			{
				((JavascriptExecutor) driver).executeScript("arguments[0].click();",
						driver.findElement(By.xpath("(//*[@class='x-grid3-hd-btn'])[last()-27]")));
			}
			else
			{
				((JavascriptExecutor) driver).executeScript("arguments[0].click();",
						driver.findElement(By.xpath("(//*[@class='x-grid3-hd-btn'])[last()-25]")));
			}
		
			WebElement ele = driver.findElement(By.xpath("(//*[@class='x-menu-list-item'])[last()-0]"));
			
			ref.moveToElement(ele);
			ref.click().build().perform();
			Thread.sleep(1500);
	
			List<WebElement> filtermenu1 = driver
			        .findElements(By.xpath("//*[@class='x-menu-item x-menu-check-item filteroption-undefined']"));

			WebElement item = filtermenu1.get(0);
			
		
			item.click();
			
			Thread.sleep(4000);
			
		
			//SWITCH TO TXN SCREEN
			try{
				try
				{
				//driver.findElement(By.xpath("(//a[contains(text(),'"+Workitemid+"')])")).click();
				((JavascriptExecutor) driver).executeScript("arguments[0].click();",
						driver.findElement(By.xpath("(//a[contains(text(),'"+Workitemid+"')])")));
				}
				catch(Exception e)
				{
					returnstring.append(":FAIL-Workitemid not available in Txn queue");
					//return "Workitemid not available in Txn queue";
					continue;
				}
				driver.switchTo().defaultContent();
				boolean flag=false;
				String parentWinHandle=driver.getWindowHandle();
			    System.out.println(driver.getTitle());
		        Set<String> winHandles = driver.getWindowHandles();
		        for(String handle: winHandles){
		        	
		            if(!handle.equals(parentWinHandle)){
		            driver.switchTo().window(handle);
		            
		            if(driver.getTitle().equals("Label Entry Window"))
		            break;
		        	System.out.println(driver.getTitle());
		         	log.info(driver.getTitle());
		            }
		            else
		            {
		            	System.out.println("traversing through windows");
		            	log.info("Window not found");
		            	
		            }}
		        //SWITCH TO INFRAME IN TXN SCRREN
		        driver.switchTo().defaultContent();
		        WebElement frame_1;
		        if(Meta[1].equals("Authorize"))
		        {	
		      frame_1 = driver.findElement(By.xpath("//div[@id='TAB_0']//iframe"));
		        }
		        else
		        {
		       frame_1 = driver.findElement(By.xpath("//div[@id='TAB_0-body']//iframe"));
		        }
				driver.switchTo().frame(frame_1);

				driver.switchTo().frame("IwMiddleFrame");
				
			//click on user remarks
				String  XPATH_NAV="//*[(text()='User Remark')]";
				WebElement clickable = wait.until(
				        ExpectedConditions.visibilityOfElementLocated(By.xpath(XPATH_NAV)));

				((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath(XPATH_NAV)));
				
				//click on expand all in user remarks
			 XPATH_NAV="(//*[@id='PEAL'])[last()]";
				WebElement clic = wait.until(
				        ExpectedConditions.visibilityOfElementLocated(By.xpath(XPATH_NAV)));

				((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath(XPATH_NAV)));
				String USERTYPE=FileUtilities.GetFilevalue("VAR_USER");
				//enter user remarks
			       if(Meta[1].equals("Authorize")||(Meta[1].equals("Reject")&&USERTYPE.equals("BCCCKR1")))
			        {	
			
				XPATH_NAV="//*[@id='P162943174-331294617']";
				Keywordoperations.SendKeysie(XPATH_NAV,"default automation remarks checker ",driver);
				//click on user remarks
				 XPATH_NAV="//a[@id='P162943174-565944340']/b";
	    		 Thread.sleep(2500);
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath(XPATH_NAV)));
				
			        }
			       else
			       {
			    		XPATH_NAV="//*[@id='P162943174-335820275']";
			    		Keywordoperations.SendKeysie(XPATH_NAV,"default automation remarks maker ",driver);
			    		//click on user remarks
			    		 XPATH_NAV="//a[@id='P162943174-565944470']/b";
			    		 Thread.sleep(2500);
							((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath(XPATH_NAV)));
						
			       }
			
				//click on user remarks
					//click on bottom frame
					  driver.switchTo().defaultContent();
					  WebElement frame_2;
				       if(Meta[1].equals("Authorize"))
				        {	
				     frame_2 = driver.findElement(By.xpath("//div[@id='TAB_0']//iframe"));
				        }
				       else
				       {
				   frame_2 = driver.findElement(By.xpath("//div[@id='TAB_0-body']//iframe"));
				       }
					driver.switchTo().frame(frame_2);

					driver.switchTo().frame("IwBottomFrame");
					
					//click on submit button
					if(!(Meta[1].equals("Authorize")))
							{
						XPATH_NAV="//span[text()='"+Meta[1]+"']";
						System.out.println(XPATH_NAV);
						//WebElement cl= wait.until(
						  //      ExpectedConditions.visibilityOfElementLocated(By.xpath(XPATH_NAV)));
						 Thread.sleep(2000);
						getvalidxpath(driver,XPATH_NAV);
						 Thread.sleep(3000);
							}
					else
					{
						//
						XPATH_NAV="//span[text()='Authorize']";
						//WebElement cl= wait.until(
						 //       ExpectedConditions.visibilityOfElementLocated(By.xpath(XPATH_NAV)));
						getvalidxpath(driver,XPATH_NAV);
					}
						
						// Thread.sleep(3000);
					//	 driver.quit();
					
			}
			catch (Exception e) {
			
			System.out.println(e.getLocalizedMessage());
				log.debug(e.toString());
				returnstring.append(":FAIL-"+e.toString());
				continue;
			}
			  Set<String> winHandles = driver.getWindowHandles();
		        for(String handle: winHandles){
		        	
		      
		            driver.switchTo().window(handle);
		            
		            if(driver.getTitle().equals("BPS"))
		            {
		            	CurrentWindow=driver.getWindowHandle();
		                driver.switchTo().window(handle);
		            break;
		            }
		            
		        }
			 driver.switchTo().window(CurrentWindow);
			 System.out.println(driver.getTitle());
			// return "PASS";
			}
		
		}catch (Exception e) {
			e.printStackTrace();
			returnstring.append(":FAIL-"+e.toString());
			//continue;
		}
		finally
		{
		Thread.sleep(3000);
		driver.close();
		
		Runtime.getRuntime().exec("taskkill /F /IM chromedriver.exe /T");
		Runtime.getRuntime().exec("taskkill /F /IM IEDriverServer.exe /T");
		Thread.sleep(6000);
		}
		return returnstring.toString();
		
	}
	
	public static void SearchWorkitemIDActionClick(WebDriver driver, String TestData, String XpathNav)
			throws Exception {
		try {

			String Workitemid = Fileutil.GetFilevalue(TestData);
			Thread.sleep(6000);

			driver.switchTo().defaultContent();
			driver.switchTo().frame("TabFrame");
			String currentFrameName = (String) ((JavascriptExecutor) driver)
					.executeScript("return window.frameElement.name");
			System.out.println("TabFrame::" + currentFrameName);
			driver.switchTo().defaultContent();

			driver.switchTo().frame("MenuFrame");
			String currentFrameName1 = (String) ((JavascriptExecutor) driver)
					.executeScript("return window.frameElement.name");
			System.out.println("MenuFrame::" + currentFrameName1);
			driver.switchTo().defaultContent();

			driver.switchTo().frame("SearchFrame");
			String currentFrameName2 = (String) ((JavascriptExecutor) driver)
					.executeScript("return window.frameElement.name");
			System.out.println("SearchFrame::" + currentFrameName2);

			WebElement Search_WitemId = driver.findElement(By.xpath(XpathNav));
			/*
			 * WebElement Search_WitemId = driver .findElement(By.
			 * xpath("(//*[@class=' x-form-text x-form-field x-box-item'])[last()-23]"
			 * ));
			 */
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].value='" + Workitemid + "';", Search_WitemId);

			((JavascriptExecutor) driver).executeScript("arguments[0].click();",
					driver.findElement(By.xpath("(//*[@class='x-grid3-hd-btn'])[last()-25]")));

			WebElement ele = driver.findElement(By.xpath("(//*[@class='x-menu-list-item'])[last()-0]"));
			Actions ref = new Actions(driver);
			ref.moveToElement(ele);
			ref.click().build().perform();

			WebElement ele1 = driver.findElement(By.xpath("(//*[@class='x-menu-item-icon '])[last()-7]"));
			Actions ref1 = new Actions(driver);
			ref1.moveToElement(ele1);
			ref1.click().build().perform();

			Thread.sleep(3000);
			// driver.findElement(By.xpath("(//*[contains(text(),'"+Workitemid+"')])[last()-1]")).click();
			Actions actions = new Actions(driver);
			actions.moveToElement(
					driver.findElement(By.xpath("//*[@class='x-grid3-cell-inner x-grid3-col-viewWorkItemID']//img[1]")))
					.click().perform();

			driver.switchTo().defaultContent();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void ExecutePSH_InquirySystem(WebDriver driver, String Credentials) throws FilloException, Exception {
		

		PSH_Login.Login_Execute(driver, Credentials);

		if (Credentials.equalsIgnoreCase("BCCMKR1")) {
			System.out.println("Maker Script Begins");
			
		}
	}

	

	public static String SearchClientFileID(WebDriver driver, String TestData, String xPathNav) throws Exception {
		String Action = null;
		String Remarks = "dnd";

		HashMap<String, String> h = new HashMap();
		try {
			int length;
			if (TestData != null) {
				String[] Data = TestData.split("\\|");
				length = Data.length;

				Action = Data[0];
				Remarks = Data[1];
				for (int i = 2; i < length; i++) {
					h.put(Data[i], Data[i]);
				}
			}
			DBConnector db = new DBConnector();
			db.GetpshFileId();
			Thread.sleep(5000);
			String Workitemid = Fileutil.GetFilevalue("VAR_PSHFILEID");
			Thread.sleep(6000);

			driver.switchTo().defaultContent();
			driver.switchTo().frame("TabFrame");
			String currentFrameName = (String) ((JavascriptExecutor) driver)
					.executeScript("return window.frameElement.name");
			System.out.println("TabFrame::" + currentFrameName);
			driver.switchTo().defaultContent();

			driver.switchTo().frame("MenuFrame");
			String currentFrameName1 = (String) ((JavascriptExecutor) driver)
					.executeScript("return window.frameElement.name");
			System.out.println("MenuFrame::" + currentFrameName1);
			driver.switchTo().defaultContent();

			driver.switchTo().frame("SearchFrame");
			String currentFrameName2 = (String) ((JavascriptExecutor) driver)
					.executeScript("return window.frameElement.name");
			System.out.println("SearchFrame::" + currentFrameName2);

		
			//WebElement Search_WitemId = driver.findElement(By.xpath(xPathNav));
			WebElement Search_WitemId = driver.findElement(By.xpath("//*[@id='ext-comp-1006']"));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].value='" + Workitemid + "';", Search_WitemId);
			Search_WitemId.sendKeys(Keys.ENTER);
			
			String queue=FileUtilities.GetFilevalue("VAR_QUEUE");

			if(queue.contains("ACH"))
			{
				((JavascriptExecutor) driver).executeScript("arguments[0].click();",
						driver.findElement(By.xpath("(//*[@class='x-grid3-hd-btn'])[last()-25]")));
			}
			else if(queue.startsWith("EFT"))
			{
				((JavascriptExecutor) driver).executeScript("arguments[0].click();",
						driver.findElement(By.xpath("(//*[@class='x-grid3-hd-btn'])[last()-24]")));
			}
			else
			{
				((JavascriptExecutor) driver).executeScript("arguments[0].click();",
						driver.findElement(By.xpath("(//*[@class='x-grid3-hd-btn'])[last()-22]")));
			}

			WebElement ele = driver.findElement(By.xpath("(//*[@class='x-menu-list-item'])[last()-0]"));

			Actions ref = new Actions(driver);
			ref.moveToElement(ele);
			ref.click().build().perform();
			Thread.sleep(5000);
			WebElement ele1 = driver.findElement(By.xpath("(//*[@class='x-menu-item-icon '])[last()-6]"));
			Actions ref1 = new Actions(driver);
			ref1.moveToElement(ele1);
			ref1.click().build().perform();

			Thread.sleep(3000);

			List<WebElement> we = driver.findElements(By.className("x-grid3-row-checker"));
			if(we!=null)
			{
			Actions actions = new Actions(driver);
			for (WebElement w : we)

			{

				System.out.println(w.toString());

				actions.moveToElement(w);
				Thread.sleep(1000);

				actions.click().perform();

			}
			driver.findElement(By.className("x-grid3-hd-checker")).click();
			driver.findElement(By.xpath("//button[contains(text(), 'Bulk Action')]")).click();
			//boolean b = isDialogPresent(driver);
			try
			{
			Nfocusonwindow(driver, "IntellectFlow");

			Thread.sleep(5000);
			driver.switchTo().defaultContent();
		
			driver.switchTo().frame("IwMiddleFrame");
			
			WebElement elem = driver.findElement(By.id("P162943174-525099944"));
			
			elem.click();

			((JavascriptExecutor) driver).executeScript("arguments[0].value='" + Remarks + "';",
					driver.findElement(By.id("P162943174-525099944")));

			driver.switchTo().defaultContent();
			Thread.sleep(2000);

			driver.switchTo().frame("IwBottomFrame");
			WebDriverWait w3=new WebDriverWait(driver,30);
			if (!Action.equals("Authorize")) {
			//	WebElement butons = driver.findElement(By.xpath("//td/a/span[text()='" + Action + "']"));
				getvalidxpath(driver,"//td/a/span[text()='" + Action + "']");
				//w3.until(ExpectedConditions.visibilityOf(butons));
				//butons.click();

			} else 
			{
				//WebElement authorize = driver.findElement(By.id("P162943174-542545414"));
				//w3.until(ExpectedConditions.visibilityOf(authorize));
				//authorize.click();
				getvalidxpath(driver,"//td/a/span[text()='" + Action + "']");
			}
			Thread.sleep(3500);
			
			String message = driver.switchTo().alert().getText();
			driver.switchTo().alert().accept();
			System.out.println(message);
			driver.switchTo().defaultContent();
			return "PASS";
			}
			catch(Exception e)
			{
				return "No txns in queue with the given client id";
			}
			}
			
		
			else
			{
				return "No txns in queue with the given client id";
			}

		} catch (Exception e) {
			e.printStackTrace();
			return e.toString();
		}
		finally
		{
			Thread.sleep(3000);
			driver.quit();
			
			Runtime.getRuntime().exec("taskkill /F /IM chromedriver.exe /T");
			Runtime.getRuntime().exec("taskkill /F /IM IEDriverServer.exe /T");
			Thread.sleep(6000);
		}
		
	}

	public static boolean Nfocusonwindow(WebDriver driver, String title) {
		boolean flg = true;
		try
		{

			System.out.println("----Entering Nfocusonwindow----");

			System.out.println("current window:-->" + driver.getTitle());
			if (!title.equalsIgnoreCase(driver.getTitle())) 
			{
				System.out.println("searching for :-->" + title);
				Set<String> window = driver.getWindowHandles();
				System.out.println("total no of windows:-->" + window.size());
				Iterator<String> itr = window.iterator();
				while (itr.hasNext())
				{

					driver.switchTo().window(itr.next());
					System.out.println("Current window:  " + driver.getTitle());
					if (title.equalsIgnoreCase(driver.getTitle())) 
					{
						System.out.println("Correct Window");
						break;
					}

				}

			} 
			else 
			{
				System.out.println("You are on right window");
			}
			if ("Internet Explorer cannot display the webpage".equalsIgnoreCase(driver.getTitle())) 
			{
				driver.close();
				Set<String> window = driver.getWindowHandles();
				System.out.println("total no of windows:-->" + window.size());
				Iterator<String> itr = window.iterator();
				while (itr.hasNext())
				{
					System.out.println("I am here 1");
					driver.switchTo().window(itr.next());
				}
				flg = false;
			}

		} 
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}
		return flg;
	}

	static boolean isDialogPresent(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		if (wait.until(ExpectedConditions.alertIsPresent()) == null)

		{
			System.out.println("alert was not present");

			return true;
		} else 
		{
			System.out.println("alert was present:" + driver.switchTo().alert().getText());
			driver.switchTo().alert().accept();
			return false;
		}
	}
	public static String GroupAction(WebDriver driver, String TestData) throws Exception {
		try {
			String Data[]=TestData.split("\\|");
		
			DBConnector DBConnect=new DBConnector();
			String Workitemid  = FileUtilities.GetFilevalue("VAR_PSHFILEWORKITEMID");
		
			String CurrentWindow=driver.getWindowHandle();
			WebDriverWait wait = new WebDriverWait(driver, 30);
			Keywordoperations Keywordoperations=new Keywordoperations();
			Actions ref = new Actions(driver);
		
			Thread.sleep(3000);
			driver.switchTo().defaultContent();
			
	

			driver.switchTo().frame("MenuFrame");
			String currentFrameName1 = (String) ((JavascriptExecutor) driver)
					.executeScript("return window.frameElement.name");
			System.out.println("MenuFrame::" + currentFrameName1);
			driver.switchTo().defaultContent();

			driver.switchTo().frame("SearchFrame");
			String currentFrameName2 = (String) ((JavascriptExecutor) driver)
					.executeScript("return window.frameElement.name");
			System.out.println("SearchFrame::" + currentFrameName2);
			
					driver.switchTo().frame("GroupedSuspensionSearch");
			String currentFrameName = (String) ((JavascriptExecutor) driver)
					.executeScript("return window.frameElement.name");
			System.out.println("GroupedSuspensionSearch::" + currentFrameName);
			//driver.switchTo().defaultContent();
			//enter fileworkitemid into searchbox
			String xPathNav="//*[@id='fileWId']";
			WebDriverWait wait2=new WebDriverWait(driver,10);
			WebElement Search_WitemId = driver.findElement(By.id("fileWId"));
			wait2.until(ExpectedConditions.visibilityOf(Search_WitemId));
			Search_WitemId.sendKeys(Workitemid);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].value='" + Workitemid + "';", Search_WitemId);
	
			WebElement ele7 = driver.findElement(By.xpath("//*[text()='Show Groups']"));
		
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",ele7);
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("SearchFrame");
			driver.switchTo().frame("GroupedSuspensionList");
			try
			{
				
					WebElement ele8 = driver.findElement(By.xpath("//*[@id='GroupedSuspensionListForm']//tr[2]/td[1]/span"));
		
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",ele8);
			
			Set<String> Windows = driver.getWindowHandles();
			for (String win : Windows)
			{
				driver.switchTo().window(win);
			
				if (!"Intellect Suite - Enterprise Platform for Boundaryless Banking from Intellect Design Arena-EN"
				        .equalsIgnoreCase(driver.getTitle()) && !"BPS".equalsIgnoreCase(driver.getTitle()))
				{
					break;
				}
			}
			String sUser=FileUtilities.GetFilevalue("VAR_USER");
			if (sUser.contains("BCCMKR"))
			{
				System.out.println("maker");
				driver.findElement(By.id("makerComments")).sendKeys("Automation maker Remarks");
				js.executeScript("arguments[0].value='" + "Automation maker Remarks" + "';", driver.findElement(By.id("makerComments")));
				driver.findElement(By.xpath("//span[text()='" + TestData + "']")).click();
			}
			else
			{
				driver.findElement(By.id("checkerComments")).sendKeys("Automation checker Remarks");
				js.executeScript("arguments[0].value='" + "Automation CHECKER Remarks" + "';", driver.findElement(By.id("checkerComments")));
				driver.findElement(By.xpath("//span[text()='" + TestData + "']")).click();
			}
			return "PASS";
			}
			catch(Exception e)
			{
				e.printStackTrace();
				return "No txns available by the given search criteria";
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			return e.toString();
		}
		finally
		{
			Thread.sleep(3000);
			driver.quit();
			Runtime.getRuntime().exec("taskkill /F /IM chromedriver.exe /T");
			Runtime.getRuntime().exec("taskkill /F /IM IEDriverServer.exe /T");
			Thread.sleep(6000);
			
		}
	
	}
}

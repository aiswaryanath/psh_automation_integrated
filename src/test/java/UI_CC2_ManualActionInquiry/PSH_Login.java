package UI_CC2_ManualActionInquiry;

import java.io.File;
import java.util.Iterator;
import java.util.Set;
import Utilities.*;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.io.Files;

public class PSH_Login {
	public static void Login_Execute(WebDriver driver, String credentials)throws Exception{
	 
	     try{

		driver.findElement(By.name("ArmorTicket")).clear();
		driver.findElement(By.name("ArmorTicket")).sendKeys(credentials);
		((JavascriptExecutor) driver).executeScript("arguments[0].value='"+credentials+"';", driver.findElement(By.name("ArmorTicket")));
		HandleWebElements.SuccessfullClickJS(driver, "//*[@src='images/indeximages/Login_button.gif']");


	    if(credentials.contains("BCCMKR")||credentials.contains("BCCCKR1"))
	    {
        System.out.println("AFTER CRED LOGIN");
		WebDriverWait wait = new WebDriverWait(driver, 45);
	//	wait.until(ExpectedConditions.(By.xpath("//*[@src='images/indeximages/Login_button.gif']")));
		  Thread.sleep(15000);
		Set<String> Windows = driver.getWindowHandles();
		//wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[@src='images/indeximages/Login_button.gif']")));
		for (String win : Windows) {
			driver.switchTo().window(win);
			if (!"Intellect Suite - Enterprise Platform for Boundaryless Banking from Intellect Design Arena-EN"
					.equalsIgnoreCase(driver.getTitle()) && !"BPS".equalsIgnoreCase(driver.getTitle())) {
				System.out.println("Automation Begins!!"+driver.getTitle());
				break;
			}
			else if("Workspace Picklist".equalsIgnoreCase(driver.getTitle()))
			{
				driver.switchTo().window(win);
			}
		}

		//Thread.sleep(15000);
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[text()='CIBC Payments Hub']")));
		//HandleWebElements.SuccessfullClickFE(driver, "//a[text()='CIBC Payments Hub']");
		HandleWebElements.SuccessfullClickJS(driver, "//a[text()='CIBC Payments Hub']");
	     }
	     }
	     catch(Exception e)
	     {
	    	 e.printStackTrace();
	     }

		Thread.sleep(5000);
		
		Set<String> window = driver.getWindowHandles();
		System.out.println("SIZE=" + window.size());
		Iterator<String> itr = window.iterator();
		while (itr.hasNext()) {
			//Thread.sleep(18000);
			
			//wait.until(ExpectedConditions.invisibilityOf(ele1));
			driver.switchTo().window(itr.next());
			if (!driver.getTitle().equals("Workspace Picklist")) {
				break;
			}
		}
//		File src= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
//		Files.copy(src, new File("C:/Users/varun.paranganath/Desktop/TEST Data Bkp/error.png"));
	

}
}
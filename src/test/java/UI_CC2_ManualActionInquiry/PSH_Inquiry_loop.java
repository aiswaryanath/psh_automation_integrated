package UI_CC2_ManualActionInquiry;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.record.PageBreakRecord.Break;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import Logger.LoggerUtils;
import Utilities.DBConnect;
import Utilities.HandleWebElements;
import Utilities.TakeScreenShot_evidence;

public class PSH_Inquiry_loop {
	static Logger log = LoggerUtils.getLogger();
	public static void Inquiry(WebDriver driver ,String Workitemid) throws Exception {
		Thread.sleep(10000);
		driver.switchTo().defaultContent();
//		driver.switchTo().frame("TabFrame");
//		driver.findElement(By.xpath("//*[text()='Inquiries & Reports']")).click();
//		driver.switchTo().defaultContent();
		
		driver.switchTo().frame("MenuFrame");
//		driver.findElement(By.xpath("//*[@id='img_mnuWFMNU_ST1_MD1_SM1_phase2wire']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[text()='Inquiries']")).click();
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//*[text()='File Inquiry']")).click();
		driver.switchTo().defaultContent();
		
		Thread.sleep(10000);
		
		driver.switchTo().frame("SearchFrame");
		String currentFrameName2 = (String) ((JavascriptExecutor) driver)
				.executeScript("return window.frameElement.name");
		System.out.println("currentFrameName2" + currentFrameName2);
		
		//Click on CLIENT ID
		WebElement Search_WitemId = driver.findElement(By.xpath("(//*[@class='INQINP'])[last()-8]"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].value='0';", Search_WitemId);
		
		//Click on ADVANCED SEARCH
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("(//*[@id='advSearchImg'])[last()]")));
			
		//Click on LOGICAL
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("(//*[@id='S1_F17'])[last()]")));
		
		//SEND KEYS On WORKITEMID
		((JavascriptExecutor) driver).executeScript("arguments[0].value='"+Workitemid+"';", driver.findElement(By.xpath("(//*[@class = 'INQINP'])[last()]")));
		
		//CLICK ON SEARCH
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("(//*[text()='Search'])[last()]")));
		
		//AFTER CLICK ON SEARCH WAIT FOR 20 SEC
		Thread.sleep(10000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("(//*[text()='"+Workitemid+"'])[last()]")));
		
		//SWITCH TO NEW FILE INQUIRY LATEST WINDOW WITH THREE FILE  / TRANSACTION / FILE LEVEL DETAILS
		Thread.sleep(20000);
		Set<String> winHandles1 = driver.getWindowHandles();
		 for(String handle: winHandles1){
	            driver.switchTo().window(handle);
	            Thread.sleep(1000);
	            System.out.println("Title of the new window: " +	driver.getTitle());
	        }

		 //CLICK ON BATCH
		 Thread.sleep(20000);
//		 ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("(//*[text()='1'])[last()-1]")));
//		 HandleWebElements.SuccessfullClickJS(driver, "(//*[text()='1'])[last()]");
//		 driver.findElement(By.id("linkV14_R53")).click(); //added varun
		 HandleWebElements.SuccessfullClickJS(driver, "//a[@id='linkV14_R53']");
		 
		//SWITCH TO BATCH LEVEL DETAILS SCREEN
		 Thread.sleep(20000);
		Set<String> winHandles2 = driver.getWindowHandles();
		 for(String handle: winHandles2){
			    Thread.sleep(5000);
	            driver.switchTo().window(handle);
	            System.out.println("Title of the new window CHECK: " +	driver.getTitle());
	        }

//CLICK ON BATCH LEVEL DETAILS
		 Thread.sleep(25000);
		 
		 String Parent = driver.getWindowHandle();
		 
//		 Select sel=new Select(driver.findElement(By.xpath("//*[@aria-controls='result_tbl']")));
//		 sel.selectByValue("100");
		 
//		 driver.findElement(By.id("filter")).click();
		 HandleWebElements.SuccessfullClickJS(driver, "//button[@id='filter']");
		 
		 for (int i = 0; i < DBConnect._DBConnectPSHretroJYO(Workitemid).size(); i++) {
			 
			 String WORKITEMID = DBConnect._DBConnectPSHretroJYO(Workitemid).get(i);
			 
			 driver.findElement(By.name("nam_")).clear();
			 driver.findElement(By.name("nam_")).sendKeys(WORKITEMID);
			 
			 try{
				 Actions actions=new Actions(driver); 
				 actions.moveToElement(driver.findElement(By.xpath("(//*[text()='"+WORKITEMID+"'])[last()]"))).click().perform();
			 }catch (Exception e) {
				driver.findElement(By.xpath("(//*[text()='"+WORKITEMID+"'])[last()]")).click();
			}
			 
			 //CLICK ON BATCH EXCEPTION LVEL DETAILS
			Thread.sleep(20000);
			Set<String> winHandles3 = driver.getWindowHandles();
			 for(String handle: winHandles3){
		            driver.switchTo().window(handle);
		            Thread.sleep(1000);
		            System.out.println("Title of the new window: " +	driver.getTitle());
		        }
			 
//			 
			System.out.println("SWITCH TO TRANSACTION AND PAYMENT SCREEN");		 
			 
					//SWITCH TO TRANSACTION AND PAYMENT SCREEN
					Thread.sleep(30000);
					Set<String> winHandles5 = driver.getWindowHandles();
					 for(String handle: winHandles5){
				            driver.switchTo().window(handle);
				            Thread.sleep(2000);
				            if (driver.getTitle().equalsIgnoreCase("Label Entry Window")) {
				            	System.out.println("Inside loop!!");
								driver.switchTo().window(handle);
								break;
							}
				            System.out.println("Outside loop!!: " +	driver.getTitle());
				        }

					 //ADD SCRIPTS HERE
						WebElement frame_1 = driver.findElement(By.xpath("//div[@id='TAB_0-body']//iframe"));
						driver.switchTo().frame(frame_1);
						System.out.println("Parent Frame:"+ (String) ((JavascriptExecutor) driver).executeScript("return window.frameElement.name"));

						driver.switchTo().frame("IwMiddleFrame");
						System.out.println("IwMiddleFrame:"	+ (String) ((JavascriptExecutor) driver).executeScript("return window.frameElement.name"));
						
						//Click on Expand All / PAYMENT INFORMATION
						((JavascriptExecutor) driver).executeScript("arguments[0].click();",driver.findElement(By.xpath("//*[text()='Expand All']")));
						
						TakeScreenShot_evidence.Evidences(driver, "Payment_information");
						Thread.sleep(2000);
						
						System.out.println("Attribute Value::"+driver.findElement(By.id("P162943174-172305185")).getAttribute("Value"));
						
//						System.exit(0);
//						driver.quit();
						
						//CLICK ON ROUTING
						try{
							((JavascriptExecutor) driver).executeScript("arguments[0].click();",
									driver.findElement(By.xpath("(//*[@id='P162943174-TD231'])")));
							
							//Click on Expand All
							((JavascriptExecutor) driver).executeScript("arguments[0].click();",
									driver.findElement(By.xpath("(//*[@id='PEAL'])[last()-3]")));
							
							TakeScreenShot_evidence.Evidences(driver, "ROUTING");
							Thread.sleep(2000);
						}catch (Exception e) {
							System.out.println("Routing is Missing!!");
						}
						
						//CLICK ON DOWNSTREAM SYSTEM STATUS
						((JavascriptExecutor) driver).executeScript("arguments[0].click();",
								driver.findElement(By.xpath("(//*[text()='Downstream System Status'])")));
						
						//Click on Expand All
						((JavascriptExecutor) driver).executeScript("arguments[0].click();",
								driver.findElement(By.xpath("(//*[@id='PEAL'])[last()-2]")));
						Thread.sleep(2000);
						
						TakeScreenShot_evidence.Evidences(driver, "DOWNSTREAM_SYSTEM_STATUS");

						Thread.sleep(2000);
						//CLICK ON EXCEPTION
						((JavascriptExecutor) driver).executeScript("arguments[0].click();",
								driver.findElement(By.xpath("(//*[@id='P162943174-TD143'])")));
						
						//Click on Expand All
						((JavascriptExecutor) driver).executeScript("arguments[0].click();",
								driver.findElement(By.xpath("(//*[@id='PEAL'])[last()-1]")));
						
						TakeScreenShot_evidence.Evidences(driver, "EXCEPTION");

						Thread.sleep(2000);
						//CLICK ON USER REMARKS
						((JavascriptExecutor) driver).executeScript("arguments[0].click();",
								driver.findElement(By.xpath("(//*[@id='P162943174-TD176'])")));
						
						//Click on Expand All
						((JavascriptExecutor) driver).executeScript("arguments[0].click();",
								driver.findElement(By.xpath("(//*[@id='PEAL'])[last()]")));

						driver.switchTo().defaultContent();
						
						WebElement frame2 = driver.findElement(By.xpath("//div[@id='TAB_0-body']//iframe"));
						driver.switchTo().frame(frame2);
						System.out.println("Parent Frame:"+ (String) ((JavascriptExecutor) driver).executeScript("return window.frameElement.name"));

						driver.switchTo().frame("IwBottomFrame");
						System.out.println("IwBottomFrame:"
								+ (String) ((JavascriptExecutor) driver).executeScript("return window.frameElement.name"));
						
						TakeScreenShot_evidence.Evidences(driver, "USER_REMARKS");
						
				 //CLOSE THE TRANSACTION SCREEN
						((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("(//*[text()='Close'])[last()]")));
				 
				 driver.switchTo().window(Parent);
		}
		 

		// Click On Search WorkitemID

/*		((JavascriptExecutor) driver).executeScript("arguments[0].click();",
				driver.findElement(By.xpath("(//*[@class='x-grid3-hd-btn'])[last()-25]")));
		Thread.sleep(1000);

		WebElement ele = driver.findElement(By.xpath("(//*[@class='x-menu-list-item'])[last()-0]"));
		Actions ref = new Actions(driver);
		ref.moveToElement(ele);
		ref.click().build().perform();

		WebElement ele1 = driver.findElement(By.xpath("(//*[@class='x-menu-item-icon '])[last()-7]"));
		Actions ref1 = new Actions(driver);
		ref1.moveToElement(ele1);
		ref1.click().build().perform();

		Thread.sleep(3000);

		driver.findElement(By.xpath("(//*[contains(text(),'"+Workitemid+"')])[last()-1]")).click();

		driver.switchTo().defaultContent();

		Thread.sleep(10000);
		System.out.println("After Click on Work Item ID:" + driver.getTitle());

		Set<String> winHandles = driver.getWindowHandles();
		// Loop through all handles
		for (String handle : winHandles) {
			driver.switchTo().window(handle);
			Thread.sleep(5000);
			System.out.println("Title of the new window: " + driver.getTitle());
			if (driver.getTitle().equalsIgnoreCase("Label Entry Window")) {
				driver.switchTo().window(handle);
				System.out.println("Switch Successfull!!: " + driver.getTitle());
				System.out.println("Check Switch is Successfull:" + driver.getTitle());
				// IFrame_WK_TAB_270308
				Thread.sleep(10000);

				WebElement frame1 = driver.findElement(By.xpath("//div[@id='TAB_0-body']//iframe"));
				driver.switchTo().frame(frame1);
				System.out.println("Parent Frame:"
						+ (String) ((JavascriptExecutor) driver).executeScript("return window.frameElement.name"));

				driver.switchTo().frame("IwMiddleFrame");
				System.out.println("IwMiddleFrame:"
						+ (String) ((JavascriptExecutor) driver).executeScript("return window.frameElement.name"));
				
				//Click on Expand All
				((JavascriptExecutor) driver).executeScript("arguments[0].click();",
						driver.findElement(By.xpath("(//*[@id='PEAL'])[last()-4]")));
				
				TakeScreenShot_evidence.Evidences(driver, "Payment_information");
				
//				driver.findElement(By.id("P162943174-357506513")).click();
//				
//				Robot robo=new Robot();
//				for (int i = 0; i < 10; i++) {
//					robo.keyPress(KeyEvent.VK_DOWN);
////					robo.keyRelease(KeyEvent.VK_DOWN);
//					Thread.sleep(2000);
//				}
				
				//CLICK ON ROUTING
				((JavascriptExecutor) driver).executeScript("arguments[0].click();",
						driver.findElement(By.xpath("(//*[@id='P162943174-TD231'])")));
				
				//Click on Expand All
				((JavascriptExecutor) driver).executeScript("arguments[0].click();",
						driver.findElement(By.xpath("(//*[@id='PEAL'])[last()-3]")));
				
				TakeScreenShot_evidence.Evidences(driver, "ROUTING");
				Thread.sleep(2000);
				
				//CLICK ON DOWNSTREAM SYSTEM STATUS
				((JavascriptExecutor) driver).executeScript("arguments[0].click();",
						driver.findElement(By.xpath("(//*[@id='P162943174-TD242'])")));
				
				//Click on Expand All
				((JavascriptExecutor) driver).executeScript("arguments[0].click();",
						driver.findElement(By.xpath("(//*[@id='PEAL'])[last()-2]")));
				Thread.sleep(2000);
				
				driver.findElement(By.id("P162943174-309768222")).sendKeys(SaltString.getRandNo(5));
				
				driver.findElement(By.id("P162943174-526740975")).sendKeys("01");
				
				driver.findElement(By.id("P162943174-1026198110")).sendKeys("20190501-1234-4321-abcd-190501105051");
				
				TakeScreenShot_evidence.Evidences(driver, "DOWNSTREAM_SYSTEM_STATUS");
				
				Thread.sleep(2000);
				//CLICK ON EXCEPTION
				((JavascriptExecutor) driver).executeScript("arguments[0].click();",
						driver.findElement(By.xpath("(//*[@id='P162943174-TD303'])")));
				
				//Click on Expand All
				((JavascriptExecutor) driver).executeScript("arguments[0].click();",
						driver.findElement(By.xpath("(//*[@id='PEAL'])[last()-1]")));
				
				TakeScreenShot_evidence.Evidences(driver, "EXCEPTION");
				
				Thread.sleep(2000);
				//CLICK ON USER REMARKS
				((JavascriptExecutor) driver).executeScript("arguments[0].click();",
						driver.findElement(By.xpath("(//*[@id='P162943174-TD336'])")));
				
				//Click on Expand All
				((JavascriptExecutor) driver).executeScript("arguments[0].click();",
						driver.findElement(By.xpath("(//*[@id='PEAL'])[last()]")));

				driver.findElement(By.id("P162943174-335820275")).sendKeys("Send Maker");

				driver.findElement(By.id("P162943174-565944470")).click();

				driver.switchTo().defaultContent();

				WebElement frame2 = driver.findElement(By.xpath("//div[@id='TAB_0-body']//iframe"));
				driver.switchTo().frame(frame2);
				System.out.println("Parent Frame:"+ (String) ((JavascriptExecutor) driver).executeScript("return window.frameElement.name"));

				driver.switchTo().frame("IwBottomFrame");
				System.out.println("IwBottomFrame:"
						+ (String) ((JavascriptExecutor) driver).executeScript("return window.frameElement.name"));
				
				TakeScreenShot_evidence.Evidences(driver, "USER_REMARKS");
				
//				((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.id("P162943174-524972874")));
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.id("P162943174-561275385")));
				
				Thread.sleep(2000);
				TakeScreenShot_evidence.Evidences(driver, "Error_Popup");
				
				driver.quit();
			}
		}
*/		
	}
}
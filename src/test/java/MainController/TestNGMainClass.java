package MainController;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.beust.testng.TestNG;

public class TestNGMainClass {
	public static String suite="SAMPLE";
	public static void main(String[] args) throws Exception {

		TestNG runner=new TestNG();
		 
		List<String> suitefiles=new ArrayList<String>();
		 
	
		suitefiles.add(System.getProperty("user.dir")+"\\src\\test\\resources\\"+"testng.xml");
	
		runner.setTestSuites(suitefiles);
		 
	
		runner.run();
		
}
}
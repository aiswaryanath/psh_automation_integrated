package MainController;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import PSHExtentReport.Reports.*;
import org.apache.log4j.Category;
import org.apache.log4j.Logger; 
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import Constants.TestParameters;
import Logger.LoggerUtils;
import Modules.Modules;
import UIOperations.Keywordoperations;
import Utilities.FileUtilities;
import Utilities.PropertyUtils;
import bsh.org.objectweb.asm.Constants;

public class Main_Executable {
	//public static Logger					logger			= Logger.getLogger("CBX");
	public static String suite="SUITE-BBD";
	public static String config="17925";
	private static ExtentReports extent;
	static ExtentTest logger;
	public static synchronized ExtentReports GetExtent(String suite,String config) {
	    if (extent != null) return extent;

	    extent = new ExtentReports (System.getProperty("user.dir") +"/test-output/STMExtentReport.html", true);
		//extent.addSystemInfo("Environment","Environment Name")
		extent
                .addSystemInfo("Suite Name", suite)
                .addSystemInfo("Environment", config)
                .addSystemInfo("User Name", "Aishwarya Nath");
		extent.loadConfig(new File(System.getProperty("user.dir")+"\\src\\test\\resources"+"\\extent-config.xml"));
	    return extent;
	    
	}
	public static void main(String[] args) throws Exception{
				Logger log = LoggerUtils.getLogger();
	    log.info("Start of Main Controller");
	    log.debug("debug message from logger");
	    System.out.println("");
		System.out.println("");
		System.out.println("******************Intellect Design Arena...<<Design For Digital>>*********************");
		System.out.println("");
		System.out.println("*****************Welcome To Integrated PSH Automation Tool*****************************-V 5.0");
		System.out.println("");
		System.out.println("Select environment for execution");
		System.out.println("");
		System.out.println("(1) - SIR17925");
		System.out.println("");
		System.out.println("(2) - SIR17929");
		System.out.println("");
		System.out.println("(3) - SIR18360");
		System.out.println("");
		System.out.println("(4) - SIR18713");
		System.out.println("");
		System.out.println("(5) - SIR18750");
		System.out.println("");
		System.out.println("(6) - DIT1");
		System.out.println("");
		System.out.println("(7) - DIT2");
		System.out.println("");
		System.out.println("(8) - PTE_CLOUD");
		System.out.println("");
		System.out.println("(9) - In case of new env");
		System.out.println("");
		System.out.println("(10) - SIR18076");
		System.out.println("");
		System.out.println("(11) - SIR17952");
		System.out.println("");
		System.out.println("(12) - SIR21481");
		System.out.println("");
		System.out.println("(13) - SIR19849");
		System.out.println("");
		System.out.println("Please enter the above keys for suite selection.");
		BufferedReader reader =new BufferedReader(new InputStreamReader(System.in));
		String _Op = reader.readLine(); 
		switch (_Op) {
		case "9":
			System.out.println("If new environment enter the created folder name in the project");
			BufferedReader reader1 =new BufferedReader(new InputStreamReader(System.in));
			String newf = reader1.readLine(); 
			config=newf;
			break;
		case "1":
			config="17925";
			break;
		case "2":
			config="17929";
			break;
		case "3":
			config="18360";
			break;
		case "4":
			config="18713";
			break;
		case "5":
			config="18750";
			break;
		case "6":
			config="DIT1";
			break;
		case "7":
			config="DIT2_DEV7";
			break;
		case "8":
			config="PTE_CLOUD";
			break;
		case "10":
			config="18076";
			break;
		case "11":
			config="17952";
			break;
		case "12":
			config="21481";
			break;
		case "13":
			config="19849";
			break;
		default:
			System.out.println("It seems you have entered invalid input...Please Try Again!!");
			Thread.sleep(10000);
			System.exit(0);
			break;
			
		
			
		}
		System.out.println("Select suite for execution");
		System.out.println("");
		System.out.println("(1) - SUITE-SANITY");
		System.out.println("");
		System.out.println("(2) - SUITE-PHASE2");
		System.out.println("");
		System.out.println("(3) - SUITE-FILEBATCHBVM");
		System.out.println("");
		System.out.println("(4) - SUITE-CC2E2E");
		System.out.println("");
		System.out.println("(5) - SUITE-CC2BVM");
		System.out.println("");
		System.out.println("(6) - SUITE-CC1GOW");
		System.out.println("");
		System.out.println("(7) - SUITE-CC1E2E-INT");
		System.out.println("");
		System.out.println("(8) - SUITE-CC1CTR");
		System.out.println("");
		System.out.println("(9) - SUITE-CC1AW");
		System.out.println("");
		System.out.println("(10) - SUITE-BULKE2E");
		System.out.println("");
		System.out.println("(11) - SUITE-BULKBVM");
		System.out.println("");
		System.out.println("(12) - SUITE-BTRE2E");
		System.out.println("");
		System.out.println("(13) - SUITE-BTRBVM");
		System.out.println("");
		System.out.println("(14) - SUITE-BBD");
		System.out.println("");
		System.out.println("(15) - SUITE-MA-ACH");
		System.out.println("");
		System.out.println("(16) - SUITE-MA-EFT");
		System.out.println("");
		System.out.println("(17) - SUITE-MA-WIRES");
		System.out.println("");
		System.out.println("(18) - SUITE-MA-CTR");
		System.out.println("");
		System.out.println("(19) - SUITE-MA-BET");
		System.out.println("");
		System.out.println("(20) - SUITE-MA-MINI");
		System.out.println("");
		System.out.println("Please enter the keys above keys for selection of suite.");
		BufferedReader reader3 =new BufferedReader(new InputStreamReader(System.in));
		String _Op1 = reader.readLine(); 
		switch (_Op1) {

		case "1":
			suite="SUITE-SANITY";
			break;
		case "2":
			suite="SUITE-PHASE2";
			break;
		case "3":
			suite="SUITE-FILEBATCHBVM";
			break;
		case "4":
			suite="SUITE-CC2E2E";
			break;
		case "5":
			suite="SUITE-CC2BVM";
			break;
		case "6":
			suite="SUITE-CC1GOW";
			break;
		case "7":
			suite="SUITE-CC1E2E-INT";
			break;
		case "8":
			suite="SUITE-CC1CTR";
			break;
		case "9":
			suite="SUITE-CC1AW";
			break;
		case "10":
			suite="SUITE-3BULKE2E";
			break;
		case "11":
			suite="SUITE-BULKBVM";
			break;
		case "12":
			suite="SUITE-BTRE2E";
			break;
		case "13":
			suite="SUITE-BTRBVM";
			break;
		case "14":
			suite="SUITE-BBD";
			break;
		case "15":
			suite="SUITE-MA-ACH";
			break;
		case "16":
			suite="SUITE-MA-EFT";
			break;
		case "17":
			suite="SUITE-MA-WIRES";
			break;
		case "18":
			suite="SUITE-MA-CTR";
			break;
		case "19":
			suite="SUITE-MA-BET";
			break;
		case "20":
			suite="SUITE-MA-MINI";
			break;

		default:
			System.out.println("It seems you have entered invalid input...Please Try Again!!");
			Thread.sleep(10000);
			System.exit(0);
			break;
			
		
			
		}
		System.out.println("Final suite:"+suite);
		System.out.println("Final environment:"+config);
	    FileUtilities.setFoldervalue(suite+"\\");
	    PropertyUtils.setFoldervalue(config);
	    TestParameters.setConfigValue(suite);
	    extent = GetExtent(suite,config);
	    //Logger logger = Logger.getLogger("Main_Controller.class");
	       String log4jConfigFile = System.getProperty("user.dir")+"\\src"+"\\test"+"\\resources"+ File.separator + "log4j.properties";
	        PropertyConfigurator.configure(log4jConfigFile);

//	*/
		Fillo fill=new Fillo();
		
		Connection conn=fill.getConnection(System.getProperty("user.dir")+"\\src"+"\\test"+"\\resources"+"\\TestData\\"+suite+"\\TestCase_Steps.xls");
		Recordset rs=conn.executeQuery("Select * from Main_Controller where RunStatus ='Yes'");
		while (rs.next()) {
				String _TestCases=rs.getField("TestCases");
			
				log.info("Testacases started:"+_TestCases);
				logger = extent.startTest(_TestCases);
				System.out.println("Testcases started:"+_TestCases);
				//String Process_1 = rs.getField("Process_1");
				String Process_2=rs.getField("Process_2");
				//String Process_3 = rs.getField("Process_3");
			//	if (Process_1.equalsIgnoreCase("BO_CIM")) {
				//	System.out.println("CIM BO Will Execute");
				//	log.info("In Process1");
				//}
				if (Process_2.equalsIgnoreCase("PSH")) {
					Modules mod=new Modules();
					Keywordoperations _Storevalue=new Keywordoperations();
					_Storevalue.StoreValue("VAR_TCNO",_TestCases);
				   System.out.println(_TestCases);
					mod.BO_CIM(_TestCases,suite,logger,extent);
					
			
				}
				//if (Process_3.equalsIgnoreCase("PSH")) {
				//	System.out.println("PSH Will Execute");
				//}
		}
		conn.close();
		
		try
		{
			runTestNGTest r=new runTestNGTest();
			r.createtestng(suite,config);
			  File excel = new File(System.getProperty("user.dir")+"\\src\\test\\resources\\TestData\\"+suite+"\\TestCase_Steps.xls");
			    FileInputStream fis = new FileInputStream(excel);
			    HSSFWorkbook book = new HSSFWorkbook(fis);
			    FileOutputStream fos = new FileOutputStream(System.getProperty("user.dir")+"\\src\\test\\resources\\TestData\\"+suite+"\\TestCase_Steps.xls");
			    book.setForceFormulaRecalculation(true);
			    book.write(fos);
			    fis.close();
			    fos.flush();
			    fos.close();
			    extent.flush();
			    extent.close();
			    
		}
		catch(Exception e)
		{
			
		}
	}
}

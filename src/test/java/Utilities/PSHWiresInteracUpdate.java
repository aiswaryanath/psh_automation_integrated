/***Class written for Wires  API call
 * written by Aishwarya Nath
 * 
 */

package Utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javafx.application.Application;
import com.google.gson.Gson;

import UIOperations.Keywordoperations;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class PSHWiresInteracUpdate {

	public String Status;
	public static Response response;

	static FileUtilities Fileutil = new FileUtilities();
	public static String POSTToAPI(String Body_Json,String Reqid) throws Exception{
		String status=null;
		try{
			String InteracUpdate = PropertyUtils.readProperty("InteracUpdate");
			RequestSpecBuilder builder = new RequestSpecBuilder();
			builder.setBody(Body_Json);
			builder.setContentType("application/json; charset=UTF-8");
			builder.addHeader("channelId", "CBX");
			builder.addHeader("requestId", Reqid);
			builder.addHeader("FLOW_ID", "B5");
			RequestSpecification request = builder.build();
			response = RestAssured.given().spec(request).when().post(InteracUpdate);
			Thread.sleep(5000);
			System.out.println(response.getStatusCode());
			status = response.body().asString();
			System.out.println("Response Body"+status);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}
	
	public String getresponse(String INTERACCODE,String TXNREFNUM) throws Exception{
		String Result_Status=null;
		try {


			String input1=null;

	        //if(api.equals("CONTRACT"))
	        {
	        	input1 = "{  \r\n  \"form\":{  \r\n     \"payload\":{  \r\n        \"ServicePayload\":[  \r\n           {  \r\n              \"InteracPaymentStatusCode\":\"#VAR1\",\r\n              \"TransactionRefNum\":\"#VAR2\",\r\n              \"NotificationEvent\":\"Event Name\",\r\n      \"ClrSysRef\":\"#VAR3\",\r\n      \"TxSts\":\"RJCT\",\r\n      \"RsnCd\":\"Reason Code\",\r\n      \"RsnDesc\":\"Reason Description\"\r\n           }\r\n        ]\r\n     }\r\n  }\r\n  }";
	        }

			
			Keywordoperations key=new Keywordoperations();
			String VAR_Reqid = key.getSaltString("Y", 10,"Reqid");
			System.out.println(VAR_Reqid);
			String clrsysref=key.getSaltString("Y", 7,"");
			FileUtilities FileUtilities=new FileUtilities();
			HashMap<String, String> Result1=new HashMap();   
			String TxnGowStatus="NA";   
			Keywordoperations _Storevalue=new Keywordoperations();
			String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
			String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
			String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
			String PSH_Query="select ACCTNG_REQ_ID,TXN_CLGSYSREF  FROM TXN_PROC_OPN WHERE WORKITEMID=?";
			String workitemid = FileUtilities.GetFilevalue("VAR_PSHTXNWORKITEMID");
			String clrdb=null;
			System.out.println("Workitmeid:"+workitemid);
			Connection connection=null;
			try{
				String query = PSH_Query;
				System.out.println(query);
				Class.forName("oracle.jdbc.driver.OracleDriver");
				connection=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
				PreparedStatement p = connection.prepareStatement(query);
				p.setString(1,workitemid);
				ResultSet _ResultSet=p.executeQuery();
			  while (_ResultSet.next()) {
					System.out.println("here1");


					TxnGowStatus= _ResultSet.getString("ACCTNG_REQ_ID");
					clrdb=_ResultSet.getString("TXN_CLGSYSREF");
				  System.out.println("ACCTNG_REQ_ID:"+TxnGowStatus);
				
				  
				}
			  _ResultSet.close();
			  connection.close();
			}catch (Exception e) {
				e.printStackTrace();
				  connection.close();
			}
			
	
			String CUSTREFNUM=	TxnGowStatus;

			String input2 = input1.replace("#VAR1",INTERACCODE);
			input2= input2.replace("#VAR2",CUSTREFNUM);
			if(clrdb != null && !clrdb.isEmpty())
			{
				clrsysref=clrdb;
			}
			input2= input2.replace("#VAR3",clrsysref);
			System.out.println("clrsysref" + clrsysref);
			System.out.println("clrdb" + clrdb);
			System.out.println("Final:" + input2);


			String Finalstatus=null;
		
			Finalstatus=POSTToAPI(input2,VAR_Reqid);
			if(Finalstatus.contains("Success"))
			{
				Result_Status="PASS";
				System.out.println("PASS");
			}


			
		}catch (Exception e) {
			e.printStackTrace();
			Result_Status="FAIL";
		}
		return Result_Status;
	}


	
	public static void main(String[] args) throws Exception{
		String Result_Status=null;
		try {


			String input1=null;

	        //if(api.equals("CONTRACT"))
	        {
	       // input1 = "{\"form\":{\"payload\":{\"ServicePayload\":[{\"InteracPaymentStatusCode\":\"#VAR1\",\"TransactionRefNum\":\"#VAR2\"}]}}}";
	        input1 = "{  \r\n  \"form\":{  \r\n     \"payload\":{  \r\n        \"ServicePayload\":[  \r\n           {  \r\n              \"InteracPaymentStatusCode\":\"#VAR1\",\r\n              \"TransactionRefNum\":\"#VAR2\",\r\n              \"NotificationEvent\":\"Event Name\",\r\n      \"ClrSysRef\":\"678900010\",\r\n      \"TxSts\":\"RJCT\",\r\n      \"RsnCd\":\"Reason Code\",\r\n      \"RsnDesc\":\"Reason Description\"\r\n           }\r\n        ]\r\n     }\r\n  }\r\n  }";
	        }

			
			Keywordoperations key=new Keywordoperations();
			String VAR_Reqid = key.getSaltString("Y", 10,"Reqid");
			System.out.println(VAR_Reqid);
		

			String input2 = input1.replace("#VAR1","CANCELLED");
			input2= input2.replace("#VAR2","QBJMSWKD4C");
		
			System.out.println("Final:" + input2);


			String Finalstatus=null;
		
			Finalstatus=POSTToAPI(input2,VAR_Reqid);
			if(Finalstatus.contains("Success"))
			{
				Result_Status="PASS";
				System.out.println("PASS");
			}


			
		}catch (Exception e) {
			e.printStackTrace();
			Result_Status="FAIL";
		}
	

	}
}


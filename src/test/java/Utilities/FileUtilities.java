package Utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import org.apache.log4j.Logger;
import Logger.LoggerUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import java.util.Set;

import javax.jws.soap.SOAPBinding.Style;

import Utilities.PropertyUtils;

  public class FileUtilities	{ 
	   static Logger log = LoggerUtils.getLogger();
		public static File file=null;
		
		public static String foldervalue="SUITE-MA-BET\\";
		
		public static String getFoldervalue() {
			return foldervalue;
		}

		public static void setFoldervalue(String foldervalue) {
			FileUtilities.foldervalue = foldervalue;
		}

		public static void ReadFileUtil()
		{
			try {
			String	 filepath = System.getProperty("user.dir")+"\\src\\test\\resources\\TestData\\"+foldervalue+"values.properties";	
			//System.out.println("File path value read:"+filepath);
			file=new File(filepath);
			//file=new File(PropertyUtils.readProperty("OpvalueFilepath"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.debug(e.toString(),e.getCause());
		}
		} 

		public  void writeUsingFileWriter(String data) 
		{
		        FileWriter fr = null;
		        try {
		        	//file=new File(PropertyUtils.readProperty("OpvalueFilepath"));
		    		String	 filepath =System.getProperty("user.dir")+"\\src\\test\\resources\\TestData\\"+foldervalue+"values.properties";		
		    		//System.out.println("File path value write:"+filepath);
					file=new File(filepath);
		            //if data grid then store entire value in one variable in same line
		            if((data.contains(PropertyUtils.readProperty("DataGridVar"))))
		            {	
		            	fr = new FileWriter(file);	
		            	fr.write(data);    
		            } 
		            else//if not data grid then have all variables in new line
		            {
		              fr = new FileWriter(file,true);	
		              fr.write("\r\n");
		              fr.write(data);              
		            }
		        } catch (IOException e) {
		            e.printStackTrace();
		        }finally{
		            //close resources
		            try {
		                fr.close();
		            } catch (IOException e) {
		                e.printStackTrace();
		            }
		        }
		    }
  		

		public void emptyFilecontents()
			{
				PrintWriter writer;
				try {
					writer = new PrintWriter(file);
					writer.print("");
					writer.close();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}

	//jyoti chNGD THIS TO STATIC ON 21ST FEB
		public static  String GetFilevalue(String propertyname)
		{
		     String value="";
		     String filepath="";
		 		try {
					//filepath = PropertyUtils.readProperty("OpvalueFilepath");
		 	       // filepath = System.getProperty("user.dir")+"\\Config\\values.properties";
		 			filepath =System.getProperty("user.dir")+"\\src\\test\\resources\\TestData\\"+foldervalue+"values.properties";	
		 			//System.out.println("File path value read2:"+filepath);
					file=new File(filepath);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		 		
			    try {
			    	
					FileReader file = new FileReader(filepath);
					Properties prop = new Properties();
					prop.load(file);
					propertyname=propertyname.trim();
					value = prop.getProperty(propertyname);
					
					if(value.equals(null))
					{
						value="No Such Variable";
					}
					}
			    catch(NullPointerException e)
			    {
			    	value="No Such Variable";
			    	
			    }
			    catch(FileNotFoundException e) 
				{
				  System.out.println("Exception caught: "+e.toString());	
				  
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				return value;			

		}		
  	
		public  void UpdateResult(String k,LinkedHashMap Result,String suite)
		{
			
			
			FileInputStream inputStream = null;
			FileOutputStream outputStream = null;
		//	Workbook workbook = null;
			boolean SetPreviousResult=false;
		
			try {
				 String filepath = System.getProperty("user.dir")+"\\src\\test\\resources\\TestData\\"+suite+"\\TestCase_Steps.xls";	
				File file =    new File(filepath);
			
			//FileInputStream inputStream = new FileInputStream(file);
			Workbook _Workbook1 = null;
			inputStream = new FileInputStream(file);
			_Workbook1 = new HSSFWorkbook(inputStream);
				
				
			
				Sheet sheetName = _Workbook1.getSheet("PSH");
				
				// Set<String> keys = Result.keySet();
				/*for(String k:keys && k==rownum){*/
					String rowval=k;
					String res=Result.get(k).toString();
				Row row=sheetName.getRow(Integer.parseInt(rowval));
				
				/*
				   CellStyle backgroundStyle = _Workbook1.createCellStyle();

				    backgroundStyle.setFillBackgroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
				    backgroundStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);*/
				    
				  /*  XSSFCellStyle style = workbook.createCellStyle();
				    style.setFillForegroundColor(IndexedColors.RED.getIndex());*/
				   
				
				
				//CellStyle style = sheetName.getWorkbook().createCellStyle();
			//	style.setFillPattern();
			
				Cell cell1 = row.createCell(7);		
				
						/*if (res.equalsIgnoreCase("PASS")) {
							
							style.setFillBackgroundColor(IndexedColors.GREEN.getIndex());
							style.setFillForegroundColor(IndexedColors.GREEN.getIndex());
							style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
						//style.setFillForegroundColor(IndexedColors.GREEN.getIndex());
							
						//} else if (res.equals("FAIL")) {
						} else if (res.contains("FAIL")) {
							style.setFillForegroundColor(IndexedColors.RED.getIndex());
							style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
						}
						else if (res.equals("NA"))
						{	style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
						style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
						}*/
				
				
						
				     //   cell1.setCellStyle(style);
						cell1.setCellValue(res);	
						//Cell cell2 = row.createCell(7);
						/*if((StartSeleniumTesting.approvalCheck.get(operation))!=null)
						{
							cell2.setCellValue(StartSeleniumTesting.approvalCheck.get(operation));
						}*/
					
					//}
				/*}*/
				
				outputStream = new FileOutputStream(file);
				_Workbook1.write(outputStream);
				_Workbook1.close();
				inputStream.close();
				outputStream.close();
		}
			
			catch (Exception e) {
					System.out.println("Unable to update result");
					log.info("Unable to update result");
			} 
		}
		
		public  void StackTraceResult(String k,LinkedHashMap Result,String suite){
			FileInputStream inputStream = null;
			FileOutputStream outputStream = null;
		//	boolean SetPreviousResult=false;
			try {
				String filepath = System.getProperty("user.dir")+"\\src\\test\\resources\\TestData\\"+suite+"\\TestCase_Steps.xls";
					File file =    new File(filepath);
		//	File file =    new File("D:\\CBX_Automation\\TestData\\TestCase_Steps.xls");
			Workbook _Workbook1 = null;
			inputStream = new FileInputStream(file);
			_Workbook1 = new HSSFWorkbook(inputStream);
				Sheet sheetName = _Workbook1.getSheet("PSH");

			//	 Set<String> keys = Result.keySet();
			/*	for(String k:keys){*/
					String rowval=k;
					String res=Result.get(k).toString();
				Row row=sheetName.getRow(Integer.parseInt(rowval));

				Cell cell1 = row.createCell(8);		

				/*if (res.contains("STACKTRACE:")) {
							style.setFillForegroundColor(IndexedColors.RED.getIndex());
							style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
				}*/
				//cell1.setCellStyle(style);
				cell1.setCellValue(res);	
				//}
				outputStream = new FileOutputStream(file);
				_Workbook1.write(outputStream);
				_Workbook1.close();
				inputStream.close();
				outputStream.close();
		}
			catch (Exception e) {
					System.out.println("Unable to update stack trace");
					log.info("Unable to update stack trace");
			} 
		}
	
		public static void StackTraceResult_JSONBODY(String k,String Result){
			FileInputStream inputStream = null;
			FileOutputStream outputStream = null;
			try {
				 String filepath = System.getProperty("user.dir")+"\\TestData\\TestCase_Steps.xls";	
					File file =    new File(filepath);
			//File file =    new File("D:\\CBX_Automation\\TestData\\TestCase_Steps.xls");
			Workbook _Workbook1 = null;
			inputStream = new FileInputStream(file);
			_Workbook1 = new HSSFWorkbook(inputStream);
				Sheet sheetName = _Workbook1.getSheet("PSH");
					String rowval=k;
					String res=Result;
				Row row=sheetName.getRow(Integer.parseInt(rowval));
				Cell cell1 = row.createCell(9);		
				cell1.setCellValue(res);	
				outputStream = new FileOutputStream(file);
				_Workbook1.write(outputStream);
				_Workbook1.close();
				inputStream.close();
				outputStream.close();
			}
			catch (Exception e) {
					System.out.println("Unable to update stack trace");
			} 
		}
		
	public static void main(String[] args) {
		StackTraceResult_JSONBODY("10", "asdds");
	}		
}

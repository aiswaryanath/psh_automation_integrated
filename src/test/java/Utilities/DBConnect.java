package Utilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.testng.reporters.jq.Main;

public class DBConnect {

	public static String _DBConnect(String Excel_dataOperations) throws Exception{
		String Return_Operation = null;
		Connection connection=null;
		try{
			
			ResultSet _ResultSet=DBConnect.getResultString(connection, "E2E_AUTOMATION_TESTING",Excel_dataOperations);
		  while (_ResultSet.next()) {
			  Return_Operation = _ResultSet.getString("OPERATION_MODE");
		  }  
		  _ResultSet.close();
		  
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			connection.close();
			
		}
		return Return_Operation;
	}
	
	public static ResultSet _ResultSetDBConnect(String Excel_dataOperations,Connection connection) throws Exception{
		ResultSet _ResultSet = null;
		
		try{
			/*Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@10.10.9.26:1521:SIR13199","CIBC_IR_162_BASE","CIBC_IR_162_BASE");*/
			 //connection=DriverManager.getConnection("jdbc:oracle:thin:@10.10.9.26:1521:SIR13199","CIBC_IR_162_BASE","CIBC_IR_162_BASE");*/
			
			_ResultSet=DBConnect.getResultString(connection, "E2E_AUTOMATION_TESTING",Excel_dataOperations);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
		//	_ResultSet.close();
		}
		return _ResultSet;
	}

	public static ResultSet getResultString(final Connection connection, final String tableName,String Testcaseid) {
	     String query = "Select OPERATION_MODE,KEYNAME,TYPEOFATTRIBUTE from  "+tableName+" where OPERATION_MODE = '"+Testcaseid+"'";
	    return executeQuery(connection, query);
	}

	
	public static ResultSet getResultStringPSH(final Connection connection, final String tableName,String Testcaseid) {
	     String query = "select workitemid,file_status,message_id,file_name,creation_date,file_processing_date,FILE_SOURCE,INIT_PRTY_ID,CUST_ID from "+tableName+" where message_id = '"+Testcaseid+"'";
	    return executeQuery(connection, query);
	}
	
	private static final ResultSet executeQuery(final Connection connection, final String query) {
	    try {
	        return connection.createStatement().executeQuery(query);
	    } catch (SQLException e) {
	        throw new RuntimeException(e);
	    }
	}

/*	public static String GetXPATH(String KeywordOperation) throws Exception, SQLException {
		String XPATH_NAV = null;
		ResultSet rs1= DBConnect._ResultSetDBConnect(KeywordOperation,);
		while (rs1.next()) {
			XPATH_NAV = rs1.getString("KEYNAME");
			System.out.println(XPATH_NAV);
		}
		return XPATH_NAV;
	}*/
	
	public static ArrayList GetValues(String KeywordOperation,Connection connection) throws Exception, SQLException {
		ArrayList<String> _ObVal = new ArrayList<String>(); 
		String XPATH_NAV = null;
		String Return_Operation=null;
		String TYPEOFATTRIBUTE = null;
		ResultSet rs1= DBConnect._ResultSetDBConnect(KeywordOperation,connection);
		while (rs1.next()) {
			XPATH_NAV = rs1.getString("KEYNAME");
			Return_Operation= rs1.getString("OPERATION_MODE");
			TYPEOFATTRIBUTE = rs1.getString("TYPEOFATTRIBUTE");
			_ObVal.add(XPATH_NAV);
			_ObVal.add(Return_Operation);
			_ObVal.add(TYPEOFATTRIBUTE);
			System.out.println(XPATH_NAV);
		}
		rs1.close();
		return _ObVal;
	}

	public static String _GetWorkItemIDPSH_C(String Excel_dataOperations) throws Exception{
		String Return_Operation = null;
		
		try{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@10.200.12.2:1521:CIBCDEV3","DITPSH","DITPSH");
			ResultSet _ResultSet=DBConnect.getResultStringPSH(connection, "File_Proc_Opn",Excel_dataOperations);
		  while (_ResultSet.next()) {
			  Return_Operation = _ResultSet.getString("workitemid");
			  System.out.println(Return_Operation);
		  }
		  _ResultSet.close();
		  connection.close();
		}catch (Exception e) {
			e.printStackTrace();
			
		}
		return Return_Operation;
	}
	
	public static ResultSet getResultStringPSHRetro(final Connection connection, final String tableName,String Testcaseid) {
	     String query = "SELECT WORKITEMID, FILE_WORKITEM_ID, TXN_STATUS, TRN_STATUS_CODE from "+tableName+" where file_WORKITEM_ID = '"+Testcaseid+"'";
	    return executeQuery(connection, query);
	}
	
	public static ArrayList<String> _DBConnectPSHretroJYO(String Excel_dataOperations) throws Exception{
		String Return_Operation = null;
		ArrayList<String> arr=new ArrayList<String>(); 
		try{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@10.10.9.32:1521:SIR18362","PSH","PSHRetroJoyo");
			ResultSet _ResultSet=DBConnect.getResultStringPSHRetro(connection, "TXN_PROC_OPN",Excel_dataOperations);
		  while (_ResultSet.next()) {
			  Return_Operation = _ResultSet.getString("WORKITEMID");
			  arr.add(Return_Operation);
		  }  
		}catch (Exception e) {
			e.printStackTrace();
		}
		return arr;
	}
	
//public static void main(String[] args) throws Exception{
//	System.out.println(_DBConnectPSHretroJYO("8044081").size());
//}	
}

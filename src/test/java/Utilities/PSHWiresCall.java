/***Class written for PSH-C BET API call
 * written by Aishwarya Nath
 * 
 */

package Utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javafx.application.Application;
import com.google.gson.Gson;

import UIOperations.Keywordoperations;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class PSHWiresCall {

	public String Status;
	public static Response response;
	public static String VDate="VAR_VDATE";
	public static String CURRDate="VAR_CURRDATE";
	public static String POSTToAPI(String Body_Json) throws Exception{
		String status=null;
		try{
			String WiresURL = PropertyUtils.readProperty("WiresURL");
			RequestSpecBuilder builder = new RequestSpecBuilder();
			builder.setBody(Body_Json);
			builder.setContentType("application/json; charset=UTF-8");
			builder.addHeader("channelId", "CBX");
			RequestSpecification request = builder.build();
			response = RestAssured.given().spec(request).when().post(WiresURL);
			Thread.sleep(5000);
			System.out.println(response.getStatusCode());
			status = response.body().asString();
			System.out.println("Response Body"+status);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}
	
	public String getresponse(String Type,String ValueDate,String Dbtraccno,String TransitNo, String PanNo, String Fxid, String AcctType, String Ctrctid, String InstrucCurrency) throws Exception{
		String Result_Status=null;
		try {

	        String api=Type;
			String input1=null;
	        System.out.println(api);
	        if(api.equals("CONTRACT"))
	        {
	       // input1 = "{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T08:48:38\",\"Authstn\":{\"Prtry\":\"N\"},\"NbOfTxs\":\"1\",\"CtrlSum\":\"6963.51\",\"InitgPty\":{\"Nm\":\"Mary Collins\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"90460234\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"CMOBatchId0810\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"6963.51\",\"PmtTpInf\":{\"SvcLvl\":{\"Cd\":\"URGP\"},\"LclInstrm\":{\"Cd\":\"TRF\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"Mary Collins\",\"PstlAdr\":{\"Ctry\":\"CA\",\"AdrLine\":[\"181-University Ave\",\"Suite-32012\",\"Toronot, ON, M5P 8W7, CA\"]},\"Id\":{\"OrgId\":{\"AnyBIC\":\"CIBCCATAXXX\",\"Othr\":{\"Id\":\"CMOOrigIdOptional\"}}}},\"DbtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"#VAR5\"}},\"Ccy\":\"USD\",\"Nm\":\"Mary CAD Account\"},\"DbtrAgt\":{\"FinInstnId\":{\"BICFI\":\"CIBCCATAXXX\",\"Nm\":\"John Mullens\",\"PstlAdr\":{\"Ctry\":\"CA\",\"AdrLine\":[\"55-Yonge\",\"Suite-1201\",\"Toronot, ON, M5K 8C3, CA\"]}},\"BrnchId\":{\"Id\":\"#VAR6\"}},\"ChrgsAcct\":{\"Id\":{\"Othr\":{\"Id\":\"004120101613\"}},\"Ccy\":\"CAD\"},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"#VAR4\"},\"Amt\":{\"EqvtAmt\":{\"Amt\":\"9004.51\",\"Ccy\":\"#VARCUR\",\"CcyOfTrf\":\"EUR\"}},\"XchgRateInf\":{\"XchgRate\":\"1.2931\",\"RateTp\":\"AGRD\",\"CtrctId\":\"#VAR9\"},\"ChrgBr\":\"DEBT\",\"IntrmyAgt1\":{\"FinInstnId\":{\"BICFI\":\"CITIINBXXXX\"}},\"CdtrAgt\":{\"FinInstnId\":{\"BICFI\":\"AARBDE5W100\",\"ClrSysMmbId\":{\"ClrSysId\":{\"Cd\":\"INFSC\"},\"MmbId\":\"AARBDE5W100\"},\"Nm\":\"AAREAL Bank\",\"PstlAdr\":{\"Ctry\":\"DE\",\"AdrLine\":[\"THE ARCADE\",\"WORLD TRADE CENTRE CUFFE PARADE\",\"COLABA MUMBAI 400 005\"]}}},\"Cdtr\":{\"Nm\":\"Jason Vale\",\"PstlAdr\":{\"Ctry\":\"DE\",\"AdrLine\":[\"Bungalow No 21\",\"West Coast Mumbai\",\"Worli, Mumbai 400 080\"]}},\"CdtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"2229021\"}}},\"RmtInf\":{\"Ustrd\":\"Remitance Info Line01 DataXXXXXXX01Remitance Info Line02 DataXXXXXXX02Remitance Info Line03 DataXXXXXXX03Remitance Info Line04 DataXXXXXXX04\"},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"A\",\"PymtAmt\":\"6963.51\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"#VAR7\",\"FXId\":\"#VAR8\",\"PmtRlsDt\":\"#VARRD\"}}}}}}";
	    		input1 ="{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T13:48:38\",\"Authstn\":{\"Prtry\":\"N\"},\"NbOfTxs\":\"1\",\"CtrlSum\":\"6963.51\",\"InitgPty\":{\"Nm\":\"Mary Collins\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"90460543\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"CMOBatchId0810\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"6963.51\",\"PmtTpInf\":{\"SvcLvl\":{\"Cd\":\"URGP\"},\"LclInstrm\":{\"Cd\":\"TRF\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"Mary Collins\",\"PstlAdr\":{\"Ctry\":\"CA\",\"AdrLine\":[\"181-University Ave\",\"Suite-32012\",\"Toronot, ON, M5P 8W7, CA\"]},\"Id\":{\"OrgId\":{\"AnyBIC\":\"CIBCCATAXXX\",\"Othr\":{\"Id\":\"CMOOrigIdOptional\"}}}},\"DbtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"#VAR4\"}},\"Ccy\":\"USD\",\"Nm\":\"Mary CAD Account\"},\"DbtrAgt\":{\"FinInstnId\":{\"BICFI\":\"CIBCCATAXXX\",\"Nm\":\"John Mullens\",\"PstlAdr\":{\"Ctry\":\"CA\",\"AdrLine\":[\"55-Yonge\",\"Suite-1201\",\"Toronot, ON, M5K 8C3, CA\"]}},\"BrnchId\":{\"Id\":\"#VAR5\"}},\"ChrgsAcct\":{\"Id\":{\"Othr\":{\"Id\":\"004120101613\"}},\"Ccy\":\"CAD\"},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"#VAR6\"},\"Amt\":{\"EqvtAmt\":{\"Amt\":\"9004.51\",\"Ccy\":\"#VARCCY\",\"CcyOfTrf\":\"EUR\"}},\"XchgRateInf\":{\"XchgRate\":\"1.2931\",\"RateTp\":\"AGRD\",\"CtrctId\":\"#VAR9\"},\"ChrgBr\":\"DEBT\",\"IntrmyAgt1\":{\"FinInstnId\":{\"BICFI\":\"CITIINBXXXX\"}},\"CdtrAgt\":{\"FinInstnId\":{\"BICFI\":\"AARBDE5W100\",\"ClrSysMmbId\":{\"ClrSysId\":{\"Cd\":\"INFSC\"},\"MmbId\":\"AARBDE5W100\"},\"Nm\":\"AAREAL Bank\",\"PstlAdr\":{\"Ctry\":\"DE\",\"AdrLine\":[\"THE ARCADE\",\"WORLD TRADE CENTRE CUFFE PARADE\",\"COLABA MUMBAI 400 005\"]}}},\"Cdtr\":{\"Nm\":\"Jason Vale\",\"PstlAdr\":{\"Ctry\":\"DE\",\"AdrLine\":[\"Bungalow No 21\",\"West Coast Mumbai\",\"Worli, Mumbai 400 080\"]}},\"CdtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"2229021\"}}},\"RmtInf\":{\"Ustrd\":[\"Remitance Info Line01 DataXXXXXXX01Remitance Info Line02 DataXXXXXXX02Remitance Info Line03 DataXXXXXXX03Remitance Info Line04 DataXXXXXXX04\"]},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"#VARTYPE\",\"PymtAmt\":\"6963.51\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"#VAR7\",\"FXId\":\"#VAR8\",\"PmtRlsDt\":\"#VARRD\"}}}}}}";

	        }
	        if(api.equals("BANK"))
	        {
	        	input1 = "{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T13:48:38\",\"Authstn\":{\"Prtry\":\"N\"},\"NbOfTxs\":\"1\",\"CtrlSum\":\"6963.51\",\"InitgPty\":{\"Nm\":\"Mary Collins\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"90460543\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"CMOBatchId0810\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"6963.51\",\"PmtTpInf\":{\"SvcLvl\":{\"Cd\":\"URGP\"},\"LclInstrm\":{\"Cd\":\"TRF\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"Mary Collins\",\"PstlAdr\":{\"Ctry\":\"CA\",\"AdrLine\":[\"181-University Ave\",\"Suite-32012\",\"Toronot, ON, M5P 8W7, CA\"]},\"Id\":{\"OrgId\":{\"AnyBIC\":\"CIBCCATAXXX\",\"Othr\":{\"Id\":\"CMOOrigIdOptional\"}}}},\"DbtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"#VAR4\"}},\"Ccy\":\"USD\",\"Nm\":\"Mary CAD Account\"},\"DbtrAgt\":{\"FinInstnId\":{\"BICFI\":\"CIBCCATAXXX\",\"Nm\":\"John Mullens\",\"PstlAdr\":{\"Ctry\":\"CA\",\"AdrLine\":[\"55-Yonge\",\"Suite-1201\",\"Toronot, ON, M5K 8C3, CA\"]}},\"BrnchId\":{\"Id\":\"#VAR5\"}},\"ChrgsAcct\":{\"Id\":{\"Othr\":{\"Id\":\"004120101613\"}},\"Ccy\":\"CAD\"},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"#VAR6\"},\"Amt\":{\"EqvtAmt\":{\"Amt\":\"9004.51\",\"Ccy\":\"#VARCCY\",\"CcyOfTrf\":\"EUR\"}},\"ChrgBr\":\"DEBT\",\"IntrmyAgt1\":{\"FinInstnId\":{\"BICFI\":\"CITIINBXXXX\"}},\"CdtrAgt\":{\"FinInstnId\":{\"BICFI\":\"AARBDE5W100\",\"ClrSysMmbId\":{\"ClrSysId\":{\"Cd\":\"INFSC\"},\"MmbId\":\"AARBDE5W100\"},\"Nm\":\"AAREAL Bank\",\"PstlAdr\":{\"Ctry\":\"DE\",\"AdrLine\":[\"THE ARCADE\",\"WORLD TRADE CENTRE CUFFE PARADE\",\"COLABA MUMBAI 400 005\"]}}},\"Cdtr\":{\"Nm\":\"Jason Vale\",\"PstlAdr\":{\"Ctry\":\"DE\",\"AdrLine\":[\"Bungalow No 21\",\"West Coast Mumbai\",\"Worli, Mumbai 400 080\"]}},\"CdtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"2229021\"}}},\"RmtInf\":{\"Ustrd\":[\"Remitance Info Line01 DataXXXXXXX01Remitance Info Line02 DataXXXXXXX02Remitance Info Line03 DataXXXXXXX03Remitance Info Line04 DataXXXXXXX04\"]},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"#VARTYPE\",\"PymtAmt\":\"6963.51\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"#VAR7\",\"FXId\":\"#VAR8\",\"PmtRlsDt\":\"#VARRD\"}}}}}}";	        
	        }
			
			Keywordoperations key=new Keywordoperations();
			String VAR_messageid = key.getSaltString("Y", 10,"PSHBETAUT");
			System.out.println(VAR_messageid);
			String VAR_E2Eid = key.getSaltString("Y", 10,"BETE2E");
			System.out.println(VAR_E2Eid);
			key.StoreValue("VAR_PSHBETWORKITEMID",VAR_messageid);
			String CurrentDate=key._DateFormat();
			System.out.println(CurrentDate);
			String ExecutionDate=ValueDate;
			String ExecDate=null;
			String ReleaseDate = null;
			if(ExecutionDate.equals("CBD"))
			{
				ExecDate=CurrentDate;
			}
			else
			{
				   String NoOfDays = ExecutionDate.substring(4);
				   int Days=Integer.parseInt(NoOfDays); 
				   ExecDate=AddDate(CurrentDate,Days);
			}
			
			ReleaseDate = SubDate(ExecDate,2);
			String input2 = input1.replace("#VAR1",VAR_messageid);
			input2= input2.replace("#VAR2",CurrentDate);
			input2= input2.replace("#VAR3",ExecDate);
			input2= input2.replace("#VAR6",VAR_E2Eid);
			input2= input2.replace("#VAR4",Dbtraccno);
			input2= input2.replace("#VAR5",TransitNo);
			input2= input2.replace("#VAR7",PanNo);
			input2= input2.replace("#VAR8",Fxid);
			input2= input2.replace("#VAR9",Ctrctid);
			input2= input2.replace("#VARTYPE",AcctType);
			input2= input2.replace("#VARCCY", InstrucCurrency);
			input2= input2.replace("#VARRD", ReleaseDate);
			System.out.println("Final:" + input2);


			String Finalstatus=null;
		
			Finalstatus=POSTToAPI(input2);
			if(Finalstatus.contains("\"GrpSts\":\"ACCP\""))
			{
				Result_Status="PASS";
				System.out.println("PASS");
			}


			
		}catch (Exception e) {
			e.printStackTrace();
			Result_Status="FAIL";
		}
		return Result_Status;
	}

	public String parseJson2(String jsonString2) throws JSONException {
		JSONObject obj = new JSONObject(jsonString2);
		String status = obj.getString("status");

		System.out.println("Status is: " + status);
		return status;
		
	}
	
	public static void main(String[] args) throws Exception{
		String Result_Status=null;
		String input1 ="{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T13:48:38\",\"Authstn\":{\"Prtry\":\"N\"},\"NbOfTxs\":\"1\",\"CtrlSum\":\"6963.51\",\"InitgPty\":{\"Nm\":\"Mary Collins\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"90460543\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"CMOBatchId0810\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"6963.51\",\"PmtTpInf\":{\"SvcLvl\":{\"Cd\":\"URGP\"},\"LclInstrm\":{\"Cd\":\"TRF\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"Mary Collins\",\"PstlAdr\":{\"Ctry\":\"CA\",\"AdrLine\":[\"181-University Ave\",\"Suite-32012\",\"Toronot, ON, M5P 8W7, CA\"]},\"Id\":{\"OrgId\":{\"AnyBIC\":\"CIBCCATAXXX\",\"Othr\":{\"Id\":\"CMOOrigIdOptional\"}}}},\"DbtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"#VAR4\"}},\"Ccy\":\"USD\",\"Nm\":\"Mary CAD Account\"},\"DbtrAgt\":{\"FinInstnId\":{\"BICFI\":\"CIBCCATAXXX\",\"Nm\":\"John Mullens\",\"PstlAdr\":{\"Ctry\":\"CA\",\"AdrLine\":[\"55-Yonge\",\"Suite-1201\",\"Toronot, ON, M5K 8C3, CA\"]}},\"BrnchId\":{\"Id\":\"#VAR5\"}},\"ChrgsAcct\":{\"Id\":{\"Othr\":{\"Id\":\"004120101613\"}},\"Ccy\":\"CAD\"},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"#VAR6\"},\"Amt\":{\"EqvtAmt\":{\"Amt\":\"9004.51\",\"Ccy\":\"#VARCUR\",\"CcyOfTrf\":\"EUR\"}},\"XchgRateInf\":{\"XchgRate\":\"1.2931\",\"RateTp\":\"AGRD\",\"CtrctId\":\"#VAR9\"},\"ChrgBr\":\"DEBT\",\"IntrmyAgt1\":{\"FinInstnId\":{\"BICFI\":\"CITIINBXXXX\"}},\"CdtrAgt\":{\"FinInstnId\":{\"BICFI\":\"AARBDE5W100\",\"ClrSysMmbId\":{\"ClrSysId\":{\"Cd\":\"INFSC\"},\"MmbId\":\"AARBDE5W100\"},\"Nm\":\"AAREAL Bank\",\"PstlAdr\":{\"Ctry\":\"DE\",\"AdrLine\":[\"THE ARCADE\",\"WORLD TRADE CENTRE CUFFE PARADE\",\"COLABA MUMBAI 400 005\"]}}},\"Cdtr\":{\"Nm\":\"Jason Vale\",\"PstlAdr\":{\"Ctry\":\"DE\",\"AdrLine\":[\"Bungalow No 21\",\"West Coast Mumbai\",\"Worli, Mumbai 400 080\"]}},\"CdtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"2229021\"}}},\"RmtInf\":{\"Ustrd\":[\"Remitance Info Line01 DataXXXXXXX01Remitance Info Line02 DataXXXXXXX02Remitance Info Line03 DataXXXXXXX03Remitance Info Line04 DataXXXXXXX04\"]},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"L\",\"PymtAmt\":\"6963.51\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"#VAR7\",\"FXId\":\"#VAR8\",\"PmtRlsDt\":\"#VARRD\"}}}}}}";
		//String input1 = "{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T07:48:38\",\"Authstn\":{\"Prtry\":\"N\"},\"NbOfTxs\":\"1\",\"CtrlSum\":\"6963.51\",\"InitgPty\":{\"Nm\":\"Mary Collins\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"90460234\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"CMOBatchId0810\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"6963.51\",\"PmtTpInf\":{\"SvcLvl\":{\"Cd\":\"URGP\"},\"LclInstrm\":{\"Cd\":\"TRF\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"Mary Collins\",\"PstlAdr\":{\"Ctry\":\"CA\",\"AdrLine\":[\"181-University Ave\",\"Suite-32012\",\"Toronot, ON, M5P 8W7, CA\"]},\"Id\":{\"OrgId\":{\"AnyBIC\":\"CIBCCATAXXX\",\"Othr\":{\"Id\":\"CMOOrigIdOptional\"}}}},\"DbtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"#VAR4\"}},\"Ccy\":\"USD\",\"Nm\":\"Mary CAD Account\"},\"DbtrAgt\":{\"FinInstnId\":{\"BICFI\":\"CIBCCATAXXX\",\"Nm\":\"John Mullens\",\"PstlAdr\":{\"Ctry\":\"CA\",\"AdrLine\":[\"55-Yonge\",\"Suite-1201\",\"Toronot, ON, M5K 8C3, CA\"]}},\"BrnchId\":{\"Id\":\"#VAR5\"}},\"ChrgsAcct\":{\"Id\":{\"Othr\":{\"Id\":\"004120101613\"}},\"Ccy\":\"CAD\"},\"CdtTrfTxInf\":[{\"PmtId\":{\"EndToEndId\":\"#VAR6\"},\"Amt\":{\"EqvtAmt\":{\"Amt\":\"9004.51\",\"Ccy\":\"USD\",\"CcyOfTrf\":\"EUR\"}},\"XchgRateInf\":{\"XchgRate\":\"1.2931\",\"RateTp\":\"AGRD\",\"CtrctId\":\"#VAR9\"},\"ChrgBr\":\"DEBT\",\"IntrmyAgt1\":{\"FinInstnId\":{\"BICFI\":\"CITIINBXXXX\"}},\"CdtrAgt\":{\"FinInstnId\":{\"BICFI\":\"AARBDE5W100\",\"ClrSysMmbId\":{\"ClrSysId\":{\"Cd\":\"INFSC\"},\"MmbId\":\"AARBDE5W100\"},\"Nm\":\"AAREAL Bank\",\"PstlAdr\":{\"Ctry\":\"DE\",\"AdrLine\":[\"THE ARCADE\",\"WORLD TRADE CENTRE CUFFE PARADE\",\"COLABA MUMBAI 400 005\"]}}},\"Cdtr\":{\"Nm\":\"Jason Vale\",\"PstlAdr\":{\"Ctry\":\"DE\",\"AdrLine\":[\"Bungalow No 21\",\"West Coast Mumbai\",\"Worli, Mumbai 400 080\"]}},\"CdtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"2229021\"}}},\"RmtInf\":{\"Ustrd\":\"Remitance Info Line01 DataXXXXXXX01Remitance Info Line02 DataXXXXXXX02Remitance Info Line03 DataXXXXXXX03Remitance Info Line04 DataXXXXXXX04\"},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"A\",\"PymtAmt\":\"6963.51\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"#VAR7\",\"FXId\":\"#VAR8\"}}}}}}";
		//String FullFillRTP = "{\"Document\":{\"CstmrCdtTrfInitn\":{\"GrpHdr\":{\"MsgId\":\"#VAR1\",\"CreDtTm\":\"#VAR2T08:59:10\",\"Authstn\":{\"Prtry\":\"Y\"},\"NbOfTxs\":\"1\",\"CtrlSum\":\"100\",\"InitgPty\":{\"Nm\":\"BILL PAY CEU Auto\",\"Id\":{\"OrgId\":{\"Othr\":{\"Id\":\"10000048\"}}}}},\"PmtInf\":[{\"PmtInfId\":\"ACHRMTBATCH1\",\"PmtMtd\":\"TRF\",\"NbOfTxs\":\"1\",\"CtrlSum\":\"100\",\"PmtTpInf\":{\"SvcLvl\":{\"Cd\":\"URGP\"},\"LclInstrm\":{\"Cd\":\"BTR\"}},\"ReqdExctnDt\":\"#VAR3\",\"Dbtr\":{\"Nm\":\"BILL PAY CEU Auto\",\"PstlAdr\":{\"StrtNm\":\"Times Square\",\"BldgNb\":\"22\",\"PstCd\":\"R1R1R1\",\"TwnNm\":\"Toronto\",\"CtrySubDvsn\":\"ON\",\"Ctry\":\"CA\",\"AdrLine\":[\"dsdsad\",\"sadasdsad\",\"sad\"]},\"Id\":{\"OrgId\":{\"AnyBIC\":\"CIBCCATAXXX\",\"Othr\":{\"Id\":\"1480000002\"}}}},\"DbtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"#VAR5\"}},\"Ccy\":\"INR\",\"Nm\":\"Abi\"},\"DbtrAgt\":{\"FinInstnId\":{\"BICFI\":\"CIBCCATAXXX\",\"Nm\":\"CIBC\",\"PstlAdr\":{\"Ctry\":\"CA\"},\"Othr\":{\"Id\":\"0010\"}},\"BrnchId\":{\"Id\":\"#VAR6\"}},\"CdtTrfTxInf\":[{\"PmtId\":{\"InstrId\":\"ACHREMIT\",\"EndToEndId\":\"#VAR4\"},\"Amt\":{\"InstdAmt\":{\"Ccy\":\"CAD\",\"Amt\":\"100\"}},\"XchgRateInf\":{\"XchgRate\":\"1.2931\",\"RateTp\":\"AGRD\",\"CtrctId\":\"213887431\"},\"ChrgBr\":\"DEBT\",\"IntrmyAgt1\":{\"FinInstnId\":{\"BICFI\":\"BUKBGB22\"}},\"CdtrAgt\":{\"FinInstnId\":{\"BICFI\":\"NWBKGB21\",\"ClrSysMmbId\":{\"ClrSysId\":{\"Cd\":\"USABA\"},\"MmbId\":\"011000015\"},\"Nm\":\"FEDERAL RESERVE BANK\",\"PstlAdr\":{\"StrtNm\":\"ELM ST\",\"BldgNb\":\"420\",\"PstCd\":\"1R1R1R\",\"TwnNm\":\"NEW YORK\",\"CtrySubDvsn\":\"NY\",\"Ctry\":\"US\",\"AdrLine\":[\"sasadaead\",\"dsfgs\",\"dsfsdf\"]}}},\"Cdtr\":{\"Nm\":\"SAM PVT LTD\",\"PstlAdr\":{\"StrtNm\":\"PISCATWAY\",\"BldgNb\":\"201\",\"PstCd\":\"12345\",\"TwnNm\":\"NEW YORK\",\"CtrySubDvsn\":\"NY\",\"Ctry\":\"US\",\"AdrLine\":[\"dsd\",\"sadlkl\",\"kllklk\"]},\"CtctDtls\":{\"EmailAdr\":\"abc123\",\"Othr\":\"en\"}},\"CdtrAcct\":{\"Id\":{\"Othr\":{\"Id\":\"123456789\"}}},\"RmtInf\":{\"Ustrd\":\"Remitance Info Line01\"},\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"StlmMth\":\"L\",\"PymtAmt\":\"6963.51\",\"AutoDep\":\"Yes\",\"RTPFlflmntInd\":\"Yes\",\"originatorFIRTPRefNum\":\"1231\",\"interacRTPRefNum\":\"12321\"}}}}]}],\"SplmtryData\":{\"Envlp\":{\"Cnts\":{\"PANID\":\"#VAR7\",\"FXId\":\"09612 CMO\",\"channelSeqId\":\"23451 09877\"}}}}}}";
		Keywordoperations key=new Keywordoperations();
		String VAR_messageid = key.getSaltString("Y", 10,"PSHWIRES");
		System.out.println(VAR_messageid);
		String VAR_E2Eid = key.getSaltString("Y", 10,"WIRESE2E");
		System.out.println(VAR_E2Eid);
		key.StoreValue("VAR_PSHBETWORKITEMID",VAR_messageid);
		key.StoreValue("VAR_CUSTREFNUM",VAR_E2Eid);
		String CurrentDate=key._DateFormat();
		System.out.println(CurrentDate);
		String ExecutionDate="CBD+3";
		String ExecDate=null;
		String ReleaseDate = null;
		if(ExecutionDate.equals("CBD"))
		{
			ExecDate=CurrentDate;
		}
		else
		{
			   String NoOfDays = ExecutionDate.substring(4);
			   int Days=Integer.parseInt(NoOfDays); 
			   ExecDate=AddDate(CurrentDate,Days);
		}
		
		ReleaseDate = SubDate(ExecDate,2);
		String input2 = input1.replace("#VAR1",VAR_messageid);
		input2= input2.replace("#VAR2",CurrentDate);
		input2= input2.replace("#VAR3",ExecDate);
		input2= input2.replace("#VAR4","2000033");
		input2= input2.replace("#VAR5","00021");
		input2= input2.replace("#VAR6",VAR_E2Eid);
		input2= input2.replace("#VAR7","2000000000000005");
		input2= input2.replace("#VAR8","FX20180001");
		input2= input2.replace("#VAR9","213887431");
		input2= input2.replace("#VARCUR", "CAD");
		input2= input2.replace("#VARRD", ReleaseDate);
		System.out.println("Final:" + input2);
		//Result_Status = POSTToAPI(input2);
		String Finalstatus=POSTToAPI(input2);
		if(Finalstatus.contains("\"GrpSts\":\"ACCP\""))
		{
			Result_Status="PASS";
			System.out.println("PASS");
		}

		}
	public static String AddDate(String CurrentDate,Integer NoOfDays){
		//Given Date in String format
		String CurrDate = CurrentDate;  
		System.out.println("Date before Addition: "+CurrDate);
		//Specifying date format that matches the given date
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		try{
		   //Setting the date to the given date
		   c.setTime(sdf.parse(CurrDate));
		}catch(ParseException e){
			e.printStackTrace();
		 }
		   
		//Number of Days to add
		c.add(Calendar.DAY_OF_MONTH,NoOfDays);  
		//Date after adding the days to the given date
		String newDate = sdf.format(c.getTime());  
		//Displaying the new Date after addition of Days
		System.out.println("Date after Addition: "+newDate);
		Keywordoperations _Storevalue=new Keywordoperations();
		_Storevalue.StoreValue(VDate, newDate);
		
		return newDate;
	   }

	public static String SubDate(String ExecutionDate,Integer SpotDays)
	{
	LocalDate date = LocalDate.parse(ExecutionDate);

	      System.out.println("LocalDate before"
	                      + " subtracting days: " + date);

	      LocalDate returnvalue = date.minusDays(SpotDays);  

	     
	System.out.println("ReleaseDate: "+returnvalue);

	return returnvalue.toString();
	}

	}


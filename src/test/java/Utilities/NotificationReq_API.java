package Utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import UIOperations.Keywordoperations;

public class NotificationReq_API {
	
	public static String ReplaceValueJSON(String VAR_PAYMENTREFNO,String VAR_PARTICIPANTPAYMENTREFNO,String VAR_MESSAGEID) throws Exception {
		String NotificationReq_API = PropertyUtils.readProperty("NotificationReq_API");
		String finalstring = NotificationReq_API
				.replace("VAR_PAYMENTREFNO", VAR_PAYMENTREFNO)
				.replace("VAR_PARTICIPANTPAYMENTREFNO", VAR_PARTICIPANTPAYMENTREFNO);
		return finalstring;
	}
	
	public static String getSaltString(String Flag, int _NumLength,String VAR_MESSAGEID)  throws Exception {
		String SALTCHARS = "1234567890";
		String Result = null;
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < _NumLength) {
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String saltStr = salt.toString();
		if (Flag.equals("Y")) {
			Result = VAR_MESSAGEID + saltStr;
		} else {
			Result = saltStr;
		}
		return Result;
	}

	public static String getSimpleDateFormat() throws Exception {
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'20:53:23.234'Z'");
		String dateAsISOString = df.format(date);
		System.out.println(dateAsISOString);
		return dateAsISOString;
	}
	
	public static void POST_TriggerInteracAPI(String TestData) throws Exception {
		String POST_TriggerInteracAPI = PropertyUtils.readProperty("POST_TriggerInteracAPI");
		try {
			String VAR_PAYMENTREFNO = Keywordoperations.get_DataValue(TestData).get(0);
			String VAR_PARTICIPANTPAYMENTREFNO = Keywordoperations.get_DataValue(TestData).get(1);
			String MESSAGE_ID = Keywordoperations.get_DataValue(TestData).get(2);
			
			URL url = new URL(POST_TriggerInteracAPI);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("x-et-api-signature", "CA000003");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("x-et-channel-indicator", "ONLINE");
			conn.setRequestProperty("x-et-participant-id", "CA000010");
			conn.setRequestProperty("x-et-request-id","GSPR_" + getSaltString("N", 5,MESSAGE_ID));
			conn.setRequestProperty("x-et-retry-indicator", "TRUE");
			conn.setRequestProperty("x-et-transaction-time",getSimpleDateFormat());
			conn.setRequestProperty("x-et-participant-user-id", "DEV40122");
			conn.setRequestProperty("X-et-api-signature-type","PAYLOAD_DIGEST_SHA256");

			String input = ReplaceValueJSON(VAR_PAYMENTREFNO,VAR_PARTICIPANTPAYMENTREFNO,MESSAGE_ID);

			System.out.println(input);
			OutputStream os = conn.getOutputStream();
			os.write(input.getBytes());
			os.flush();
			
			System.out.println(conn.getResponseCode());
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			String output;
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}
			conn.disconnect();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}	
	
	
	public static void POST_UpdateStatelessAPI(String TestData) throws Exception {
		String POST_TriggerInteracAPI = PropertyUtils.readProperty("POST_TriggerInteracAPI");
		try {
			String VAR_PAYMENTREFNO = Keywordoperations.get_DataValue(TestData).get(0);
			String VAR_PARTICIPANTPAYMENTREFNO = Keywordoperations.get_DataValue(TestData).get(1);
			String MESSAGE_ID = Keywordoperations.get_DataValue(TestData).get(2);
			
			URL url = new URL(POST_TriggerInteracAPI);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("x-et-api-signature", "CA000003");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("x-et-channel-indicator", "ONLINE");
			conn.setRequestProperty("x-et-participant-id", "CA000010");
			conn.setRequestProperty("x-et-request-id","GSPR_" + getSaltString("N", 5,MESSAGE_ID));
			conn.setRequestProperty("x-et-retry-indicator", "TRUE");
			conn.setRequestProperty("x-et-transaction-time",getSimpleDateFormat());
			conn.setRequestProperty("x-et-participant-user-id", "DEV40122");
			conn.setRequestProperty("X-et-api-signature-type","PAYLOAD_DIGEST_SHA256");

			String input = ReplaceValueJSON(VAR_PAYMENTREFNO,VAR_PARTICIPANTPAYMENTREFNO,MESSAGE_ID);

			System.out.println(input);
			OutputStream os = conn.getOutputStream();
			os.write(input.getBytes());
			os.flush();
			
			System.out.println(conn.getResponseCode());
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			String output;
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}
			conn.disconnect();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}	

	
	
public static void main(String[] args) throws Exception {
	//Below is working API for POST
	POST_TriggerInteracAPI("CA1MR291120191010|ACHRMd7a2d06c-9960-cb97-4d44-484389|DEV");
	
	//Working API for All other Templates
	POST_TriggerInteracAPI("sendmoneynotification|participant_user_id|CA1MR291120191010|ACHRMd7a2d06c-9960-cb97-4d44-484389|DEV|DIRECT_DEPOSIT_PENDING");
	
}
}

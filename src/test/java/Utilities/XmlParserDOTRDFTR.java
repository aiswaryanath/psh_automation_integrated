package Utilities;


import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XmlParserDOTRDFTR {



	public static void main(String argv[]) {

		//DOTR("Aishwarya","Midhun");
		DFTR("CAET201275941756","BOOK-COMPLETED-NAT","CAET201275941757","INFO-CANCELLED-0","CAET201275941758","INFO-CANCELLED-1");
	}
	
	public static void DOTR(String txnrefnum,String txnrefnum1) {

		   try {
			String filepath = System.getProperty("user.dir")+"\\upload\\ISOBulkDlyOutTrfRpt_CA000010_17000003_2020-05-05_110922.xml";
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(filepath);

			// Get the root element
			Node Document = doc.getFirstChild();
			System.out.println(Document.getNodeName());
			Node employee = doc.getElementsByTagName("ns3:NtryDtls").item(0);
			System.out.println(employee.getNodeName());
			Node txndtls = doc.getElementsByTagName("ns3:TxDtls").item(0);
			System.out.println(txndtls.getNodeName());
			Node refs = doc.getElementsByTagName("ns3:Refs").item(0);
			System.out.println(refs.getNodeName());
			
			 NodeList nodes = refs.getChildNodes();
			 
			  for (int i = 0; i < nodes.getLength(); i++) {
				  
	              Node element = nodes.item(i);

	              if ("ns3:InstrId".equals(element.getNodeName())) {
	                  element.setTextContent(txnrefnum);
	              }
			  }
			  
			  Node refs1 = doc.getElementsByTagName("ns3:Refs").item(1);
				System.out.println(refs1.getNodeValue()+refs1.getNodeName());
				
				 NodeList nodes1 = refs1.getChildNodes();
				 
				  for (int i = 0; i < nodes1.getLength(); i++) {
					  
		              Node element = nodes1.item(i);

		              if ("ns3:InstrId".equals(element.getNodeName())) {
		                  element.setTextContent(txnrefnum1);
		              }
				  }
			
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filepath));
			transformer.transform(source, result);

			System.out.println("Done");

		   } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		   } catch (TransformerException tfe) {
			tfe.printStackTrace();
		   } catch (IOException ioe) {
			ioe.printStackTrace();
		   } catch (SAXException sae) {
			sae.printStackTrace();
		   }
		}
	
	
	public static void DFTR(String txnrefnum,String Action,String txnrefnum1,String Action1,String txnrefnum2,String Action2) {

		   try {
			String filepath = System.getProperty("user.dir")+"upload\\ISOBulkDlyFnlzTrfRpt_CA000010_17000003_2020-05-05_020933.xml";
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(filepath);

			// Get the root element
			Node Document = doc.getFirstChild();
			System.out.println(Document.getNodeName());
			Node employee = doc.getElementsByTagName("ns3:NtryDtls").item(0);
			System.out.println(employee.getNodeName());
			Node txndtls = doc.getElementsByTagName("ns3:TxDtls").item(0);
			System.out.println(txndtls.getNodeName());
			
			
			if(txnrefnum!=null && !txnrefnum.isEmpty()&& Action!=null && !Action.isEmpty())
			  {	 
				  System.out.println(Action);
				  String[] Data = Action.split("-",0); 
				  String Sts =  Data[0];
			      String PrtryStatus = Data[1];
			      String cnclreason = Data[2];
			      Node refs2 = doc.getElementsByTagName("ns3:Refs").item(0);
				  Node Entry2 = doc.getElementsByTagName("ns3:Ntry").item(0);
				  Node Prtry2 = doc.getElementsByTagName("ns3:Prtry").item(2);
				  Node Envlp2 = doc.getElementsByTagName("BulkSndTrfFnlzRptRecSplmtryData").item(0);
				  Node Txndtls2 = doc.getElementsByTagName("ns3:TxDtls").item(1);
				   System.out.println(refs2.getNodeName());
				   System.out.println(Entry2.getNodeName());
					 NodeList nodes2 = refs2.getChildNodes();
					 NodeList entrynodes2=Entry2.getChildNodes();
					 NodeList Prtrynodes2=Prtry2.getChildNodes();
					 NodeList Envlpnodes2=Envlp2.getChildNodes();
					 NodeList Txndtlsnodes2=Txndtls2.getChildNodes();
					  for (int i = 0; i < nodes2.getLength(); i++) {
						  
			              Node element = nodes2.item(i);

			              if ("ns3:InstrId".equals(element.getNodeName())) {
			                  element.setTextContent(txnrefnum);
			              }
					  }
					  
					  	for (int i = 0; i < entrynodes2.getLength(); i++) {
						  
			              Node element = entrynodes2.item(i);

			              if ("ns3:Sts".equals(element.getNodeName())) {
			                  element.setTextContent(Sts);
			              }
					  }
					  	
					  	for (int i = 0; i < Prtrynodes2.getLength(); i++) {
							  
				              Node element = Prtrynodes2.item(i);

				              if ("ns3:Cd".equals(element.getNodeName())) {
				                  element.setTextContent(PrtryStatus);
				              }
						  }	
						for (int i = 0; i < Envlpnodes2.getLength(); i++) {
							  
				              Node element = Envlpnodes2.item(i);
				        
				              if ("CnclRsn".equals(element.getNodeName())) {
				 
				                  element.setTextContent(cnclreason);
				              }
						  }	
						if(cnclreason.equalsIgnoreCase("NA"))
						{	
						for (int i = 0; i < Txndtlsnodes2.getLength(); i++) {
							  
				              Node element = Txndtlsnodes2.item(i);
				        
				              if ("ns3:SplmtryData".equals(element.getNodeName())) {
				            	  Txndtls2.removeChild(element);
				              }
						  }	
						}
			  } 
			  //node item 1
			  if(txnrefnum1!=null && !txnrefnum1.isEmpty()&& Action1!=null && !Action1.isEmpty())
			  {	 
				  System.out.println(Action1);
				  System.out.println(txnrefnum1);
				  String[] Data = Action1.split("-",0); 
				  String Sts =  Data[0];
			      String PrtryStatus = Data[1];
			      String cnclreason = Data[2];
			      Node refs2 = doc.getElementsByTagName("ns3:Refs").item(1);
				  Node Entry2 = doc.getElementsByTagName("ns3:Ntry").item(1);
				  Node Prtry2 = doc.getElementsByTagName("ns3:Prtry").item(3);
				  Node Envlp2 = doc.getElementsByTagName("BulkSndTrfFnlzRptRecSplmtryData").item(1);
				  Node Txndtls2 = doc.getElementsByTagName("ns3:TxDtls").item(1);
				   System.out.println(refs2.getNodeName());
				   System.out.println(Entry2.getNodeName());
					 NodeList nodes2 = refs2.getChildNodes();
					 NodeList entrynodes2=Entry2.getChildNodes();
					 NodeList Prtrynodes2=Prtry2.getChildNodes();
					 NodeList Envlpnodes2=Envlp2.getChildNodes();
					 NodeList Txndtlsnodes2=Txndtls2.getChildNodes();
					  for (int i = 0; i < nodes2.getLength(); i++) {
						  
			              Node element = nodes2.item(i);

			              if ("ns3:InstrId".equals(element.getNodeName())) {
			                  element.setTextContent(txnrefnum1);
			              }
					  }
					  
					  	for (int i = 0; i < entrynodes2.getLength(); i++) {
						  
			              Node element = entrynodes2.item(i);

			              if ("ns3:Sts".equals(element.getNodeName())) {
			                  element.setTextContent(Sts);
			              }
					  }
					  	
					  	for (int i = 0; i < Prtrynodes2.getLength(); i++) {
							  
				              Node element = Prtrynodes2.item(i);

				              if ("ns3:Cd".equals(element.getNodeName())) {
				                  element.setTextContent(PrtryStatus);
				              }
						  }	
						for (int i = 0; i < Envlpnodes2.getLength(); i++) {
							  
				              Node element = Envlpnodes2.item(i);
				        
				              if ("CnclRsn".equals(element.getNodeName())) {
				 
				                  element.setTextContent(cnclreason);
				              }
						  }	
						if(cnclreason.equalsIgnoreCase("NA"))
						{	
						for (int i = 0; i < Txndtlsnodes2.getLength(); i++) {
							  
				              Node element = Txndtlsnodes2.item(i);
				        
				              if ("ns3:SplmtryData".equals(element.getNodeName())) {
				            	  Txndtls2.removeChild(element);
				              }
						  }	
						}
			  } 
				//node item 2
				  if(txnrefnum2!=null && !txnrefnum2.isEmpty()&& Action2!=null && !Action2.isEmpty())
				  {	  
					  System.out.println(Action2);
					  String[] Data = Action2.split("-",0); 
					  String Sts =  Data[0];
				      String PrtryStatus = Data[1];
				      String cnclreason = Data[2];
				  Node refs2 = doc.getElementsByTagName("ns3:Refs").item(2);
				  Node Entry2 = doc.getElementsByTagName("ns3:Ntry").item(2);
				  Node Prtry2 = doc.getElementsByTagName("ns3:Prtry").item(4);
				  Node Envlp2 = doc.getElementsByTagName("BulkSndTrfFnlzRptRecSplmtryData").item(2);
				  Node Txndtls2 = doc.getElementsByTagName("ns3:TxDtls").item(2);
				   System.out.println(refs2.getNodeName());
				   System.out.println(Entry2.getNodeName());
					 NodeList nodes2 = refs2.getChildNodes();
					 NodeList entrynodes2=Entry2.getChildNodes();
					 NodeList Prtrynodes2=Prtry2.getChildNodes();
					 NodeList Envlpnodes2=Envlp2.getChildNodes();
					 NodeList Txndtlsnodes2=Txndtls2.getChildNodes();
					  for (int i = 0; i < nodes2.getLength(); i++) {
						  
			              Node element = nodes2.item(i);

			              if ("ns3:InstrId".equals(element.getNodeName())) {
			                  element.setTextContent(txnrefnum2);
			              }
					  }
					  
					  	for (int i = 0; i < entrynodes2.getLength(); i++) {
						  
			              Node element = entrynodes2.item(i);

			              if ("ns3:Sts".equals(element.getNodeName())) {
			                  element.setTextContent(Sts);
			              }
					  }
					  	
					  	for (int i = 0; i < Prtrynodes2.getLength(); i++) {
							  
				              Node element = Prtrynodes2.item(i);

				              if ("ns3:Cd".equals(element.getNodeName())) {
				                  element.setTextContent(PrtryStatus);
				              }
						  }	
						for (int i = 0; i < Envlpnodes2.getLength(); i++) {
							  
				              Node element = Envlpnodes2.item(i);
				        
				              if ("CnclRsn".equals(element.getNodeName())) {
				 
				                  element.setTextContent(cnclreason);
				              }
						  }	
						if(cnclreason.equalsIgnoreCase("NA"))
						{	
						for (int i = 0; i < Txndtlsnodes2.getLength(); i++) {
							  
				              Node element = Txndtlsnodes2.item(i);
				        
				              if ("ns3:SplmtryData".equals(element.getNodeName())) {
				            	  Txndtls2.removeChild(element);
				              }
						  }	
						}
				  }
			
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filepath));
			transformer.transform(source, result);

			System.out.println("Done");

		   } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		   } catch (TransformerException tfe) {
			tfe.printStackTrace();
		   } catch (IOException ioe) {
			ioe.printStackTrace();
		   } catch (SAXException sae) {
			sae.printStackTrace();
		   }
		}
}
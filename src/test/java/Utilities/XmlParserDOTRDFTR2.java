package Utilities;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;

import Logger.LoggerUtils;
import UIOperations.Keywordoperations;
import Utilities.FTPExample;
public class XmlParserDOTRDFTR2 {
	private Channel channel;
	private static ChannelSftp sftpChannel;
	static Logger log = LoggerUtils.getLogger();
	public static void main(String argv[]) throws Exception {
        //String txnrefnum=GetTXNREFNUM("BULK6");
		//DOTR("CAET201285944756","NA");
		 //DFTRBOOK("CAET201275941773","BOOK-COMPLETED-1");
		//DFTRCANCEL("CAET201275941772","INFO-CANCELLED-1");
		//String s=filecopy("ISOBulkDlyFnlzTrfRpt_CA000010_17000003_2020-05-05_020933.xml");
		//System.out.println(s);
		runDOTR("17000003","BULK1","BULK1");
		//runDFTR("17000003","BULK6","INFO-CANCELLED-1");
//GetREBULKID();

//uploadfile("ISOBulkDlyFnlzTrfRpt_CA000010_17000003_2020-05-07_409799");
		//runACK("BULK4","NA","0-0-0");
	}
	
	public static void runDOTR(String Custid,String txnrefnum1,String txnrefnum2) throws Exception {
		String txnrefumber="NA";
        String txnrefnum=GetTXNREFNUM(txnrefnum1);
        if(!txnrefnum2.equalsIgnoreCase("NA"))

        {
        	txnrefumber=GetTXNREFNUM(txnrefnum2);
        }
		DOTR(Custid,txnrefnum,txnrefumber);
		 
	}
	
	public static void runACK(String txnrefnum1,String txnrefnum2,String Actionid) throws Exception {
		String txnrefumber="NA";
		String REBULKID="986766";
		String FCNNO="888";
        String txnrefnum=GetTXNREFNUM(txnrefnum1);
        if(txnrefnum2.equalsIgnoreCase("NA"))

        {
        	//txnrefumber=GetTXNREFNUM(txnrefnum2);
        	txnrefumber="9282828";
        }
        FileUtilities Fileutil=new  FileUtilities();
        String rebulkid=Fileutil.GetFilevalue("VAR_REBULKID");
        HashMap<String, String> handoffdata=GetREBULKID(rebulkid);
 
        REBULKID=handoffdata.get("REBULKID");
        FCNNO=handoffdata.get("FCNNO");
	    System.out.println("hasmap data:"+REBULKID+"   "+FCNNO);
	    log.info("hasmap data:"+REBULKID+"   "+FCNNO);
	  
		ACK(REBULKID,FCNNO,txnrefnum,txnrefumber);
		 
	}
	public static void runDFTR(String Custid,String txnrefnum1,String Action) throws Exception {

        String txnrefnum=GetTXNREFNUM(txnrefnum1);
        if(Action.startsWith("BOOK"))
        {	
        DFTRBOOK(Custid,txnrefnum,Action);
        }
        else
        {
        DFTRCANCEL(Custid,txnrefnum,Action);
        }
	}
	
	public static void DOTR(String Custid,String txnrefnum,String txnrefnum1) throws Exception {

		   try {
				String filename=filecopy(Custid,"ISOBulkDlyOutTrfRpt_CA000010_17000003_2020-01-15_111156.xml","DOTR");  
				 String filepath = System.getProperty("user.dir")+"\\src\\test\\resources\\upload\\DOTRDFTR\\"+filename+".xml";	
			
				//String filepath = "D:\\CBX_Automation\\upload\\DOTRDFTR\\"+filename+".xml";
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(filepath);

			// Get the root element
			Node Document = doc.getFirstChild();
			System.out.println(Document.getNodeName());
			Node employee = doc.getElementsByTagName("ns3:NtryDtls").item(0);
			System.out.println(employee.getNodeName());
			Node txndtls = doc.getElementsByTagName("ns3:TxDtls").item(0);
			System.out.println(txndtls.getNodeName());
			Node refs = doc.getElementsByTagName("ns3:Refs").item(0);
			System.out.println(refs.getNodeName());
			
			 NodeList nodes = refs.getChildNodes();
			 
			  for (int i = 0; i < nodes.getLength(); i++) {
				  
	              Node element = nodes.item(i);

	              if ("ns3:InstrId".equals(element.getNodeName())) {
	                  element.setTextContent(txnrefnum);
	              }
			  }
			  if(!txnrefnum1.isEmpty()||txnrefnum1!=null)
			  {	  
			  Node refs1 = doc.getElementsByTagName("ns3:Refs").item(1);
				System.out.println(refs1.getNodeValue()+refs1.getNodeName());
				
				 NodeList nodes1 = refs1.getChildNodes();
				 
				  for (int i = 0; i < nodes1.getLength(); i++) {
					  
		              Node element = nodes1.item(i);

		              if ("ns3:InstrId".equals(element.getNodeName())) {
		                  element.setTextContent(txnrefnum1);
		              }
				  }
			  }
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filepath));
			transformer.transform(source, result);

			System.out.println("Done");
			uploadfile(filename);
		   } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		   } catch (TransformerException tfe) {
			tfe.printStackTrace();
		   } catch (IOException ioe) {
			ioe.printStackTrace();
		   } catch (SAXException sae) {
			sae.printStackTrace();
		   }
		
		}
	
	public static void ACK(String REBULKID,String fcnno,String txnrefnum1,String txnrefnum2) throws Exception {
		 Keywordoperations key=new Keywordoperations();
		   try {
				String filename=filecopy(fcnno,"ISOBulkSndTrfAckPain_CA000010_2020-05-07_002303.xml","ACK");  
				 String filepath = System.getProperty("user.dir")+"\\src\\test\\resources\\upload\\DOTRDFTR\\"+filename+".xml";	
				//String filepath = "D:\\CBX_Automation\\upload\\DOTRDFTR\\"+filename+".xml";
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(filepath);

			// Get the root element
			Node Document = doc.getFirstChild();
			System.out.println(Document.getNodeName());
			Node GRPHDR = doc.getElementsByTagName("GrpHdr").item(0);
			System.out.println(GRPHDR.getNodeName());
			
			 NodeList nodes = GRPHDR.getChildNodes();
			 
			  for (int i = 0; i < nodes.getLength(); i++) {
				  
	              Node element = nodes.item(i);

	              if ("MsgId".equals(element.getNodeName())) {
	                  element.setTextContent(key.getSaltString("Y",6,"PSHBULK"));
	              }
			  }
		
			  Node refs1 = doc.getElementsByTagName("OrgnlGrpInfAndSts").item(0);
				System.out.println(refs1.getNodeName());
				
				 NodeList nodes1 = refs1.getChildNodes();
				 
				  for (int i = 0; i < nodes1.getLength(); i++) {
					  
		              Node element = nodes1.item(i);

		              if ("OrgnlMsgId".equals(element.getNodeName())) {
		                  element.setTextContent(REBULKID);
		              }
				  }
				  Node refs2 = doc.getElementsByTagName("TxInfAndSts").item(0);
					System.out.println(refs2.getNodeName());
					
					 NodeList nodes2 = refs2.getChildNodes();
					 
					  for (int i = 0; i < nodes2.getLength(); i++) {
						  
			              Node element = nodes2.item(i);

			              if ("OrgnlInstrId".equals(element.getNodeName())) {
			                  element.setTextContent(txnrefnum1);
			              }
					  }
					  Node refs3 = doc.getElementsByTagName("TxInfAndSts").item(1);
						System.out.println(refs3.getNodeName());
						
						 NodeList nodes3 = refs1.getChildNodes();
						 
						  for (int i = 0; i < nodes3.getLength(); i++) {
							  
				              Node element = nodes3.item(i);

				              if ("OrgnlInstrId".equals(element.getNodeName())) {
				                  //element.setTextContent(txnrefnum2);
				              }
						  }		  
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filepath));
			transformer.transform(source, result);

			System.out.println("Done");
			uploadfile(filename);
		   } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		   } catch (TransformerException tfe) {
			tfe.printStackTrace();
		   } catch (IOException ioe) {
			ioe.printStackTrace();
		   } catch (SAXException sae) {
			sae.printStackTrace();
		   }
		
		}
	
	public static void DFTRBOOK(String Custid,String txnrefnum,String Action) throws Exception {

		   try {
			String filename=filecopy(Custid,"ISOBulkDlyFnlzTrfRpt_CA000010_17000003_2020-05-05_020933.xml","dftr");  
			String filepath = System.getProperty("user.dir")+"\\src\\test\\resources\\upload\\DOTRDFTR\\"+filename+".xml";	
			//String filepath = "D:\\CBX_Automation\\upload\\DOTRDFTR\\"+filename+".xml";
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(filepath);

			// Get the root element
			Node Document = doc.getFirstChild();
			System.out.println(Document.getNodeName());
			Node employee = doc.getElementsByTagName("ns3:NtryDtls").item(0);
			System.out.println(employee.getNodeName());
			Node txndtls = doc.getElementsByTagName("ns3:TxDtls").item(0);
			System.out.println(txndtls.getNodeName());
			
			
			if(txnrefnum!=null && !txnrefnum.isEmpty()&& Action!=null && !Action.isEmpty())
			  {	 
				  System.out.println(Action);
				  String[] Data = Action.split("-",0); 
				  String Sts =  Data[0];
			      String PrtryStatus = Data[1];
			      String cnclreason = Data[2];
			      Node refs2 = doc.getElementsByTagName("ns3:Refs").item(0);
				  Node Entry2 = doc.getElementsByTagName("ns3:Ntry").item(0);
				  Node Prtry2 = doc.getElementsByTagName("ns3:Prtry").item(2);
			
				  Node Txndtls2 = doc.getElementsByTagName("ns3:TxDtls").item(0);
				   System.out.println(refs2.getNodeName());
				   System.out.println(Entry2.getNodeName());
					 NodeList nodes2 = refs2.getChildNodes();
					 NodeList entrynodes2=Entry2.getChildNodes();
					 NodeList Prtrynodes2=Prtry2.getChildNodes();
				
					 NodeList Txndtlsnodes2=Txndtls2.getChildNodes();
					  for (int i = 0; i < nodes2.getLength(); i++) {
						  
			              Node element = nodes2.item(i);

			              if ("ns3:InstrId".equals(element.getNodeName())) {
			                  element.setTextContent(txnrefnum);
			              }
					  }
					  
					  	for (int i = 0; i < entrynodes2.getLength(); i++) {
						  
			              Node element = entrynodes2.item(i);

			              if ("ns3:Sts".equals(element.getNodeName())) {
			                  element.setTextContent(Sts);
			              }
					  }
					  	
					  	for (int i = 0; i < Prtrynodes2.getLength(); i++) {
							  
				              Node element = Prtrynodes2.item(i);

				              if ("ns3:Cd".equals(element.getNodeName())) {
				                  element.setTextContent(PrtryStatus);
				              }
						  }	
					  	if(!Sts.equalsIgnoreCase("BOOK"))
					  	{	
					  	  Node Envlp2 = doc.getElementsByTagName("BulkSndTrfFnlzRptRecSplmtryData").item(0);
					  	  NodeList Envlpnodes2=Envlp2.getChildNodes();	
						for (int i = 0; i < Envlpnodes2.getLength(); i++) {
							  
				              Node element = Envlpnodes2.item(i);
				        
				              if ("CnclRsn".equals(element.getNodeName())) {
				 
				                  element.setTextContent(cnclreason);
				              }
						  }	
					  	}
						if(Sts.equalsIgnoreCase("BOOK"))
						{	
						for (int i = 0; i < Txndtlsnodes2.getLength(); i++) {
							  
				              Node element = Txndtlsnodes2.item(i);
				        
				              if ("ns3:SplmtryData".equals(element.getNodeName())) {
				            	  Txndtls2.removeChild(element);
				              }
						  }	
						}
			  } 


			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filepath));
			transformer.transform(source, result);

			System.out.println("Done");
			uploadfile(filename);

		   } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		   } catch (TransformerException tfe) {
			tfe.printStackTrace();
		   } catch (IOException ioe) {
			ioe.printStackTrace();
		   } catch (SAXException sae) {
			sae.printStackTrace();
		   }
		}
	
	public static void DFTRCANCEL(String Custid,String txnrefnum,String Action) throws Exception {

		   try {
			   String filename=filecopy(Custid,"ISOBulkDlyFnlzTrfRpt_CA000010_17000003_2020-05-05_020934.xml","dftr");  
				String filepath = System.getProperty("user.dir")+"\\src\\test\\resources\\upload\\DOTRDFTR\\"+filename+".xml";	
				//String filepath = "D:\\CBX_Automation\\upload\\DOTRDFTR\\"+filename+".xml";
				DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(filepath);

			// Get the root element
			Node Document = doc.getFirstChild();
			System.out.println(Document.getNodeName());
			Node employee = doc.getElementsByTagName("ns3:NtryDtls").item(0);
			System.out.println(employee.getNodeName());
			Node txndtls = doc.getElementsByTagName("ns3:TxDtls").item(0);
			System.out.println(txndtls.getNodeName());
			
			
			if(txnrefnum!=null && !txnrefnum.isEmpty()&& Action!=null && !Action.isEmpty())
			  {	 
				  System.out.println(Action);
				  String[] Data = Action.split("-",0); 
				  String Sts =  Data[0];
			      String PrtryStatus = Data[1];
			      String cnclreason = Data[2];
			      Node refs2 = doc.getElementsByTagName("ns3:Refs").item(1);
				  Node Entry2 = doc.getElementsByTagName("ns3:Ntry").item(1);
				  Node Prtry2 = doc.getElementsByTagName("ns3:Prtry").item(3);
			
				  Node Txndtls2 = doc.getElementsByTagName("ns3:TxDtls").item(1);
				   System.out.println(refs2.getNodeName());
				   System.out.println(Entry2.getNodeName());
					 NodeList nodes2 = refs2.getChildNodes();
					 NodeList entrynodes2=Entry2.getChildNodes();
					 NodeList Prtrynodes2=Prtry2.getChildNodes();
				
					 NodeList Txndtlsnodes2=Txndtls2.getChildNodes();
					  for (int i = 0; i < nodes2.getLength(); i++) {
						  
			              Node element = nodes2.item(i);

			              if ("ns3:InstrId".equals(element.getNodeName())) {
			                  element.setTextContent(txnrefnum);
			              }
					  }
					  
					  	for (int i = 0; i < entrynodes2.getLength(); i++) {
						  
			              Node element = entrynodes2.item(i);

			              if ("ns3:Sts".equals(element.getNodeName())) {
			                  element.setTextContent(Sts);
			              }
					  }
					  	
					  	for (int i = 0; i < Prtrynodes2.getLength(); i++) {
							  
				              Node element = Prtrynodes2.item(i);

				              if ("ns3:Cd".equals(element.getNodeName())) {
				                  element.setTextContent(PrtryStatus);
				              }
						  }	
					  	if(!Sts.equalsIgnoreCase("BOOK"))
					  	{	
					  	  Node Envlp2 = doc.getElementsByTagName("BulkSndTrfFnlzRptRecSplmtryData").item(0);
					  	  NodeList Envlpnodes2=Envlp2.getChildNodes();	
						for (int i = 0; i < Envlpnodes2.getLength(); i++) {
							  
				              Node element = Envlpnodes2.item(i);
				        
				              if ("CnclRsn".equals(element.getNodeName())) {
				 
				                  element.setTextContent(cnclreason);
				              }
						  }	
					  	}
					
			  } 


			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filepath));
			transformer.transform(source, result);

			System.out.println("Done");
			uploadfile(filename);
		   } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		   } catch (TransformerException tfe) {
			tfe.printStackTrace();
		   } catch (IOException ioe) {
			ioe.printStackTrace();
		   } catch (SAXException sae) {
			sae.printStackTrace();
		   }
		}
	
	
	
	public static String filecopy(String Custid,String Basefilename,String type) throws Exception {
		 Keywordoperations key=new Keywordoperations();
		 String CurrentDate=key._DateFormat();
		 String VAR_E2Eid = key.getSaltString("Y",6,""); 
		String filename="ISOBulkDlyFnlzTrfRpt_CA000010_"+Custid+"_";
		 if(type.equalsIgnoreCase("DOTR"))
		 {
			filename="ISOBulkDlyOutTrfRpt_CA000010_"+Custid+"_";
		 }
		 if(type.equalsIgnoreCase("ACK"))
		 {
			filename="ISOBulkSndTrfAckPain_CA000010_";
			VAR_E2Eid=Custid;
		 } 
		
		filename=filename+CurrentDate+"_"+VAR_E2Eid;
		 System.out.println(filename);
		 log.info(filename);
			try {
				 String filepath1 = System.getProperty("user.dir")+"\\src\\test\\resources\\upload\\DOTRDFTR";	
				 String filepath2 = System.getProperty("user.dir")+"\\src\\test\\resources\\upload\\BASE\\";	
				 String filepath3 = System.getProperty("user.dir")+"\\src\\test\\resources\\upload\\DOTRDFTR\\"+filename+".xml";	
				//FileUtils.cleanDirectory(new File("D:/CBX_Automation/upload/DOTRDFTR"));
				FileUtils.cleanDirectory(new File(filepath1));
				//FileReader fr = new FileReader("D:\\CBX_Automation\\upload\\BASE\\"+Basefilename);
				FileReader fr = new FileReader(filepath2+Basefilename);
				BufferedReader br = new BufferedReader(fr);
				//FileWriter fw = new FileWriter("D:\\CBX_Automation\\upload\\DOTRDFTR\\"+filename+".xml", true);
				FileWriter fw = new FileWriter(filepath3, true);
				String s;
	 
				while ((s = br.readLine()) != null) { // read a line
					fw.write(s); // write to output file
					fw.flush();
				}
				br.close();
				fw.close();
	                        System.out.println("file copied");
	                        log.info("filec copied");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return filename;
}
	
	public static String GetTXNREFNUM(String custrefnum) throws Exception{
		FileUtilities FileUtilities=new FileUtilities();
		String txnrefnum=null;
		String rebulkid=null;
	
		String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
		String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
		String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
		String fileworkitemid= FileUtilities.GetFilevalue("VAR_PSHFILEWORKITEMID");

		Statement stmt=null;

		try{
			String query= "Select txn_ref_num,IPS_TRN_REF from txn_proc_opn where file_workitem_id like '"+fileworkitemid+'%'+"'"+"and cust_ref_num like '"+custrefnum+"'";
			System.out.println(query);
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			stmt = connection.createStatement();

			  stmt.executeUpdate(query);
				ResultSet _ResultSet1=connection.createStatement().executeQuery(query);
				Keywordoperations _Storevalue=new Keywordoperations();

				while (_ResultSet1.next()) {
		
					   txnrefnum = _ResultSet1.getString("TXN_REF_NUM");
					   rebulkid= _ResultSet1.getString("IPS_TRN_REF");
						  System.out.println("Txnrefnum:"+txnrefnum);
						  log.info("Txnrefnum:"+txnrefnum);
						  if(!(rebulkid==null))
						  {
						  System.out.println("Rebulkid:"+rebulkid);
						  log.info("Rebulkid:"+rebulkid);
							_Storevalue.StoreValue("VAR_REBULKID",rebulkid);
						  }
					}
		  
		

		}catch (Exception e) {
			e.printStackTrace();
		}
		return txnrefnum;
	}
	
	public static HashMap<String, String> GetREBULKID(String txnrefnum2) throws Exception{
		FileUtilities FileUtilities=new FileUtilities();
		String status=null;
		String rebulkid=null;
		String fcnno=null;
		HashMap<String, String> handoffdata=new HashMap<String, String>();
				
	
	
		String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
		String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
		String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
		String fileworkitemid= FileUtilities.GetFilevalue("VAR_PSHFILEWORKITEMID");

		Statement stmt=null;
		Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
		
		try{
			String query= " select * from handoff_file_opn where hfo_file_workitemid IN "+"("+fileworkitemid+")"+" and hfo_rebulkid in"+"("+txnrefnum2+")";
			System.out.println(query);
			Class.forName("oracle.jdbc.driver.OracleDriver");
			stmt = connection.createStatement();

			  stmt.executeUpdate(query);
				ResultSet _ResultSet1=connection.createStatement().executeQuery(query);
		
				while (_ResultSet1.next()) {
		
					   status = _ResultSet1.getString("HFO_STATUS");
					   rebulkid = _ResultSet1.getString("HFO_REBULKID");
					   fcnno = _ResultSet1.getString("HFO_FCN");
					   handoffdata.put("REBULKID",rebulkid);
					   handoffdata.put("FILESTATUS",status);
					   handoffdata.put("FCNNO",fcnno);
						  System.out.println("status:"+status+"REBULKID:"+rebulkid+"FCNNO:"+fcnno);
						  log.info("status:"+status+"REBULKID:"+rebulkid+"FCNNO:"+fcnno);
					}
		  
		connection.close();
		stmt.close();

		}catch (Exception e) {
			e.printStackTrace();
			connection.close();
			stmt.close();

		}
		return handoffdata;
	}
	
	public static void uploadfile(String fileName) throws Exception {

		String localPath = System.getProperty("user.dir")+"\\src\\test\\resources\\upload\\DOTRDFTR\\";

		String remotePath=PropertyUtils.readProperty("DOTRDFTRremote");
		String host = PropertyUtils.readProperty("PSHAPP");
		String username=PropertyUtils.readProperty("PSHAPP_username");
		String pwd=PropertyUtils.readProperty("PSHAPP_Password");
	
	       // System.out.println("fileName:"+fileName);
			FTPExample ftp=new FTPExample(host,22,username,pwd);

			//FTPExample ftp = new FTPExample("10.11.10.60", 22, "SIR17929", "CC2Bulk@123#");
			ftp.upload(localPath+fileName+".xml", remotePath);
			
	}
}
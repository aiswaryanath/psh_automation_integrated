package Utilities;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class PropertyUtils {
	
	static String foldervalue="19849";

	public static String getFoldervalue() {
		return foldervalue;
	}

	public static void setFoldervalue(String foldervalue) {
		PropertyUtils.foldervalue = foldervalue;
	}

	public static String readProperty(String propertyname) throws IOException
	{
		FileReader fileread;
		String propertyvalue="default";
		String filepath="";
		
		 filepath = System.getProperty("user.dir")+"\\src\\test\\resources\\Config\\"+foldervalue+"\\config.properties";	
		// System.out.println("filepath of config"+filepath);
		 
				
		try {
			fileread = new FileReader(filepath);
			Properties prop = new Properties();
			prop.load(fileread);
			propertyvalue = prop.getProperty(propertyname);
		}catch(FileNotFoundException e) 
		{
		  System.out.println("Exception caught: "+e.toString());	
		  
		}
		
		return propertyvalue;
		
	}
	
	
	}
	


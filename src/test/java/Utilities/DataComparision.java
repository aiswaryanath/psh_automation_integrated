package Utilities;

	import java.io.IOException;
import java.sql.Connection;
	import java.sql.DriverManager;
	import java.sql.PreparedStatement;
	import java.sql.ResultSet;
	import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import UIOperations.Keywordoperations;

	public class DataComparision 
	{
			public static HashMap datacompare(String TYPE, String TESTCASEID, String FILENAME) throws IOException, SQLException, ClassNotFoundException
	{
		String type = TYPE;
		HashMap<String,String> h=new HashMap<String,String>();
		String custrefnum=null;
		String taskid=null;
		String queueid=null;
		String txnstatus=null;
		String testcaseid = TESTCASEID;
		String filename = FILENAME;
		String tableName = null;
		String day = "0";
		if ("PRE".equalsIgnoreCase(type)) {
			tableName = "AUT_PRECOMP_LHS";
		} else if ("POST".equalsIgnoreCase(type)) {
			tableName = "AUT_POSTCOMP_LHS";
		}
		
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		String result = "";
		
		StringBuffer StrQuery = new StringBuffer();
		
			
			String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
			String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
			String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection conn=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			
			StrQuery.append("(select tpo.cust_ref_num ,owi.taskid,owi.queueid,tpo.trn_status_code from txn_proc_opn tpo,ow_workitem_instance owi where tpo.workitemid=owi.workitemid and tpo.File_Workitem_Id in (select workitemid From File_Proc_Opn ");
			StrQuery.append("where file_name like '%" + filename + "%')  ");
			StrQuery.append("minus ");
			StrQuery.append("select CUST_REF_NUM,taskid,queueid,trn_status_code from "
					+ tableName
					+ " where testcaseid='"
					+ testcaseid+"')");
			
			
			
System.out.println(StrQuery);
			
			try {
			pstmt = conn.prepareStatement(StrQuery.toString());
			resultSet = pstmt.executeQuery();
		

			if (!resultSet.next()) {

			System.out.println("test case passed");
			} 
			else {

				System.out.println("test case failed");

				do {
			
			result = result + "\n"
					+ resultSet.getNString("cust_ref_num")
					+"  "
					+ resultSet.getNString("taskid")
					+"   "
					+ resultSet.getNString("trn_status_code") 
					+"   "
					+ resultSet.getNString("queueid");
		    custrefnum=resultSet.getNString("cust_ref_num");
		   // System.out.println(custrefnum);
			taskid=resultSet.getNString("taskid");
			//System.out.println(taskid);
			queueid=resultSet.getNString("queueid");
			//System.out.println(queueid);
			txnstatus=resultSet.getNString("trn_status_code");
			//System.out.println(txnstatus);
			String data=custrefnum+"  "+taskid+"  "+queueid+"  "+txnstatus;
			System.out.println(data);
			h.put(custrefnum,data);
			

		} while (resultSet.next());

		}

		
	}
		catch (Exception ex) 
			{

			ex.printStackTrace();
			if (resultSet != null)
				resultSet.close();

			if (pstmt != null)
				pstmt.close();

			if (conn != null)
				conn.close();

		}

			finally {
				
				if (resultSet != null)
					resultSet.close();

				if (pstmt != null)
					pstmt.close();

				if (conn != null)
					conn.close();
			}

	
	return h;

	}
			public static HashMap datacompareUsingFullQuery(String TYPE, String TESTCASEID, String FILENAME) throws IOException, SQLException, ClassNotFoundException
			{
				String type = TYPE;
				HashMap<String,String> h=new HashMap<String,String>();
				String custrefnum=null;
				String taskid=null;
				String queueid=null;
				String txnstatus=null;
				String testcaseid = TESTCASEID;
				String filename = FILENAME;
				String tableName = null;
				String day = "0";
				if ("PRE".equalsIgnoreCase(type)) {
					tableName = "AUT_PRECOMP_LHS";
				} else if ("POST".equalsIgnoreCase(type)) {
					tableName = "AUT_POSTCOMP_LHS";
				}
				
				PreparedStatement pstmt = null;
				ResultSet resultSet = null;
				String result = "";
				
				StringBuffer StrQuery = new StringBuffer();
				
					
					String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
					String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
					String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
					Class.forName("oracle.jdbc.driver.OracleDriver");
					Connection conn=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
					StrQuery.append("(select CUST_REF_NUM,taskid,queueid,trn_status_code from "
							+ tableName
							+ " where testcaseid='"
							+ testcaseid
							+ "' and day='" + day + "' ");
					StrQuery.append("minus ");
					StrQuery.append("select tpo.cust_ref_num ,owi.taskid,owi.queueid,tpo.trn_status_code from txn_proc_opn tpo,ow_workitem_instance owi where tpo.workitemid=owi.workitemid and tpo.File_Workitem_Id in (select workitemid From File_Proc_Opn ");
					StrQuery.append("where file_name like '%" + filename + "%') ");
					StrQuery.append("union ");
					StrQuery.append("(select tpo.cust_ref_num ,owi.taskid,owi.queueid,tpo.trn_status_code from txn_proc_opn tpo,ow_workitem_instance owi where tpo.workitemid=owi.workitemid and tpo.File_Workitem_Id in (select workitemid From File_Proc_Opn ");
					StrQuery.append("where file_name like '%" + filename + "%')  ");
					StrQuery.append("minus ");
					StrQuery.append("select CUST_REF_NUM,taskid,queueid,trn_status_code from "
							+ tableName
							+ " where testcaseid='"
							+ testcaseid+"'))");
					
					
					
		System.out.println(StrQuery);
					
					try {
					pstmt = conn.prepareStatement(StrQuery.toString());
					resultSet = pstmt.executeQuery();
				

					if (!resultSet.next()) {

					System.out.println("test case passed");
					} 
					else {

						System.out.println("test case failed");

						do {
					
					result = result + "\n"
							+ resultSet.getNString("cust_ref_num")
							+"  "
							+ resultSet.getNString("taskid")
							+"   "
							+ resultSet.getNString("trn_status_code") 
							+"   "
							+ resultSet.getNString("queueid");
				    custrefnum=resultSet.getNString("cust_ref_num");
				  //  System.out.println(custrefnum);
					taskid=resultSet.getNString("taskid");
				//	System.out.println(taskid);
					queueid=resultSet.getNString("queueid");
					//System.out.println(queueid);
					txnstatus=resultSet.getNString("trn_status_code");
				//	System.out.println(txnstatus);
					String data=custrefnum+"  "+taskid+"  "+queueid+"  "+txnstatus;
					System.out.println(data);
					h.put(custrefnum,data);
					

				} while (resultSet.next());

				}

				
			}
				catch (Exception ex) 
					{

					ex.printStackTrace();
					if (resultSet != null)
						resultSet.close();

					if (pstmt != null)
						pstmt.close();

					if (conn != null)
						conn.close();

				}

					finally {
						
						if (resultSet != null)
							resultSet.close();

						if (pstmt != null)
							pstmt.close();

						if (conn != null)
							conn.close();
					}

			
			return h;

			}
			public static HashMap datacompareusingCustRefnum(String TYPE, String TESTCASEID, String FILENAME) throws IOException, SQLException, ClassNotFoundException
			{
				ArrayList al = new ArrayList();
				String type = TYPE;
				HashMap<String,String> h=new HashMap<String,String>();
				String custrefnum=null;
				
				String testcaseid = TESTCASEID;
				String filename = FILENAME;
				String tableName = null;
				String day = "0";
				String RHS_taskid=null;
				String RHS_queueid=null;
				String RHS_transactionStatus=null;
				String data=null;
				String LHS_taskid=null;
				String LHS_queueid=null;
				String LHS_transactionStatus=null;
				String data2 =null;
				if ("PRE".equalsIgnoreCase(type)) {
					tableName = "AUT_PRECOMP_LHS";
				} else if ("POST".equalsIgnoreCase(type)) {
					tableName = "AUT_POSTCOMP_LHS";
				}
				
				PreparedStatement pstmt = null;
				PreparedStatement pstmt2 = null;
				PreparedStatement pstmt3 = null;
				ResultSet resultSet1 = null;
				ResultSet resultSet2 = null;
				ResultSet resultSet3 = null;
				String result = "";
				
				StringBuffer StrQuery = new StringBuffer();
				String query=null;
				String query2=null;
				String query3=null;
				
					String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
					String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
					String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
					Class.forName("oracle.jdbc.driver.OracleDriver");
					Connection conn=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
					
					query="select cust_ref_num from "+tableName+" where testcaseid='"+ testcaseid+"'";
					
					System.out.println(query);
					
					try {
					pstmt = conn.prepareStatement(query.toString());
					resultSet1 = pstmt.executeQuery();
				
					while(resultSet1.next())
		            {
		                 al.add(resultSet1.getString("cust_ref_num"));
		            
		            }
					System.out.println("cust ref numbers are  :"+al);
					
					for (int i = 0; i < al.size(); i++) 
					{
						custrefnum = (String) al.get(i);
					query2= "select owi.taskid,owi.queueid,tpo.trn_status_code" + 
							" from txn_proc_opn tpo,ow_workitem_instance owi" + 
							" where tpo.workitemid=owi.workitemid" + 
							" and tpo.CUST_REF_NUM like '%"+al.get(i)+"%'" + 
							" and tpo.File_Workitem_Id in" + 
							" (select workitemid From File_Proc_Opn where file_name like '%"+filename+"%')";
					System.out.println("Query for cust ref num "+al.get(i)+"is "+query2);
					pstmt2 = conn.prepareStatement(query2.toString());
					resultSet2 = pstmt2.executeQuery();
					while(resultSet2.next())
					{
					RHS_taskid=resultSet2.getNString("taskid");
					RHS_queueid=resultSet2.getNString("queueid");
					RHS_transactionStatus=resultSet2.getNString("trn_status_code");
					data=al.get(i)+" "+RHS_taskid+" "+RHS_queueid+" "+RHS_transactionStatus;
					System.out.println("Data from RHS for cust ref num:"+al.get(i)+ " ="+data);
					}
					query3="select taskid,queueid, trn_status_code from  "+tableName+"  where testcaseid like '%"+ testcaseid+"%' and cust_ref_num like '%"+al.get(i)+"%'";
					System.out.println("Query for LHS for cust ref num"+al.get(i)+"="+query3);
					pstmt3 = conn.prepareStatement(query3.toString());
					resultSet3 = pstmt3.executeQuery();
					while(resultSet3.next()) {
					LHS_taskid=resultSet3.getNString("taskid");
					LHS_queueid=resultSet3.getNString("queueid");
					LHS_transactionStatus=resultSet3.getNString("trn_status_code");
					data2 = LHS_taskid+" "+LHS_queueid+" "+LHS_transactionStatus;
					System.out.println("Data from LHS for cust_ref_num"+al.get(i)+"="+data2);
					}
					if(RHS_taskid.equals(LHS_taskid))
					{
						System.out.println("taskid is equal to LHS for cust_ref_num"+al.get(i));
						if(RHS_queueid.equals(LHS_queueid))
						{
							System.out.println("Queueid is equal to LHS for cust_ref_num"+al.get(i));
						
							if(RHS_transactionStatus.equals(LHS_transactionStatus))
							{
								System.out.println("transaction status is equal to LHS for cust_ref_num"+al.get(i));
						
							}
							else
							{
								System.out.println("transaction status is not equal to RHS for cust_ref_num"+al.get(i));
								h.put(custrefnum,data);
							}
						}
						else
						{
							System.out.println("Queueid is not equal to LHS for cust_ref_num"+al.get(i));
							h.put(custrefnum,data);
						}
					}
					else
					{
						System.out.println("taskid is not equal to LHS for cust_ref_num"+al.get(i));
						h.put(custrefnum,data);
					}
					//for (int i=0; i<)
					
				/*	if (!resultSet.next()) {

					System.out.println("test case passed");
					} 
					else {

						System.out.println("test case failed");

						do {
					
					result = result + "\n"
							+ resultSet.getNString("cust_ref_num")
							+"  "
							+ resultSet.getNString("taskid")
							+"   "
							+ resultSet.getNString("trn_status_code") 
							+"   "
							+ resultSet.getNString("queueid");
				    custrefnum=resultSet.getNString("cust_ref_num");
				   // System.out.println(custrefnum);
					taskid=resultSet.getNString("taskid");
					//System.out.println(taskid);
					queueid=resultSet.getNString("queueid");
					//System.out.println(queueid);
					txnstatus=resultSet.getNString("trn_status_code");
					//System.out.println(txnstatus);
					String data=custrefnum+"  "+taskid+"  "+queueid+"  "+txnstatus;
					System.out.println("Value from RHS"+data);
					h.put(custrefnum,data);
					

				} while (resultSet.next());

				}*/
					}

					
			}
				catch (Exception ex) 
					{

					ex.printStackTrace();
					if (resultSet1 != null)
						resultSet1.close();

					if (pstmt != null)
						pstmt.close();

					if (conn != null)
						conn.close();

				}

					finally {
						
						if (resultSet1 != null)
							resultSet1.close();

						if (pstmt != null)
							pstmt.close();

						if (conn != null)
							conn.close();
					}

			
			return h;

			}
					
			public static HashMap datacompareBVM( String TESTCASEID, String FILENAME) throws IOException, SQLException, ClassNotFoundException
	{
	
		HashMap<String,String> h=new HashMap<String,String>();
		String custrefnum=null;
		String taskid=null;
		String queueid=null;
		String txnstatus=null;
		String testcaseid = TESTCASEID;
		String filename = FILENAME;
		String tableName = "AUT_PREBV_LHS";

		
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		String result = "";
		
		StringBuffer StrQuery = new StringBuffer();
		
			
			String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
			String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
			String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection conn=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
			
					
		
			StrQuery.append("(select tpo.cust_ref_num ,owi.taskid,owi.queueid,tpo.trn_status_code,e.exception_id,e.exception_desc,e.external_exception_id,e.external_exception_desc from txn_proc_opn tpo inner join ow_workitem_instance owi on tpo.workitemid=owi.workitemid left join gtb_excep_grid_detail e  on e.workitemid=tpo.workitemid where tpo.File_Workitem_Id in (select workitemid From File_Proc_Opn ");
			StrQuery.append("where file_name like '%" + filename + "%')  ");
			StrQuery.append("minus ");
			StrQuery.append("select CUST_REF_NUM,taskid,queueid,trn_status_code,int_ex_code,int_ex_desc,ext_ex_code,ext_ex_desc from "
					+ tableName
					+ " where testcaseid='"
					+ testcaseid+"')");
			
			
System.out.println(StrQuery);
			
			try {
			pstmt = conn.prepareStatement(StrQuery.toString());
			resultSet = pstmt.executeQuery();
		

			if (!resultSet.next()) {

			System.out.println("test case passed");
			} 
			else {

				System.out.println("test case failed");

				do {
			
			result = result + "\n"
					+ resultSet.getNString("cust_ref_num")
					+"  "
					+ resultSet.getNString("taskid")
					+"   "
					+ resultSet.getNString("trn_status_code") 
					+"   "
					+ resultSet.getNString("queueid");
		    custrefnum=resultSet.getNString("cust_ref_num");
			taskid=resultSet.getNString("taskid");
			queueid=resultSet.getNString("queueid");
			txnstatus=resultSet.getNString("trn_status_code");
			String intcode=resultSet.getNString("exception_id");
			String intexdesc=resultSet.getNString("exception_desc");
			String excode=resultSet.getNString("external_exception_id");
			String exdesc=resultSet.getNString("external_exception_desc");
			String data=custrefnum+"  "+taskid+"  "+queueid+"  "+txnstatus+"  "+intcode+"  "+intexdesc+"  "+excode+"  "+exdesc;
			
			h.put(custrefnum,data);
			

		} while (resultSet.next());

		}

		
	}
		catch (Exception ex) 
			{

			ex.printStackTrace();
			if (resultSet != null)
				resultSet.close();

			if (pstmt != null)
				pstmt.close();

			if (conn != null)
				conn.close();

		}

			finally {
				
				if (resultSet != null)
					resultSet.close();

				if (pstmt != null)
					pstmt.close();

				if (conn != null)
					conn.close();
			}

	
	return h;

	}
			
			public static HashMap<String, String> Getlhstable(String Testcaseid,String Type) throws Exception{
				FileUtilities FileUtilities=new FileUtilities();
				HashMap<String,String> Result1=new HashMap<String,String>();   
				Keywordoperations _Storevalue=new Keywordoperations();
				String tableName=null;
			
				String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
				String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
				String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
				String testcaseid= FileUtilities.GetFilevalue("VAR_TCNO");
				HashMap<String,String> h=new HashMap<String,String>();
				String custrefnum=null;
				String taskid=null;
				String queueid=null;
				String txnstatus=null;
				//Connection connection=null;
				if ("PRE".equalsIgnoreCase(Type)) {
					tableName = "AUT_PRECOMP_LHS";
				} else if ("POST".equalsIgnoreCase(Type)) {
					tableName = "AUT_POSTCOMP_LHS";
				}
				Statement stmt=null;
				System.out.println("testcaseid:"+testcaseid);
				try{
					String query = "select * from "+tableName+" where testcaseid ='"+testcaseid+"'";
					
					System.out.println(query);
					Class.forName("oracle.jdbc.driver.OracleDriver");
					Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
					stmt = connection.createStatement();
					
					stmt.executeUpdate(query);
					ResultSet _ResultSet=connection.createStatement().executeQuery(query);
				
			
				  while (_ResultSet.next()) {

					  custrefnum=_ResultSet.getNString("cust_ref_num");
						taskid=_ResultSet.getNString("taskid");
						queueid=_ResultSet.getNString("queueid");
						txnstatus=_ResultSet.getNString("trn_status_code");
						String data=custrefnum+"  "+taskid+"  "+queueid+"  "+txnstatus;
					//	System.out.println("Value from LHS"+data);
						h.put(custrefnum,data);
						Result1.put(custrefnum,data);
				
					}
				  
				

				}catch (Exception e) {
					e.printStackTrace();
				}
				
				return Result1;
			}
			public static void main(String[] args) throws Exception {
				FileUtilities fileutil=new FileUtilities(); 
				String TESTCASEID=fileutil.GetFilevalue("VAR_TCNO");
				String FILENAME=fileutil.GetFilevalue("VAR_FILENAME");
				System.out.println(TESTCASEID+"  "+FILENAME);
				//System.out.println(datacompare("PRE",TESTCASEID,FILENAME));
				HashMap<String,String> hrhs=new HashMap<String,String>();
				//hrhs=datacompare("POST",TESTCASEID,FILENAME);
				hrhs=datacompareBVM(TESTCASEID,FILENAME);
				HashMap<String,String> hlhs=new HashMap<String,String>();
				hlhs=GetlhstableBVM(TESTCASEID);	
				//hlhs=Getlhstable(TESTCASEID,"POST");
				for(Map.Entry m2:hrhs.entrySet()){  
					for(Map.Entry m1:hlhs.entrySet()){  
				      if(m2.getKey().equals(m1.getKey()))
				      {	  
				      System.out.println("Values from rhs"+" "+m2.getValue()+"\n"+"Values from lhs"+" "+m1.getValue());
				      }  
					}
			}
	}
			private static HashMap<String, String> GetlhstableBVM(String tESTCASEID) throws IOException {
				FileUtilities FileUtilities=new FileUtilities();
				HashMap<String,String> Result1=new HashMap<String,String>();   
				Keywordoperations _Storevalue=new Keywordoperations();
				String tableName=null;
			
				String PSH_DBURL=PropertyUtils.readProperty("PSH_DBURL");
				String PSH_DBUserName=PropertyUtils.readProperty("PSH_DBUserName");
				String PSH_DBUserPass=PropertyUtils.readProperty("PSH_DBUserPass");
				String testcaseid= FileUtilities.GetFilevalue("VAR_TCNO");
				HashMap<String,String> h=new HashMap<String,String>();
				String custrefnum=null;
				String taskid=null;
				String queueid=null;
				String txnstatus=null;
				String intexcode=null;
				String intexdesc=null;
				String Extexcode=null;
				String Extexdesc=null;
				//Connection connection=null;
		
					tableName = "AUT_PREBV_LHS";

				Statement stmt=null;
				System.out.println("testcaseid:"+testcaseid);
				try{
					String query = "select * from "+tableName+" where testcaseid ='"+testcaseid+"'";
					
					System.out.println(query);
					Class.forName("oracle.jdbc.driver.OracleDriver");
					Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@"+PSH_DBURL,PSH_DBUserName,PSH_DBUserPass);
					stmt = connection.createStatement();
					
					stmt.executeUpdate(query);
					ResultSet _ResultSet=connection.createStatement().executeQuery(query);
				
			
				  while (_ResultSet.next()) {

					  custrefnum=_ResultSet.getNString("cust_ref_num");
						taskid=_ResultSet.getNString("taskid");
						queueid=_ResultSet.getNString("queueid");
						txnstatus=_ResultSet.getNString("trn_status_code");
				         intexcode=_ResultSet.getNString("int_ex_code");
				         intexdesc=_ResultSet.getNString("int_ex_desc");
					     Extexcode=_ResultSet.getNString("ext_ex_code");
				         Extexdesc=_ResultSet.getNString("int_ex_desc");
						String data=custrefnum+"  "+taskid+"  "+queueid+"  "+txnstatus+"  "+intexcode+"  "+intexdesc+"  "+Extexcode+"  "+ Extexdesc;
						h.put(custrefnum,data);
						Result1.put(custrefnum,data);
				
					}
				  
				

				}catch (Exception e) {
					e.printStackTrace();
				}
				
				return Result1;
			
			}
			public static String PrePostcompUsingCustRefNum(String TYPE) throws Exception {
				FileUtilities fileutil=new FileUtilities(); 
				String TESTCASEID=fileutil.GetFilevalue("VAR_TCNO");
				String FILENAME=fileutil.GetFilevalue("VAR_FILENAME");
				System.out.println(TESTCASEID+FILENAME);
		        String mismatch="MATCH";
				HashMap<String,String> hrhs=new HashMap<String,String>();
				//hrhs=datacompare(TYPE,TESTCASEID,FILENAME);
				hrhs=datacompareusingCustRefnum(TYPE,TESTCASEID,FILENAME);
				HashMap<String,String> hlhs=new HashMap<String,String>();
				hlhs=Getlhstable(TESTCASEID,TYPE);	
				if(!hrhs.isEmpty())
				{	
				for(Map.Entry m2:hrhs.entrySet()){  
					for(Map.Entry m1:hlhs.entrySet()){  
				      if(m2.getKey().equals(m1.getKey()))
				      {	  
				     
				      mismatch=mismatch+"\n"+"Values from rhs"+" "+m2.getValue()+"\n"+"Values from lhs"+" "+m1.getValue();
				      }  
					}
			}
			}
				System.out.println(mismatch);
				return mismatch;
	}
			public static String PrePostcompUsingFullQuery(String TYPE) throws Exception {
				FileUtilities fileutil=new FileUtilities(); 
				String TESTCASEID=fileutil.GetFilevalue("VAR_TCNO");
				String FILENAME=fileutil.GetFilevalue("VAR_FILENAME");
				System.out.println(TESTCASEID+FILENAME);
		        String mismatch="MATCH";
				HashMap<String,String> hrhs=new HashMap<String,String>();
				//hrhs=datacompare(TYPE,TESTCASEID,FILENAME);
				hrhs=datacompareUsingFullQuery(TYPE,TESTCASEID,FILENAME);
				HashMap<String,String> hlhs=new HashMap<String,String>();
				hlhs=Getlhstable(TESTCASEID,TYPE);	
				if(!hrhs.isEmpty())
				{	
				for(Map.Entry m2:hrhs.entrySet()){  
					for(Map.Entry m1:hlhs.entrySet()){  
				      if(m2.getKey().equals(m1.getKey()))
				      {	  
				     
				      mismatch=mismatch+"\n"+"Values from rhs"+" "+m2.getValue()+"\n"+"Values from lhs"+" "+m1.getValue();
				      }  
					}
			}
			}
				System.out.println(mismatch);
				return mismatch;
	}
			public static String performPrePostcomp(String TYPE) throws Exception {
				FileUtilities fileutil=new FileUtilities(); 
				String TESTCASEID=fileutil.GetFilevalue("VAR_TCNO");
				String FILENAME=fileutil.GetFilevalue("VAR_FILENAME");
				String FILEstatus=fileutil.GetFilevalue("VAR_FILE_STATUS");
				if(!FILEstatus.contains(",") && FILEstatus.contains("FLREJE") && TYPE.equals("PRE"))
				{
					return "FILE REJECTED";
				}
				System.out.println(TESTCASEID+FILENAME);
		        String mismatch="";
				HashMap<String,String> hrhs=new HashMap<String,String>();
				hrhs=datacompare(TYPE,TESTCASEID,FILENAME);
				//hrhs=datacompareFullQuery(TYPE,TESTCASEID,FILENAME);
				HashMap<String,String> hlhs=new HashMap<String,String>();
				hlhs=Getlhstable(TESTCASEID,TYPE);	
				if(!hrhs.isEmpty())
				{	
				for(Map.Entry m2:hrhs.entrySet()){  
					for(Map.Entry m1:hlhs.entrySet()){  
				      if(m2.getKey().equals(m1.getKey()))
				      {	  
				     
				      mismatch=mismatch+"\n"+"Values from rhs"+" "+m2.getValue()+"\n"+"Values from lhs"+" "+m1.getValue()+"\n";
				      }  
					}
			}
			}
				if(mismatch.length()>5)
				{System.out.println(mismatch);
				return mismatch;
				}
				else
				{
					return "MATCH";
				}
	}
			
			public static String performPrePostcompBVM(String TYPE) throws Exception {
				FileUtilities fileutil=new FileUtilities(); 
				String TESTCASEID=fileutil.GetFilevalue("VAR_TCNO");
				String FILENAME=fileutil.GetFilevalue("VAR_FILENAME");
			//	System.out.println(TESTCASEID+FILENAME);
		        String mismatch="MATCH";
				HashMap<String,String> hrhs=new HashMap<String,String>();
				hrhs=datacompareBVM(TESTCASEID,FILENAME);
				HashMap<String,String> hlhs=new HashMap<String,String>();
				hlhs=GetlhstableBVM(TESTCASEID);	
				if(!hrhs.isEmpty())
				{	
				for(Map.Entry m2:hrhs.entrySet()){  
					for(Map.Entry m1:hlhs.entrySet()){  
				      if(m2.getKey().equals(m1.getKey()))
				      {	  
				     
				      mismatch=mismatch+"\n"+"Actual value rhs"+" "+m2.getValue()+"\n"+"Expected value lhs"+" "+m1.getValue();
				      }  
					}
			}
			}
				System.out.println(mismatch);
				return mismatch;
	}
	}



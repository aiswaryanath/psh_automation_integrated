package ReportGeneration;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Program {
	  public String baseUrl = "http://demo.guru99.com/test/newtours/";
	
	    public WebDriver driver ; 

	@Test
	public void verifySeleniumBlog() {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\Driver\\chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		//options.setProxy(null);
		driver = new ChromeDriver();
		
		driver.get(baseUrl);
	      String expectedTitle = "Welcome: Mercury Tours";
	  	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	      String actualTitle = driver.getTitle();
	      Assert.assertEquals(actualTitle, expectedTitle);
	      driver.close();

	}
}